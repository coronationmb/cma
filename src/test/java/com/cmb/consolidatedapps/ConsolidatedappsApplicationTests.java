package com.cmb.consolidatedapps;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.cmb.consolidatedapps.controller.LoginControllers;
import com.cmb.consolidatedapps.interfaces.CustomerRepository;
import com.cmb.consolidatedapps.interfaces.CustomerTypeRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerType;
import com.cmb.consolidatedapps.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
// @SpringBootTest
@WebMvcTest(LoginControllers.class)
public class ConsolidatedappsApplicationTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private CustomerRepository customerRepository;

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private StatusRepository statusRepository;

	@MockBean
	private ProcessInterfaces processInterface;

	@MockBean
	private JavaMailSender javaMailSender;

	@MockBean
	private CustomerTypeRepository customerTypeRepository;

	@MockBean
	private UserRoleRepository userRoleRepo;
	
	@Test
	public void test(){}

	// @Test
	public void saveCustomerDetail() throws Exception {

		Customer customer = new Customer();
		customer.setBvn("22123456789");
		customer.setCreatedBy("Waliu");
		customer.setCreatedDate(new Date());
		CustomerType customerType = customerTypeRepository.getOne(Long.valueOf("1"));
		System.out.println(customerType);
		customer.setCustomerTypeId(Long.valueOf("1"));
		customer.setEmail("wfaleye@coronationmb.com");
		customer.setName("Ade");
		customer.setPhone("08053953344");

		String body = (new ObjectMapper()).valueToTree(customer).toString();

		mvc.perform(post("/saveCustomerDetail").content(body).contentType("application/json; charset=utf-8"))
				.andExpect(status().isOk());
		// .andExpect(jsonPath("$", hasSize(1)))
		// .andExpect(jsonPath("$[0].name", is(alex.getName())));
	}

	//@Test
	public void saveUserDetail() throws Exception {

		User user = new User();
		user.setCustomerId(Long.valueOf("1"));

		user.setCreatedBy("Waliu");
		user.setCreatedDate(new Date());
		CustomerType customerType = customerTypeRepository.getOne(Long.valueOf("1"));
		System.out.println(customerType);
		user.setCustomerName("Walex");
		;
		user.setEmail("wfaleye@coronationmb.com");
		user.setPhone("08053953344");

		String body = (new ObjectMapper()).valueToTree(user).toString();

		mvc.perform(post("/saveUserDetail").content(body).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
		// .andExpect(jsonPath("$", hasSize(1)))
		// .andExpect(jsonPath("$[0].name", is(alex.getName())));
	}
	
	//@Test
	public void getCustomer() throws Exception {

		User user = new User();
		user.setCustomerId(Long.valueOf("1"));

		user.setCreatedBy("Waliu");
		user.setCreatedDate(new Date());
		CustomerType customerType = customerTypeRepository.getOne(Long.valueOf("1"));
		System.out.println(customerType);
		user.setCustomerName("Walex");
		;
		user.setEmail("wfaleye@coronationmb.com");
		user.setPhone("08053953344");
		Pageable pageable = Pageable.unpaged();
		String body = (new ObjectMapper()).valueToTree(pageable).toString();

		mvc.perform(get("/getCustomer").content(body).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
		// .andExpect(jsonPath("$", hasSize(1)))
		// .andExpect(jsonPath("$[0].name", is(alex.getName())));
	}
	
	//@Test
	public void login() throws Exception {

		User user = new User();
		user.setCustomerId(Long.valueOf("1"));
		user.setUserId("wfaleye@coronationmb.com");
		user.setPassword("");

		user.setCreatedBy("Waliu");
		user.setCreatedDate(new Date());
		CustomerType customerType = customerTypeRepository.getOne(Long.valueOf("1"));
		System.out.println(customerType);
		user.setCustomerName("Walex");
		
		user.setEmail("wfaleye@coronationmb.com");
		user.setPhone("08053953344");

		String body = (new ObjectMapper()).valueToTree(user).toString();

		mvc.perform(post("/login").content(body).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
		// .andExpect(jsonPath("$", hasSize(1)))
		// .andExpect(jsonPath("$[0].name", is(alex.getName())));
	}


}
