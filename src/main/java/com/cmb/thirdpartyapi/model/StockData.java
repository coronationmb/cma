/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class StockData {

	private String stockCode;
	private String name;
	private String totalUnits;
	private String avgUnitPrice;
	private String cost;
	private String fees;
	private String mktValue;
	private String mktQuote;
	private String gain;
	private String gainPercent;
	/**
	 * @return the stockCode
	 */
	public String getStockCode() {
		return stockCode;
	}
	/**
	 * @param stockCode the stockCode to set
	 */
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the totalUnits
	 */
	public String getTotalUnits() {
		return totalUnits;
	}
	/**
	 * @param totalUnits the totalUnits to set
	 */
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}
	/**
	 * @return the avgUnitPrice
	 */
	public String getAvgUnitPrice() {
		return avgUnitPrice;
	}
	/**
	 * @param avgUnitPrice the avgUnitPrice to set
	 */
	public void setAvgUnitPrice(String avgUnitPrice) {
		this.avgUnitPrice = avgUnitPrice;
	}
	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}
	/**
	 * @return the fees
	 */
	public String getFees() {
		return fees;
	}
	/**
	 * @param fees the fees to set
	 */
	public void setFees(String fees) {
		this.fees = fees;
	}
	/**
	 * @return the mktValue
	 */
	public String getMktValue() {
		return mktValue;
	}
	/**
	 * @param mktValue the mktValue to set
	 */
	public void setMktValue(String mktValue) {
		this.mktValue = mktValue;
	}
	/**
	 * @return the mktQuote
	 */
	public String getMktQuote() {
		return mktQuote;
	}
	/**
	 * @param mktQuote the mktQuote to set
	 */
	public void setMktQuote(String mktQuote) {
		this.mktQuote = mktQuote;
	}
	/**
	 * @return the gain
	 */
	public String getGain() {
		return gain;
	}
	/**
	 * @param gain the gain to set
	 */
	public void setGain(String gain) {
		this.gain = gain;
	}
	/**
	 * @return the gainPercent
	 */
	public String getGainPercent() {
		return gainPercent;
	}
	/**
	 * @param gainPercent the gainPercent to set
	 */
	public void setGainPercent(String gainPercent) {
		this.gainPercent = gainPercent;
	}
}
