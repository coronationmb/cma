/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMFundPriceHistoryRows {

	String ValuationDate;
	String BidPrice;
	String OfferPrice;
	/**
	 * @return the valuationDate
	 */
	public String getValuationDate() {
		return ValuationDate;
	}
	/**
	 * @param valuationDate the valuationDate to set
	 */
	public void setValuationDate(String valuationDate) {
		ValuationDate = valuationDate;
	}
	/**
	 * @return the bidPrice
	 */
	public String getBidPrice() {
		return BidPrice;
	}
	/**
	 * @param bidPrice the bidPrice to set
	 */
	public void setBidPrice(String bidPrice) {
		BidPrice = bidPrice;
	}
	/**
	 * @return the offerPrice
	 */
	public String getOfferPrice() {
		return OfferPrice;
	}
	/**
	 * @param offerPrice the offerPrice to set
	 */
	public void setOfferPrice(String offerPrice) {
		OfferPrice = offerPrice;
	}
}
