/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class MFRows {

	String fundCode;
	String fundName;
	String startDate;
	String endDate;
	String mktValue;
	String lastValuationDate;
	String fundType;
	String unitPrice;
	String bidPrice;
	String offerPrice;
	String mgmtFee;
	String description;
	String debit;
	String credit;
	String percentChange;
	String minInvest;
	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}
	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}
	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the mktValue
	 */
	public String getMktValue() {
		return mktValue;
	}
	/**
	 * @param mktValue the mktValue to set
	 */
	public void setMktValue(String mktValue) {
		this.mktValue = mktValue;
	}
	/**
	 * @return the lastValuationDate
	 */
	public String getLastValuationDate() {
		return lastValuationDate;
	}
	/**
	 * @param lastValuationDate the lastValuationDate to set
	 */
	public void setLastValuationDate(String lastValuationDate) {
		this.lastValuationDate = lastValuationDate;
	}
	/**
	 * @return the fundType
	 */
	public String getFundType() {
		return fundType;
	}
	/**
	 * @param fundType the fundType to set
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	/**
	 * @return the unitPrice
	 */
	public String getUnitPrice() {
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the bidPrice
	 */
	public String getBidPrice() {
		return bidPrice;
	}
	/**
	 * @param bidPrice the bidPrice to set
	 */
	public void setBidPrice(String bidPrice) {
		this.bidPrice = bidPrice;
	}
	/**
	 * @return the offerPrice
	 */
	public String getOfferPrice() {
		return offerPrice;
	}
	/**
	 * @param offerPrice the offerPrice to set
	 */
	public void setOfferPrice(String offerPrice) {
		this.offerPrice = offerPrice;
	}
	/**
	 * @return the mgmtFee
	 */
	public String getMgmtFee() {
		return mgmtFee;
	}
	/**
	 * @param mgmtFee the mgmtFee to set
	 */
	public void setMgmtFee(String mgmtFee) {
		this.mgmtFee = mgmtFee;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the debit
	 */
	public String getDebit() {
		return debit;
	}
	/**
	 * @param debit the debit to set
	 */
	public void setDebit(String debit) {
		this.debit = debit;
	}
	/**
	 * @return the credit
	 */
	public String getCredit() {
		return credit;
	}
	/**
	 * @param credit the credit to set
	 */
	public void setCredit(String credit) {
		this.credit = credit;
	}
	/**
	 * @return the percentChange
	 */
	public String getPercentChange() {
		return percentChange;
	}
	/**
	 * @param percentChange the percentChange to set
	 */
	public void setPercentChange(String percentChange) {
		this.percentChange = percentChange;
	}
	/**
	 * @return the minInvest
	 */
	public String getMinInvest() {
		return minInvest;
	}
	/**
	 * @param minInvest the minInvest to set
	 */
	public void setMinInvest(String minInvest) {
		this.minInvest = minInvest;
	}

}
