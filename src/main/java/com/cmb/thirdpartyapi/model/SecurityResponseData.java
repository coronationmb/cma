/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class SecurityResponseData {
	
	private DataTable dataTable;
	
	private Long statusId;
	
	private String statusMessage;
	
	private String outValue;


	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the dataTable
	 */
	public DataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the dataTable to set
	 */
	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @return the outValue
	 */
	public String getOutValue() {
		return outValue;
	}

	/**
	 * @param outValue the outValue to set
	 */
	public void setOutValue(String outValue) {
		this.outValue = outValue;
	}



}
