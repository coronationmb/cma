/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMFundTransactions {

	String ValueDate;
	String FundName;
	String Subscription;
	String Price;
	String MktValue;
	/**
	 * @return the valueDate
	 */
	public String getValueDate() {
		return ValueDate;
	}
	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(String valueDate) {
		ValueDate = valueDate;
	}
	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return FundName;
	}
	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		FundName = fundName;
	}
	/**
	 * @return the subscription
	 */
	public String getSubscription() {
		return Subscription;
	}
	/**
	 * @param subscription the subscription to set
	 */
	public void setSubscription(String subscription) {
		Subscription = subscription;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return Price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		Price = price;
	}
	/**
	 * @return the mktValue
	 */
	public String getMktValue() {
		return MktValue;
	}
	/**
	 * @param mktValue the mktValue to set
	 */
	public void setMktValue(String mktValue) {
		MktValue = mktValue;
	}

}
