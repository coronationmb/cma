/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class SubFundsRows {
	String fundName;
	String fundCode;
	String fundType;
	String custAID;
	String totalAssetValue;
	String valuationDate;
	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}
	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}
	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	/**
	 * @return the fundType
	 */
	public String getFundType() {
		return fundType;
	}
	/**
	 * @param fundType the fundType to set
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	/**
	 * @return the custAID
	 */
	public String getCustAID() {
		return custAID;
	}
	/**
	 * @param custAID the custAID to set
	 */
	public void setCustAID(String custAID) {
		this.custAID = custAID;
	}
	/**
	 * @return the totalAssetValue
	 */
	public String getTotalAssetValue() {
		return totalAssetValue;
	}
	/**
	 * @param totalAssetValue the totalAssetValue to set
	 */
	public void setTotalAssetValue(String totalAssetValue) {
		this.totalAssetValue = totalAssetValue;
	}
	/**
	 * @return the valuationDate
	 */
	public String getValuationDate() {
		return valuationDate;
	}
	/**
	 * @param valuationDate the valuationDate to set
	 */
	public void setValuationDate(String valuationDate) {
		this.valuationDate = valuationDate;
	}
}
