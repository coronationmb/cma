/**
 * 
 */
package com.cmb.thirdpartyapi.model;

import java.util.List;

/**
 * @author waliu.faleye
 *
 */
public class DataTable {

	private ColumnDef columnDef;

	private List<Rows> rows;

	private List<MFRows> mfrows;

	private List<AMCustBalRows> aMCustBalRows;

	private List<SubFundsRows> subFundsRows;

	private List<AMCurrentPortfolioValueRows> amcurrentPortfolioValueRows;

	private List<AMFundPriceHistoryRows> aMFundPriceHistoryRows;

	private List<AMInvestmentSummaryRows> aminvestmentSummaryRows;

	private List<AMFundAccountStatementRows> amfundAccountStatementRows;

	private List<AMFundTransactions> aMFundTransactions;

	private List<AMMFCumulativeReturn> aMMFCumulativeReturn;

	private List<AMYearEndToDateRows> aMYearEndToDateRows;
	
	private List<LosersGainersRows> losersGainersRows;
	
	private List<StockData> stockDataRows;

	private List<SecurityTransaction> securityTransactions;

	/**
	 * @return the rows
	 */
	public List<Rows> getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(List<Rows> rows) {
		this.rows = rows;
	}

	/**
	 * @return the columnDef
	 */
	public ColumnDef getColumnDef() {
		return columnDef;
	}

	/**
	 * @param columnDef the columnDef to set
	 */
	public void setColumnDef(ColumnDef columnDef) {
		this.columnDef = columnDef;
	}

	/**
	 * @return the aMCustBalRows
	 */
	public List<AMCustBalRows> getaMCustBalRows() {
		return aMCustBalRows;
	}

	/**
	 * @param aMCustBalRows the aMCustBalRows to set
	 */
	public void setaMCustBalRows(List<AMCustBalRows> aMCustBalRows) {
		this.aMCustBalRows = aMCustBalRows;
	}

	/**
	 * @return the subFundsRows
	 */
	public List<SubFundsRows> getSubFundsRows() {
		return subFundsRows;
	}

	/**
	 * @param subFundsRows the subFundsRows to set
	 */
	public void setSubFundsRows(List<SubFundsRows> subFundsRows) {
		this.subFundsRows = subFundsRows;
	}

	/**
	 * @return the aMFundPriceHistoryRows
	 */
	public List<AMFundPriceHistoryRows> getaMFundPriceHistoryRows() {
		return aMFundPriceHistoryRows;
	}

	/**
	 * @param aMFundPriceHistoryRows the aMFundPriceHistoryRows to set
	 */
	public void setaMFundPriceHistoryRows(List<AMFundPriceHistoryRows> aMFundPriceHistoryRows) {
		this.aMFundPriceHistoryRows = aMFundPriceHistoryRows;
	}

	/**
	 * @return the aMFundTransactions
	 */
	public List<AMFundTransactions> getaMFundTransactions() {
		return aMFundTransactions;
	}

	/**
	 * @param aMFundTransactions the aMFundTransactions to set
	 */
	public void setaMFundTransactions(List<AMFundTransactions> aMFundTransactions) {
		this.aMFundTransactions = aMFundTransactions;
	}

	/**
	 * @return the aMMFCumulativeReturn
	 */
	public List<AMMFCumulativeReturn> getaMMFCumulativeReturn() {
		return aMMFCumulativeReturn;
	}

	/**
	 * @param aMMFCumulativeReturn the aMMFCumulativeReturn to set
	 */
	public void setaMMFCumulativeReturn(List<AMMFCumulativeReturn> aMMFCumulativeReturn) {
		this.aMMFCumulativeReturn = aMMFCumulativeReturn;
	}

	/**
	 * @return the aMYearEndToDateRows
	 */
	public List<AMYearEndToDateRows> getaMYearEndToDateRows() {
		return aMYearEndToDateRows;
	}

	/**
	 * @param aMYearEndToDateRows the aMYearEndToDateRows to set
	 */
	public void setaMYearEndToDateRows(List<AMYearEndToDateRows> aMYearEndToDateRows) {
		this.aMYearEndToDateRows = aMYearEndToDateRows;
	}

	/**
	 * @return the losersGainersRows
	 */
	public List<LosersGainersRows> getLosersGainersRows() {
		return losersGainersRows;
	}

	/**
	 * @param losersGainersRows the losersGainersRows to set
	 */
	public void setLosersGainersRows(List<LosersGainersRows> losersGainersRows) {
		this.losersGainersRows = losersGainersRows;
	}

	/**
	 * @return the mfrows
	 */
	public List<MFRows> getMfrows() {
		return mfrows;
	}

	/**
	 * @param mfrows the mfrows to set
	 */
	public void setMfrows(List<MFRows> mfrows) {
		this.mfrows = mfrows;
	}

	/**
	 * @return the amcurrentPortfolioValueRows
	 */
	public List<AMCurrentPortfolioValueRows> getAmcurrentPortfolioValueRows() {
		return amcurrentPortfolioValueRows;
	}

	/**
	 * @param amcurrentPortfolioValueRows the amcurrentPortfolioValueRows to set
	 */
	public void setAmcurrentPortfolioValueRows(List<AMCurrentPortfolioValueRows> amcurrentPortfolioValueRows) {
		this.amcurrentPortfolioValueRows = amcurrentPortfolioValueRows;
	}

	/**
	 * @return the aminvestmentSummaryRows
	 */
	public List<AMInvestmentSummaryRows> getAminvestmentSummaryRows() {
		return aminvestmentSummaryRows;
	}

	/**
	 * @param aminvestmentSummaryRows the aminvestmentSummaryRows to set
	 */
	public void setAminvestmentSummaryRows(List<AMInvestmentSummaryRows> aminvestmentSummaryRows) {
		this.aminvestmentSummaryRows = aminvestmentSummaryRows;
	}

	/**
	 * @return the amfundAccountStatementRows
	 */
	public List<AMFundAccountStatementRows> getAmfundAccountStatementRows() {
		return amfundAccountStatementRows;
	}

	/**
	 * @param amfundAccountStatementRows the amfundAccountStatementRows to set
	 */
	public void setAmfundAccountStatementRows(List<AMFundAccountStatementRows> amfundAccountStatementRows) {
		this.amfundAccountStatementRows = amfundAccountStatementRows;
	}

	/**
	 * @return the stockDataRows
	 */
	public List<StockData> getStockDataRows() {
		return stockDataRows;
	}

	/**
	 * @param stockDataRows the stockDataRows to set
	 */
	public void setStockDataRows(List<StockData> stockDataRows) {
		this.stockDataRows = stockDataRows;
	}

	/**
	 * @return the securityTransactions
	 */
	public List<SecurityTransaction> getSecurityTransactions() {
		return securityTransactions;
	}

	/**
	 * @param securityTransactions the securityTransactions to set
	 */
	public void setSecurityTransactions(List<SecurityTransaction> securityTransactions) {
		this.securityTransactions = securityTransactions;
	}

}
