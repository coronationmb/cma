/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMCustBalRows {
	String CustAID;
	String Email1;
	String Phone1;
	String AccountOfficer;
	String Balance;
	String ValuationDate;

	/**
	 * @return the custAID
	 */
	public String getCustAID() {
		return CustAID;
	}

	/**
	 * @param custAID
	 *            the custAID to set
	 */
	public void setCustAID(String custAID) {
		CustAID = custAID;
	}

	/**
	 * @return the email1
	 */
	public String getEmail1() {
		return Email1;
	}

	/**
	 * @param email1
	 *            the email1 to set
	 */
	public void setEmail1(String email1) {
		Email1 = email1;
	}

	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return Phone1;
	}

	/**
	 * @param phone1
	 *            the phone1 to set
	 */
	public void setPhone1(String phone1) {
		Phone1 = phone1;
	}

	/**
	 * @return the accountOfficer
	 */
	public String getAccountOfficer() {
		return AccountOfficer;
	}

	/**
	 * @param accountOfficer
	 *            the accountOfficer to set
	 */
	public void setAccountOfficer(String accountOfficer) {
		AccountOfficer = accountOfficer;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return Balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(String balance) {
		Balance = balance;
	}

	/**
	 * @return the valuationDate
	 */
	public String getValuationDate() {
		return ValuationDate;
	}

	/**
	 * @param valuationDate
	 *            the valuationDate to set
	 */
	public void setValuationDate(String valuationDate) {
		ValuationDate = valuationDate;
	}
}
