/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMMFCumulativeReturn {
	
	String OneMth;
	String ThreeMth;
	String SixMth;
	/**
	 * @return the oneMth
	 */
	public String getOneMth() {
		return OneMth;
	}
	/**
	 * @param oneMth the oneMth to set
	 */
	public void setOneMth(String oneMth) {
		OneMth = oneMth;
	}
	/**
	 * @return the threeMth
	 */
	public String getThreeMth() {
		return ThreeMth;
	}
	/**
	 * @param threeMth the threeMth to set
	 */
	public void setThreeMth(String threeMth) {
		ThreeMth = threeMth;
	}
	/**
	 * @return the sixMth
	 */
	public String getSixMth() {
		return SixMth;
	}
	/**
	 * @param sixMth the sixMth to set
	 */
	public void setSixMth(String sixMth) {
		SixMth = sixMth;
	}

}
