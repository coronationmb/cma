/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMYearEndToDateRows {
	
	String StartPrice;
	String EndPrice;
	String Diff;
	String Percentage;
	/**
	 * @return the startPrice
	 */
	public String getStartPrice() {
		return StartPrice;
	}
	/**
	 * @param startPrice the startPrice to set
	 */
	public void setStartPrice(String startPrice) {
		StartPrice = startPrice;
	}
	/**
	 * @return the endPrice
	 */
	public String getEndPrice() {
		return EndPrice;
	}
	/**
	 * @param endPrice the endPrice to set
	 */
	public void setEndPrice(String endPrice) {
		EndPrice = endPrice;
	}
	/**
	 * @return the diff
	 */
	public String getDiff() {
		return Diff;
	}
	/**
	 * @param diff the diff to set
	 */
	public void setDiff(String diff) {
		Diff = diff;
	}
	/**
	 * @return the percentage
	 */
	public String getPercentage() {
		return Percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(String percentage) {
		Percentage = percentage;
	}

}
