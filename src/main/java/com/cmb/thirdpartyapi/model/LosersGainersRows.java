/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class LosersGainersRows {

	private String stockCode;

	private String name;

	private String sector;

	private String lClose;

	private String price;

	private String date;

	private String change;

	private String percentChange;

	/**
	 * @return the stockCode
	 */
	public String getStockCode() {
		return stockCode;
	}

	/**
	 * @param stockCode the stockCode to set
	 */
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the sector
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(String sector) {
		this.sector = sector;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the change
	 */
	public String getChange() {
		return change;
	}

	/**
	 * @param change the change to set
	 */
	public void setChange(String change) {
		this.change = change;
	}

	/**
	 * @return the percentChange
	 */
	public String getPercentChange() {
		return percentChange;
	}

	/**
	 * @param percentChange the percentChange to set
	 */
	public void setPercentChange(String percentChange) {
		this.percentChange = percentChange;
	}

	/**
	 * @return the lClose
	 */
	public String getlClose() {
		return lClose;
	}

	/**
	 * @param lClose the lClose to set
	 */
	public void setlClose(String lClose) {
		this.lClose = lClose;
	}

}
