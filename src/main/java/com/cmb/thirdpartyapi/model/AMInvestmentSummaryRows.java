/**
 * 
 */
package com.cmb.thirdpartyapi.model;

/**
 * @author waliu.faleye
 *
 */
public class AMInvestmentSummaryRows {
	String valueDate;
	String fundName;
	String subscription;
	String mktValue;
	String fundInception;
	String accruedInt;
	String cashBalance;
	/**
	 * @return the valueDate
	 */
	public String getValueDate() {
		return valueDate;
	}
	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}
	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	/**
	 * @return the subscription
	 */
	public String getSubscription() {
		return subscription;
	}
	/**
	 * @param subscription the subscription to set
	 */
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	/**
	 * @return the mktValue
	 */
	public String getMktValue() {
		return mktValue;
	}
	/**
	 * @param mktValue the mktValue to set
	 */
	public void setMktValue(String mktValue) {
		this.mktValue = mktValue;
	}
	/**
	 * @return the fundInception
	 */
	public String getFundInception() {
		return fundInception;
	}
	/**
	 * @param fundInception the fundInception to set
	 */
	public void setFundInception(String fundInception) {
		this.fundInception = fundInception;
	}
	/**
	 * @return the accruedInt
	 */
	public String getAccruedInt() {
		return accruedInt;
	}
	/**
	 * @param accruedInt the accruedInt to set
	 */
	public void setAccruedInt(String accruedInt) {
		this.accruedInt = accruedInt;
	}
	/**
	 * @return the cashBalance
	 */
	public String getCashBalance() {
		return cashBalance;
	}
	/**
	 * @param cashBalance the cashBalance to set
	 */
	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}
}
