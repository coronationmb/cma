/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.CustomerBankAccount;

/**
 * @author waliu.faleye
 *
 */
public class CustomerBankAccountMapper implements RowMapper<CustomerBankAccount> {

	@Override
	public CustomerBankAccount mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		CustomerBankAccount cba = new CustomerBankAccount();
		cba.setAccountCurrency(rs.getString("acct_crncy_code"));
		cba.setAccountNumber(rs.getString("accountNumber"));
		cba.setAccountType(rs.getString("acct_type"));
		cba.setAcid(rs.getString("acid"));
		cba.setBalanceAmount(rs.getBigDecimal("clr_bal_amt"));
		cba.setBalanceAmountStr(formatAmount(rs.getBigDecimal("clr_bal_amt")));
		cba.setCifId(rs.getString("cif_id"));
		cba.setSchemeCode(rs.getString("schm_code"));
		cba.setSchemeType(rs.getString("schm_type"));
		cba.setNgnBalanceAmount(rs.getBigDecimal("balance"));
		cba.setNgnBalanceAmountStr(formatAmount(rs.getBigDecimal("balance")));
		cba.setAccountName(rs.getString("acct_name"));
		return cba;
	}

	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}

}
