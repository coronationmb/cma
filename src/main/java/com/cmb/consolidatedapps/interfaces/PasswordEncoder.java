/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

/**
 * @author waliu.faleye
 *
 */
public interface PasswordEncoder {

	String encode(CharSequence rawPassword);

	boolean matches(CharSequence rawPassword, String encodedPassword);

}
