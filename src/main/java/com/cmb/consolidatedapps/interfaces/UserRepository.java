/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;

/**
 * @author waliu.faleye
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByUserIdAndPassword(String userId, String password);

	public User findByUserIdIgnoreCaseAndPasswordAndDeleteFlgNot(String userId, String password,String deleteFlg);

	public User findByUserId(String userId);

	public User findByUserIdAndDeleteFlgNot(String userId,String deleteFlg);

	public List<User> findByCustomer(Customer customer);
	
	public User findByCustomerAndUsername(Customer customer, String username);
	
	public User findByCorporateUserBvn(String bvn);
	
	public List<User> findByUserRoleNot(UserRole userRole);
	
	public List<User> findByUserRole(UserRole userRole);

}
