/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.TopupTransactionDetail;

/**
 * @author waliu.faleye
 *
 */
public interface TopupTransactionDetailRepository extends JpaRepository<TopupTransactionDetail, Long> {

}
