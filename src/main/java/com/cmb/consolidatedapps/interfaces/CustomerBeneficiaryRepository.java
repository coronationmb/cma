/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerBeneficiaryRepository extends JpaRepository<CustomerBeneficiary, Long> {

	public List<CustomerBeneficiary> findByCustomerAndDeleteFlgNot(Customer customer, String deleteFlg);

	public List<CustomerBeneficiary> findByCustomerAndDeleteFlgAndBeneficiaryAccountNumber(Customer customer,
			String deleteFlg, String beneficiaryAccountNumber);

}
