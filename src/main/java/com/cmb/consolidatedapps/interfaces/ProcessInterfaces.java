/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRp;
import com.cmb.consolidatedapps.model.ChartObject;
import com.cmb.consolidatedapps.model.ContactUsInfo;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.CustomerBankAccountMonthData;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.FTSingleCreditRequest;
import com.cmb.consolidatedapps.model.FinancialInstitution;
import com.cmb.consolidatedapps.model.HtmlTemplate;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.InfowareResponseCode;
import com.cmb.consolidatedapps.model.LoginDetail;
import com.cmb.consolidatedapps.model.MailDetail;
import com.cmb.consolidatedapps.model.MutualFundProduct;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.TopUpSource;
import com.cmb.consolidatedapps.model.TopUpType;
import com.cmb.consolidatedapps.model.TopupTransactionDetail;
import com.cmb.consolidatedapps.model.TransactionDetail;
import com.cmb.consolidatedapps.model.TransactionType;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.cmb.thirdpartyapi.model.RequestData;
import com.cmb.thirdpartyapi.model.ResponseData;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;

/**
 * @author waliu.faleye
 *
 */
public interface ProcessInterfaces {

	ApplicationCustomerDetailRp getCustomerDetail(Application app, String queryParam);

	boolean sendMail(MailDetail mailDetail);

	String autoGenerateUserId(Long id);

	String autoGeneratePassword(Long id, String initPass);

	public List<CustomerBankAccountMonthData> getBankAccountMonthData(Application app,
			CustomerBankAccount customerBankAccount, JdbcTemplate jdbcTemp);

	public CustomerDashboardDetail dashboardDetails(User user);

	<T, K> List<T> getBankCustomerDetail(Application app, String queryParam, RowMapper<K> k, JdbcTemplate jdbcTemp);

	public LoginDetail save(LoginDetail loginDetail);

	public HtmlTemplate findByTemplateName(String templateName);

	// boolean sendCustomerMail(User);

	public TransactionDetail save(TransactionDetail transactionDetail);

	public TransactionType findByTransactionTypeById(Long transactionTypeId);

	public CustomerBeneficiary save(CustomerBeneficiary customerBeneficiary);

	public Customer findCustomerById(Long customerId);

	public User findUserById(Long userId);

	public FinancialInstitution findFinancialInstitutionById(String instituteCode);

	public List<FinancialInstitution> financialInstitutions();

	public List<CustomerBeneficiary> findBeneficiariesByCustomer(Customer customer);

	public CustomerBeneficiary findCustomerBeneficiaryById(Long customerBeneficiaryId);

	public String formatAmount(BigDecimal value);

	public ResponseData interbankTransfer(FTSingleCreditRequest req);

	public ResponseData localTransfer(RequestData req);

	public String getSessionId(String bankCode);

	public TransactionDetail findTransactionDetailById(Long id);

	public List<MutualFundProduct> findAllMutualFundProducts();

	public List<TopUpSource> findAllTopUpSources();

	public List<TopUpType> findAllTopUpTypes();

	public List<CustomerSubsidiaryBeneficiary> findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(
			Customer customer, Application application);

	public TopupTransactionDetail saveTopupTransactionDetail(TopupTransactionDetail topupTransactionDetail);

	public TopupTransactionDetail findTopupTransactionDetailById(Long topupTransactionDetailId);

	public CustomerSubsidiaryBeneficiary saveCustomerSubsidiaryBeneficiary(
			CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary);

	public TopUpSource findTopUpSourceById(Long topUpSourceId);

	public TopUpType findTopUpTypeById(Long topUpTypeId);

	public MutualFundProduct findMutualFundProductById(Long mutualFundProductId);

	public ResponseData reverseLocalTransfer(String uniqueIdentifier);
	
	public InfowareResponseCode findByStatusId(String statusId);
	
	public CustomerSubsidiaryBeneficiary findCustomerSubsidiaryBeneficiaryById(Long id);
	
	public List<CustomerBeneficiary> findByCustomerAndDeleteFlgAndBeneficiaryAccountNumber(Customer customer,
			String deleteFlg, String beneficiaryAccountNumber);

	public List<CustomerSubsidiaryBeneficiary> findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(Customer customer, Application application, String deleteFlg, String beneficiaryCustomerAid);
	
	public List<Application> findByNameNot(String name);
	
	public List<ChartObject> getChartObject(String accountNumber);
	
	List<CustomerBeneficiary> findCustomerBeneficiariesByCustomer(Customer customer);

	List<CustomerSubsidiaryBeneficiary> findCustomerSubBeneficiariesByCustomerAndApplication(Customer customer,
			Application application);

	void deleteCustomerBeneficiary(Long id);

	void deleteCustomerSubsidiaryBeneficiary(Long id);

	Application findApplicationById(Long id);
	
	public List<User> findByUserRoleNot(UserRole userRole);
	
	public UserRole findByRoleName(String roleName);
	
	public List<UserRole> findByRoleNameNot(String roleName);
	
	public User saveUser(User user);
	
	public UserRole findById(Long id);
	
	public Status findByStatus(String status);
	
	public List<Status> findAllStatus();
	
	public Status findStatusById(Long id);

	void deleteUser(Long id);
	
	public AdminResponseDTO createEntrustUser(User updatingUser);
	
	public ContactUsInfo saveContactUsInfo(ContactUsInfo contactUsInfo);
	
	public List<User> findUsersByRoleName(UserRole userRole);
	
	public Image saveImage(Image image);

}
