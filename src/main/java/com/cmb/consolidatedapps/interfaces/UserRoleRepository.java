/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.UserRole;

/**
 * @author waliu.faleye
 *
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

	public UserRole findByRoleName(String roleName);
	
	public List<UserRole> findByRoleNameNot(String roleName);

}
