/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.LoginDetail;
import com.cmb.consolidatedapps.model.User;

/**
 * @author waliu.faleye
 *
 */
public interface LoginDetailRepository extends JpaRepository<LoginDetail, Long> {
	
	LoginDetail findTopByUserOrderByLoginTimeDesc(User user);

}
