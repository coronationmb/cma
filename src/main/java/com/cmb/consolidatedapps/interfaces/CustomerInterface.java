/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.ContactUsInfo;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerInterface {

	List<CustomerBeneficiary> findCustomerBeneficiariesByCustomer(Customer customer);

	List<CustomerSubsidiaryBeneficiary> findCustomerSubBeneficiariesByCustomerAndApplication(Customer customer,
			Application application);

	void deleteCustomerBeneficiary(Long id);

	void deleteCustomerSubsidiaryBeneficiary(Long id);

	Application findApplicationById(Long id);
	
	public List<User> findByUserRoleNot(UserRole userRole);
	
	public UserRole findByRoleName(String roleName);
	
	public List<UserRole> findByRoleNameNot(String roleName);
	
	public User saveUser(User user);
	
	public UserRole findById(Long id);
	
	public Status findByStatus(String status);
	
	public List<Status> findAllStatus();
	
	public Status findStatusById(Long id);

	void deleteUser(Long id);
	
	public AdminResponseDTO createEntrustUser(User updatingUser);
	
	public ContactUsInfo saveContactUsInfo(ContactUsInfo contactUsInfo);
	
	public List<User> findUsersByRoleName(UserRole userRole);
	
	public Image saveImage(Image image);
}
