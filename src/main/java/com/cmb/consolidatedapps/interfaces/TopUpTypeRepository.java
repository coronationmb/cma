/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.TopUpType;

/**
 * @author waliu.faleye
 *
 */
public interface TopUpTypeRepository extends JpaRepository<TopUpType, Long> {

}
