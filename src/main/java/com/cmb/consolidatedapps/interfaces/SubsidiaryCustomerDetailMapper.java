/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;

/**
 * @author waliu.faleye
 *
 */
public class SubsidiaryCustomerDetailMapper implements RowMapper<SubsidiaryCustomerDetail> {

	@Override
	public SubsidiaryCustomerDetail mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		SubsidiaryCustomerDetail detail = new SubsidiaryCustomerDetail();
		detail.setCustomerAid(rs.getString("custAID"));
		detail.setAccountOfficer(rs.getString("AccountOfficer"));
		return detail;
	}

}
