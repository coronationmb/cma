/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Image;

/**
 * @author waliu.faleye
 *
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

}
