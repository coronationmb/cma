/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Application;

/**
 * @author waliu.faleye
 *
 */
public interface ApplicationRepository extends JpaRepository<Application, Long> {
	
	List<Application> findByNameNot(String name);

}
