/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.TransactionDetail;

/**
 * @author waliu.faleye
 *
 */
public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Long> {

}
