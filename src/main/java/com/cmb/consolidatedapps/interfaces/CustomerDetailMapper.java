/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRp;

/**
 * @author waliu.faleye
 *
 */
public class CustomerDetailMapper implements RowMapper<ApplicationCustomerDetailRp> {

	@Override
	public ApplicationCustomerDetailRp mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		ApplicationCustomerDetailRp resp = new ApplicationCustomerDetailRp();
		resp.setCustomerName(rs.getString("acct_name"));
		resp.setEmail(rs.getString("email"));
		resp.setPhoneNumber(rs.getString("phone"));
		//resp.setCorporateUserBvn(rs.getString(""));
		resp.setFirstName(rs.getString("cust_first_name"));
		resp.setLastName(rs.getString("cust_last_name"));
		resp.setMiddleName(rs.getString("cust_middle_name"));
		
		return resp;
		
	}}