/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.InfowareResponseCode;

/**
 * @author waliu.faleye
 *
 */
public interface InfowareResponseCodeRepository extends JpaRepository<InfowareResponseCode, String> {

}
