/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.MutualFundProduct;

/**
 * @author waliu.faleye
 *
 */
public interface MutualFundProductRepository extends JpaRepository<MutualFundProduct, Long> {

}
