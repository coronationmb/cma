/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.RelationshipOfficer;

/**
 * @author waliu.faleye
 *
 */
public interface RelationshipOfficerRepository extends JpaRepository<RelationshipOfficer, Long> {

	public List<RelationshipOfficer> findByEmailAndNameAndPhone(String email, String name, String phone);
}
