/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.TopUpSource;

/**
 * @author waliu.faleye
 *
 */
public interface TopUpSourceRepository extends JpaRepository<TopUpSource, Long> {

}
