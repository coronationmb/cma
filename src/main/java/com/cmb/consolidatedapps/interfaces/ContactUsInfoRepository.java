/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.ContactUsInfo;

/**
 * @author waliu.faleye
 *
 */
public interface ContactUsInfoRepository extends JpaRepository<ContactUsInfo, Long> {

}
