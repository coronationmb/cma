/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerSubsidiaryBeneficiaryRepository extends JpaRepository<CustomerSubsidiaryBeneficiary, Long> {
	
	public List<CustomerSubsidiaryBeneficiary> findByCustomerAndApplicationAndDeleteFlgNot(Customer customer, Application application, String deleteFlg);
	
	public List<CustomerSubsidiaryBeneficiary> findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(Customer customer, Application application, String deleteFlg, String beneficiaryCustomerAid);

}
