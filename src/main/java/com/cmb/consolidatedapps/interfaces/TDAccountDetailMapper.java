/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.TDAccountDetail;

/**
 * @author waliu.faleye
 *
 */
public class TDAccountDetailMapper implements RowMapper<TDAccountDetail> {

	@Override
	public TDAccountDetail mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		TDAccountDetail tdDetail = new TDAccountDetail();
		tdDetail.setAccountName(rs.getString("acct_name"));
		tdDetail.setAccountNumber(rs.getString("foracid"));
		tdDetail.setAmount(formatAmount(rs.getBigDecimal("AMOUNT")));
		//tdDetail.setBalance(formatAmount(rs.getBigDecimal("")));
		tdDetail.setInterestAmount(formatAmount(rs.getBigDecimal("INTEREST_AMOUNT")));
		tdDetail.setMaturityDate(new SimpleDateFormat("dd-MMM-yyyy").format(rs.getDate("MATURITYDATE")));
		tdDetail.setRate(formatAmount(rs.getBigDecimal("RATE")));
		return tdDetail;
	}

	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}

}
