/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.CorporateSubGroup;

/**
 * @author waliu.faleye
 *
 */
public interface CorporateSubGroupRepository extends JpaRepository<CorporateSubGroup, Long> {

}
