/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerApplication;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerApplicationRepository extends JpaRepository<CustomerApplication, Long> {
	
	public List<CustomerApplication> findByCustomer(Customer customer);
	
	public void deleteByCustomer(Customer customer);

}
