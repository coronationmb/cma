/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.CustomerBankAccountMonthData;

/**
 * @author waliu.faleye
 *
 */
public class CustomerBankAccountMonthMapper implements RowMapper<CustomerBankAccountMonthData> {

	@Override
	public CustomerBankAccountMonthData mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		CustomerBankAccountMonthData cbamd = new CustomerBankAccountMonthData();
		cbamd.setReferenceNumber(rs.getString("ref_num"));
		cbamd.setTranAmount(rs.getBigDecimal("tran_amt"));
		cbamd.setTranDate(rs.getDate("tran_date"));
		cbamd.setTranId(rs.getString("tran_id"));
		cbamd.setTranParticular(rs.getString("tran_particular"));
		cbamd.setTranRemarks(rs.getString("tran_rmks"));
		cbamd.setPartTranType(rs.getString("part_tran_type"));
		return cbamd;
	}

}
