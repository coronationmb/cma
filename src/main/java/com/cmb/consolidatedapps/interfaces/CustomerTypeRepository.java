/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.CustomerType;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerTypeRepository extends JpaRepository<CustomerType, Long> {

}
