/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import com.cmb.consolidatedapps.model.StatementData;
import com.cmb.consolidatedapps.model.TopupTransactionDetail;
import com.cmb.consolidatedapps.model.TransactionDetail;

/**
 * @author waliu.faleye
 *
 */
public interface ReportGeneratorInterface {

	public String generateTransactionReciept(TransactionDetail transactionDetail);

	public String generateTopupReciept(TopupTransactionDetail data);

	public String generateStatementPdf(StatementData data);

}
