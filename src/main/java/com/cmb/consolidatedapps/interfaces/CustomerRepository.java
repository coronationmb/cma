/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.cmb.consolidatedapps.model.Customer;

/**
 * @author waliu.faleye
 *
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
	
	public Customer findByBvn(String bvn);
	
	public Customer findByRcNumber(String rcNumber);

}
