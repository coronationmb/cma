/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.FinancialInstitution;

/**
 * @author waliu.faleye
 *
 */
public interface FinancialInstitutionRepository extends JpaRepository<FinancialInstitution, String> {

}
