/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.Status;

/**
 * @author waliu.faleye
 *
 */
public interface StatusRepository extends JpaRepository<Status, Long> {

	public Status findByStatus(String status);

}
