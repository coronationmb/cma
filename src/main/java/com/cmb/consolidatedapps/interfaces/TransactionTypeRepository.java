/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.TransactionType;

/**
 * @author waliu.faleye
 *
 */
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {

}
