/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import com.cmb.consolidatedapps.model.LoanAccountDetail;

/**
 * @author waliu.faleye
 *
 */
public class LoanAccountDetailMapper implements RowMapper<LoanAccountDetail> {

	@Override
	public LoanAccountDetail mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		LoanAccountDetail laDetail = new LoanAccountDetail();
		laDetail.setAccountName(rs.getString("acct_name"));
		laDetail.setAccountNumber(rs.getString("foracid"));
		laDetail.setAmount(formatAmount(rs.getBigDecimal("AMOUNT")));
		laDetail.setBalance(formatAmount(rs.getBigDecimal("BALANCE")));
		laDetail.setInterestAmount(formatAmount(rs.getBigDecimal("INTEREST_AMOUNT")));
		laDetail.setMaturityDate(new SimpleDateFormat("dd-MMM-yyyy").format(rs.getDate("MATURITYDATE")));
		laDetail.setRate(formatAmount(rs.getBigDecimal("RATE")));
		return laDetail;
	}

	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}

}
