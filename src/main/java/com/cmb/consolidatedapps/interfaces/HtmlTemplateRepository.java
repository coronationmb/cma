/**
 * 
 */
package com.cmb.consolidatedapps.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cmb.consolidatedapps.model.HtmlTemplate;

/**
 * @author waliu.faleye
 *
 */
public interface HtmlTemplateRepository extends JpaRepository<HtmlTemplate, Long> {
	
	public HtmlTemplate findByTemplateName(String templateName);

}
