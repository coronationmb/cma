/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.assertj.core.util.Strings;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRp;
import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRq;
import com.cmb.consolidatedapps.model.ChartObject;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.DailyLimitBalanceResponse;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.service.ThirdPartyService;

/**
 * @author waliu.faleye
 *
 */
@RestController
public class ServiceControllers {
	ApplicationRepository applicationRepository;
	ProcessInterfaces processInterface;

	public ServiceControllers(ApplicationRepository applicationRepository, ProcessInterfaces processInterface) {
		this.applicationRepository = applicationRepository;
		this.processInterface = processInterface;
	}

	@PostMapping(value = "/customerDetail")
	public ApplicationCustomerDetailRp getCustomerDetail(@RequestBody ApplicationCustomerDetailRq req) {
		ApplicationCustomerDetailRp responseData = new ApplicationCustomerDetailRp();
		try {
			if ((Strings.isNullOrEmpty(req.getBvn()) && Strings.isNullOrEmpty(req.getRcNumber()))
					|| req.getApplicationId() == null)
				return new ApplicationCustomerDetailRp();
			String bvnRcNumber = req.getBvn();
			Optional<Application> app = applicationRepository.findById(req.getApplicationId());

			if (Strings.isNullOrEmpty(bvnRcNumber))
				bvnRcNumber = req.getRcNumber();
			try {
				if (app.isPresent()) {
					// responseData =
					// processInterface.getCustomerDetail(app.get(),
					// bvnRcNumber);
					ThirdPartyService tdptService = new ThirdPartyService();
					if ("BANK".equals(app.get().getName())) {
						List<ApplicationCustomerDetailRp> resp = tdptService.getBankCustomerDetailsFromBvn(bvnRcNumber);
						if(!resp.isEmpty())
						responseData = resp.get(0);
					} else if ("ASSET MANAGEMENT".equals(app.get().getName())) {
						List<SubsidiaryCustomerDetail> scdList = tdptService.getAMCustomerDetailFromBvn(bvnRcNumber);
						if(scdList.isEmpty()){
						SubsidiaryCustomerDetail scd = scdList.get(0);
						responseData.setCustomerName(scd.getName());
						responseData.setEmail(scd.getEmail1());
						responseData.setFirstName(scd.getFirstName());
						responseData.setLastName(scd.getLastName());
						responseData.setMiddleName(scd.getMiddleName());
						responseData.setPhoneNumber(scd.getPhone1());}
					} else if ("SECURITIES".equals(app.get().getName())) {
						List<SubsidiaryCustomerDetail> scdList = tdptService.getSecCustomerDetailFromBvn(bvnRcNumber);
						if(scdList.isEmpty()){
						SubsidiaryCustomerDetail scd = scdList.get(0);
						responseData.setCustomerName(scd.getName());
						responseData.setEmail(scd.getEmail1());
						responseData.setFirstName(scd.getFirstName());
						responseData.setLastName(scd.getLastName());
						responseData.setMiddleName(scd.getMiddleName());
						responseData.setPhoneNumber(scd.getPhone1());}

					} else if ("TRUSTEES".equals(app.get().getName())) {

					}
				}
			} catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseData;

	}

	@PostMapping(value = "/subsidiaryCustomerDetail")
	public ApplicationCustomerDetailRp subsidiaryCustomerDetail(@RequestBody ApplicationCustomerDetailRq req) {
		ApplicationCustomerDetailRp responseData = new ApplicationCustomerDetailRp();
		Application app1 = new Application();
		try {
			if ((Strings.isNullOrEmpty(req.getBvn()) && Strings.isNullOrEmpty(req.getRcNumber()))
					|| req.getApplicationId() == null)
				return new ApplicationCustomerDetailRp();
			String bvnRcNumber = req.getBvn();
			Optional<Application> app = applicationRepository.findById(req.getApplicationId());
			System.out.println("bvnRcNumber===" + bvnRcNumber);

			if (Strings.isNullOrEmpty(bvnRcNumber))
				bvnRcNumber = req.getRcNumber();
			try {
				if (app.isPresent()) {

					ThirdPartyService tdptService = new ThirdPartyService();
					app1 = app.get();
					app1.setCustomerDetailQuery(
							"SELECT  a.Name acct_name, a.Phone1 phone, a.Email1 email, a.FirstName cust_first_name, a.MiddleName cust_middle_name, a.LastName cust_last_name, a.*  FROM IWVW_Cust_Customers_WithAttributes a where CustAID = 'var1'");
					// responseData = processInterface.getCustomerDetail(app1,
					// bvnRcNumber);
					if ("ASSET MANAGEMENT".equals(app.get().getName())) {
						List<SubsidiaryCustomerDetail> scdList = tdptService
								.getAMCustomerDetailFromCustomerAid(bvnRcNumber);
						if (!scdList.isEmpty()) {
							SubsidiaryCustomerDetail scd = scdList.get(0);
							responseData.setCustomerName(scd.getName());
							responseData.setEmail(scd.getEmail1());
							responseData.setFirstName(scd.getFirstName());
							responseData.setLastName(scd.getLastName());
							responseData.setMiddleName(scd.getMiddleName());
							responseData.setPhoneNumber(scd.getPhone1());
						}
					} else if ("SECURITIES".equals(app.get().getName())) {
						List<SubsidiaryCustomerDetail> scdList = tdptService
								.getSecCustomerDetailFromCustomerAid(bvnRcNumber);
						if (!scdList.isEmpty()) {
							SubsidiaryCustomerDetail scd = scdList.get(0);
							responseData.setCustomerName(scd.getName());
							responseData.setEmail(scd.getEmail1());
							responseData.setFirstName(scd.getFirstName());
							responseData.setLastName(scd.getLastName());
							responseData.setMiddleName(scd.getMiddleName());
							responseData.setPhoneNumber(scd.getPhone1());
						}

					}
				}
			} catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseData;

	}

	@PostMapping(value = "/accountChartData")
	public List<ChartObject> accountChartData(@RequestBody CustomerBankAccount req) {
		List<ChartObject> returnList = new ArrayList<ChartObject>();

		returnList = processInterface.getChartObject(req.getAccountNumber());
		returnList.forEach(a -> a.setNaira(a.getNaira() == null ? BigDecimal.ZERO : a.getNaira()));

		return returnList;
	}

	@PostMapping(value = "/bankAccountList")
	public List<CustomerBankAccount> bankAccountList(@RequestBody ApplicationCustomerDetailRq req) {
		List<CustomerBankAccount> responseData = new ArrayList<CustomerBankAccount>();
		List<CustomerBankAccount> nairaResponseData = new ArrayList<CustomerBankAccount>();
		List<CustomerBankAccount> nonNairaResponseData = new ArrayList<CustomerBankAccount>();
		// List<String> response = new ArrayList<String>();
		try {
			if ((Strings.isNullOrEmpty(req.getBvn()) && Strings.isNullOrEmpty(req.getRcNumber()))
					|| req.getApplicationId() == null) {
				// response.add("");
				return responseData;
			}
			String bvnRcNumber = req.getBvn();
			Optional<Application> app = applicationRepository.findById(req.getApplicationId());

			if (Strings.isNullOrEmpty(bvnRcNumber))
				bvnRcNumber = req.getRcNumber();
			try {
				if (app.isPresent()) {
					// responseData =
					// processInterface.getCustomerDetail(app.get(),
					// bvnRcNumber);
					ThirdPartyService tdptService = new ThirdPartyService();
					if ("BANK".equals(app.get().getName())) {
						List<CustomerBankAccount> myResponseData = tdptService
								.getBankCustomerAccountDetailsFromBvn(bvnRcNumber);
						nairaResponseData = myResponseData.stream().filter(a -> a.getAccountCurrency().equals("NGN"))
								.collect(Collectors.toList());
						nonNairaResponseData = myResponseData.stream()
								.filter(a -> !a.getAccountCurrency().equals("NGN")).collect(Collectors.toList());

						responseData.addAll(nairaResponseData);
						responseData.addAll(nonNairaResponseData);
						// for (CustomerBankAccount data : responseData) {
						// response.add(data.getAccountNumber());
						// }
					}
				}
			} catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseData;

	}

	@PostMapping(value = "/accountDailyTranLimit")
	public DailyLimitBalanceResponse accountDailyTranLimit(@RequestBody ApplicationCustomerDetailRq req) {
		DailyLimitBalanceResponse responseData = new DailyLimitBalanceResponse();
		try {
			if ((Strings.isNullOrEmpty(req.getAccountNumber()) && Strings.isNullOrEmpty(req.getTransactionChannel()))) {
				// response.add("");
				return responseData;
			}
			ThirdPartyService tdptService = new ThirdPartyService();
			responseData = tdptService.accountDailyTranLimit(req.getTransactionChannel(), req.getAccountNumber());
			BigDecimal resValue = responseData.getTotalDailyLimit() != null
					? new BigDecimal(responseData.getTotalDailyLimit()) : BigDecimal.ZERO;
			responseData.setTotalDailyLimit(formatAmount(resValue));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseData;

	}

	@PostMapping(value = "/accountDailyBalance")
	public DailyLimitBalanceResponse accountDailyBalance(@RequestBody ApplicationCustomerDetailRq req) {
		DailyLimitBalanceResponse responseData = new DailyLimitBalanceResponse();
		try {
			if ((Strings.isNullOrEmpty(req.getAccountNumber()) && Strings.isNullOrEmpty(req.getTransactionChannel()))) {
				// response.add("");
				return responseData;
			}
			ThirdPartyService tdptService = new ThirdPartyService();
			responseData = tdptService.accountDailyBalance(req.getTransactionChannel(), req.getAccountNumber());
			BigDecimal resValue = responseData.getDailyBalance() != null
					? new BigDecimal(responseData.getDailyBalance()) : BigDecimal.ZERO;
			responseData.setDailyBalance(formatAmount(resValue));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseData;

	}

	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}
}
