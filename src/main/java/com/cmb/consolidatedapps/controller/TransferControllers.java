/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.ReportGeneratorInterface;
import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.FTSingleCreditRequest;
import com.cmb.consolidatedapps.model.FinancialInstitution;
import com.cmb.consolidatedapps.model.HtmlTemplate;
import com.cmb.consolidatedapps.model.MailDetail;
import com.cmb.consolidatedapps.model.MutualFundProduct;
import com.cmb.consolidatedapps.model.TopUpSource;
import com.cmb.consolidatedapps.model.TopUpType;
import com.cmb.consolidatedapps.model.TopupTransactionDetail;
import com.cmb.consolidatedapps.model.TransactionDetail;
import com.cmb.consolidatedapps.model.TransactionType;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.service.ThirdPartyService;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.RequestData;
import com.cmb.thirdpartyapi.model.ResponseData;
import com.cmb.thirdpartyapi.model.SecurityResponseData;
import com.expertedge.entrustplugin.ws.AuthResponseDTO;
import com.expertedge.entrustplugin.ws.EntrustMultiFactorAuthImplService;
import com.expertedge.entrustplugin.ws.TokenAuthDTO;

/**
 * @author waliu.faleye
 *
 */
@Controller
public class TransferControllers {
	@Value("${am.notification.email}")
	String amNotificationEmail;

	@Value("${sec.notification.email}")
	String secNotificationEmail;
	@Value("${email.sender}")
	String sender;

	@Value("${topup.mail.subject}")
	String topupMailSubject;

	@Autowired
	JavaMailSender javaMailSender;

	@Value("${thirdpartyapi.baseurl.assetmgt}")
	String thirdpartyapiBaseUrlAssetMgt;

	@Value("${thirdpartyapi.baseurl.localapi}")
	String thirdpartyapiBaseUrlLocal;

	@Value("${thirdpartyapi.baseurl.securities}")
	String thirdpartyapiBaseUrlSec;

	ApplicationRepository applicationRepo;
	ProcessInterfaces processInterface;

	ReportGeneratorInterface reportGeneratorInterface;

	@Value("${interbankTranTypeId}")
	String interbankTranTypeId;
	@Value("${internalTranTypeId}")
	String internalTranTypeId;
	@Value("${ownAccountTranTypeId}")
	String ownAccountTranTypeId;
	@Value("${bank.code}")
	String bankCode;
	@Value("${cosec.bank.credit.account}")
	String secBankCreditAccount;

	@Value("${entrust.app.code}")
	String entrustAppCode;

	@Value("${entrust.app.desc}")
	String entrustAppDesc;

	@Value("${entrust.token.authentication.failed}")
	String entrustTokenAuthFailed;

	public TransferControllers(ApplicationRepository applicationRepo, ProcessInterfaces processInterface,
			ReportGeneratorInterface reportGeneratorInterface) {
		this.applicationRepo = applicationRepo;
		this.processInterface = processInterface;
		this.reportGeneratorInterface = reportGeneratorInterface;
	}

	@GetMapping(value = "/topup")
	public String bankTopup(Model model) {
		List<CustomerSubsidiaryBeneficiary> secBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
		List<CustomerSubsidiaryBeneficiary> amBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
		List<CustomerSubsidiaryBeneficiary> trusteesBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

		try {
			CustomerBankAccount cba = new CustomerBankAccount();
			cba.setBalanceAmountStr("Select Current Account");
			cba.setDisplayAccountNumber("Select Current Account");
			List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
			listAccounts.add(cba);
			List<Application> allApps = new ArrayList<Application>();
			Application defApp = new Application();
			defApp.setName("Select App");
			allApps.add(defApp);
			// System.out.println("my id ==" + id);
			List<Application> apps = processInterface.findByNameNot("BANK");
			allApps.addAll(apps);
			System.out.println("my apps.size() ==" + apps.size());
			CustomerSubsidiaryBeneficiary customerBeneficiary = new CustomerSubsidiaryBeneficiary();
			customerBeneficiary.setBeneficiaryCustomerAid("000000");
			customerBeneficiary.setBeneficiaryCustomerName("Select Beneficiary");
			customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
			// List<CustomerSubsidiaryBeneficiary> beneficiaries = new
			// ArrayList<CustomerSubsidiaryBeneficiary>();
			secBeneficiaries.add(customerBeneficiary);
			amBeneficiaries.add(customerBeneficiary);
			trusteesBeneficiaries.add(customerBeneficiary);
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");
			List<CustomerBankAccount> currentAccountList = dashboardDetails != null
					? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
			currentAccountList
					.forEach(a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
							.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
							.getClrBalanceAmount()))));
			currentAccountList.forEach(a -> a.setDisplayAccountNumber(
					"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
			Customer customer = dashboardDetails.getUser().getCustomer();
			for (Application app : apps) {
				if ("ASSET MANAGEMENT".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> amBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					amBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
					amBeneficiaries.addAll(amBenList);
				} else if ("SECURITIES".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> secBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					secBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
					secBeneficiaries.addAll(secBenList);
				} else if ("TRUSTEES".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> trustBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					trustBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));

					trusteesBeneficiaries.addAll(trustBenList);
				} else {
				}
			}
			System.out.println("amBeneficiaries.size() ==" + amBeneficiaries.size());
			System.out.println("secBeneficiaries.size() ==" + secBeneficiaries.size());
			System.out.println("trusteesBeneficiaries.size() ==" + trusteesBeneficiaries.size());

			TopupTransactionDetail tranDetail = new TopupTransactionDetail();
			List<MutualFundProduct> mfps = new ArrayList<MutualFundProduct>();
			MutualFundProduct defMfp = new MutualFundProduct();
			defMfp.setFundName("Select Fund Type");
			mfps.add(defMfp);
			mfps.addAll(processInterface.findAllMutualFundProducts());
			List<TopUpSource> tus = new ArrayList<TopUpSource>();
			TopUpSource defTus = new TopUpSource();
			defTus.setType("Select Top-Up Source");
			tus.add(defTus);
			tus.addAll(processInterface.findAllTopUpSources());
			List<TopUpType> tut = new ArrayList<TopUpType>();
			TopUpType defTut = new TopUpType();
			defTut.setType("Select Top-Up Type");
			tut.add(defTut);
			tut.addAll(processInterface.findAllTopUpTypes());
			model.addAttribute("transactionDetail", tranDetail);
			model.addAttribute("mfProducts", mfps);
			model.addAttribute("topUpSources", tus);
			model.addAttribute("topUpTypes", tut);
			model.addAttribute("amBeneficiaries", amBeneficiaries);
			model.addAttribute("secBeneficiaries", secBeneficiaries);
			model.addAttribute("trusteesBeneficiaries", trusteesBeneficiaries);
			model.addAttribute("apps", allApps);
			model.addAttribute("customerDashboard", dashboardDetails);
			listAccounts.addAll(currentAccountList);
			model.addAttribute("currentAccountList", listAccounts);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "banktopup";
	}

	// @GetMapping(value = { "/transfer/{id}", "/topup/{id}" })
	// @GetMapping(value = "/transfer/{id}")
	@GetMapping(value = "/transfer")
	public String transfer(Model model) {
		CustomerBeneficiary customerBeneficiary = new CustomerBeneficiary();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		try {
			customerBeneficiary.setBeneficiaryAccountName("Default");
			customerBeneficiary.setBeneficiaryAccountNumber("0000000000");
			customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
			customerBeneficiary.setId(0L);
			List<CustomerBeneficiary> externalbeneficiaries = new ArrayList<CustomerBeneficiary>();
			externalbeneficiaries.add(customerBeneficiary);
			List<CustomerBeneficiary> internalbeneficiaries = new ArrayList<CustomerBeneficiary>();
			internalbeneficiaries.add(customerBeneficiary);
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");
			Customer customer = dashboardDetails.getUser().getCustomer();
			List<CustomerBankAccount> currentAccountList = dashboardDetails != null
					? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
			currentAccountList
					.forEach(a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
							.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
							.getClrBalanceAmount()))));
			currentAccountList.forEach(a -> a.setDisplayAccountNumber(
					"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
			List<CustomerBeneficiary> beneficiaries = processInterface.findBeneficiariesByCustomer(customer);
			System.out.println("beneficiaries.size() ==" + beneficiaries.size());
			if (beneficiaries != null) {
				List<CustomerBeneficiary> extBen = beneficiaries.stream()
						.filter(a -> a.getTransactionType()
								.equals(processInterface.findByTransactionTypeById(Long.valueOf(interbankTranTypeId))))
						.collect(Collectors.toList());
				extBen.forEach(a -> a.setDisplayBeneficiary(
						a.getBeneficiaryAccountName().concat(" | ").concat(a.getBeneficiaryAccountNumber())));
				externalbeneficiaries.addAll(extBen);
				List<CustomerBeneficiary> intBen = beneficiaries.stream()
						.filter(a -> a.getTransactionType()
								.equals(processInterface.findByTransactionTypeById(Long.valueOf(internalTranTypeId))))
						.collect(Collectors.toList());
				intBen.forEach(a -> a.setDisplayBeneficiary(
						a.getBeneficiaryAccountName().concat(" | ").concat(a.getBeneficiaryAccountNumber())));
				internalbeneficiaries.addAll(intBen);

			}
			System.out.println("my id ==" + 1);
			Application app = applicationRepo.getOne(1L);
			TransactionType defaultTranType = new TransactionType();
			defaultTranType.setTransactionType("Select Transaction Type");
			CustomerBankAccount cba = new CustomerBankAccount();
			cba.setBalanceAmountStr("Select Current Account");
			cba.setDisplayAccountNumber("Select Current Account");
			List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
			listAccounts.add(cba);
			List<TransactionType> listTranTypes = new ArrayList<TransactionType>();
			listTranTypes.add(defaultTranType);
			listTranTypes.addAll(app.getTransactionTypes());
			System.out.println("my app.getTransactionTypes().size() ==" + app.getTransactionTypes().size());
			TransactionDetail tranDetail = new TransactionDetail();
			model.addAttribute("transactionTypes", listTranTypes);
			model.addAttribute("transactionDetail", tranDetail);
			model.addAttribute("externalbeneficiaries", externalbeneficiaries);
			model.addAttribute("internalbeneficiaries", internalbeneficiaries);
			FinancialInstitution dummyInstitute = new FinancialInstitution();
			dummyInstitute.setInstitutionCode("000000");
			dummyInstitute.setInstitutionName("Select Bank");
			List<FinancialInstitution> allInstitutes = new ArrayList<FinancialInstitution>();
			allInstitutes.add(dummyInstitute);
			List<FinancialInstitution> institutes = processInterface.financialInstitutions();// .sort(Comparator.comparing(FinancialInstitution::getInstitutionName));
			Collections.sort(institutes, Comparator.comparing(FinancialInstitution::getInstitutionName));
			allInstitutes.addAll(institutes);
			model.addAttribute("financialInstitutions", allInstitutes);
			listAccounts.addAll(currentAccountList);
			model.addAttribute("currentAccountList", listAccounts);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "transfer";
	}

	@GetMapping(value = "/topupassetMgt")
	public String topup(Model model) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		try {
			CustomerBankAccount cba = new CustomerBankAccount();
			cba.setBalanceAmountStr("Select Current Account");
			cba.setDisplayAccountNumber("Select Current Account");
			List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
			listAccounts.add(cba);
			// System.out.println("my id ==" + id);
			Application app = applicationRepo.getOne(2L);
			System.out.println("my app.getTransactionTypes().size() ==" + app.getTransactionTypes().size());
			CustomerSubsidiaryBeneficiary customerBeneficiary = new CustomerSubsidiaryBeneficiary();
			customerBeneficiary.setBeneficiaryCustomerAid("000000");
			customerBeneficiary.setBeneficiaryCustomerName("Select Beneficiary");
			customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
			List<CustomerSubsidiaryBeneficiary> beneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
			beneficiaries.add(customerBeneficiary);
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");
			List<CustomerBankAccount> currentAccountList = dashboardDetails != null
					? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
			currentAccountList
					.forEach(a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
							.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
							.getClrBalanceAmount()))));
			currentAccountList.forEach(a -> a.setDisplayAccountNumber(
					"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
			Customer customer = dashboardDetails.getUser().getCustomer();
			List<CustomerSubsidiaryBeneficiary> custbeneficiaries = processInterface
					.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
			System.out.println("custbeneficiaries.size() ==" + custbeneficiaries.size());
			if (custbeneficiaries != null && !custbeneficiaries.isEmpty()) {
				custbeneficiaries.forEach(a -> a.setDisplayBeneficiary(
						a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
				beneficiaries.addAll(custbeneficiaries);

			}
			TopupTransactionDetail tranDetail = new TopupTransactionDetail();
			tranDetail.setApplicationId(app.getId());
			tranDetail.setApplication(app);
			List<MutualFundProduct> mfps = new ArrayList<MutualFundProduct>();
			MutualFundProduct defMfp = new MutualFundProduct();
			defMfp.setFundName("Select Fund Type");
			mfps.add(defMfp);
			mfps.addAll(processInterface.findAllMutualFundProducts());
			List<TopUpSource> tus = new ArrayList<TopUpSource>();
			TopUpSource defTus = new TopUpSource();
			defTus.setType("Select Top-Up Source");
			tus.add(defTus);
			tus.addAll(processInterface.findAllTopUpSources());
			List<TopUpType> tut = new ArrayList<TopUpType>();
			TopUpType defTut = new TopUpType();
			defTut.setType("Select Top-Up Type");
			tut.add(defTut);
			tut.addAll(processInterface.findAllTopUpTypes());
			model.addAttribute("transactionTypes", app.getTransactionTypes());
			model.addAttribute("transactionDetail", tranDetail);
			model.addAttribute("mfProducts", mfps);
			model.addAttribute("topUpSources", tus);
			model.addAttribute("topUpTypes", tut);
			model.addAttribute("beneficiaries", beneficiaries);
			listAccounts.addAll(currentAccountList);
			model.addAttribute("currentAccountList", listAccounts);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "subsidiarytransfer";
	}

	@GetMapping(value = "/topupsec")
	public String topupSec(Model model) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		try {
			CustomerBankAccount cba = new CustomerBankAccount();
			cba.setBalanceAmountStr("Select Current Account");
			cba.setDisplayAccountNumber("Select Current Account");
			List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
			listAccounts.add(cba);
			// System.out.println("my id ==" + id);
			Application app = applicationRepo.getOne(3L);
			System.out.println("my app.getTransactionTypes().size() ==" + app.getTransactionTypes().size());
			CustomerSubsidiaryBeneficiary customerBeneficiary = new CustomerSubsidiaryBeneficiary();
			customerBeneficiary.setBeneficiaryCustomerAid("000000");
			customerBeneficiary.setBeneficiaryCustomerName("Select Beneficiary");
			customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
			List<CustomerSubsidiaryBeneficiary> beneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
			beneficiaries.add(customerBeneficiary);
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");
			List<CustomerBankAccount> currentAccountList = dashboardDetails != null
					? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
			currentAccountList
					.forEach(a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
							.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
							.getClrBalanceAmount()))));
			currentAccountList.forEach(a -> a.setDisplayAccountNumber(
					"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
			Customer customer = dashboardDetails.getUser().getCustomer();
			List<CustomerSubsidiaryBeneficiary> custbeneficiaries = processInterface
					.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
			System.out.println("custbeneficiaries.size() ==" + custbeneficiaries.size());
			if (custbeneficiaries != null && !custbeneficiaries.isEmpty()) {
				custbeneficiaries.forEach(a -> a.setDisplayBeneficiary(
						a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
				beneficiaries.addAll(custbeneficiaries);

			}
			TopupTransactionDetail tranDetail = new TopupTransactionDetail();
			System.out.println("app.getId() ==" + app.getId());
			tranDetail.setApplicationId(app.getId());
			tranDetail.setApplication(app);
			List<MutualFundProduct> mfps = new ArrayList<MutualFundProduct>();
			MutualFundProduct defMfp = new MutualFundProduct();
			defMfp.setFundName("Select Fund Type");
			mfps.add(defMfp);
			mfps.addAll(processInterface.findAllMutualFundProducts());
			List<TopUpSource> tus = new ArrayList<TopUpSource>();
			TopUpSource defTus = new TopUpSource();
			defTus.setType("Select Top-Up Source");
			tus.add(defTus);
			tus.addAll(processInterface.findAllTopUpSources());
			List<TopUpType> tut = new ArrayList<TopUpType>();
			TopUpType defTut = new TopUpType();
			defTut.setType("Select Top-Up Type");
			tut.add(defTut);
			tut.addAll(processInterface.findAllTopUpTypes());
			model.addAttribute("transactionTypes", app.getTransactionTypes());
			model.addAttribute("transactionDetail", tranDetail);
			model.addAttribute("mfProducts", mfps);
			model.addAttribute("topUpSources", tus);
			model.addAttribute("topUpTypes", tut);
			model.addAttribute("beneficiaries", beneficiaries);
			listAccounts.addAll(currentAccountList);
			model.addAttribute("currentAccountList", listAccounts);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "subsidiarytransfer";
	}

	@PostMapping(value = "/saveTopupTransactionDetail")
	public String saveTopupTransactionDetail(Model model, TopupTransactionDetail transactionDetail) {
		Application app = null;
		System.out.println("transactionDetail.getMode()===" + transactionDetail.getMode());
		try {
			String retStr = this.validateTransactionDetail(transactionDetail);
			if (!Strings.isNullOrEmpty(retStr)) {
				transactionDetail.setNewBenCheck(transactionDetail.getNewBen());
				transactionDetail.setExistBenCheck(transactionDetail.getExistingBen());
				transactionDetail.setErrorMessage(retStr);
				app = processInterface.findApplicationById(transactionDetail.getApplicationId());
				String returnPage = "";
				if ("bank".equals(transactionDetail.getMode())) {
					returnPage = "banktopup";
				} else {
					returnPage = "subsidiarytransfer";
				}
				System.out.println("returnPage===" + returnPage);
				List<CustomerSubsidiaryBeneficiary> secBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
				List<CustomerSubsidiaryBeneficiary> amBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
				List<CustomerSubsidiaryBeneficiary> trusteesBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
				CustomerBankAccount cba = new CustomerBankAccount();
				cba.setBalanceAmountStr("Select Current Account");
				cba.setDisplayAccountNumber("Select Current Account");
				List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
				listAccounts.add(cba);
				List<Application> allApps = new ArrayList<Application>();
				Application defApp = new Application();
				defApp.setName("Select App");
				allApps.add(defApp);
				// System.out.println("my id ==" + id);
				List<Application> apps = processInterface.findByNameNot("BANK");
				allApps.addAll(apps);
				System.out.println("my apps.size() ==" + apps.size());
				CustomerSubsidiaryBeneficiary customerBeneficiary = new CustomerSubsidiaryBeneficiary();
				customerBeneficiary.setBeneficiaryCustomerAid("000000");
				customerBeneficiary.setBeneficiaryCustomerName("Select Beneficiary");
				customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
				// List<CustomerSubsidiaryBeneficiary> beneficiaries = new
				// ArrayList<CustomerSubsidiaryBeneficiary>();
				secBeneficiaries.add(customerBeneficiary);
				amBeneficiaries.add(customerBeneficiary);
				trusteesBeneficiaries.add(customerBeneficiary);
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);
				CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
						.getAttribute("dashboardDetail");
				List<CustomerBankAccount> currentAccountList = dashboardDetails != null
						? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
				currentAccountList.forEach(
						a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
								.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
								.getClrBalanceAmount()))));
				currentAccountList.forEach(a -> a.setDisplayAccountNumber(
						"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
				Customer customer = dashboardDetails.getUser().getCustomer();
				// for (Application app : apps) {
				if ("ASSET MANAGEMENT".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> amBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					amBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
					amBeneficiaries.addAll(amBenList);
				} else if ("SECURITIES".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> secBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					secBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));
					secBeneficiaries.addAll(secBenList);
				} else if ("TRUSTEES".equals(app.getName())) {
					List<CustomerSubsidiaryBeneficiary> trustBenList = processInterface
							.findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(customer, app);
					trustBenList.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryCustomerName().concat(" | ").concat(a.getBeneficiaryCustomerAid())));

					trusteesBeneficiaries.addAll(trustBenList);
				} else {
				}
				// }
				System.out.println("amBeneficiaries.size() ==" + amBeneficiaries.size());
				System.out.println("secBeneficiaries.size() ==" + secBeneficiaries.size());
				System.out.println("trusteesBeneficiaries.size() ==" + trusteesBeneficiaries.size());

				// TopupTransactionDetail tranDetail = new
				// TopupTransactionDetail();
				List<MutualFundProduct> mfps = new ArrayList<MutualFundProduct>();
				MutualFundProduct defMfp = new MutualFundProduct();
				defMfp.setFundName("Select Fund Type");
				mfps.add(defMfp);
				mfps.addAll(processInterface.findAllMutualFundProducts());
				List<TopUpSource> tus = new ArrayList<TopUpSource>();
				TopUpSource defTus = new TopUpSource();
				defTus.setType("Select Top-Up Source");
				tus.add(defTus);
				tus.addAll(processInterface.findAllTopUpSources());
				List<TopUpType> tut = new ArrayList<TopUpType>();
				TopUpType defTut = new TopUpType();
				defTut.setType("Select Top-Up Type");
				tut.add(defTut);
				tut.addAll(processInterface.findAllTopUpTypes());
				transactionDetail.setApplication(app);
				model.addAttribute("transactionDetail", transactionDetail);
				model.addAttribute("mfProducts", mfps);
				model.addAttribute("topUpSources", tus);
				model.addAttribute("topUpTypes", tut);
				model.addAttribute("amBeneficiaries", amBeneficiaries);
				model.addAttribute("secBeneficiaries", secBeneficiaries);
				model.addAttribute("trusteesBeneficiaries", trusteesBeneficiaries);
				model.addAttribute("apps", allApps);
				model.addAttribute("customerDashboard", dashboardDetails);
				listAccounts.addAll(currentAccountList);
				model.addAttribute("currentAccountList", listAccounts);
				return returnPage;
			}
			if (transactionDetail.getApplicationId() > 0) {
				app = applicationRepo.getOne(transactionDetail.getApplicationId());
				transactionDetail.setApplication(app);
			}
			if (transactionDetail.getFundTypeId() != null && transactionDetail.getFundTypeId() > 0) {
				transactionDetail.setMutualFundProduct(
						processInterface.findMutualFundProductById(transactionDetail.getFundTypeId()));
			}
			if (transactionDetail.getTopUpSrcId() > 0) {
				transactionDetail
						.setTopUpSource(processInterface.findTopUpSourceById(transactionDetail.getTopUpSrcId()));
			}
			if (transactionDetail.getTopUpTypeId() > 0) {
				transactionDetail.setTopUpType(processInterface.findTopUpTypeById(transactionDetail.getTopUpTypeId()));
			}
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");

			User user = processInterface.findUserById(transactionDetail.getUserId());
			transactionDetail.setUser(user);

			CustomerBankAccount accountDetail = dashboardDetails.getCurrentAccountList().stream()
					.filter(a -> "NGN".equals(a.getAccountCurrency())).collect(Collectors.toList()).get(0);
			// .getAccountNumber();
			transactionDetail.setAccountNumber(accountDetail.getAccountNumber());
			System.out.println("transactionDetail.getCustomerAid()==" + transactionDetail.getCustomerAid());

			transactionDetail.setRequestDate(LocalDateTime.now());
			if ("Self".equals(transactionDetail.getTopUpType().getType())) {
				transactionDetail.setCustomerName(accountDetail.getAccountName());
				//System.out.println("self transactionDetail.getMutualFundProduct().getFundCode()=="
				//		+ transactionDetail.getMutualFundProduct().getFundCode());
				System.out.println("self dashboardDetails.getCmmfCustAid()==" + dashboardDetails.getCmmfCustAid());
				// if (transactionDetail.getCustomerAid() != null) {
				if ("ASSET MANAGEMENT".equals(transactionDetail.getApplication().getName())) {
					if ("CBF".equals(transactionDetail.getMutualFundProduct().getFundCode())) {
						transactionDetail.setCustomerAid(dashboardDetails.getCbfCustAid());
					} else if ("CFIF".equals(transactionDetail.getMutualFundProduct().getFundCode())) {
						transactionDetail.setCustomerAid(dashboardDetails.getCfifCustAid());
					} else if ("CMMFUND".equals(transactionDetail.getMutualFundProduct().getFundCode())) {
						transactionDetail.setCustomerAid(dashboardDetails.getCmmfCustAid());
					}
					transactionDetail.setCustomerAid(transactionDetail.getOwnAmCustomerAid());
				} else if ("SECURITIES".equals(transactionDetail.getApplication().getName())) {
					// transactionDetail.setCustomerAid(dashboardDetails.getCsecCustAid());
					transactionDetail.setCustomerAid(transactionDetail.getOwnSecCustomerAid());
				} else if ("TRUSTEES".equals(transactionDetail.getApplication().getName())) {
					transactionDetail.setCustomerAid(transactionDetail.getOwnTrustCustomerAid());
				}
				// }
				// transactionDetail.setCustomerAid(transactionDetail.getOwnAmCustomerAid());
				System.out.println("self transactionDetail.getCustomerAid()==" + transactionDetail.getCustomerAid());
			} else {
			}

			if (!Strings.isNullOrEmpty(transactionDetail.getAddBeneficiary())
					&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
				CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = new CustomerSubsidiaryBeneficiary();
				customerSubsidiaryBeneficiary.setApplication(app);
				customerSubsidiaryBeneficiary.setBeneficiaryCustomerAid(transactionDetail.getCustomerAid());
				customerSubsidiaryBeneficiary.setBeneficiaryCustomerName(transactionDetail.getCustomerName());
				customerSubsidiaryBeneficiary.setCustomer(user.getCustomer());
				if (transactionDetail.getFundTypeId() != null) {
					customerSubsidiaryBeneficiary.setMutualFundProduct(
							processInterface.findMutualFundProductById(transactionDetail.getFundTypeId()));
				}

				List<CustomerSubsidiaryBeneficiary> csbExist = processInterface
						.findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(user.getCustomer(), app, "N",
								transactionDetail.getCustomerAid());

				if (csbExist.isEmpty()) {

					transactionDetail.setBeneficiaryExist("Y");
					model.addAttribute("transactionDetail", transactionDetail);
					// return "subsidiarytransfer";
					processInterface.saveCustomerSubsidiaryBeneficiary(customerSubsidiaryBeneficiary);
				}

			}
			if (transactionDetail.getBeneficiaryId() != null) {
				CustomerSubsidiaryBeneficiary csb = processInterface
						.findCustomerSubsidiaryBeneficiaryById(transactionDetail.getBeneficiaryId());
				transactionDetail.setCustomerAid(csb.getBeneficiaryCustomerAid());
				transactionDetail.setCustomerName(csb.getBeneficiaryCustomerName());
				transactionDetail.setCustomerSubsidiaryBeneficiary(csb);
			}

			// top up from bank
			if ("bank".equals(transactionDetail.getMode())) {
				System.out.println("transactionDetail.getApplicationId()===" + transactionDetail.getApplicationId());
				if (transactionDetail.getApplicationId().equals(2L)) {
					if (transactionDetail.getAmBeneficiaryId() != null
							&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
						CustomerSubsidiaryBeneficiary csb = processInterface
								.findCustomerSubsidiaryBeneficiaryById(transactionDetail.getAmBeneficiaryId());
						transactionDetail.setCustomerAid(csb.getBeneficiaryCustomerAid());
						transactionDetail.setCustomerName(csb.getBeneficiaryCustomerName());
						transactionDetail.setCustomerSubsidiaryBeneficiary(csb);
					} else {
						if (!Strings.isNullOrEmpty(transactionDetail.getAmAddBeneficiary())
								&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
							CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = new CustomerSubsidiaryBeneficiary();
							customerSubsidiaryBeneficiary.setApplication(app);
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerAid(transactionDetail.getAmCustomerAid());
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerName(transactionDetail.getAmCustomerName());
							customerSubsidiaryBeneficiary.setCustomer(user.getCustomer());
							if (transactionDetail.getFundTypeId() != null) {
								customerSubsidiaryBeneficiary.setMutualFundProduct(
										processInterface.findMutualFundProductById(transactionDetail.getFundTypeId()));
							}
							transactionDetail.setCustomerAid(transactionDetail.getAmCustomerAid());
							transactionDetail.setCustomerName(transactionDetail.getAmCustomerName());

							List<CustomerSubsidiaryBeneficiary> csbExist = processInterface
									.findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(
											user.getCustomer(), app, "N", transactionDetail.getAmCustomerAid());

							if (csbExist.isEmpty()) {

								transactionDetail.setBeneficiaryExist("Y");
								model.addAttribute("transactionDetail", transactionDetail);
								// return "subsidiarytransfer";
								processInterface.saveCustomerSubsidiaryBeneficiary(customerSubsidiaryBeneficiary);
							}
						}
					}

				}
				if (transactionDetail.getApplicationId().equals(3L)) {
					if (transactionDetail.getSecBeneficiaryId() != null
							&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
						CustomerSubsidiaryBeneficiary csb = processInterface
								.findCustomerSubsidiaryBeneficiaryById(transactionDetail.getSecBeneficiaryId());
						transactionDetail.setCustomerAid(csb.getBeneficiaryCustomerAid());
						transactionDetail.setCustomerName(csb.getBeneficiaryCustomerName());
						transactionDetail.setCustomerSubsidiaryBeneficiary(csb);
					} else {
						if (!Strings.isNullOrEmpty(transactionDetail.getSecAddBeneficiary())
								&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
							CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = new CustomerSubsidiaryBeneficiary();
							customerSubsidiaryBeneficiary.setApplication(app);
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerAid(transactionDetail.getSecCustomerAid());
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerName(transactionDetail.getSecCustomerName());
							customerSubsidiaryBeneficiary.setCustomer(user.getCustomer());
							if (transactionDetail.getFundTypeId() != null) {
								customerSubsidiaryBeneficiary.setMutualFundProduct(
										processInterface.findMutualFundProductById(transactionDetail.getFundTypeId()));
							}
							transactionDetail.setCustomerAid(transactionDetail.getSecCustomerAid());
							transactionDetail.setCustomerName(transactionDetail.getSecCustomerName());

							List<CustomerSubsidiaryBeneficiary> csbExist = processInterface
									.findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(
											user.getCustomer(), app, "N", transactionDetail.getSecCustomerAid());
							if (csbExist.isEmpty()) {

								transactionDetail.setBeneficiaryExist("Y");
								model.addAttribute("transactionDetail", transactionDetail);
								// return "subsidiarytransfer";
								processInterface.saveCustomerSubsidiaryBeneficiary(customerSubsidiaryBeneficiary);
							}
						}
					}
					transactionDetail.setMutualFundProduct(null);

				}
				if (transactionDetail.getApplicationId().equals(4L)) {
					if (transactionDetail.getTrustBeneficiaryId() != null
							&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
						CustomerSubsidiaryBeneficiary csb = processInterface
								.findCustomerSubsidiaryBeneficiaryById(transactionDetail.getTrustBeneficiaryId());
						transactionDetail.setCustomerAid(csb.getBeneficiaryCustomerAid());
						transactionDetail.setCustomerName(csb.getBeneficiaryCustomerName());
						transactionDetail.setCustomerSubsidiaryBeneficiary(csb);
					} else {
						if (!Strings.isNullOrEmpty(transactionDetail.getTrustAddBeneficiary())
								&& "Third Party".equals(transactionDetail.getTopUpType().getType())) {
							CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = new CustomerSubsidiaryBeneficiary();
							customerSubsidiaryBeneficiary.setApplication(app);
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerAid(transactionDetail.getTrustCustomerAid());
							customerSubsidiaryBeneficiary
									.setBeneficiaryCustomerName(transactionDetail.getTrustCustomerName());
							customerSubsidiaryBeneficiary.setCustomer(user.getCustomer());
							if (transactionDetail.getFundTypeId() != null) {
								customerSubsidiaryBeneficiary.setMutualFundProduct(
										processInterface.findMutualFundProductById(transactionDetail.getFundTypeId()));
							}
							transactionDetail.setCustomerAid(transactionDetail.getTrustCustomerAid());
							transactionDetail.setCustomerName(transactionDetail.getTrustCustomerName());

							List<CustomerSubsidiaryBeneficiary> csbExist = processInterface
									.findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(
											user.getCustomer(), app, "N", transactionDetail.getTrustCustomerAid());

							if (csbExist.isEmpty()) {

								transactionDetail.setBeneficiaryExist("Y");
								model.addAttribute("transactionDetail", transactionDetail);
								// return "subsidiarytransfer";
								processInterface.saveCustomerSubsidiaryBeneficiary(customerSubsidiaryBeneficiary);
							}
						}
					}
					transactionDetail.setMutualFundProduct(null);

				}
			}

			if (transactionDetail.getTopUpSrcId().equals(1L)) {
				transactionDetail.setAccountNumber(transactionDetail.getOwnAccountNumber());
			} else {

			}

			processInterface.saveTopupTransactionDetail(transactionDetail);
			transactionDetail.setFormattedAmount(processInterface.formatAmount(transactionDetail.getAmount()));
			// model.addAttribute("transactionTypes",
			// app.getTransactionTypes());
			model.addAttribute("transactionDetail", transactionDetail);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "subsidiarytransfersummary";

	}

	@PostMapping(value = "/postTopupTransfer")
	public String postTopupTransfer(Model model, TopupTransactionDetail transactionDetail) {

		TopupTransactionDetail tranDetail = new TopupTransactionDetail();
		tranDetail = processInterface.findTopupTransactionDetailById(transactionDetail.getId());
		tranDetail.setFormattedAmount(processInterface.formatAmount(tranDetail.getAmount()));
		User user = processInterface.findUserById(transactionDetail.getUserId());
		EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
		TokenAuthDTO arg0 = new TokenAuthDTO();
		arg0.setAppCode(entrustAppCode);
		arg0.setAppDesc(entrustAppDesc);
		arg0.setGroup(user.getCustomer().getCustomerType().getEntrustGroup());
		arg0.setTokenPin(transactionDetail.getToken());
		arg0.setUserName(user.getUserId());
		AuthResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performTokenAuth(arg0);

		if (res != null) {
			if (res.getRespCode() == 1) {
				System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
				System.out.println("token res.getRespCode() ==" + res.getRespCode());
				System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
				tranDetail.setTokenRespMessage(entrustTokenAuthFailed);
				model.addAttribute("transactionDetail", tranDetail);
				return "subsidiarytransfersummary";
			} else {

			}
		} else {
			System.out.println("token response ==" + res);
			return "subsidiarytransfersummary";
		}
		String notificationEmail = "";
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		AssetManagementResponseData amLoginRes = new AssetManagementResponseData();
		AssetManagementResponseData amFundAcctRes = new AssetManagementResponseData();
		SecurityResponseData secLoginRes = new SecurityResponseData();
		SecurityResponseData secFundAcctRes = new SecurityResponseData();
		String CRAccountSub = "";
		boolean reverseTransfer = false;
		ThirdPartyService thirdServ = new ThirdPartyService();
		ResponseData localRes = new ResponseData();
		String bankCreditAccount = "";
		String uniqueIdentifier = LocalDateTime.now().format(format);
		String status = "";
		try {
			if (tranDetail.getCustomerSubsidiaryBeneficiary() != null) {
				CRAccountSub = tranDetail.getCustomerSubsidiaryBeneficiary().getBeneficiaryCustomerAid();
			} else {
				CRAccountSub = tranDetail.getCustomerAid();
			}
			if ("ASSET MANAGEMENT".equals(tranDetail.getApplication().getName())) {
				notificationEmail = amNotificationEmail;
				if ("CBF".equals(tranDetail.getMutualFundProduct().getFundCode())) {
					bankCreditAccount = tranDetail.getMutualFundProduct().getBankCreditAccount();
				} else if ("CFIF".equals(tranDetail.getMutualFundProduct().getFundCode())) {
					bankCreditAccount = tranDetail.getMutualFundProduct().getBankCreditAccount();
				} else if ("CMMFUND".equals(tranDetail.getMutualFundProduct().getFundCode())) {
					bankCreditAccount = tranDetail.getMutualFundProduct().getBankCreditAccount();
				}
			} else if ("SECURITIES".equals(tranDetail.getApplication().getName())) {
				notificationEmail = secNotificationEmail;
				bankCreditAccount = secBankCreditAccount;
			}
			if ("My CoronationMB Account".equals(tranDetail.getTopUpSource().getType())) {
				// Local Transfer for Debit
				RequestData req = new RequestData();
				req.setDebitAccountNumber(tranDetail.getAccountNumber());
				req.setCreditAccountNumber(bankCreditAccount);
				req.setTranAmount(tranDetail.getAmount().toString());
				req.setNaration(tranDetail.getRemark());
				req.setUniqueIdentifier(uniqueIdentifier);
				localRes = processInterface.localTransfer(req);
				tranDetail.setUniqueIdentifier(uniqueIdentifier);
			} else {
				// Other bank debit of customer account
			}
			tranDetail.setDrResponseCode(localRes.getResponseCode());
			tranDetail.setDrResponseDescription(localRes.getResponseDescription());
			if ("000".equals(localRes.getResponseCode())) {
				// calling topup api
				try {
					if ("ASSET MANAGEMENT".equals(tranDetail.getApplication().getName())) {
						thirdServ.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
						amLoginRes = thirdServ.assetMngmtLogin();

						if (amLoginRes.getStatusId() != null && amLoginRes.getStatusId().equals(0L)) {
							amFundAcctRes = thirdServ.fundAccountAssetMgt(amLoginRes.getOutValue(), CRAccountSub,
									tranDetail.getId().toString(), tranDetail.getAmount().toString(),
									tranDetail.getRemark(), tranDetail.getMutualFundProduct().getCrMasterAid(),
									tranDetail.getMutualFundProduct().getDrMasterAid(),
									tranDetail.getMutualFundProduct().getDrSubAid());
							// to get the description
							tranDetail.setResponseCode(amFundAcctRes.getStatusId().toString());
							tranDetail.setResponseDescription(processInterface
									.findByStatusId(amFundAcctRes.getStatusId().toString()).getResponseDescription());
						}
						if (amFundAcctRes.getStatusId() != null && !amFundAcctRes.getStatusId().equals(0L)) {
							reverseTransfer = true;
						} else {
							status = "success";
						}
					} else if ("SECURITIES".equals(tranDetail.getApplication().getName())) {
						thirdServ.setBaseUrl(thirdpartyapiBaseUrlSec);
						secLoginRes = thirdServ.securityLogin();

						if (secLoginRes.getStatusId() != null && secLoginRes.getStatusId().equals(0L)) {
							secFundAcctRes = thirdServ.fundAccountSecurities(secLoginRes.getOutValue(), CRAccountSub,
									tranDetail.getId().toString(), tranDetail.getAmount().toString(),
									tranDetail.getRemark());
							// to get the description
							tranDetail.setResponseCode(secFundAcctRes.getStatusId().toString());
							tranDetail.setResponseDescription(processInterface
									.findByStatusId(secFundAcctRes.getStatusId().toString()).getResponseDescription());
						}
						if (secLoginRes.getStatusId() != null && !secFundAcctRes.getStatusId().equals(0L)) {
							reverseTransfer = true;
						} else {
							status = "success";
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					reverseTransfer = true;
					System.out.println("there is a failure internal");
				}
			}
		} catch (Exception ex) {
			reverseTransfer = true;
			System.out.println("there is a failure external");
			ex.printStackTrace();
		}

		if (reverseTransfer) {
			System.out.println("reverseTransfer==" + reverseTransfer);
			localRes = processInterface.reverseLocalTransfer(uniqueIdentifier);
			if ("000".equals(localRes.getResponseCode())) {
				tranDetail.setReverseTransactionFlag("Y");
			}
		}
		if ("success".equals(status)) {
			HtmlTemplate template = processInterface.findByTemplateName("topup");
			String userName = "Team";
			String htmlTemplate = template.getTemplate();
			// .replace("var1", userName.concat("
			// ").concat(dbUser.getCustomer().getLastName()))
			// .replace("var2", now.getMonth().name())
			// .replace("var3", String.valueOf(now.getDayOfMonth()))
			// .replace("var4", String.valueOf(now.getYear()))
			// .replace("var5",
			// now.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

			MailDetail mailDetail = new MailDetail();
			mailDetail.setSender(sender);
			mailDetail.setReciever(notificationEmail);
			mailDetail.setMailSender(javaMailSender);
			mailDetail.setSubject(topupMailSubject);
			mailDetail.setHtmlEnabled(true);
			mailDetail.setEmailBody(htmlTemplate);
			processInterface.sendMail(mailDetail);
		}
		processInterface.saveTopupTransactionDetail(tranDetail);
		model.addAttribute("status", status);
		model.addAttribute("amount", "₦ ".concat(processInterface.formatAmount(tranDetail.getAmount())));
		model.addAttribute("transactionDetail", tranDetail);
		model.addAttribute("id", tranDetail.getId());
		return "subsidiarytransfersuccess";

	}

	@PostMapping(value = "/saveTransactionDetail")
	public String saveTransactionDetail(Model model, TransactionDetail transactionDetail) {

		// System.out.println("my id ==" + id);
		// Application app = applicationRepo.getOne(id);
		try {
			String retStr = this.validateTransactionDetail(transactionDetail);
			if (!Strings.isNullOrEmpty(retStr)) {
				System.out.println("transactionDetail.getRemark() ==== " + transactionDetail.getRemark());
				transactionDetail.setNewBenCheck(transactionDetail.getNewBen());
				transactionDetail.setExistBenCheck(transactionDetail.getExistingBen());
				transactionDetail.setErrorMessage(retStr);
				CustomerBeneficiary customerBeneficiary = new CustomerBeneficiary();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
				customerBeneficiary.setBeneficiaryAccountName("Default");
				customerBeneficiary.setBeneficiaryAccountNumber("0000000000");
				customerBeneficiary.setDisplayBeneficiary("Select Beneficiary");
				customerBeneficiary.setId(0L);
				List<CustomerBeneficiary> externalbeneficiaries = new ArrayList<CustomerBeneficiary>();
				externalbeneficiaries.add(customerBeneficiary);
				List<CustomerBeneficiary> internalbeneficiaries = new ArrayList<CustomerBeneficiary>();
				internalbeneficiaries.add(customerBeneficiary);
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes();
				HttpSession session = attr.getRequest().getSession(true);
				CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
						.getAttribute("dashboardDetail");
				Customer customer = dashboardDetails.getUser().getCustomer();
				List<CustomerBankAccount> currentAccountList = dashboardDetails != null
						? dashboardDetails.getCurrentAccountList() : new ArrayList<CustomerBankAccount>();
				currentAccountList.forEach(
						a -> a.setBalanceAmountStr("₦".concat(processInterface.formatAmount(new ThirdPartyService()
								.getBankCustomerAccountBalance(a.getAccountNumber(), LocalDate.now().format(formatter))
								.getClrBalanceAmount()))));
				currentAccountList.forEach(a -> a.setDisplayAccountNumber(
						"Current | ".concat(a.getAccountNumber()).concat(" | ").concat(a.getBalanceAmountStr())));
				List<CustomerBeneficiary> beneficiaries = processInterface.findBeneficiariesByCustomer(customer);
				System.out.println("beneficiaries.size() ==" + beneficiaries.size());
				if (beneficiaries != null) {
					List<CustomerBeneficiary> extBen = beneficiaries.stream()
							.filter(a -> a.getTransactionType().equals(
									processInterface.findByTransactionTypeById(Long.valueOf(interbankTranTypeId))))
							.collect(Collectors.toList());
					extBen.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryAccountName().concat(" | ").concat(a.getBeneficiaryAccountNumber())));
					externalbeneficiaries.addAll(extBen);
					List<CustomerBeneficiary> intBen = beneficiaries.stream()
							.filter(a -> a.getTransactionType().equals(
									processInterface.findByTransactionTypeById(Long.valueOf(internalTranTypeId))))
							.collect(Collectors.toList());
					intBen.forEach(a -> a.setDisplayBeneficiary(
							a.getBeneficiaryAccountName().concat(" | ").concat(a.getBeneficiaryAccountNumber())));
					internalbeneficiaries.addAll(intBen);

				}
				System.out.println("my id ==" + 1);
				Application app = applicationRepo.getOne(1L);
				TransactionType defaultTranType = new TransactionType();
				defaultTranType.setTransactionType("Select Transaction Type");
				CustomerBankAccount cba = new CustomerBankAccount();
				cba.setBalanceAmountStr("Select Current Account");
				cba.setDisplayAccountNumber("Select Current Account");
				List<CustomerBankAccount> listAccounts = new ArrayList<CustomerBankAccount>();
				listAccounts.add(cba);
				List<TransactionType> listTranTypes = new ArrayList<TransactionType>();
				listTranTypes.add(defaultTranType);
				listTranTypes.addAll(app.getTransactionTypes());
				System.out.println("my app.getTransactionTypes().size() ==" + app.getTransactionTypes().size());
				model.addAttribute("transactionTypes", listTranTypes);
				model.addAttribute("transactionDetail", transactionDetail);
				model.addAttribute("externalbeneficiaries", externalbeneficiaries);
				model.addAttribute("internalbeneficiaries", internalbeneficiaries);
				FinancialInstitution dummyInstitute = new FinancialInstitution();
				dummyInstitute.setInstitutionCode("000000");
				dummyInstitute.setInstitutionName("Select Bank");
				List<FinancialInstitution> allInstitutes = new ArrayList<FinancialInstitution>();
				allInstitutes.add(dummyInstitute);
				List<FinancialInstitution> institutes = processInterface.financialInstitutions();// .sort(Comparator.comparing(FinancialInstitution::getInstitutionName));
				Collections.sort(institutes, Comparator.comparing(FinancialInstitution::getInstitutionName));
				allInstitutes.addAll(institutes);
				model.addAttribute("financialInstitutions", allInstitutes);
				listAccounts.addAll(currentAccountList);
				model.addAttribute("currentAccountList", listAccounts);
				return "transfer";
			}

			System.out
					.println("transactionDetail.getTransactionTypeId() ==" + transactionDetail.getTransactionTypeId());
			TransactionType transactionType = processInterface
					.findByTransactionTypeById(transactionDetail.getTransactionTypeId());
			transactionDetail.setTransactionType(transactionType);
			FinancialInstitution financialInstitution = new FinancialInstitution();
			if (transactionDetail.getTransactionTypeId().equals(3L)) {
				if (!"000000".equals(transactionDetail.getFinancialInstitutionCode())) {
					financialInstitution = processInterface
							.findFinancialInstitutionById(transactionDetail.getFinancialInstitutionCode());
				}
			} else {
				financialInstitution = processInterface.findFinancialInstitutionById(bankCode);
			}
			System.out
					.println("transactionDetail.getTransactionTypeId() 1==" + transactionDetail.getTransactionTypeId());

			transactionDetail.setFinancialInstitution(financialInstitution);
			transactionDetail.setRequestDate(LocalDateTime.now());

			User user = processInterface.findUserById(transactionDetail.getUserId());
			System.out.println("transactionDetail.getUserId() 2==" + transactionDetail.getUserId());
			transactionDetail.setUser(user);

			if (Long.valueOf(ownAccountTranTypeId).equals(transactionDetail.getTransactionTypeId())) {
				transactionDetail.setDestinationAccountNumber(transactionDetail.getOwnAccountNumber());
			}
			System.out.println(
					"transactionDetail.getExternalBeneficiaryId() ==" + transactionDetail.getExternalBeneficiaryId());
			System.out.println(
					"transactionDetail.getInternalBeneficiaryId() ==" + transactionDetail.getInternalBeneficiaryId());

			if (transactionDetail.getExternalBeneficiaryId() != null
					&& transactionDetail.getExternalBeneficiaryId() > 0L) {
				CustomerBeneficiary customerBeneficiaryData = processInterface
						.findCustomerBeneficiaryById(transactionDetail.getExternalBeneficiaryId());
				transactionDetail.setCustomerBeneficiary(customerBeneficiaryData);
				transactionDetail.setFinancialInstitution(customerBeneficiaryData.getFinancialInstitution());
			}

			if (transactionDetail.getInternalBeneficiaryId() != null
					&& transactionDetail.getInternalBeneficiaryId() > 0L) {
				CustomerBeneficiary customerBeneficiaryData = processInterface
						.findCustomerBeneficiaryById(transactionDetail.getInternalBeneficiaryId());
				transactionDetail.setCustomerBeneficiary(customerBeneficiaryData);
				transactionDetail.setFinancialInstitution(customerBeneficiaryData.getFinancialInstitution());
			}
			if (!Strings.isNullOrEmpty(transactionDetail.getAddBeneficiary())) {
				CustomerBeneficiary customerBeneficiary = new CustomerBeneficiary();

				customerBeneficiary.setCustomer(user.getCustomer());
				customerBeneficiary.setFinancialInstitution(financialInstitution);
				customerBeneficiary.setTransactionType(transactionType);
				customerBeneficiary.setBeneficiaryAccountName(transactionDetail.getDestinationAccountName());
				customerBeneficiary.setBeneficiaryAccountNumber(transactionDetail.getDestinationAccountNumber());
				customerBeneficiary.setBeneficiaryBvn(transactionDetail.getDestinationAccountBvn());
				customerBeneficiary.setNameEnquirySessionId(transactionDetail.getNameEnquirySessionId());
				customerBeneficiary.setBeneficiaryKyc(transactionDetail.getBeneficiaryKyc());

				List<CustomerBeneficiary> csbExist = processInterface
						.findByCustomerAndDeleteFlgAndBeneficiaryAccountNumber(user.getCustomer(), "N",
								transactionDetail.getDestinationAccountNumber());

				if (csbExist.isEmpty()) {

					transactionDetail.setBeneficiaryExist("Y");
					// model.addAttribute("transactionDetail",
					// transactionDetail);
					// return "transfer";

					processInterface.save(customerBeneficiary);
				}
			}
			processInterface.save(transactionDetail);
			transactionDetail.setFormattedAmount(processInterface.formatAmount(transactionDetail.getAmount()));
			// model.addAttribute("transactionTypes",
			// app.getTransactionTypes());
			model.addAttribute("transactionDetail", transactionDetail);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "transfersummary";
	}

	@PostMapping(value = "/postTransfer")
	public String performTransfer(Model model, TransactionDetail transactionDetail) {

		TransactionDetail tranDetail = processInterface.findTransactionDetailById(transactionDetail.getId());
		tranDetail.setFormattedAmount(processInterface.formatAmount(tranDetail.getAmount()));
		if (tranDetail.getCustomerBeneficiary() != null) {
			tranDetail.setDestinationAccountName(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountName());
			tranDetail.setDestinationAccountNumber(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountNumber());
		}
		User user = processInterface.findUserById(transactionDetail.getUserId());
		EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
		TokenAuthDTO arg0 = new TokenAuthDTO();
		arg0.setAppCode(entrustAppCode);
		arg0.setAppDesc(entrustAppDesc);
		arg0.setGroup(user.getCustomer().getCustomerType().getEntrustGroup());
		arg0.setTokenPin(transactionDetail.getToken());
		arg0.setUserName(user.getUserId());
		AuthResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performTokenAuth(arg0);

		if (res != null) {
			if (res.getRespCode() == 1) {
				System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
				System.out.println("token res.getRespCode() ==" + res.getRespCode());
				System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
				tranDetail.setTokenRespMessage(entrustTokenAuthFailed);
				model.addAttribute("transactionDetail", tranDetail);
				return "transfersummary";
			} else {

			}
		} else {
			System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
			System.out.println("token res.getRespCode() ==" + res.getRespCode());
			System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
			tranDetail.setTokenRespMessage(entrustTokenAuthFailed);
			return "transfersummary";
		}
		String status = "";
		ResponseData resp = new ResponseData();
		try {
			System.out.println("postTransfer ==" + transactionDetail);
			// Application app = applicationRepo.getOne(id);
			System.out.println("my app.getTransactionTypes().size() ==");
			// model.addAttribute("transactionTypes",
			// app.getTransactionTypes());
			// model.addAttribute("transactionDetail", tranDetail);
			if (tranDetail.getTransactionType().getId().equals(Long.valueOf(interbankTranTypeId))) {
				tranDetail.setFtSingleCreditSessionId(processInterface.getSessionId(bankCode));
				FTSingleCreditRequest req = new FTSingleCreditRequest();
				req.setAmount(tranDetail.getAmount().toString());
				req.setBeneficiaryAccountName(tranDetail.getDestinationAccountName());
				req.setBeneficiaryAccountNumber(tranDetail.getDestinationAccountNumber());
				req.setBeneficiaryBankVerificationNumber(tranDetail.getDestinationAccountBvn());
				req.setNameEnquiryRef(tranDetail.getNameEnquirySessionId());
				req.setBeneficiaryKYCLevel(tranDetail.getBeneficiaryKyc());
				if (tranDetail.getCustomerBeneficiary() != null && tranDetail.getCustomerBeneficiary().getId() > 0) {
					req.setBeneficiaryAccountName(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountName());
					req.setBeneficiaryAccountNumber(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountNumber());
					req.setBeneficiaryBankVerificationNumber(tranDetail.getCustomerBeneficiary().getBeneficiaryBvn());
					req.setNameEnquiryRef(tranDetail.getCustomerBeneficiary().getNameEnquirySessionId());
					req.setBeneficiaryKYCLevel(tranDetail.getCustomerBeneficiary().getBeneficiaryKyc());
				}
				req.setChannelCode("1");
				req.setDestinationInstitutionCode(tranDetail.getFinancialInstitution().getInstitutionCode());
				req.setNarration(tranDetail.getRemark());
				req.setOriginatorAccountName(tranDetail.getUser().getCustomer().getName());
				req.setOriginatorAccountNumber(tranDetail.getSourceAccountNumber());
				req.setOriginatorBankVerificationNumber(tranDetail.getUser().getCustomer().getBvn());
				req.setOriginatorKYCLevel("1");
				req.setSessionID(tranDetail.getFtSingleCreditSessionId());
				req.setTransactionChannel("NIP");
				req.setPaymentReference(tranDetail.getRemark());
				resp = processInterface.interbankTransfer(req);
				if ("00".equals(resp.getResponseCode()))
					status = "success";
			} else if (tranDetail.getTransactionType().getId().equals(Long.valueOf(internalTranTypeId))
					|| tranDetail.getTransactionType().getId().equals(Long.valueOf(ownAccountTranTypeId))) {
				RequestData req = new RequestData();
				DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
				req.setDebitAccountNumber(tranDetail.getSourceAccountNumber());
				req.setCreditAccountNumber(tranDetail.getDestinationAccountNumber());
				if (tranDetail.getCustomerBeneficiary() != null && tranDetail.getCustomerBeneficiary().getId() > 0) {
					req.setCreditAccountNumber(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountNumber());
				}
				req.setTranAmount(tranDetail.getAmount().toString());
				req.setNaration(tranDetail.getRemark());
				String uinqueIdentifier = LocalDateTime.now().format(format);
				tranDetail.setUniqueIdentifier(uinqueIdentifier);
				req.setUniqueIdentifier(uinqueIdentifier);
				resp = processInterface.localTransfer(req);
				if ("000".equals(resp.getResponseCode()))
					status = "success";
			} else {
			}

			tranDetail.setResponseCode(resp.getResponseCode());
			tranDetail.setResponseDescription(resp.getResponseDescription());
			processInterface.save(tranDetail);
			String beneficiaryNameAccountNumber = tranDetail.getDestinationAccountName() == null
					? tranDetail.getCustomerBeneficiary().getBeneficiaryAccountName().concat(" ")
							.concat(tranDetail.getCustomerBeneficiary().getBeneficiaryAccountNumber())
					: tranDetail.getDestinationAccountName().concat(" ")
							.concat(tranDetail.getDestinationAccountNumber());
			model.addAttribute("beneficiaryNameAccountNumber", beneficiaryNameAccountNumber);
			model.addAttribute("amount", "₦ ".concat(processInterface.formatAmount(tranDetail.getAmount())));
			model.addAttribute("transactionDetail", tranDetail);
			model.addAttribute("status", status);
			model.addAttribute("id", tranDetail.getId());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "transfersuccess";
	}

	private String validateTransactionDetail(TransactionDetail tranDetail) {
		String returnStr = "";
		System.out.println("tranDetail.getAccountBalance()>>>>" + tranDetail.getAccountBalance());
		System.out.println(
				"tranDetail.getAccountBalance().substring(2)>>>>" + tranDetail.getAccountBalance().substring(2));
		try {
			DecimalFormat decimalFormat = new DecimalFormat("#,###.##");
			decimalFormat.setParseBigDecimal(true);
			BigDecimal dailyBalance = (BigDecimal) decimalFormat
					.parse(tranDetail.getCustomerDailyTransactionAccountBalance());
			String accountBalanceStr = tranDetail.getAccountBalance().substring(2);
			System.out.println("accountBalanceStr>>>>" + accountBalanceStr);
			BigDecimal accountBalance = (BigDecimal) decimalFormat.parse(accountBalanceStr);
			System.out.println("dailyBalance>>>>" + dailyBalance);
			System.out.println("accountBalance>>>>" + accountBalance);
			if (tranDetail.getAmount().compareTo(dailyBalance) > 0) {
				returnStr = "Transaction Amount cannot be greater than the Daily balance";
			}

			if (tranDetail.getAmount().compareTo(accountBalance) > 0) {
				returnStr = "Transaction Amount cannot be greater than the Account balance";
			}
			if (tranDetail.getTransactionTypeId().equals(1L)) {
				if (Strings.isNullOrEmpty(tranDetail.getOwnAccountNumber())) {
					returnStr = "Own Destnation Account is mandatory";
				} else {
					if (tranDetail.getOwnAccountNumber().equals(tranDetail.getSourceAccountNumber())) {
						returnStr = "Own Destnation Account and Source Account cannot be the same";
					}
				}
			}

			if (tranDetail.getTransactionTypeId().equals(2L)) {
				System.out.println("tranDetail.getExistingBen()>>>>" + tranDetail.getExistingBen());
				if (!Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
					System.out.println(
							"tranDetail.getInternalBeneficiaryId()>>>>" + tranDetail.getInternalBeneficiaryId());
					if (tranDetail.getInternalBeneficiaryId().equals(0L)) {
						returnStr = "Select Existing Internal Beneficiary";
					}
				} else if (!Strings.isNullOrEmpty(tranDetail.getNewBen())) {
					if (Strings.isNullOrEmpty(tranDetail.getDestinationAccountNumber())
							|| Strings.isNullOrEmpty(tranDetail.getDestinationAccountName())
							|| tranDetail.getFinancialInstitutionCode() == null) {
						returnStr = "Destination Account Number and Name are mandatory";
					}

				}
			}

			if (tranDetail.getTransactionTypeId().equals(3L)) {
				if (!Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
					if (tranDetail.getExternalBeneficiaryId().equals(0L)) {
						returnStr = "Select Existing External Beneficiary";
					}
				} else if (!Strings.isNullOrEmpty(tranDetail.getNewBen())) {
					if (Strings.isNullOrEmpty(tranDetail.getDestinationAccountNumber())
							|| Strings.isNullOrEmpty(tranDetail.getDestinationAccountName())
							|| tranDetail.getFinancialInstitutionCode() == null) {
						returnStr = "Destination Account Number and Name are mandatory";
					}

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return returnStr;
	}

	private String validateTransactionDetail(TopupTransactionDetail tranDetail) {
		String returnStr = "";
		System.out.println("tranDetail.getOwnAmCustomerAid()====" + tranDetail.getOwnAmCustomerAid());
		System.out.println("tranDetail.getApplicationId()====" + tranDetail.getApplicationId());
		System.out.println("tranDetail.getTopUpTypeId()====" + tranDetail.getTopUpTypeId());
		if (tranDetail.getApplicationId().equals(2L)) {
			if (tranDetail.getFundTypeId() == null) {
				returnStr = "Fund Type is mandatory";
			}
			if (!Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getTopUpTypeId().equals(2L) && tranDetail.getAmBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer is mandatory";
				}
			} else {
				if (!Strings.isNullOrEmpty(tranDetail.getNewBen())) {
					if (Strings.isNullOrEmpty(tranDetail.getAmCustomerAid())
							|| Strings.isNullOrEmpty(tranDetail.getAmCustomerName())) {
						returnStr = "Beneficiary Customer Detail is mandatory";
					}
				}
			}
			if (tranDetail.getTopUpTypeId().equals(1L) && Strings.isNullOrEmpty(tranDetail.getOwnAmCustomerAid())) {
				returnStr = "Beneficiary Customer ID is mandatory";
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getAmBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer ID is mandatory";
				}
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getNewBen())) {
				if (Strings.isNullOrEmpty(tranDetail.getAmCustomerAid())
						|| Strings.isNullOrEmpty(tranDetail.getAmCustomerName())) {
					returnStr = "Beneficiary Customer ID and Name is mandatory";
				}
			}
		}

		if (tranDetail.getApplicationId().equals(3L)) {
			if (!Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getTopUpTypeId().equals(2L) && tranDetail.getSecBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer is mandatory";
				}
			} else {
				if (!Strings.isNullOrEmpty(tranDetail.getNewBen())) {
					if (Strings.isNullOrEmpty(tranDetail.getSecCustomerAid())
							|| Strings.isNullOrEmpty(tranDetail.getSecCustomerName())) {
						returnStr = "Beneficiary Customer Detail is mandatory";
					}
				}
			}
			if (tranDetail.getTopUpTypeId().equals(1L) && Strings.isNullOrEmpty(tranDetail.getOwnSecCustomerAid())) {
				returnStr = "Beneficiary Customer ID is mandatory";
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getSecBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer ID is mandatory";
				}
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getNewBen())) {
				if (Strings.isNullOrEmpty(tranDetail.getSecCustomerAid())
						|| Strings.isNullOrEmpty(tranDetail.getSecCustomerName())) {
					returnStr = "Beneficiary Customer ID and Name is mandatory";
				}
			}
		}

		if (tranDetail.getApplicationId().equals(4L)) {
			if (!Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getTrustBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer is mandatory";
				}
			} else {
				if (!Strings.isNullOrEmpty(tranDetail.getNewBen())) {
					if (Strings.isNullOrEmpty(tranDetail.getTrustCustomerAid())
							|| Strings.isNullOrEmpty(tranDetail.getTrustCustomerName())) {
						returnStr = "Beneficiary Customer Detail is mandatory";
					}
				}
			}
			if (tranDetail.getTopUpTypeId().equals(1L) && Strings.isNullOrEmpty(tranDetail.getOwnTrustCustomerAid())) {
				returnStr = "Beneficiary Customer ID is mandatory";
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getExistingBen())) {
				if (tranDetail.getTrustBeneficiaryId() == null) {
					returnStr = "Beneficiary Customer ID is mandatory";
				}
			}
			if (tranDetail.getTopUpTypeId().equals(2L) && !Strings.isNullOrEmpty(tranDetail.getNewBen())) {
				if (Strings.isNullOrEmpty(tranDetail.getTrustCustomerAid())
						|| Strings.isNullOrEmpty(tranDetail.getTrustCustomerName())) {
					returnStr = "Beneficiary Customer ID and Name is mandatory";
				}
			}
		}

		if (tranDetail.getTopUpSrcId().equals(1L)) {
			if (Strings.isNullOrEmpty(tranDetail.getOwnAccountNumber())) {
				returnStr = "Source Account is mandatory";
			} else {
			}

		}

		return returnStr;
	}

	@GetMapping(value = "/downloadTransactionReciept")
	public String downloadTransactionReciept(Model model, Long id, HttpServletResponse response) {
		try {
			TransactionDetail transactionDetail = processInterface.findTransactionDetailById(id);
			String returnFile = reportGeneratorInterface.generateTransactionReciept(transactionDetail);

			if (returnFile != null) {
				Path pdfPath = Paths.get(returnFile);
				byte[] dataObj = Files.readAllBytes(pdfPath);
				File file = new File(returnFile);
				streamReport(response, dataObj, file.getName());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@GetMapping(value = "/downloadTopupTransactionReciept")
	public String downloadTopupTransactionReciept(Model model, @RequestParam Long id, HttpServletResponse response) {
		try {
			TopupTransactionDetail transactionDetail = processInterface.findTopupTransactionDetailById(id);
			String returnFile = reportGeneratorInterface.generateTopupReciept(transactionDetail);

			if (returnFile != null) {
				Path pdfPath = Paths.get(returnFile);
				byte[] dataObj = Files.readAllBytes(pdfPath);
				File file = new File(returnFile);
				streamReport(response, dataObj, file.getName());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	protected void streamReport(HttpServletResponse response, byte[] data, String name) throws IOException {

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=" + name);
		response.setContentLength(data.length);

		response.getOutputStream().write(data);
		response.getOutputStream().flush();
	}
}
