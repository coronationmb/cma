package com.cmb.consolidatedapps.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.ReportGeneratorInterface;
import com.cmb.consolidatedapps.model.CustomerBankAccountBalance;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.FITransactionDetail;
import com.cmb.consolidatedapps.model.StatementData;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.model.TransactionHistoryData;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.service.ThirdPartyService;
import com.cmb.thirdpartyapi.model.AMFundAccountStatementRows;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.SecurityTransaction;
import com.google.common.base.Strings;
import com.infosys.ci.fiusb.webservice.FIUsbWebServiceService;

@Controller
public class DashboardControllers {

	@Value("${thirdpartyapi.baseurl.assetmgt}")
	String thirdpartyapiBaseUrlAssetMgt;

	@Value("${thirdpartyapi.baseurl.localapi}")
	String thirdpartyapiBaseUrlLocal;

	@Value("${thirdpartyapi.baseurl.securities}")
	String thirdpartyapiBaseUrlSec;

	ProcessInterfaces processInterface;
	ReportGeneratorInterface reportGenInt;

	public DashboardControllers(ProcessInterfaces processInterface, ReportGeneratorInterface reportGenInt) {

		this.processInterface = processInterface;
		this.reportGenInt = reportGenInt;
	}

	@GetMapping(value = "/dashboard")
	public String dashboard(Model model) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		User user = (User) session.getAttribute("user");

		CustomerDashboardDetail dashboardDetails = processInterface.dashboardDetails(user);

		model.addAttribute("customerDashboard", dashboardDetails);
		model.addAttribute("dashboardDetail", null);
		model.addAttribute("dashboardDetail", dashboardDetails);
		model.addAttribute("imageuser", processInterface.findUserById(user.getId()));

		return "index";
	}

	@GetMapping(value = "/statement")
	public String statement(Model model) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		User user = (User) session.getAttribute("user");

		CustomerDashboardDetail dashboardDetails = processInterface.dashboardDetails(user);

		model.addAttribute("customerDashboard", dashboardDetails);
		model.addAttribute("dashboardDetail", null);
		model.addAttribute("dashboardDetail", dashboardDetails);
		model.addAttribute("imageuser", processInterface.findUserById(user.getId()));
		model.addAttribute("statement", new StatementData());

		return "account_statement";
	}

	@PostMapping(value = "/generateStatement")
	public String generateStatement(Model model, StatementData data, HttpServletResponse response) {
		try {
			// set to test
			data.setFileType("P");
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			User user = (User) session.getAttribute("user");

			CustomerDashboardDetail dashboardDetails = processInterface.dashboardDetails(user);

			model.addAttribute("customerDashboard", dashboardDetails);
			model.addAttribute("dashboardDetail", null);
			model.addAttribute("dashboardDetail", dashboardDetails);
			model.addAttribute("imageuser", processInterface.findUserById(user.getId()));
			String returnFile = reportGenInt.generateStatementPdf(data);
			model.addAttribute("statement", data);
			if (returnFile != null) {
				Path pdfPath = Paths.get(returnFile);
				byte[] dataObj = Files.readAllBytes(pdfPath);
				File file = new File(returnFile);
				streamReport(response, dataObj, file.getName());

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	protected void streamReport(HttpServletResponse response, byte[] data, String name) throws IOException {

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=" + name);
		response.setContentLength(data.length);

		response.getOutputStream().write(data);
		response.getOutputStream().flush();
	}

	@GetMapping(value = "/transactionHistory")
	public String transactionHistory(Model model) {
		AssetManagementResponseData amLoginRes = new AssetManagementResponseData();
		AssetManagementResponseData amFundAcctStmntRes = new AssetManagementResponseData();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		TransactionHistoryData data = new TransactionHistoryData();
		data.setCurrentAccountList(dashboardDetails.getCurrentAccountList());
		data.setAmCustAIDList(dashboardDetails.getAmCustAIDList());
		data.setSecCustAIDList(dashboardDetails.getSecCustAIDList());
		ThirdPartyService tps = new ThirdPartyService();
		List<SecurityTransaction> secRows = new ArrayList<SecurityTransaction>();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		DateTimeFormatter displayFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter txtDisplayFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		DateTimeFormatter dbDateFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		LocalDateTime now = LocalDateTime.now();
		String endDate = now.toString();
		String fromDate = now.minusMonths(1L).toString();
		// CustomerDashboardDetail dashboardDetails =
		// processInterface.dashboardDetails(user);
		if (Long.valueOf("1").equals(dashboardDetails.getApp1())) {
			String accountNumber = dashboardDetails.getCurrentAccountList().get(0).getAccountNumber();
			data.setBankStmtAccount(accountNumber);
			CustomerBankAccountBalance cbab = tps.getBankCustomerAccountBalance(accountNumber,
					now.minusMonths(1L).format(dbDateFormatter));
			data.setOpeningBalance("N ".concat(processInterface.formatAmount(cbab.getOpeningBalance())));
			FIUsbWebServiceService svc = new FIUsbWebServiceService();

			String reqUuid = "Req-".concat(dashboardDetails.getUser().getId().toString()).concat("-")
					.concat(now.format(formatter));
			List<FITransactionDetail> custBankTranList = new ArrayList<FITransactionDetail>();
			FITransactionDetail lastTranDetail = new FITransactionDetail();
			int i = 0;
			do {
				System.out.println("lastTranDetail.getHasMoreData()==" + lastTranDetail.getHasMoreData());
				List<FITransactionDetail> bankTranList = svc.getAccountStatement(reqUuid.concat(String.valueOf(i)),
						now.toString(), data.getBankStmtAccount(), fromDate, endDate, lastTranDetail);
				custBankTranList.addAll(bankTranList);
				lastTranDetail = bankTranList.get(bankTranList.size() - 1);
				i++;
			} while ("Y".equals(lastTranDetail.getHasMoreData()));
			System.out.println("bankTranList.size()==" + custBankTranList.size());
			custBankTranList.forEach(a -> a.setPstdDateDt(LocalDateTime.parse(a.getPstdDate())));
			custBankTranList.forEach(a -> a.setPstdDate(LocalDateTime.parse(a.getPstdDate()).format(displayFormatter)));
			custBankTranList
					.forEach(a -> a.setValueDate(LocalDateTime.parse(a.getValueDate()).format(displayFormatter)));
			// Comparator<FITransactionDetail> pstdDateComparator = (o1, o2) ->
			// o1.getPstdDateDt()
			// .compareTo(o2.getPstdDateDt());
			// bankTranList.sort(pstdDateComparator.reversed());
			custBankTranList.sort((FITransactionDetail s1, FITransactionDetail s2) -> s1.getPstdDateDt()
					.compareTo(s2.getPstdDateDt()));
			if (!custBankTranList.isEmpty()) {
				custBankTranList.forEach(a -> a.setCredit(processInterface.formatAmount(new BigDecimal(a.getCredit()))));
				custBankTranList.forEach(a -> a.setDebit(processInterface.formatAmount(new BigDecimal(a.getDebit()))));
				custBankTranList.forEach(a -> a.setAmountValue(processInterface.formatAmount(new BigDecimal(a.getAmountValue()))));
			}

			data.setBankTransactionsDetail(custBankTranList);
		}
		data.setBankStmtFromDate(now.minusMonths(1L).format(txtDisplayFormatter));
		data.setBankStmtToDate(now.format(txtDisplayFormatter));

		ThirdPartyService thirdServ = new ThirdPartyService();
		if (Long.valueOf("2").equals(dashboardDetails.getApp2())) {
			List<SubsidiaryCustomerDetail> scdList = dashboardDetails.getAmCustAIDList();
			scdList.stream().filter(a -> a.getFundCode() != null).collect(Collectors.toList());
			if (scdList.size() > 0) {
				List<SubsidiaryCustomerDetail> validScdList = dashboardDetails.getAmCustAIDList().stream()
						.filter(a -> !Strings.isNullOrEmpty(a.getFundCode())).collect(Collectors.toList());
				SubsidiaryCustomerDetail scd = validScdList.isEmpty() ? new SubsidiaryCustomerDetail()
						: validScdList.get(0);
				data.setAmCustAID("Account ID = ".concat(scd.getCustomerAid() == null ? "" : scd.getCustomerAid()));
				thirdServ.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
				amLoginRes = thirdServ.assetMngmtLogin();

				// List<String> val =
				// Arrays.asList(data.getAmCustAID().split("#"));

				if (amLoginRes.getStatusId() != null && amLoginRes.getStatusId().equals(0L)) {
					amFundAcctStmntRes = thirdServ.getCustomerFundAccountStatement(amLoginRes.getOutValue(),
							scd.getCustomerAid(), scd.getFundCode());
					if (amFundAcctStmntRes.getStatusId() != null && amFundAcctStmntRes.getStatusId().equals(0L)) {
						List<AMFundAccountStatementRows> rows = amFundAcctStmntRes.getDataTable()
								.getAmfundAccountStatementRows();
						rows.forEach(a -> a.setCredit(processInterface.formatAmount(new BigDecimal(a.getCredit()))));
						rows.forEach(a -> a.setDebit(processInterface.formatAmount(new BigDecimal(a.getDebit()))));
						Comparator<AMFundAccountStatementRows> valueDateComparator = (o1, o2) -> o1.getValueDate()
								.compareTo(o2.getValueDate());
						rows.sort(valueDateComparator.reversed());
						data.setAmTransactionsDetail(rows);
					}
				}
				// System.out.println("data.getAmTransactionsDetail().size()=="
				// + data.getAmTransactionsDetail().size());
			}

		}

		if (Long.valueOf("3").equals(dashboardDetails.getApp3())) {
			data.setSecStmtFromDate(now.minusMonths(1L).format(txtDisplayFormatter));
			data.setSecStmtToDate(now.format(txtDisplayFormatter));
			thirdServ.setBaseUrl(null);
			thirdServ.setBaseUrl(thirdpartyapiBaseUrlSec);
			if (dashboardDetails.getSecCustAIDList().size() > 0) {
				List<SubsidiaryCustomerDetail> validScdList = dashboardDetails.getSecCustAIDList().stream()
						.filter(a -> !Strings.isNullOrEmpty(a.getFundCode())).collect(Collectors.toList());
				SubsidiaryCustomerDetail scdLocal = validScdList.isEmpty() ? new SubsidiaryCustomerDetail()
						: validScdList.get(0);
				data.setSecCustAID(
						"Account ID = ".concat(scdLocal.getCustomerAid() == null ? "" : scdLocal.getCustomerAid()));
				for (SubsidiaryCustomerDetail scd : dashboardDetails.getSecCustAIDList()) {
					secRows = thirdServ
							.getSecurityTransactionsData(scd.getCustomerAid() == null ? "" : scd.getCustomerAid(),
									now.minusMonths(1L).format(txtDisplayFormatter), now.format(txtDisplayFormatter))
							.getDataTable().getSecurityTransactions();
				}
				System.out.println("secRows.size()===" + secRows.size());

				data.setSecTransactionsDetail(secRows);
			}
		}

		if (Long.valueOf("4").equals(dashboardDetails.getApp4())) {

		}

		model.addAttribute("transactionHist", data);
		// model.addAttribute("dashboardDetail", dashboardDetails);

		return "transactionhistory";
	}

	@PostMapping(value = "/transactionHistory")
	public String getTransactionHistory(Model model, TransactionHistoryData data) {
		AssetManagementResponseData amLoginRes = new AssetManagementResponseData();
		AssetManagementResponseData amFundAcctStmntRes = new AssetManagementResponseData();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("customerDashboard");

		// CustomerDashboardDetail dashboardDetails =
		// processInterface.dashboardDetails(user);
		if (dashboardDetails.getApp1().equals(1L)) {
			FIUsbWebServiceService svc = new FIUsbWebServiceService();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			LocalDateTime now = LocalDateTime.now();
			String reqUuid = "Req-".concat(dashboardDetails.getUser().getId().toString()).concat("-")
					.concat(now.format(formatter));
			List<FITransactionDetail> custBankTranList = new ArrayList<FITransactionDetail>();
			FITransactionDetail lastTranDetail = new FITransactionDetail();
			int i = 0;
			do {
				List<FITransactionDetail> bankTranList = svc.getAccountStatement(reqUuid.concat(String.valueOf(i)),
						now.toString(), data.getBankStmtAccount(), data.getBankStmtFromDate(), data.getBankStmtToDate(),
						lastTranDetail);
				custBankTranList.addAll(bankTranList);
				lastTranDetail = bankTranList.get(bankTranList.size() - 1);
				i++;
			} while ("Y".equals(lastTranDetail.getHasMoreData()));
			data.setBankTransactionsDetail(custBankTranList);
		}

		ThirdPartyService thirdServ = new ThirdPartyService();
		if (dashboardDetails.getApp2().equals(2L)) {
			thirdServ.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
			amLoginRes = thirdServ.assetMngmtLogin();

			List<String> val = Arrays.asList(data.getAmCustAID().split("#"));

			if (amLoginRes.getStatusId() != null && amLoginRes.getStatusId().equals(0L)) {
				amFundAcctStmntRes = thirdServ.getCustomerFundAccountStatement(amLoginRes.getOutValue(), val.get(0),
						val.get(1));
				if (amFundAcctStmntRes.getStatusId() != null && amFundAcctStmntRes.getStatusId().equals(0L)) {
					data.setAmTransactionsDetail(amFundAcctStmntRes.getDataTable().getAmfundAccountStatementRows());
				}
			}
		}

		if (dashboardDetails.getApp3().equals(3L)) {

		}

		if (dashboardDetails.getApp4().equals(4L)) {

		}

		model.addAttribute("transactionHist", data);
		// model.addAttribute("dashboardDetail", dashboardDetails);

		return "transactionhistory";
	}
}
