/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.CorporateSubGroupRepository;
import com.cmb.consolidatedapps.interfaces.CustomerApplicationRepository;
import com.cmb.consolidatedapps.interfaces.CustomerRepository;
import com.cmb.consolidatedapps.interfaces.CustomerTypeRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.CorporateSubGroup;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerApplication;
import com.cmb.consolidatedapps.model.CustomerType;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.MailDetail;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.cmb.consolidatedapps.service.CustomerImplementation;
import com.google.common.base.Strings;

/**
 * @author waliu.faleye
 *
 */
@Controller
@Transactional
public class CustomersRegistrationController {

	@Value("${profile.uploadedfile.path}")
	String filePath;

	@Value("${email.sender}")
	String sender;

	@Value("${login.mail.subject}")
	String loginMailSubject;

	@Value("${initial.pass}")
	String initPass;

	ProcessInterfaces processInterface;
	CustomerRepository customerRepository;
	UserRepository userRepository;
	StatusRepository statusRepo;
	UserRoleRepository userRoleRepo;
	CustomerTypeRepository customerTypeRepository;
	CustomerApplicationRepository customerApplicationRepo;
	ApplicationRepository applicationRepo;
	CorporateSubGroupRepository corporateSubGroupRepository;
	private CustomerImplementation custImpl;

	@Autowired
	JavaMailSender javaMailSender;

	public CustomersRegistrationController(CustomerRepository customerRepository, UserRepository userRepository,
			StatusRepository statusRepo, ProcessInterfaces processInterface,
			CustomerTypeRepository customerTypeRepository, UserRoleRepository userRoleRepo,
			CustomerApplicationRepository customerApplicationRepo, ApplicationRepository applicationRepo,
			CorporateSubGroupRepository corporateSubGroupRepository, CustomerImplementation custImpl) {
		this.customerRepository = customerRepository;
		this.userRepository = userRepository;
		this.statusRepo = statusRepo;
		this.processInterface = processInterface;
		this.customerTypeRepository = customerTypeRepository;
		this.userRoleRepo = userRoleRepo;
		this.customerApplicationRepo = customerApplicationRepo;
		this.applicationRepo = applicationRepo;
		this.corporateSubGroupRepository = corporateSubGroupRepository;
		this.custImpl = custImpl;
	}

	@Transactional
	@PostMapping(value = "/saveCustomerDetail")
	public String saveCustomer(Model model, Customer customer) {
		System.out.println("saveCustomerDetail id ==="+customer.getId());
		String returnPage = "create";
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		try {
			HttpSession session = attr.getRequest().getSession(true);
			User loginUser = (User) session.getAttribute("user");
			List<CustomerApplication> customerApps = getCustomerApplications(customer);
			if (customerApps.isEmpty()) {
				List<CustomerType> customerTypes = customerTypeRepository.findAll();

				model.addAttribute("customer", customer);
				model.addAttribute("customerTypes", customerTypes);
				return "create";
			}
			customer.setCreatedBy(loginUser.getUserId());
			customer.setCreatedDate(new Date());
			customer.setCustomerType(customerTypeRepository.getOne(customer.getCustomerTypeId()));
			if (customer.getCustomerTypeId() == 2)
				customer.setCorporateSubgroup(corporateSubGroupRepository.getOne(customer.getCorporateSubGroupId()));
			// customer.setCustomerApplications(customerApps);
			customerRepository.save(customer);
			customerApplicationRepo.deleteByCustomer(customer);
			customerApplicationRepo.saveAll(customerApps);
			User user = new User();
			if("update".equals(customer.getMode())){
				List<User> users = userRepository.findByCustomer(customer); //(List<User>) customer.getUsers();
				if (customerTypeRepository.getOne(customer.getCustomerTypeId()).getType().equals("INDIVIDUAL")) {
					user = users.get(0);
				} else if (customerTypeRepository.getOne(customer.getCustomerTypeId()).getType().equals("CORPORATE")) {
					if (corporateSubGroupRepository.getOne(customer.getCorporateSubGroupId()).getType().equals("SOLE")) {
						user = users.get(0);
					} else {
					}
				} else {
				}
			}
			user.setRcnBvn(customer.getBvn() != null ? customer.getBvn() : customer.getRcNumber());
			user.setCustomerName(customer.getName());
			if (customer.getCustomerTypeId().equals(Long.valueOf("1"))) {
				user.setEmail(customer.getEmail());
				user.setPhone(customer.getPhone());
			} else {
				user.setEmail("");
				user.setPhone("");

			}
			user.setCustomer(customer);
			model.addAttribute("user", user);
			if (customerTypeRepository.getOne(customer.getCustomerTypeId()).getType().equals("INDIVIDUAL")) {
				returnPage = "individual";
			} else if (customerTypeRepository.getOne(customer.getCustomerTypeId()).getType().equals("CORPORATE")) {
				if (corporateSubGroupRepository.getOne(customer.getCorporateSubGroupId()).getType().equals("SOLE")) {
					returnPage = "corporate";
				} else {
					// model
					List<User> customerUsers = userRepository.findByCustomer(customer);
					model.addAttribute("customerUsers", customerUsers);
					returnPage = "multiuser";
				}
				// returnPage = "corporate";
			} else {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnPage;
	}

	//@Transactional
	@GetMapping(value = "/updateUserDetail")
	public String updateUserDetail(Model model, Long id) {
		System.out.println("updateUserDetail id==="+id);
		String returnPage = "create";
		try {

			User user = processInterface.findUserById(id);
			Customer customer = user.getCustomer();
			this.setCustomerApplications(customerApplicationRepo.findByCustomer(customer),customer);
			customer.setMode("update");
			user.setRcnBvn(customer.getBvn() != null ? customer.getBvn() : customer.getRcNumber());
			user.setCustomerName(customer.getName());
			if (customer.getCustomerType().getId().equals(Long.valueOf("1"))) {
				System.out.println("customer.getCustomerType().getId()==="+customer.getCustomerType().getId());
				System.out.println("user.getEmail()==="+user.getEmail());
				System.out.println("user.getPhone()==="+user.getPhone());
				user.setEmail(user.getEmail());
				user.setPhone(user.getPhone());
				customer.setEmail(user.getEmail());
				customer.setPhone(user.getPhone());
			} else {
				user.setEmail("");
				user.setPhone("");

			}
			user.setCustomer(customer);
			model.addAttribute("user", user);
			if (customer.getCustomerType().getType().equals("INDIVIDUAL")) {
				returnPage = "individual";
			} else if (customer.getCustomerType().getType().equals("CORPORATE")) {
				if (customer.getCustomerType().getType().equals("SOLE")) {
					returnPage = "corporate";
				} else {
					// model
					List<User> customerUsers = userRepository.findByCustomer(customer);
					model.addAttribute("customerUsers", customerUsers);
					returnPage = "multiuser";
				}
				// returnPage = "corporate";
			} else {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnPage;
	}

	@GetMapping(value = "/updateCustomerDetail")
	public String updateCustomer(Model model, @RequestParam Long id) {
		try {
			Optional<Customer> cust = customerRepository.findById(id);
			Customer customer = cust.isPresent() ? cust.get() : null;
			List<CustomerApplication> customerApps = customerApplicationRepo.findByCustomer(customer);
			setCustomerApplications(customerApps, customer);
			customer.setMode("update");

			model.addAttribute("customer", customer);
			model.addAttribute("customerTypeId", customer.getCustomerType().getId());
			model.addAttribute("corporateSubGroupId",
					customer.getCorporateSubgroup() != null ? customer.getCorporateSubgroup().getId() : 0L);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "create";
	}

	@GetMapping(value = "/confirm")
	public String confirm(Model model) {
		return "confirm";
	}

	@GetMapping(value = "/home")
	public String home(Model model) {
		return "home";
	}

	@GetMapping(value = "/getCustomer")
	public String getCustomer(Model model, Pageable pageable) {
		try {
			Page<Customer> customerList = customerRepository.findAll(pageable);
			System.out.println("Customer Size===" + customerList.getTotalPages());
			model.addAttribute("customerList", customerList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "home";
	}

	@PostMapping(value = "/saveUserDetail")
	public String saveCustomerUser(Model model, User user, @RequestParam("picture") MultipartFile file1) {
		Optional<Customer> cust = null;
		Customer customer = null;
		String returnPage = "confirm";
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		try {
			Image image = new Image();

			byte[] bytes = file1.getBytes();
			Path path = Paths.get(filePath + file1.getOriginalFilename());
			System.out.println("file1.getOriginalFilename()===" + file1.getOriginalFilename());
			if (!Strings.isNullOrEmpty(file1.getOriginalFilename())) {
				Files.write(path, bytes);
				// log.info(file1.getOriginalFilename());
				image.setFileName("files/" + file1.getOriginalFilename());
				custImpl.saveImage(image);
				user.setImage(image);
			}
			HttpSession session = attr.getRequest().getSession(true);
			User loginUser = (User) session.getAttribute("user");
			if (user.getCustomerId() != null)
				cust = customerRepository.findById(user.getCustomerId());
			if (cust.isPresent()) {
				customer = cust.get();
				user.setCustomer(customer);
			}
			user.setStatus(statusRepo.findByStatus("I"));
			if (user.getUserRoleId() == null) {
				user.setUserRole(userRoleRepo.findByRoleName("CUSTOMER"));
			} else {
				Optional<UserRole> userRole = userRoleRepo.findById(user.getUserRoleId());
				if (userRole.isPresent())
					user.setUserRole(userRole.get());
			}
			user.setCreatedBy(loginUser.getUserId());
			user.setCreatedDate(new Date());
			user.setUserId("myuser");
			user.setPassword("mypass");
			userRepository.save(user);
			String userId = processInterface.autoGenerateUserId(user.getId());
			String userPass = processInterface.autoGeneratePassword(user.getId(), initPass);
			user.setUserId(userId);
			user.setPassword(userPass);
			userRepository.save(user);
			MailDetail mailDetail = new MailDetail();
			mailDetail.setSender(sender);
			mailDetail.setReciever(user.getEmail());
			mailDetail.setMailSender(javaMailSender);
			mailDetail.setSubject(loginMailSubject);
			mailDetail.setHtmlEnabled(true);
			// mailDetail.setEmailBody(
			// "Dear User, \n\nYou have just been profi le on Coronation
			// Consolidated Portal. Below are your login details\n\n UserId:"
			// + userId + "\n\nPassword: " + userPass + "\n\nRegards,");
			String userName = user.getCustomer().getFirstName() == null ? "" : user.getCustomer().getFirstName();
			mailDetail.setEmailBody(processInterface.findByTemplateName("welcome").getTemplate()
					.replace("var1", userName.concat(" ").concat(user.getCustomer().getLastName()))
					.replace("var2", userId).replace("var3", userPass));
			processInterface.sendMail(mailDetail);

			if (customer.getCorporateSubgroup() != null
					&& customer.getCorporateSubgroup().getType().equals("MULTI-ENTITY")) {
				// model
				List<User> customerUsers = userRepository.findByCustomer(customer);
				model.addAttribute("customerUsers", customerUsers);
				returnPage = "multiuser";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnPage = "failedrequest";
		}
		return returnPage;
	}

	private List<CustomerApplication> getCustomerApplications(Customer customer) {
		List<CustomerApplication> customerApps = new ArrayList<CustomerApplication>();

		try {

			if (customer.getApp1() != null) {
				CustomerApplication custApp = new CustomerApplication();
				custApp.setApplication(applicationRepo.getOne(customer.getApp1()));
				customerApps.add(custApp);
			}

			if (customer.getApp2() != null) {
				CustomerApplication custApp = new CustomerApplication();
				custApp.setApplication(applicationRepo.getOne(customer.getApp2()));
				customerApps.add(custApp);
			}

			if (customer.getApp3() != null) {
				CustomerApplication custApp = new CustomerApplication();
				custApp.setApplication(applicationRepo.getOne(customer.getApp3()));
				customerApps.add(custApp);
			}

			if (customer.getApp4() != null) {
				CustomerApplication custApp = new CustomerApplication();
				custApp.setApplication(applicationRepo.getOne(customer.getApp4()));
				customerApps.add(custApp);
			}

			customerApps.stream().forEach(data -> data.setCustomer(customer));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return customerApps;

	}

	private Customer setCustomerApplications(List<CustomerApplication> customerApps, Customer customer) {
		try {
			for (CustomerApplication customerApplication : customerApps) {
				if (customerApplication.getApplication().getId().equals(Long.valueOf("1"))) {
					customer.setApp1(Long.valueOf("1"));
				}
				if (customerApplication.getApplication().getId().equals(Long.valueOf("2"))) {
					customer.setApp2(Long.valueOf("2"));
				}
				if (customerApplication.getApplication().getId().equals(Long.valueOf("3"))) {
					customer.setApp3(Long.valueOf("3"));
				}
				if (customerApplication.getApplication().getId().equals(Long.valueOf("4"))) {
					customer.setApp4(Long.valueOf("4"));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return customer;

	}

	@GetMapping(value = "/createCustomer")
	public String createCustomer(Model model) {
		try {
			System.out.println("just testing");
			Customer customer = new Customer();
			// List<CustomerType> customerTypes =
			// customerTypeRepository.findAll();

			model.addAttribute("customer", customer);
			// model.addAttribute("customerTypes", customerTypes);
			System.out.println("just testing 2");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "create";
	}

	@ModelAttribute(value = "customerTypes")
	public List<CustomerType> getCustomerTypes() {
		List<CustomerType> list = new ArrayList<CustomerType>();
		try {
			list = customerTypeRepository.findAll();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	@ModelAttribute(value = "corporatesubgroups")
	public List<CorporateSubGroup> getCorporateSubGroups() {
		List<CorporateSubGroup> list = new ArrayList<CorporateSubGroup>();
		try {
			list = corporateSubGroupRepository.findAll();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}
}
