/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ContactUsInfoRepository;
import com.cmb.consolidatedapps.interfaces.CustomerBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.CustomerInterface;
import com.cmb.consolidatedapps.interfaces.CustomerSubsidiaryBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.ContactUsInfo;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.service.ProcessImpl;

/**
 * @author waliu.faleye
 *
 */
@ControllerAdvice
@Controller
public class CustomerControllers {
	private CustomerBeneficiaryRepository customerBeneficiaryRepo;
	private CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo;
	private ApplicationRepository applicationRepo;
	private UserRepository userRepo;
	private UserRoleRepository userRoleRepo;
	private StatusRepository statusRepo;
	private ContactUsInfoRepository contactUsInfoRepo;
	@Value("${profile.uploadedfile.path}")
	String filePath;
	private ProcessInterfaces custImpl;

	private ProcessInterfaces processImpl;

	public CustomerControllers(ProcessInterfaces custImpl,ProcessInterfaces processImpl) {
		this.custImpl = custImpl;
		this.processImpl = processImpl;
	}

	@GetMapping(value = "/contactus")
	public String contactus(Model model) {
		ContactUsInfo contactUsInfo = new ContactUsInfo();

		model.addAttribute("contactUsInfo", contactUsInfo);
		return "contact-us";
	}

	@PostMapping(value = "/saveContactus")
	public String saveContactus(Model model, ContactUsInfo contactUsInfo) {

		contactUsInfo.setUser(processImpl.findUserById(contactUsInfo.getUserId()));
		contactUsInfo.setCreateDate(LocalDateTime.now());
		custImpl.saveContactUsInfo(contactUsInfo);
		return "contact-us";
	}

	@PostMapping(value = "/submitImageDetail")
	public String submitImageDetail(@Valid @ModelAttribute("applicant") Image image, ModelMap model,
			BindingResult bindingResult, @RequestParam("picture") MultipartFile file1) {
		System.out.println("in submitApplication");
		try {
			// Get the file and save it somewhere
			if (!file1.isEmpty()) {
				byte[] bytes = file1.getBytes();
				Path path = Paths.get(filePath + file1.getOriginalFilename());
				Files.write(path, bytes);
				// log.info(file1.getOriginalFilename());
				image.setFileName("files/"+file1.getOriginalFilename());

			} else {

				return "profilepage";
			}

			if (bindingResult.hasErrors()) {
				System.out.println("There is an error >>> " + bindingResult.getFieldErrors().get(0));
				String response1 = bindingResult.toString();
				return "profilepage";
			}
			custImpl.saveImage(image);
			System.out.println("image.getUserId() >>> " + image.getUserId());
			User user = processImpl.findUserById(image.getUserId());
			user.setImage(image);
			custImpl.saveUser(user);		
			image.setUser(user);
			model.addAttribute("image", image);
			model.addAttribute("user", user);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "profilepage";
		}

		return "profilepage";
	}

	@GetMapping(value = "/userProfile")
	public String userProfile(Model model) {
		Image image = new Image();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		//model.addAttribute("localImg", dashboardDetails.getUser().getImage().getFileName());
		//System.out.println("dashboardDetails.getUser().getImage().getFileName()==="+dashboardDetails.getUser().getImage().getFileName());
		User user = processImpl.findUserById(dashboardDetails.getUser().getId());
		image.setUser(user);
		model.addAttribute("image", image);
		model.addAttribute("user", user);
		return "profilepage";
	}

	@GetMapping(value = "/customerBeneficiaries")
	public String customerBeneficiaries(Model model) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		Customer customer = dashboardDetails.getUser().getCustomer();

		List<CustomerBeneficiary> bankBeneficiaries = custImpl.findCustomerBeneficiariesByCustomer(customer);

		List<CustomerSubsidiaryBeneficiary> amBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(2L));

		List<CustomerSubsidiaryBeneficiary> secBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(3L));

		model.addAttribute("bankBeneficiaries", bankBeneficiaries);
		model.addAttribute("amBeneficiaries", amBeneficiaries);
		model.addAttribute("secBeneficiaries", secBeneficiaries);
		return "beneficiary";
	}

	@GetMapping(value = "/deleteCustomerBeneficiary")
	public String deleteCustomerBeneficiary(Model model, @RequestParam(value = "id") Long id) {

		custImpl.deleteCustomerBeneficiary(id);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		Customer customer = dashboardDetails.getUser().getCustomer();

		List<CustomerBeneficiary> bankBeneficiaries = custImpl.findCustomerBeneficiariesByCustomer(customer);

		List<CustomerSubsidiaryBeneficiary> amBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(2L));

		List<CustomerSubsidiaryBeneficiary> secBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(3L));

		model.addAttribute("bankBeneficiaries", bankBeneficiaries);
		model.addAttribute("amBeneficiaries", amBeneficiaries);
		model.addAttribute("secBeneficiaries", secBeneficiaries);
		return "beneficiary";
	}

	@GetMapping(value = "/deleteCustomerSubBeneficiary")
	public String deleteCustomerSubBeneficiary(Model model, @RequestParam(value = "id") Long id) {

		custImpl.deleteCustomerSubsidiaryBeneficiary(id);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		Customer customer = dashboardDetails.getUser().getCustomer();

		List<CustomerBeneficiary> bankBeneficiaries = custImpl.findCustomerBeneficiariesByCustomer(customer);

		List<CustomerSubsidiaryBeneficiary> amBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(2L));

		List<CustomerSubsidiaryBeneficiary> secBeneficiaries = custImpl
				.findCustomerSubBeneficiariesByCustomerAndApplication(customer, custImpl.findApplicationById(3L));

		model.addAttribute("bankBeneficiaries", bankBeneficiaries);
		model.addAttribute("amBeneficiaries", amBeneficiaries);
		model.addAttribute("secBeneficiaries", secBeneficiaries);
		return "beneficiary";
	}
	
	@ModelAttribute(value = "imageuser")
	public User myUserDetail(){
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session.getAttribute("dashboardDetail");
		
		if(dashboardDetails != null)
		return processImpl.findUserById(dashboardDetails.getUser().getId());
		
		return null;
	}
}
