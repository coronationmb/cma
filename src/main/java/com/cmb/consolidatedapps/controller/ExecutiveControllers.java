/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import com.cmb.consolidatedapps.model.CustomerApplication;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.cmb.consolidatedapps.service.CustomerImplementation;
import com.cmb.consolidatedapps.service.ProcessImpl;
import com.cmb.consolidatedapps.service.ThirdPartyService;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.Rows;
import com.cmb.thirdpartyapi.model.SecurityResponseData;
import com.cmb.thirdpartyapi.model.SubFundsRows;
import com.google.common.base.Strings;

/**
 * @author waliu.faleye
 *
 */
@Controller
public class ExecutiveControllers {
	ThirdPartyService tdptService = new ThirdPartyService();

	@Value("${thirdpartyapi.baseurl.assetmgt}")
	String thirdpartyapiBaseUrlAssetMgt;

	@Value("${thirdpartyapi.baseurl.securities}")
	String thirdpartyapiBaseUrlSec;

	CustomerImplementation customerImpl;
	ProcessImpl processImpl;

	public ExecutiveControllers(CustomerImplementation customerImpl, ProcessImpl processImpl) {
		this.customerImpl = customerImpl;
		this.processImpl = processImpl;
	}

	@GetMapping(value = "/executiveDashboard")
	public String executiveDashboard(Model model) {

		return "ceo";
	}

	@GetMapping(value = "/customerDashboard")
	public String customerDashboard(Model model, @RequestParam Long id) {

		User dbUser = processImpl.findUserById(id);

		CustomerDashboardDetail dashboardDetails = processImpl.dashboardDetails(dbUser);
		model.addAttribute("customerDashboard", dashboardDetails);
		return "customerdashboard";
	}

	@GetMapping(value = "/customerSearchDashboard")
	public String customerSearchExecutive(Model model) {

		return "ceo";
	}

	@ModelAttribute(value = "externalusers")
	public List<User> getExternalUsers() {
		UserRole userRole = customerImpl.findByRoleName("CUSTOMER");
		List<User> externalUsers = customerImpl.findUsersByRoleName(userRole);

		externalUsers.forEach(a -> a.setNetWorth(getUserNetWorth(a)));

		return externalUsers;
	}

	public String getUserNetWorth(User user) {
		String netWorth = "0.00";
		BigDecimal worth = BigDecimal.ZERO;
		BigDecimal amWorth = BigDecimal.ZERO;
		BigDecimal secWorth = BigDecimal.ZERO;
		BigDecimal trusteesWorth = BigDecimal.ZERO;
		AssetManagementResponseData amResponse = new AssetManagementResponseData();
		SecurityResponseData secResponse = new SecurityResponseData();
		List<SubFundsRows> subscribedFunds = new ArrayList<SubFundsRows>();
		String sessionId = "";
		// List<CustomerApplication> userApps =
		// user.getCustomer().getCustomerApplications();
		System.out.println("userApps.size()==" + user.getCustomer().getCustomerApplications().size());

		String bvnrcnumber = !Strings.isNullOrEmpty(user.getCustomer().getBvn()) ? user.getCustomer().getBvn()
				: !Strings.isNullOrEmpty(user.getCustomer().getRcNumber()) ? user.getCustomer().getRcNumber()
						: user.getCorporateUserBvn();

		for (CustomerApplication customerApplication : user.getCustomer().getCustomerApplications()) {
			if ("BANK".equals(customerApplication.getApplication().getName())) {
				// System.out.println("I have bank account");
				List<CustomerBankAccount> accountList = tdptService.getBankCustomerAccountDetailsFromBvn(bvnrcnumber);
				BigDecimal bankWorth = BigDecimal.ZERO;
				for (CustomerBankAccount customerBankAccount : accountList) {
					bankWorth = bankWorth.add(customerBankAccount.getNgnBalanceAmount());
				}
				worth = worth.add(bankWorth);

			}
			if ("ASSET MANAGEMENT".equals(customerApplication.getApplication().getName())) {
				// System.out.println("I have asset management account");

				BigDecimal cfif = BigDecimal.ZERO;
				BigDecimal cbf = BigDecimal.ZERO;
				BigDecimal cmmf = BigDecimal.ZERO;

				tdptService.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
				List<SubsidiaryCustomerDetail> custAIDList = tdptService.getAMCustomerDetailFromBvn(bvnrcnumber);
				for (SubsidiaryCustomerDetail subsidiaryCustomerDetail : custAIDList) {

					amResponse = tdptService.assetMngmtLogin();
					if (amResponse != null
							&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
						System.out.println("inside ==" + amResponse.getOutValue());
						sessionId = amResponse.getOutValue();
					}
					amResponse = tdptService.getCustomerSubscribedFunds(sessionId,
							subsidiaryCustomerDetail.getCustomerAid());
					if (amResponse != null && (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L)))
						subscribedFunds = amResponse.getDataTable().getSubFundsRows();
					for (SubFundsRows subFundsRows : subscribedFunds) {
						if ("CBF".equals(subFundsRows.getFundCode())) {

							amResponse = tdptService.getFundInvestmentSummary(sessionId,
									subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
							if (amResponse != null
									&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
								cbf = new BigDecimal(
										amResponse.getDataTable().getAminvestmentSummaryRows().get(0).getMktValue())
												.setScale(2, RoundingMode.CEILING);
							}
						}
						if ("CFIF".equals(subFundsRows.getFundCode())) {

							amResponse = tdptService.getFundInvestmentSummary(sessionId,
									subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
							if (amResponse != null
									&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
								cfif = new BigDecimal(
										amResponse.getDataTable().getAminvestmentSummaryRows().get(0).getMktValue())
												.setScale(2, RoundingMode.CEILING);
							}
						}
						if ("CMMFUND".equals(subFundsRows.getFundCode())) {

							amResponse = tdptService.getFundInvestmentSummary(sessionId,
									subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
							if (amResponse != null
									&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
								cmmf = new BigDecimal(
										amResponse.getDataTable().getAminvestmentSummaryRows().get(0).getMktValue())
												.setScale(2, RoundingMode.CEILING);
							}
						}

					}
				}
				amWorth = amWorth.add(cbf).add(cfif).add(cmmf);
			}
			if ("SECURITIES".equals(customerApplication.getApplication().getName())) {
				// System.out.println("I have securities account");
				tdptService.setBaseUrl(thirdpartyapiBaseUrlSec);
				BigDecimal secCashValue = BigDecimal.ZERO;
				BigDecimal secEquitiesValue = BigDecimal.ZERO;
				List<SubsidiaryCustomerDetail> custAIDList = tdptService.getSecCustomerDetailFromBvn(bvnrcnumber);
				for (SubsidiaryCustomerDetail subsidiaryCustomerDetail : custAIDList) {

					secResponse = tdptService.securityLogin();
					System.out.println("secResponse.getOutValue()>>>" + secResponse.getOutValue());
					System.out.println("secResponse>>>" + secResponse);
					if (secResponse != null
							&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
						System.out.println("secResponse.getStatusId()>>>" + secResponse.getStatusId());
						sessionId = secResponse.getOutValue();
						if (secResponse != null
								&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {

							BigDecimal secPortfolio[] = new BigDecimal[2];
							int i = 0;
							if (secResponse.getDataTable() != null) {
								for (Rows row : secResponse.getDataTable().getRows()) {
									System.out.println(
											"inside secResponse.getDataTable()>>>" + secResponse.getDataTable());
									secPortfolio[i] = new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING);
									if ("Cash".equals(row.getName())) {
										secCashValue = new BigDecimal(row.getAmount()).setScale(2,
												RoundingMode.CEILING);
									} else if ("Equities".equals(row.getName())) {
										secEquitiesValue = new BigDecimal(row.getAmount()).setScale(2,
												RoundingMode.CEILING);
										System.out.println("row.getAmount()>>>" + row.getAmount());
									}
									i++;
								}
							}
						}
					}
				}
				secWorth = secWorth.add(secCashValue).add(secEquitiesValue);
			}
			if ("TRUSTEES".equals(customerApplication.getApplication().getName())) {
				// System.out.println("I have trustees account");

			}
		}
		BigDecimal total = worth.add(amWorth).add(secWorth).add(trusteesWorth);
		netWorth = processImpl.formatAmount(total);
		return netWorth;
	}
}
