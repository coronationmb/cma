/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ContactUsInfoRepository;
import com.cmb.consolidatedapps.interfaces.CustomerBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.CustomerInterface;
import com.cmb.consolidatedapps.interfaces.CustomerSubsidiaryBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;

/**
 * @author waliu.faleye
 *
 */
@ControllerAdvice
@Controller
public class AdministrationController {

	private CustomerBeneficiaryRepository customerBeneficiaryRepo;
	private CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo;
	private ApplicationRepository applicationRepo;
	private UserRepository userRepo;
	private UserRoleRepository userRoleRepo;
	private StatusRepository statusRepo;
	private ContactUsInfoRepository contactUsInfoRepo;
	@Value("${entrust.user.creation.failed}")
	String entrustUserCreationFailed;

	
	ProcessInterfaces customerImpl;
	ProcessInterfaces processImpl;

	public AdministrationController(ProcessInterfaces customerImpl, ProcessInterfaces processImpl) {
		this.customerImpl = customerImpl;
		this.processImpl = processImpl;
	}

	@PostMapping(value = "/updateUser")
	public String updateUser(Model model, User user) {
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		UserRole userRole = null;
		List<User> internalUsers = null;
		try {
			userRole = customerImpl.findByRoleName("CUSTOMER");
			HttpSession session = attr.getRequest().getSession(true);
			User loginUser = (User) session.getAttribute("user");

			user.setCreatedBy(loginUser.getUserId());
			user.setCreatedDate(new Date());
			user.setUserRole(customerImpl.findById(user.getUserRoleId()));
			user.setPassword("00000000");
			if (user.getId() == null) {
				user.setStatus(customerImpl.findByStatus("A"));
				AdminResponseDTO res = customerImpl.createEntrustUser(user);
				if (res != null) {
					if (res.getRespCode() == 1 || res.getRespCode() == -1) {
						customerImpl.saveUser(user);
					} else {
						System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
						System.out.println("token res.getRespCode() ==" + res.getRespCode());
						System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
						// user.setErrorMessage(res.getRespMessage());
						String entrustErrMsg = entrustUserCreationFailed.replace("var1", user.getUserId());
						user.setErrorMessage(entrustErrMsg);
					}
				} else {
					user.setErrorMessage("There is an issue connecting to entrust. Contact Administrator.");
				}
			} else {
				user.setStatus(customerImpl.findStatusById(user.getStatusId()));
				customerImpl.saveUser(user);
			}
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			model.addAttribute("internalusers", internalUsers);
			model.addAttribute("user", user);
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", user);
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			model.addAttribute("internalusers", internalUsers);
		}
		return "rolemanagement";
	}

	@GetMapping(value = "/editUser")
	public String editUser(Model model, @RequestParam Long id) {
		UserRole userRole = null;
		List<User> internalUsers = null;
		User editUser = null;
		try {
			editUser = processImpl.findUserById(id);
			userRole = customerImpl.findByRoleName("CUSTOMER");
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			// editUser.setm
			editUser.setUserRoleId(editUser.getUserRole().getId());
			editUser.setStatusId(editUser.getStatus().getId());
			model.addAttribute("userRoleId", editUser.getUserRole().getId());
			model.addAttribute("statusId", editUser.getStatus().getId());
			model.addAttribute("mode", "edit");
			model.addAttribute("user", editUser);
			model.addAttribute("internalusers", internalUsers);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (editUser == null)
				editUser = new User();
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			editUser.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", editUser);
			model.addAttribute("internalusers", internalUsers);
		}

		return "rolemanagement";
	}

	@GetMapping(value = "/deleteUser")
	public String deleteUser(Model model, @RequestParam Long id) {
		UserRole userRole = null;
		List<User> internalUsers = null;
		User editUser = null;
		try {
			editUser = processImpl.findUserById(id);
			customerImpl.deleteUser(id);
			userRole = customerImpl.findByRoleName("CUSTOMER");
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			model.addAttribute("user", editUser);
			model.addAttribute("internalusers", internalUsers);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (editUser == null)
				editUser = new User();
			editUser.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", editUser);
			model.addAttribute("internalusers", internalUsers);
		}

		return "rolemanagement";
	}

	@GetMapping(value = "/editAdminUser")
	public String editAdminUser(Model model, @RequestParam Long id) {
		UserRole userRole = null;
		List<User> internalUsers = null;
		User editUser = null;
		try {
			editUser = processImpl.findUserById(id);
			//userRole = customerImpl.findByRoleName("CUSTOMER");
			//internalUsers = customerImpl.findByUserRoleNot(userRole);
			userRole = customerImpl.findByRoleName("ADMIN");
			List<User> adminUsers = customerImpl.findUsersByRoleName(userRole);
			// editUser.setm
			editUser.setUserRoleId(editUser.getUserRole().getId());
			editUser.setStatusId(editUser.getStatus().getId());
			model.addAttribute("userRoleId", editUser.getUserRole().getId());
			model.addAttribute("statusId", editUser.getStatus().getId());
			model.addAttribute("mode", "edit");
			model.addAttribute("user", editUser);
			model.addAttribute("adminusers", adminUsers);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (editUser == null)
				editUser = new User();
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			editUser.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", editUser);
			model.addAttribute("adminusers", internalUsers);
		}

		return "surolemanagement";
	}

	@GetMapping(value = "/deleteAdminUser")
	public String deleteAdminUser(Model model, @RequestParam Long id) {
		UserRole userRole = null;
		List<User> internalUsers = null;
		User editUser = null;
		try {
			editUser = processImpl.findUserById(id);
			customerImpl.deleteUser(id);
			//userRole = customerImpl.findByRoleName("CUSTOMER");
			//internalUsers = customerImpl.findByUserRoleNot(userRole);
			userRole = customerImpl.findByRoleName("ADMIN");
			List<User> adminUsers = customerImpl.findUsersByRoleName(userRole);
			model.addAttribute("user", editUser);
			model.addAttribute("adminusers", adminUsers);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (editUser == null)
				editUser = new User();
			editUser.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", editUser);
			model.addAttribute("adminusers", internalUsers);
		}

		return "surolemanagement";
	}

	@PostMapping(value = "/updateAdminUser")
	public String updateAdminUser(Model model, User user) {

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		UserRole userRole = null;
		List<User> internalUsers = null;
		try {
			//userRole = customerImpl.findByRoleName("CUSTOMER");
			userRole = customerImpl.findByRoleName("ADMIN");
			List<User> adminUsers = customerImpl.findUsersByRoleName(userRole);
			HttpSession session = attr.getRequest().getSession(true);
			User loginUser = (User) session.getAttribute("user");

			user.setCreatedBy(loginUser.getUserId());
			user.setCreatedDate(new Date());
			user.setUserRole(customerImpl.findById(user.getUserRoleId()));
			user.setPassword("00000000");
			if (user.getId() == null) {
				user.setStatus(customerImpl.findByStatus("A"));
				AdminResponseDTO res = customerImpl.createEntrustUser(user);
				if (res != null) {
					if (res.getRespCode() == 1 || res.getRespCode() == -1) {
						customerImpl.saveUser(user);
					} else {
						System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
						System.out.println("token res.getRespCode() ==" + res.getRespCode());
						System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
						// user.setErrorMessage(res.getRespMessage());
						String entrustErrMsg = entrustUserCreationFailed.replace("var1", user.getUserId());
						user.setErrorMessage(entrustErrMsg);
					}
				} else {
					user.setErrorMessage("There is an issue connecting to entrust. Contact Administrator.");
				}
			} else {
				user.setStatus(customerImpl.findStatusById(user.getStatusId()));
				customerImpl.saveUser(user);
			}
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			model.addAttribute("adminusers", internalUsers);
			model.addAttribute("user", user);
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", user);
			internalUsers = customerImpl.findByUserRoleNot(userRole);
			model.addAttribute("adminusers", internalUsers);
		}
		return "surolemanagement";
	}

	@ModelAttribute(value = "internalusers")
	public List<User> getInternalUsers() {
		UserRole userRole = customerImpl.findByRoleName("CUSTOMER");
		List<User> internalUsers = customerImpl.findByUserRoleNot(userRole);

		return internalUsers;
	}

	@ModelAttribute(value = "internaluserroles")
	public List<UserRole> getInternalUserRoles() {
		// UserRole userRole = customerImpl.findByRoleName("CUSTOMER");
		List<UserRole> internalUserRoles = customerImpl.findByRoleNameNot("CUSTOMER");

		return internalUserRoles;
	}

	@ModelAttribute(value = "adminusers")
	public List<User> getAdminUsers() {
		UserRole userRole = customerImpl.findByRoleName("ADMIN");
		List<User> adminUsers = customerImpl.findUsersByRoleName(userRole);

		return adminUsers;
	}

	@ModelAttribute(value = "adminuserroles")
	public List<UserRole> getAdminUserRoles() {
		UserRole userRole = customerImpl.findByRoleName("ADMIN");
		//List<UserRole> internalUserRoles = customerImpl.findByRoleNameNot("CUSTOMER");
		List<UserRole> adminUserRoles = new ArrayList<UserRole>();
		adminUserRoles.add(userRole);

		return adminUserRoles;
	}

	@ModelAttribute(value = "status")
	public List<Status> getStatus() {
		// UserRole userRole = customerImpl.findByRoleName("CUSTOMER");
		List<Status> internalStatus = customerImpl.findAllStatus();

		return internalStatus;
	}
}
