/**
 * 
 */
package com.cmb.consolidatedapps.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cmb.consolidatedapps.interfaces.CustomerRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.HtmlTemplate;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.LoginDetail;
import com.cmb.consolidatedapps.model.MailDetail;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.service.LdapAuth;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;
import com.expertedge.entrustplugin.ws.AuthResponseDTO;
import com.expertedge.entrustplugin.ws.EntrustMultiFactorAuthImplService;
import com.expertedge.entrustplugin.ws.TokenAuthDTO;
import com.expertedge.entrustplugin.ws.UserAdminDTO;

/**
 * @author waliu.faleye
 *
 */
@ControllerAdvice
@Controller
@Transactional
@SessionAttributes({ "dashboardDetail", "user" })
public class LoginControllers implements ErrorController  {

	@Value("${spring.ldap.ip}")
	String ldapIp;
	@Value("${spring.ldap.port}")
	String ldapPort;
	@Value("${spring.ldap.base}")
	String ldapBase;
	@Value("${spring.ldap.secauth}")
	String ldapSecAuth;

	@Value("${email.sender}")
	String sender;

	@Value("${login.mail.subject}")
	String loginMailSubject;

	@Value("${initial.pass}")
	String initPass;

	@Value("${entrust.app.code}")
	String entrustAppCode;

	@Value("${entrust.app.desc}")
	String entrustAppDesc;

	@Autowired
	JavaMailSender javaMailSender;

	@Value("${staff.entrust.group}")
	String staffEntrustGroup;

	@Value("${entrust.token.authentication.failed}")
	String entrustTokenAuthFailed;

	@Value("${entrust.user.creation.failed}")
	String entrustUserCreationFailed;

	private static final String PATH = "/error";

	UserRepository userRepo;
	StatusRepository statusRepo;
	// CustomerApplicationRepository customerApplicationRepo;
	ProcessInterfaces processInterface;
	CustomerRepository customerRepo;

	public LoginControllers(UserRepository userRepo, StatusRepository statusRepo, ProcessInterfaces processInterface,
			CustomerRepository customerRepo) {
		this.userRepo = userRepo;
		this.statusRepo = statusRepo;
		// this.customerApplicationRepo = customerApplicationRepo;
		this.processInterface = processInterface;
		this.customerRepo = customerRepo;
	}

	@RequestMapping(value = PATH)
	public String error() {
		return "firstpage";
	}

	@GetMapping(value = "/login")
	public String loginPage(Model model) {

		User user = new User();

		model.addAttribute("user", user);

		return "loginin";
	}

	@GetMapping(value = "/")
	public String firstpageIndex(Model model) {

		User user = new User();

		model.addAttribute("user", user);

		return "firstpage";
	}

	@GetMapping(value = "/detailLogin")
	public String index(Model model) {

		User user = new User();

		model.addAttribute("user", user);

		return "login";
	}

	@GetMapping(value = "/oldLogin")
	public String backupIndex(Model model) {

		User user = new User();

		model.addAttribute("user", user);

		return "loginin";
	}

	@GetMapping(value = "/forgotuid")
	public String forgotUserId(Model model) {

		User user = new User();

		user.setErrorMessage("1");
		model.addAttribute("user", user);

		return "forgotusername";
	}

	@GetMapping(value = "/forgotpwd")
	public String forgotUserPassword(Model model) {

		User user = new User();

		user.setErrorMessage("1");
		model.addAttribute("user", user);

		return "forgotpassword";
	}

	@GetMapping(value = "/cma-login")
	public String indexInternalUser(Model model) {

		User user = new User();

		model.addAttribute("user", user);
		return "logincustomer";
	}

	@PostMapping(value = "/detailLogin")
	public String validateUsernamePassword(Model model, User user) {
		String returnPage = "";
		User dbUser = new User();
		try {
			System.out.println("userid == " + user.getUserId());
			System.out.println("user.getToken() == " + user.getToken());
			if (user.getUserId().toLowerCase().endsWith("@coronationmb.com")) {
				// ldap check
				// dbUser = userRepo.findByUserId(user.getUserId());
				dbUser = userRepo.findByUserIdAndDeleteFlgNot(user.getUserId(), "Y");
				System.out.println("userid == " + dbUser);
				if (dbUser != null) {
					Status status = dbUser.getStatus();
					if (status != null) {

						if (getAdAuthentication(user) && "A".equals(status.getStatus())) {
							dbUser = userRepo.findByUserId(user.getUserId());
							// returnPage = dbUser.getUserRole().getUrl();
							System.out.println("status.getStatus()==" + status.getStatus());
							user.setStatusCode(status.getStatus());
							model.addAttribute("user", user);

							returnPage = "logincustomer";

						} else {
							// wrong AD credentials

							returnPage = "NewFile";

						}
					} else {

					}
				} else {

					// User does not exist

					returnPage = "NewFile";
				}

			} else {
				// dbUser = userRepo.findByUserIdAndPassword(user.getUserId(),
				// user.getPassword());
				dbUser = userRepo.findByUserIdIgnoreCaseAndPasswordAndDeleteFlgNot(user.getUserId(), user.getPassword(), "Y");

				if (dbUser != null) {
					Status status = dbUser.getStatus();
					if (status != null) {

						if ("I".equals(status.getStatus())) {
							// change credential page
							user.setOldUserId(user.getUserId());
							user.setOldPassword(user.getPassword());
							user.setCustomer(dbUser.getCustomer());
							user.setImage(dbUser.getImage());
							model.addAttribute("user", user);
							// returnPage = "page1";
							returnPage = "usernamechange";

						} else if ("L".equals(status.getStatus())) {
							// change password page
							// user.setOldUserId(user.getUserId());
							// user.setOldPassword(user.getPassword());
							user.setCustomer(dbUser.getCustomer());
							user.setImage(dbUser.getImage());
							model.addAttribute("user", user);
							// returnPage = "page2";
							returnPage = "passwordchange";
						} else if ("A".equals(status.getStatus())) {

							System.out.println("status.getStatus()==" + status.getStatus());
							user.setStatusCode(status.getStatus());
							model.addAttribute("user", user);

							returnPage = "login";
						}
					} else {
						user.setErrorMessage("Invalid Credential");
						returnPage = user.getParentPage();
					}
				} else {
					user.setErrorMessage("Invalid Credential");
					returnPage = user.getParentPage();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return returnPage;
	}

	// @CrossOrigin
	@PostMapping(value = "/login")
	public String login(Model model, User user) {
		String returnPage = "";
		User dbUser = new User();
		try {
			System.out.println("userid == " + user.getUserId());
			System.out.println("user.getToken() == " + user.getToken());
			if (user.getUserId().toLowerCase().endsWith("@coronationmb.com")) {
				// ldap check
				// dbUser = userRepo.findByUserId(user.getUserId());
				dbUser = userRepo.findByUserIdAndDeleteFlgNot(user.getUserId(), "Y");
				System.out.println("userid == " + dbUser);
				if (dbUser != null) {
					Status status = dbUser.getStatus();
					if (status != null) {

						if (getAdAuthentication(user) && "A".equals(status.getStatus())) {
							EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
							TokenAuthDTO arg0 = new TokenAuthDTO();
							arg0.setAppCode(entrustAppCode);
							arg0.setAppDesc(entrustAppDesc);
							arg0.setGroup(staffEntrustGroup);
							arg0.setTokenPin(user.getToken());
							arg0.setUserName(dbUser.getUserId());
							AuthResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performTokenAuth(arg0);

							if (res != null) {
								if (res.getRespCode() == 1) {
									System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
									System.out.println("token res.getRespCode() ==" + res.getRespCode());
									System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
									user.setTokenRespMessage(entrustTokenAuthFailed);
									model.addAttribute("user", user);
									return "logincustomer";
								} else {

								}
							} else {
								return "logincustomer";
							}
							// dbUser = userRepo.findByUserId(user.getUserId());
							returnPage = dbUser.getUserRole().getUrl();

						} else {
							// wrong AD credentials
							user.setErrorMessage("Invalid Credential");
							returnPage = user.getParentPage();

						}
					} else {
						user.setErrorMessage("Invalid Credential");
						returnPage = user.getParentPage();

					}
				} else {

					// User does not exist
					user.setErrorMessage("Invalid Credential");
					returnPage = user.getParentPage();
				}

			} else {
				// dbUser = userRepo.findByUserIdAndPassword(user.getUserId(),
				// user.getPassword());
				dbUser = userRepo.findByUserIdIgnoreCaseAndPasswordAndDeleteFlgNot(user.getUserId(), user.getPassword(), "Y");

				if (dbUser != null) {
					Status status = dbUser.getStatus();
					if (status != null) {

						if ("I".equals(status.getStatus())) {
							// change credential page
							user.setOldUserId(user.getUserId());
							user.setOldPassword(user.getPassword());
							model.addAttribute("user", user);
							returnPage = "page1";

						} else if ("L".equals(status.getStatus())) {
							// change password page
							// user.setOldUserId(user.getUserId());
							// user.setOldPassword(user.getPassword());
							model.addAttribute("user", user);
							returnPage = "page2";
						} else if ("A".equals(status.getStatus())) {
							EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
							TokenAuthDTO arg0 = new TokenAuthDTO();
							arg0.setAppCode(entrustAppCode);
							arg0.setAppDesc(entrustAppDesc);
							arg0.setGroup(dbUser.getCustomer().getCustomerType().getEntrustGroup());
							arg0.setTokenPin(user.getToken());
							arg0.setUserName(dbUser.getUserId());
							AuthResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performTokenAuth(arg0);

							if (res != null) {
								if (res.getRespCode() == 1) {
									System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
									System.out.println("token res.getRespCode() ==" + res.getRespCode());
									System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
									user.setTokenRespMessage(entrustTokenAuthFailed);
									model.addAttribute("user", user);
									return "login";
								} else {

								}
							} else {
								user.setTokenRespMessage(
										"There is an issue connecting to entrust. Contact Administrator.");
								model.addAttribute("user", user);
								return "login";
							}

							CustomerDashboardDetail dashboardDetails = processInterface.dashboardDetails(dbUser);
							model.addAttribute("customerDashboard", dashboardDetails);
							model.addAttribute("dashboardDetail", dashboardDetails);
							model.addAttribute("user", dbUser);
							model.addAttribute("imageuser", dbUser);
							// model.addAttribute("localImg",
							// "files/IMG_20170618_101251.jpg");
							LocalDateTime now = LocalDateTime.now();
							LoginDetail loginDetail = new LoginDetail();
							loginDetail.setLoginTime(now);
							loginDetail.setUser(dbUser);
							processInterface.save(loginDetail);

							HtmlTemplate template = processInterface.findByTemplateName("login");
							String userName = dbUser.getCustomer().getFirstName() == null ? ""
									: dbUser.getCustomer().getFirstName();
							String htmlTemplate = template.getTemplate()
									.replace("var1", userName.concat(" ").concat(dbUser.getCustomer().getLastName()))
									.replace("var2", now.getMonth().name())
									.replace("var3", String.valueOf(now.getDayOfMonth()))
									.replace("var4", String.valueOf(now.getYear()))
									.replace("var5", now.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

							MailDetail mailDetail = new MailDetail();
							mailDetail.setSender(sender);
							mailDetail.setReciever(dbUser.getEmail());
							mailDetail.setMailSender(javaMailSender);
							mailDetail.setSubject(loginMailSubject);
							mailDetail.setHtmlEnabled(true);
							mailDetail.setEmailBody(htmlTemplate);
							processInterface.sendMail(mailDetail);

							returnPage = "index";
						}
					} else {
						user.setErrorMessage("Invalid Credential");
						returnPage = user.getParentPage();

					}
				} else {
					user.setErrorMessage("Invalid Credential");
					returnPage = user.getParentPage();

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("dbUser====" + dbUser);
		model.addAttribute("mode", "view");
		model.addAttribute("user", dbUser);
		return returnPage;
	}

	@PostMapping(value = "/saveChangedUserIdPassword")
	public String saveChangedUserDetails(Model model, User user) {
		try {
			if (user.getOldUserId().equals(user.getUserId())) {
				user.setErrorMessage("New Username cannot be the same as the Old Username");
				model.addAttribute("user", user);
				return "usernamechange";
			}

			if (!user.getOldPassword().equals(user.getPassword())) {
				user.setErrorMessage("New Password must be the same as the Confirm Password");
				model.addAttribute("user", user);
				return "usernamechange";
			}
			System.out.println("my userid == " + user.getOldUserId());

			User updatingUser = userRepo.findByUserId(user.getOldUserId());
			// to check if user already exist
			User dbUser = userRepo.findByUserId(user.getUserId());
			// updatingUser.setOldUserId(user.getOldUserId());
			if (dbUser == null) {
				updatingUser.setUserId(user.getUserId());
				updatingUser.setPassword(user.getPassword());
				updatingUser.setStatus(statusRepo.findByStatus("A"));

				EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
				UserAdminDTO arg0 = new UserAdminDTO();
				arg0.setEnableOTP(true);
				arg0.setFullname(updatingUser.getUserId());
				arg0.setGroup(updatingUser.getCustomer().getCustomerType().getEntrustGroup());
				arg0.setUserName(updatingUser.getUserId());
				AdminResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performCreateEntrustUser(arg0);

				System.out.println("token res ==" + res);
				if (res != null) {
					if (res.getRespCode() == 1 || res.getRespCode() == -1) {
						userRepo.save(updatingUser);
						model.addAttribute("user", updatingUser);
					} else {
						System.out.println("token res.getRespMessage() ==" + res.getRespMessage());
						System.out.println("token res.getRespCode() ==" + res.getRespCode());
						System.out.println("token res.getRespMessageCode() ==" + res.getRespMessageCode());
						String entrustErrMsg = entrustUserCreationFailed.replace("var1", updatingUser.getUserId());
						user.setErrorMessage(entrustErrMsg);
						model.addAttribute("user", user);
						return "usernamechange";
					}
				} else {
					user.setErrorMessage("There is an issue connecting to entrust. Contact Administrator.");
					model.addAttribute("user", user);
					return "usernamechange";
				}

			} else {
				// User ID already exist
				user.setErrorMessage("User Name already exist.");
				model.addAttribute("user", user);
				return "usernamechange";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue please Contact Administrator.");
			model.addAttribute("user", user);
			return "usernamechange";
		}
		return "usernamesuccess";

	}

	@PostMapping(value = "/saveChangedPassword")
	public String saveChangedPassword(Model model, User user) {

		try {
			if (!user.getOldPassword().equals(user.getPassword())) {
				user.setErrorMessage("New Password And Confirm Password are not the same");
				return "passwordchange";
			}
			System.out.println("my userid == " + user.getOldUserId());

			// pick existing user detail
			User dbUser = userRepo.findByUserId(user.getUserId());
			if (dbUser.getPassword().equals(user.getPassword())) {
				user.setErrorMessage("Newly Changed Password And Existing Password are not the same.");
				return "passwordchange";
			}

			if (dbUser != null) {
				dbUser.setPassword(user.getPassword());
				dbUser.setStatus(statusRepo.findByStatus("A"));

				userRepo.save(dbUser);

			} else {
				// User ID already exist
				user.setErrorMessage("User Name Already Exist.");
				return "passwordchange";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("Newly Changed Password And Existing Password are not the same.");
			return "passwordchange";
		}

		return "confirmpasswordchange";

	}

	@PostMapping(value = "/changePassword")
	public String changePassword(Model model, User user) {

		try {
			User dbUser = userRepo.findByUserId(user.getUserId());

			if (dbUser != null) {
				String pass = processInterface.autoGeneratePassword(dbUser.getId(), initPass);
				dbUser.setPassword(pass);
				// System.out.println("statusRepo.findByStatus >>>> " +
				// statusRepo.findByStatus("L").getDescription());
				dbUser.setStatus(statusRepo.findByStatus("L"));

				userRepo.save(dbUser);

				MailDetail mailDetail = new MailDetail();
				mailDetail.setSender(sender);
				mailDetail.setReciever(dbUser.getEmail());
				mailDetail.setMailSender(javaMailSender);
				mailDetail.setSubject(loginMailSubject);
				mailDetail.setHtmlEnabled(true);
				// mailDetail.setEmailBody(
				// "Dear User, \n\nYou have just requested for Password change
				// on
				// Coronation Consolidated Portal. Below is your login details"
				// + "\n\nPassword: " + dbUser.getPassword() + "\n\nRegards,");
				String userName = dbUser.getCustomer().getFirstName() == null ? ""
						: dbUser.getCustomer().getFirstName();
				mailDetail.setEmailBody(processInterface.findByTemplateName("passwordchange").getTemplate()
						.replace("var1", userName.concat(" ").concat(dbUser.getCustomer().getLastName()))
						.replace("var2", dbUser.getUserId()).replace("var3", dbUser.getPassword()));
				processInterface.sendMail(mailDetail);
				user.setErrorMessage("");

			} else {
				// User ID does not exist
				user.setErrorMessage("User ID does not exist");
				model.addAttribute("user", user);
				return "forgotpassword";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue updating password. Contact Administrator");
			model.addAttribute("user", user);
		}
		return "forgotpassword";

	}

	@PostMapping(value = "/changeCurrentPassword")
	public String changeCurrentPassword(Model model, User user) {

		Image image = new Image();
		try {
			//System.out.println("user.getUserId()==="+user.getUserId());
			User dbUser = processInterface.findUserById(user.getId());
			image.setUser(dbUser);
			model.addAttribute("image", image);
			if (user.getOldPassword().equals(user.getPassword())) {
				// Current Password and New Password cannot be the same.
				System.out.println("New password and old password is the same");
				user.setErrorMessage("Current Password and New Password cannot be the same.");
				model.addAttribute("user", user);
				return "profilepage";

			}

			if (dbUser != null) {
				System.out.println("user.getOldPassword()==="+user.getOldPassword());
				System.out.println("dbUser.getPassword()==="+dbUser.getPassword());

				if (!(user.getOldPassword().equals(dbUser.getPassword()))) {
					// Current Password is not the same as the backend Password
					user.setErrorMessage("Current password is not the same as the backend password");
					model.addAttribute("user", user);
					return "profilepage";
				}
				user.setErrorMessage("Password changed successfully.");
				dbUser.setPassword(user.getPassword());
				userRepo.save(dbUser);

			} else {
				// User ID does not exist
				user.setErrorMessage("User ID does not exist");
				model.addAttribute("user", user);
				return "profilepage";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue updating password. Contact Administrator");
			model.addAttribute("user", user);
		}
		return "profilepage";

	}

	@PostMapping(value = "/getUserId")
	public String getUserId(Model model, User user) {

		User myUser = new User();
		try {
			String bvn = user.getRcnBvn();
			String userId = "";
			String email = "";
			String customerName = "";
			Customer customerFromBvn = customerRepo.findByBvn(bvn);
			Customer customer = customerFromBvn;//
			System.out.println("customer.getId() == " + customer.getId());
			List<User> dbUser = userRepo.findByCustomer(customer);
			// updatingUser.setOldUserId(user.getOldUserId());

			if (dbUser.size() == 1) {
				myUser = dbUser.get(0);
				userId = myUser.getUserId();
				email = myUser.getEmail();
				// customerName = customer.getName();
				String userName = customer.getFirstName() == null ? "" : customer.getFirstName();
				customerName = userName.concat(" ").concat(customer.getLastName());

			} else {
				// Multi entity customer
				myUser = userRepo.findByCorporateUserBvn(bvn);
				userId = myUser.getUserId();
				email = myUser.getEmail();
				customerName = myUser.getCustomerName();
			}

			MailDetail mailDetail = new MailDetail();
			mailDetail.setSender(sender);
			mailDetail.setReciever(email);
			mailDetail.setMailSender(javaMailSender);
			mailDetail.setSubject(loginMailSubject);
			mailDetail.setHtmlEnabled(true);
			System.out.println("userId == " + userId);
			// mailDetail.setEmailBody(
			// "Dear User, \n\nYou have just requested for Password change on
			// Coronation Consolidated Portal. Below is your login details"
			// + "\n\nUser Id: " + userId + "\n\nRegards,");
			mailDetail.setEmailBody(processInterface.findByTemplateName("usernamechange").getTemplate()
					.replace("var1", customerName).replace("var2", userId));
			processInterface.sendMail(mailDetail);
			user.setErrorMessage("");
		} catch (Exception ex) {
			ex.printStackTrace();
			user.setErrorMessage("There is an issue updating password. Contact Administrator");
			model.addAttribute("user", user);
		}
		return "forgotusername";

	}

	@PostMapping(value = "/changeToNewPassword")
	public String changeToNewPassword(Model model, User user) {
		try {
			if (user.getOldPassword().equals(user.getPassword())) {
				return "NewFile";
			}
			User dbUser = userRepo.findByUserId(user.getUserId());

			if (dbUser != null) {
				dbUser.setPassword(user.getPassword());
				dbUser.setStatus(statusRepo.findByStatus("A"));

				userRepo.save(dbUser);

			} else {
				// User ID does not exist
				return "NewFile";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "NewFile";

	}

	@GetMapping(value = "/logout")
	public String logout(Model model) {

		return "logout";
	}

	@GetMapping(value = "/forgotPassword")
	public String forgotPassword(Model model) {

		return "";
	}

	@GetMapping(value = "/entrustAuthentication")
	public String entrustAuthentication(Model model) {

		return "";
	}

	public boolean getAdAuthentication(User user) {
		boolean response = false;

		LdapAuth ldap = new LdapAuth();
		ldap.setBaseDN(ldapBase);
		ldap.setIp(ldapIp);
		ldap.setPort(ldapPort);
		ldap.setSecurityAuthentication(ldapSecAuth);

		response = ldap.validateCredentials(user.getUserId(), user.getPassword());

		return response;
	}

	@ModelAttribute(value = "users")
	public List<User> getUsers() {
		List<User> newList = new ArrayList<User>();
		try {
			List<User> list = userRepo.findAll();

			newList = list.stream().filter(data -> data.getCustomer() != null).collect(Collectors.toList());

			newList.forEach(
					data -> data
							.setRcnBvn(
									data.getCustomer() != null
											? Strings.isNullOrEmpty(data.getCustomer().getBvn())
													? data.getCustomer().getRcNumber() : data.getCustomer().getBvn()
											: ""));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return newList;
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return PATH;
	}
}
