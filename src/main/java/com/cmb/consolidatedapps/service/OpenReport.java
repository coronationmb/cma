/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cmb.consolidatedapps.model.FITransactionDetail;
import com.cmb.consolidatedapps.model.Person;
import com.cmb.consolidatedapps.model.StatementDataParameters;
import com.cmb.thirdpartyapi.model.AMFundAccountStatementRows;
import com.cmb.thirdpartyapi.model.SecurityTransaction;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * @author waliu.faleye
 *
 */
public class OpenReport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Map<String, Object> peopleMap = new HashMap<>();
			peopleMap.put("Sisco", 17);
			peopleMap.put("Eve", 19);
			peopleMap.put("John", 20);
			peopleMap.put("George", 21);
			peopleMap.put("Steve", 18);

			ArrayList<Person> dataList = new ArrayList<Person>();

			List<SecurityTransaction> secRows = new ArrayList<SecurityTransaction>();
			List<FITransactionDetail> custBankTranList = new ArrayList<FITransactionDetail>();
			List<AMFundAccountStatementRows> amTransactionsDetail = new ArrayList<AMFundAccountStatementRows>();
			for (Map.Entry<String, Object> personMap : peopleMap.entrySet()) {
				Person person = new Person();
				person.setName(personMap.getKey());
				person.setAge(Integer.valueOf(personMap.getValue().toString()));
				dataList.add(person);
			}
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
			custBankTranList.addAll(new OpenReport().getBankDetail());

			secRows.addAll(new OpenReport().getSecuritiesData());
			AMFundAccountStatementRows amData = new AMFundAccountStatementRows();
			amData.setCredit("10000");
			amData.setDebit("0.00");
			amData.setDescription("testing");
			amData.setValueDate(LocalDate.now().format(formatter));
			amTransactionsDetail.add(amData);
			dataList.sort((Person s1, Person s2) -> s1.getName().compareTo(s2.getName()));

			JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(custBankTranList);
			// JRBeanCollectionDataSource beanColDataSourceSec = new
			// JRBeanCollectionDataSource(dataList);
			Map parameters = new HashMap();
			parameters.put("INFO", "Hello");
			parameters.put("DS1", beanColDataSource);
			parameters.put("bankaccount_data_all", custBankTranList);
			parameters.put("security_data_all", secRows);
			parameters.put("am_data_all", amTransactionsDetail);
			parameters.put("customerName", "Waliu Faleye");
			parameters.put("address", "10, Amodu Ojikutu, Victoria Island, Lagos");
			parameters.put("currency", "NGN");

			// JasperReport report = (JasperReport)
			// JRLoader.loadObject("src/test/ireport/ShowPerson.jasper");

			InputStream inputStream = new FileInputStream(
					"C:/Users/waliu.faleye/JaspersoftWorkspace/MyReports/report1.jrxml");
			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);

			JasperExportManager.exportReportToPdfFile(jasperPrint,
					"C:/Users/waliu.faleye/JaspersoftWorkspace/MyReports/test2.pdf");
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	public String generateStatement(StatementDataParameters parameters, String fileType, InputStream inputStream,
			String fileName) {
		try {
			JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(
					parameters.getBankaccount_data_all());

			ObjectMapper oMapper = new ObjectMapper();
			
			System.out.println("parameters.getCustomerName()==="+parameters.getCustomerName());

			// object -> Map
			Map<String, Object> myParameters = oMapper.convertValue(parameters, Map.class);
			
			System.out.println("myParameters==="+myParameters);

			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, myParameters, beanColDataSource);

			JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return fileName;
	}

	public List<FITransactionDetail> getBankDetail() {
		List<FITransactionDetail> myData = new ArrayList<FITransactionDetail>();

		FITransactionDetail txnData = new FITransactionDetail();
		txnData.setAmountValue("1234");
		txnData.setCredit("11");
		txnData.setDebit("0.00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData.setPstdDate(LocalDate.now().format(formatter));
		txnData.setTxnDesc("testing");
		txnData.setTxnId("Q1234");
		txnData.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData);

		FITransactionDetail txnData1 = new FITransactionDetail();
		txnData1.setAmountValue("1234");
		txnData1.setCredit("11");
		txnData1.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData1.setPstdDate(LocalDate.now().format(formatter));
		txnData1.setTxnDesc("testing");
		txnData1.setTxnId("Q1234");
		txnData1.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData1);

		FITransactionDetail txnData2 = new FITransactionDetail();
		txnData2.setAmountValue("1234");
		txnData2.setCredit("11");
		txnData2.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData2.setPstdDate(LocalDate.now().format(formatter));
		txnData2.setTxnDesc("testing");
		txnData2.setTxnId("Q1234");
		txnData2.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData2);

		FITransactionDetail txnData3 = new FITransactionDetail();
		txnData3.setAmountValue("1234");
		txnData3.setCredit("11");
		txnData3.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData3.setPstdDate(LocalDate.now().format(formatter));
		txnData3.setTxnDesc("testing");
		txnData3.setTxnId("Q1234");
		txnData3.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData3);

		FITransactionDetail txnData4 = new FITransactionDetail();
		txnData4.setAmountValue("1234");
		txnData4.setCredit("11");
		txnData4.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData4.setPstdDate(LocalDate.now().format(formatter));
		txnData4.setTxnDesc("testing");
		txnData4.setTxnId("Q1234");
		txnData4.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData4);

		FITransactionDetail txnData5 = new FITransactionDetail();
		txnData5.setAmountValue("1234");
		txnData5.setCredit("11");
		txnData5.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData5.setPstdDate(LocalDate.now().format(formatter));
		txnData5.setTxnDesc("testing");
		txnData5.setTxnId("Q1234");
		txnData5.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData5);

		FITransactionDetail txnData6 = new FITransactionDetail();
		txnData6.setAmountValue("1234");
		txnData6.setCredit("11");
		txnData6.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData6.setPstdDate(LocalDate.now().format(formatter));
		txnData6.setTxnDesc("testing");
		txnData6.setTxnId("Q1234");
		txnData6.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData6);

		FITransactionDetail txnData7 = new FITransactionDetail();
		txnData7.setAmountValue("1234");
		txnData7.setCredit("11");
		txnData7.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData7.setPstdDate(LocalDate.now().format(formatter));
		txnData7.setTxnDesc("testing");
		txnData7.setTxnId("Q1234");
		txnData7.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData7);

		FITransactionDetail txnData8 = new FITransactionDetail();
		txnData8.setAmountValue("1234");
		txnData8.setCredit("11");
		txnData8.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData8.setPstdDate(LocalDate.now().format(formatter));
		txnData8.setTxnDesc("testing");
		txnData8.setTxnId("Q1234");
		txnData8.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData8);

		FITransactionDetail txnData9 = new FITransactionDetail();
		txnData9.setAmountValue("1234");
		txnData9.setCredit("11");
		txnData9.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData9.setPstdDate(LocalDate.now().format(formatter));
		txnData9.setTxnDesc("testing");
		txnData9.setTxnId("Q1234");
		txnData9.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData9);

		FITransactionDetail txnData10 = new FITransactionDetail();
		txnData10.setAmountValue("1234");
		txnData10.setCredit("11");
		txnData10.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData10.setPstdDate(LocalDate.now().format(formatter));
		txnData10.setTxnDesc("testing");
		txnData10.setTxnId("Q1234");
		txnData10.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData10);

		FITransactionDetail txnData11 = new FITransactionDetail();
		txnData11.setAmountValue("1234");
		txnData11.setCredit("11");
		txnData11.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData11.setPstdDate(LocalDate.now().format(formatter));
		txnData11.setTxnDesc("testing");
		txnData11.setTxnId("Q1234");
		txnData11.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData11);

		FITransactionDetail txnData12 = new FITransactionDetail();
		txnData12.setAmountValue("1234");
		txnData12.setCredit("11");
		txnData12.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData12.setPstdDate(LocalDate.now().format(formatter));
		txnData12.setTxnDesc("testing");
		txnData12.setTxnId("Q1234");
		txnData12.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData12);

		FITransactionDetail txnData13 = new FITransactionDetail();
		txnData13.setAmountValue("1234");
		txnData13.setCredit("11");
		txnData13.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData13.setPstdDate(LocalDate.now().format(formatter));
		txnData13.setTxnDesc("testing");
		txnData13.setTxnId("Q1234");
		txnData13.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData13);

		FITransactionDetail txnData14 = new FITransactionDetail();
		txnData14.setAmountValue("1234");
		txnData14.setCredit("11");
		txnData14.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData14.setPstdDate(LocalDate.now().format(formatter));
		txnData14.setTxnDesc("testing");
		txnData14.setTxnId("Q1234");
		txnData14.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData14);

		FITransactionDetail txnData15 = new FITransactionDetail();
		txnData15.setAmountValue("1234");
		txnData15.setCredit("11");
		txnData15.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData15.setPstdDate(LocalDate.now().format(formatter));
		txnData15.setTxnDesc("testing");
		txnData15.setTxnId("Q1234");
		txnData15.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData15);

		FITransactionDetail txnData16 = new FITransactionDetail();
		txnData16.setAmountValue("1234");
		txnData16.setCredit("11");
		txnData16.setDebit("0.00");
		// DateTimeFormatter formatter =
		// DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		txnData16.setPstdDate(LocalDate.now().format(formatter));
		txnData16.setTxnDesc("testing");
		txnData16.setTxnId("Q1234");
		txnData16.setValueDate(LocalDate.now().format(formatter));
		myData.add(txnData16);

		return myData;
	}

	public List<SecurityTransaction> getSecuritiesData() {
		List<SecurityTransaction> mySecData = new ArrayList<SecurityTransaction>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

		SecurityTransaction secData = new SecurityTransaction();
		secData.setAmount("2");
		secData.setDescription("testing");
		secData.setEffectiveDate(LocalDate.now().format(formatter));
		secData.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData);

		SecurityTransaction secData1 = new SecurityTransaction();
		secData1.setAmount("2");
		secData1.setDescription("testing");
		secData1.setEffectiveDate(LocalDate.now().format(formatter));
		secData1.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData1);

		SecurityTransaction secData2 = new SecurityTransaction();
		secData2.setAmount("2");
		secData2.setDescription("testing");
		secData2.setEffectiveDate(LocalDate.now().format(formatter));
		secData2.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData2);

		SecurityTransaction secData3 = new SecurityTransaction();
		secData3.setAmount("2");
		secData3.setDescription("testing");
		secData3.setEffectiveDate(LocalDate.now().format(formatter));
		secData3.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData3);

		SecurityTransaction secData4 = new SecurityTransaction();
		secData4.setAmount("2");
		secData4.setDescription("testing");
		secData4.setEffectiveDate(LocalDate.now().format(formatter));
		secData4.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData4);

		SecurityTransaction secData5 = new SecurityTransaction();
		secData5.setAmount("2");
		secData5.setDescription("testing");
		secData5.setEffectiveDate(LocalDate.now().format(formatter));
		secData5.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData5);

		SecurityTransaction secData6 = new SecurityTransaction();
		secData6.setAmount("2");
		secData6.setDescription("testing");
		secData6.setEffectiveDate(LocalDate.now().format(formatter));
		secData6.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData6);

		SecurityTransaction secData7 = new SecurityTransaction();
		secData7.setAmount("2");
		secData7.setDescription("testing");
		secData7.setEffectiveDate(LocalDate.now().format(formatter));
		secData7.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData7);

		SecurityTransaction secData8 = new SecurityTransaction();
		secData8.setAmount("2");
		secData8.setDescription("testing");
		secData8.setEffectiveDate(LocalDate.now().format(formatter));
		secData8.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData8);

		SecurityTransaction secData9 = new SecurityTransaction();
		secData9.setAmount("2");
		secData9.setDescription("testing");
		secData9.setEffectiveDate(LocalDate.now().format(formatter));
		secData9.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData9);

		SecurityTransaction secData10 = new SecurityTransaction();
		secData10.setAmount("2");
		secData10.setDescription("testing");
		secData10.setEffectiveDate(LocalDate.now().format(formatter));
		secData10.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData10);

		SecurityTransaction secData11 = new SecurityTransaction();
		secData11.setAmount("2");
		secData11.setDescription("testing");
		secData11.setEffectiveDate(LocalDate.now().format(formatter));
		secData11.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData11);

		SecurityTransaction secData12 = new SecurityTransaction();
		secData12.setAmount("2");
		secData12.setDescription("testing");
		secData12.setEffectiveDate(LocalDate.now().format(formatter));
		secData12.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData12);

		SecurityTransaction secData13 = new SecurityTransaction();
		secData13.setAmount("2");
		secData13.setDescription("testing");
		secData13.setEffectiveDate(LocalDate.now().format(formatter));
		secData13.setTxnDate(LocalDate.now().format(formatter));
		mySecData.add(secData13);

		return mySecData;
	}

}
