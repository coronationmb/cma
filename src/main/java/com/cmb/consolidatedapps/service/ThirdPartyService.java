/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRp;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.CustomerBankAccountBalance;
import com.cmb.consolidatedapps.model.CustomerBankAccountMonthData;
import com.cmb.consolidatedapps.model.DailyLimitBalanceResponse;
import com.cmb.consolidatedapps.model.LoanAccountDetail;
import com.cmb.consolidatedapps.model.OverdraftAccountDetail;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.model.TDAccountDetail;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.SecurityResponseData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

/**
 * @author waliu.faleye
 *
 */
// @Service
public class ThirdPartyService {

	private String baseUrl;

	public AssetManagementResponseData assetMngmtLogin() {
		AssetManagementResponseData myResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/login");
		System.out.println("url==" + url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

	public AssetManagementResponseData getCustomerSubscribedFunds(String sessionId, String custAID) {
		AssetManagementResponseData myResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getCustomerSubscribedFunds?sessionId=").concat(sessionId)
				.concat("&custAID=").concat(custAID);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

	public AssetManagementResponseData getMutualFundProducts(String sessionId) {
		AssetManagementResponseData myResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		System.out.println("inside sessionId==" + sessionId);
		String url = this.getBaseUrl().concat("/getMutualFundProducts?sessionId=").concat(sessionId);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;

	}

	public AssetManagementResponseData getCustomerCurrentPortfolioValue(String sessionId, String custAID) {
		AssetManagementResponseData myResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getCustomerCurrentPortfolioValue?sessionId=").concat(sessionId)
				.concat("&custAID=").concat(custAID);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

	public AssetManagementResponseData getFundInvestmentSummary(String sessionId, String custAID, String fundCode) {
		AssetManagementResponseData myResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getFundInvestmentSummary?sessionId=").concat(sessionId)
				.concat("&custAID=").concat(custAID).concat("&fundCode=").concat(fundCode);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

	public SecurityResponseData securityLogin() {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/login");
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;
	}

	public SecurityResponseData getTotalPortfolioHoldings(String sessionId, String custAID, String date,
			String showAsAssetPerClass) {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getTotalPortfolioHoldings?sessionId=").concat(sessionId)
				.concat("&custAID=").concat(custAID).concat("&date=").concat(date).concat("&ShowAsAssetPerClass=")
				.concat(showAsAssetPerClass);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;

	}

	public SecurityResponseData getLosersGainers(String sessionId, String MDFnID) {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getLosersGainers?sessionId=").concat(sessionId).concat("&MDFnID=")
				.concat(MDFnID);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;

	}

	public SecurityResponseData fundAccountSecurities(String sessionId, String CRAccountSub, String transactionId,
			String amount, String narration) {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/fundAccount?sessionId=").concat(sessionId).concat("&CRAccountSub=")
				.concat(CRAccountSub).concat("&transactionId=").concat(transactionId).concat("&amount=").concat(amount)
				.concat("&narration=").concat(narration);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;

	}

	public AssetManagementResponseData cashBalanceAssetMgt(String sessionId, String CRAccountSub) {
		AssetManagementResponseData amResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/cashBalance?sessionId=").concat(sessionId).concat("&CRAccountSub=")
				.concat(CRAccountSub);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			amResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return amResponse;

	}

	public AssetManagementResponseData fundAccountAssetMgt(String sessionId, String CRAccountSub, String transactionId,
			String amount, String narration, String assetManagementCrAccountMaster,
			String assetManagementDrAccountMaster, String assetManagementDrAccountSub) {
		AssetManagementResponseData amResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/fundAccount?sessionId=").concat(sessionId).concat("&CRAccountSub=")
				.concat(CRAccountSub).concat("&transactionId=").concat(transactionId).concat("&amount=").concat(amount)
				.concat("&narration=").concat(narration).concat("&assetManagementCrAccountMaster=")
				.concat(assetManagementCrAccountMaster).concat("&assetManagementDrAccountMaster=")
				.concat(assetManagementDrAccountMaster).concat("&assetManagementDrAccountSub=")
				.concat(assetManagementDrAccountSub);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			amResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return amResponse;

	}

	public AssetManagementResponseData getCustomerFundAccountStatement(String sessionId, String custAID,
			String fundCode) {
		AssetManagementResponseData amResponse = new AssetManagementResponseData();
		OkHttpClient client = new OkHttpClient();
		System.out.println("custAID===" + custAID);
		System.out.println("fundCode===" + fundCode);
		System.out.println("sessionId===" + sessionId);
		if (fundCode != null) {
			String url = this.getBaseUrl().concat("/getCustomerFundAccountStatement?sessionId=").concat(sessionId)
					.concat("&custAID=").concat(custAID).concat("&fundCode=").concat(fundCode);
			System.out.println(url);
			Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

			try {
				Gson gson = new Gson();

				Response response = client.newCall(request).execute();
				String bodyStr = response.body().string();
				System.out.println(bodyStr);
				amResponse = gson.fromJson(bodyStr, AssetManagementResponseData.class);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return amResponse;

	}

	public List<SubsidiaryCustomerDetail> getAMCustomerDetailFromBvn(String bvn) {
		List<SubsidiaryCustomerDetail> retStr = new ArrayList<SubsidiaryCustomerDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.136:8091/cmbreport/getAMCustomerDetailFromBvn?bvn=".concat(bvn)).get()
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			// List<Contact> contacts;
			Type listType = new TypeToken<List<SubsidiaryCustomerDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;
	}

	public List<SubsidiaryCustomerDetail> getSecCustomerDetailFromBvn(String bvn) {
		List<SubsidiaryCustomerDetail> retStr = new ArrayList<SubsidiaryCustomerDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.136:8091/cmbreport/getSecCustomerDetailFromBvn?bvn=".concat(bvn)).get()
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<SubsidiaryCustomerDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;
	}

	public List<SubsidiaryCustomerDetail> getAMCustomerDetailFromCustomerAid(String custAid) {
		List<SubsidiaryCustomerDetail> retStr = new ArrayList<SubsidiaryCustomerDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.136:8091/cmbreport/getAMCustomerDetailFromCustomerAid?custAid=".concat(custAid))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<SubsidiaryCustomerDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;
	}

	public List<SubsidiaryCustomerDetail> getSecCustomerDetailFromCustomerAid(String custAid) {
		List<SubsidiaryCustomerDetail> retStr = new ArrayList<SubsidiaryCustomerDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.136:8091/cmbreport/getSecCustomerDetailFromCustomerAid?custAid="
						.concat(custAid))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<SubsidiaryCustomerDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl
	 *            the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public List<CustomerBankAccount> getBankCustomerAccountDetailsFromBvn(String bvn) {
		List<CustomerBankAccount> retStr = new ArrayList<CustomerBankAccount>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankCustomerAccountDetailsFromBvn?bvn=".concat(bvn)).get()
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<CustomerBankAccount>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public List<ApplicationCustomerDetailRp> getBankCustomerDetailsFromBvn(String bvn) {
		List<ApplicationCustomerDetailRp> retStr = new ArrayList<ApplicationCustomerDetailRp>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankCustomerDetailsFromBvn?bvn=".concat(bvn)).get()
				.addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<ApplicationCustomerDetailRp>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public List<CustomerBankAccountMonthData> getBankAccountMonthData(String acid, String firstDay, String lastDay) {
		List<CustomerBankAccountMonthData> retStr = new ArrayList<CustomerBankAccountMonthData>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankAccountMonthData?acid=".concat(acid)
						.concat("&firstDay=").concat(firstDay).concat("&lastDay=").concat(lastDay))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<CustomerBankAccountMonthData>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public List<LoanAccountDetail> getBankLoanAccountDetail(String cifId, String date) {
		List<LoanAccountDetail> retStr = new ArrayList<LoanAccountDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankLoanAccountDetail?cifId=".concat(cifId).concat("&date=")
						.concat(date))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<LoanAccountDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public List<TDAccountDetail> getBankTDAccountDetail(String cifId, String date) {
		List<TDAccountDetail> retStr = new ArrayList<TDAccountDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankTDAccountDetail?cifId=".concat(cifId).concat("&date=")
						.concat(date))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<TDAccountDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public List<OverdraftAccountDetail> getBankOverdraftAccountDetail(String cifId, String date) {
		List<OverdraftAccountDetail> retStr = new ArrayList<OverdraftAccountDetail>();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankOverdraftAccountDetail?cifId=".concat(cifId)
						.concat("&date=").concat(date))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			// retStr = (List<SubsidiaryCustomerDetail>) gson.fromJson(bodyStr,
			// SubsidiaryCustomerDetail.class);
			Type listType = new TypeToken<List<OverdraftAccountDetail>>() {
			}.getType();
			retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public CustomerBankAccountBalance getBankCustomerAccountBalance(String accountNumber, String date) {
		CustomerBankAccountBalance retStr = new CustomerBankAccountBalance();
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url("http://132.10.200.140:9292/service/getBankCustomerAccountBalance?accountNumber="
						.concat(accountNumber).concat("&transactionDate=").concat(date))
				.get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "44c058f5-d927-dc19-6b26-d2b360ff0dfc").build();
		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			retStr = gson.fromJson(bodyStr, CustomerBankAccountBalance.class);
			retStr.setClrBalanceAmount(
					retStr.getClrBalanceAmount() != null ? retStr.getClrBalanceAmount() : BigDecimal.ZERO);
			// Type listType = new TypeToken<List<CustomerBankAccountBalance>>()
			// {
			// }.getType();
			// retStr = gson.fromJson(bodyStr, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retStr;

	}

	public SecurityResponseData getStockData(String sessionId, String custAID) {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getStockData?sessionId=").concat(sessionId).concat("&custAid=")
				.concat(custAID);
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;

	}

	public SecurityResponseData getSecurityTransactionsData(String custAID, String startDate, String endDate) {
		SecurityResponseData secResponse = new SecurityResponseData();
		OkHttpClient client = new OkHttpClient();
		String url = this.getBaseUrl().concat("/getSecCustomerStatement?sessionId=")
				.concat(this.securityLogin().getOutValue()).concat("&custAID=").concat(custAID).concat("&startDate=")
				.concat(startDate).concat("&endDate=").concat(endDate).concat("&ccyRate=1&includeReversals=0");
		System.out.println(url);
		Request request = new Request.Builder().url(url).get().addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			// System.out.println(bodyStr);
			secResponse = gson.fromJson(bodyStr, SecurityResponseData.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return secResponse;

	}

	public DailyLimitBalanceResponse accountDailyBalance(String channel, String accountNumber) {
		DailyLimitBalanceResponse myResponse = new DailyLimitBalanceResponse();
		OkHttpClient client = new OkHttpClient();
		String url = "http://132.10.200.140:9292/service/accountDailyBalance";
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, " {\r\n \"transactionChannel\": \"" + channel
				+ "\", \r\n \"accountNumber\":\"" + accountNumber + "\"\r\n}");
		System.out.println("url==" + url);
		Request request = new Request.Builder().url(url).post(body).addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, DailyLimitBalanceResponse.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

	public DailyLimitBalanceResponse accountDailyTranLimit(String channel, String accountNumber) {
		DailyLimitBalanceResponse myResponse = new DailyLimitBalanceResponse();
		OkHttpClient client = new OkHttpClient();
		String url = "http://132.10.200.140:9292/service/accountDailyTranLimit";
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, " {\r\n \"transactionChannel\": \"" + channel
				+ "\", \r\n \"accountNumber\":\"" + accountNumber + "\"\r\n}");
		System.out.println("url==" + url);
		Request request = new Request.Builder().url(url).post(body).addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {
			Gson gson = new Gson();

			Response response = client.newCall(request).execute();
			String bodyStr = response.body().string();
			System.out.println(bodyStr);
			myResponse = gson.fromJson(bodyStr, DailyLimitBalanceResponse.class);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return myResponse;
	}

}
