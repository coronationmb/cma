/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.ReportGeneratorInterface;
import com.cmb.consolidatedapps.model.CustomerBankAccountBalance;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.FITransactionDetail;
import com.cmb.consolidatedapps.model.StatementData;
import com.cmb.consolidatedapps.model.StatementDataParameters;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.model.TopupTransactionDetail;
import com.cmb.consolidatedapps.model.TransactionDetail;
import com.cmb.consolidatedapps.model.TransactionHistoryData;
import com.cmb.thirdpartyapi.model.AMFundAccountStatementRows;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.SecurityTransaction;
import com.google.common.base.Strings;
import com.infosys.ci.fiusb.webservice.FIUsbWebServiceService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * @author waliu.faleye
 *
 */
@Service
public class ReportGenerator implements ReportGeneratorInterface {
	@Value("${job.reporttemplate.path}")
	String templatePath;
	@Value("${job.reportgenerated.path}")
	String reportGeneratedPath;
	@Value("${reciept.file.template}")
	String recieptFileTemplate;
	@Value("${consolidated.statement.file.template}")
	String consolidatedStatementFileTemplate;
	@Value("${securities.statement.file.template}")
	String securitiesStatementFileTemplate;
	@Value("${assetmanagement.statement.file.template}")
	String assetManagementStatementFileTemplate;
	ProcessInterfaces processInterface;

	@Value("${thirdpartyapi.baseurl.assetmgt}")
	String thirdpartyapiBaseUrlAssetMgt;

	@Value("${thirdpartyapi.baseurl.localapi}")
	String thirdpartyapiBaseUrlLocal;

	@Value("${thirdpartyapi.baseurl.securities}")
	String thirdpartyapiBaseUrlSec;

	public ReportGenerator(ProcessInterfaces processInterface) {
		this.processInterface = processInterface;
	}

	@Override
	public String generateTransactionReciept(TransactionDetail data) {
		// TODO Auto-generated method stub
		String pdfFileName = "";
		try {
			DateTimeFormatter reportFileDateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HHmm");
			pdfFileName = reportGeneratedPath.concat(data.getSourceAccountNumber()).concat("_")
					.concat(LocalDateTime.now().format(reportFileDateformatter)).concat(".pdf");
			InputStream inputStream = new FileInputStream(templatePath.concat(recieptFileTemplate));
			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			String toAccountDetails = "";
			String reference = "";
			if (data.getCustomerBeneficiary() != null) {
				System.out.println(
						"exist beneficiary name===" + data.getCustomerBeneficiary().getBeneficiaryAccountName());
				System.out.println(
						"exist beneficiary name===" + data.getCustomerBeneficiary().getBeneficiaryAccountNumber());
				toAccountDetails = data.getCustomerBeneficiary().getBeneficiaryAccountName();
				toAccountDetails = toAccountDetails.concat(" ");
				toAccountDetails = toAccountDetails.concat(data.getCustomerBeneficiary().getBeneficiaryAccountNumber());
			} else {
				System.out.println(data.getDestinationAccountName());
				System.out.println(data.getDestinationAccountNumber());
				toAccountDetails = data.getDestinationAccountName();
				toAccountDetails = toAccountDetails.concat(" ");
				toAccountDetails = toAccountDetails.concat(data.getDestinationAccountNumber());
			}
			if (data.getTransactionType() != null) {
				if (data.getTransactionType().getId().equals(3L)) {
					reference = data.getFtSingleCreditSessionId();
				} else {
					reference = data.getUniqueIdentifier();
				}
			} else {

			}
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");
			HashMap parameters = new HashMap();
			parameters.put("transactionType", "Transfer Details");
			parameters.put("fromAccountDetails", data.getUser().getCustomer().getName().concat(" ").concat("******")
					.concat(data.getSourceAccountNumber().substring(data.getSourceAccountNumber().length() - 4)));
			parameters.put("toAccountDetails", toAccountDetails);
			parameters.put("transactionDate", data.getRequestDate().format(formatter));
			parameters.put("reference", reference);
			parameters.put("recipientBank", data.getFinancialInstitution().getInstitutionName());
			parameters.put("amount", formatAmount(data.getAmount()));
			parameters.put("remarks", data.getRemark());
			List<TransactionDetail> details = new ArrayList<TransactionDetail>();
			details.add(data);
			JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(details);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
		} catch (JRException | FileNotFoundException ex) {
			ex.printStackTrace();
			// Logger.getLogger(JasperTutorial2.class.getName()).log(Level.SEVERE,
			// null, ex);
		}
		return pdfFileName;
	}

	@Override
	public String generateTopupReciept(TopupTransactionDetail data) {
		// TODO Auto-generated method stub
		String pdfFileName = "";
		try {
			DateTimeFormatter reportFileDateformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HHmm");
			pdfFileName = reportGeneratedPath.concat(data.getAccountNumber()).concat("_")
					.concat(LocalDateTime.now().format(reportFileDateformatter)).concat(".pdf");
			InputStream inputStream = new FileInputStream(templatePath.concat(recieptFileTemplate));
			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			String toAccountDetails = "";
			String reference = "";
			if (data.getCustomerSubsidiaryBeneficiary() != null) {
				toAccountDetails = data.getCustomerSubsidiaryBeneficiary().getBeneficiaryCustomerName();
				toAccountDetails = toAccountDetails.concat(" ");
				toAccountDetails = toAccountDetails
						.concat(data.getCustomerSubsidiaryBeneficiary().getBeneficiaryCustomerAid());
			} else {
				toAccountDetails = data.getCustomerName();
				toAccountDetails = toAccountDetails.concat(" ");
				toAccountDetails = toAccountDetails.concat(data.getCustomerAid());
			}

			reference = data.getUniqueIdentifier();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");
			HashMap parameters = new HashMap();
			parameters.put("transactionType", "Top Up Details");
			parameters.put("fromAccountDetails", data.getUser().getCustomer().getName().concat(" ").concat("******")
					.concat(data.getAccountNumber().substring(data.getAccountNumber().length() - 4)));
			parameters.put("toAccountDetails", toAccountDetails);
			parameters.put("transactionDate", data.getRequestDate().format(formatter));
			parameters.put("reference", reference);
			parameters.put("recipientBank", data.getApplication().getName());
			parameters.put("amount", formatAmount(data.getAmount()));
			parameters.put("remarks", data.getRemark());
			List<TopupTransactionDetail> details = new ArrayList<TopupTransactionDetail>();
			details.add(data);
			JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(details);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
		} catch (JRException | FileNotFoundException ex) {
			ex.printStackTrace();
			// Logger.getLogger(JasperTutorial2.class.getName()).log(Level.SEVERE,
			// null, ex);
		}
		return pdfFileName;
	}

	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}

	@Override
	public String generateStatementPdf(StatementData data) {
		// TODO Auto-generated method stub
		String fileName = "";
		StatementDataParameters parameters = new StatementDataParameters();
		AssetManagementResponseData amLoginRes = new AssetManagementResponseData();
		AssetManagementResponseData amFundAcctStmntRes = new AssetManagementResponseData();
		try {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession(true);
			CustomerDashboardDetail dashboardDetails = (CustomerDashboardDetail) session
					.getAttribute("dashboardDetail");
			fileName = reportGeneratedPath.concat("cmaStatement").concat("-")
					.concat(dashboardDetails.getUser().getUserId())
					.concat("P".equals(data.getFileType()) ? ".pdf" : "E".equals(data.getFileType()) ? ".xls" : "");

			ThirdPartyService tps = new ThirdPartyService();
			List<SecurityTransaction> secRows = new ArrayList<SecurityTransaction>();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			DateTimeFormatter displayFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			DateTimeFormatter txtDisplayFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			DateTimeFormatter dbDateFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
			LocalDateTime now = LocalDateTime.now();
			String endDate = LocalDateTime
					.of(LocalDate.parse(data.getEndDate(), txtDisplayFormatter), LocalTime.parse("23:59:59.799"))
					.toString();
			// LocalDateTime ldt = LocalDateTime.of(ld,
			// LocalDateTime.now().toLocalTime());
			String fromDate = LocalDateTime
					.of(LocalDate.parse(data.getStartDate(), txtDisplayFormatter), LocalTime.parse("00:00:01.799"))
					.toString();
			String templateFile = "";
			InputStream inputStream = null;
			// CustomerDashboardDetail dashboardDetails =
			// processInterface.dashboardDetails(user);
			if (Long.valueOf("1").equals(data.getApp1())) {
				templateFile = consolidatedStatementFileTemplate;
				parameters.setApp1(data.getApp1());
				BigDecimal totalDebitAmt = BigDecimal.ZERO;
				BigDecimal totalCreditAmt = BigDecimal.ZERO;
				String accountNumber = data.getAccountNumber();
				CustomerBankAccountBalance cbab = tps.getBankCustomerAccountBalance(accountNumber, LocalDateTime
						.of(LocalDate.parse(data.getStartDate(), txtDisplayFormatter), LocalTime.parse("00:00:00"))
						.format(dbDateFormatter));
				parameters.setAccountNo(accountNumber);
				parameters.setOpeningBalance(processInterface
						.formatAmount(cbab.getOpeningBalance() == null ? BigDecimal.ZERO : cbab.getOpeningBalance()));
				parameters.setCustomerName(dashboardDetails.getUser().getCustomer().getName());
				parameters.setAddress(cbab.getAddress());
				// parameters.setClosingBalance(formatAmount(cbab.getClosingBalance()));
				parameters.setAvailableBalance(formatAmount(cbab.getCurrentAvailableBalance()));
				FIUsbWebServiceService svc = new FIUsbWebServiceService();

				String reqUuid = "Req-".concat(dashboardDetails.getUser().getId().toString()).concat("-")
						.concat(now.format(formatter));
				List<FITransactionDetail> custBankTranList = new ArrayList<FITransactionDetail>();
				FITransactionDetail lastTranDetail = new FITransactionDetail();
				int i = 0;
				do {
					System.out.println("lastTranDetail.getHasMoreData()==" + lastTranDetail.getHasMoreData());
					System.out.println("now==" + now);
					System.out.println("accountNumber==" + accountNumber);
					System.out.println("fromDate==" + fromDate);
					System.out.println("endDate==" + endDate);
					System.out.println("lastTranDetail.getAmountValue()==" + lastTranDetail.getAmountValue());
					List<FITransactionDetail> bankTranList = svc.getAccountStatement(reqUuid.concat(String.valueOf(i)),
							now.toString(), accountNumber, fromDate, endDate, lastTranDetail);
					System.out.println("bankTranList.size()==" + bankTranList.size());
					custBankTranList.addAll(bankTranList);
					if (!bankTranList.isEmpty())
						lastTranDetail = bankTranList.get(bankTranList.size() - 1);
					i++;
				} while ("Y".equals(lastTranDetail.getHasMoreData()));
				System.out.println("bankTranList.size()==" + custBankTranList.size());
				custBankTranList.forEach(a -> a.setPstdDateDt(LocalDateTime.parse(a.getPstdDate())));
				custBankTranList
						.forEach(a -> a.setPstdDate(LocalDateTime.parse(a.getPstdDate()).format(displayFormatter)));
				custBankTranList
						.forEach(a -> a.setValueDate(LocalDateTime.parse(a.getValueDate()).format(displayFormatter)));
				// Comparator<FITransactionDetail> pstdDateComparator = (o1, o2)
				// ->
				// o1.getPstdDateDt()
				// .compareTo(o2.getPstdDateDt());
				// bankTranList.sort(pstdDateComparator.reversed());
				custBankTranList.sort((FITransactionDetail s1, FITransactionDetail s2) -> s1.getPstdDateDt()
						.compareTo(s2.getPstdDateDt()));
				List<FITransactionDetail> creditList = custBankTranList.stream()
						.filter(a -> "C".equals(a.getFiTxnSummary().getTxnType())).collect(Collectors.toList());
				List<FITransactionDetail> debitList = custBankTranList.stream()
						.filter(a -> "D".equals(a.getFiTxnSummary().getTxnType())).collect(Collectors.toList());

				// creditList.forEach(
				// a -> totalCreditAmt.add(new
				// BigDecimal(a.getFiTxnSummary().getFiTxnAmount().getAmountValue())));
				if (!creditList.isEmpty())
					totalCreditAmt = creditList.stream()
							.map(a -> new BigDecimal(a.getFiTxnSummary().getFiTxnAmount().getAmountValue()))
							.reduce(BigDecimal::add).get();
				// debitList.forEach(
				// a -> totalDebitAmt.add(new
				// BigDecimal(a.getFiTxnSummary().getFiTxnAmount().getAmountValue())));
				if (!debitList.isEmpty())
					totalDebitAmt = debitList.stream()
							.map(a -> new BigDecimal(a.getFiTxnSummary().getFiTxnAmount().getAmountValue()))
							.reduce(BigDecimal::add).get();
				custBankTranList.forEach(a -> parameters.setCurrency(a.getFiTxnBalance().getCurrencyCode()));
				if (!custBankTranList.isEmpty()) {
					parameters.setClosingBalance(formatAmount(
							new BigDecimal(custBankTranList.get(custBankTranList.size() - 1).getAmountValue())));
					custBankTranList.forEach(a -> a.setCredit(formatAmount(new BigDecimal(a.getCredit()))));
					custBankTranList.forEach(a -> a.setDebit(formatAmount(new BigDecimal(a.getDebit()))));
					custBankTranList.forEach(a -> a.setAmountValue(formatAmount(new BigDecimal(a.getAmountValue()))));
				}

				parameters.setCurrency(cbab.getCurrency());
				parameters.setBankaccount_data_all(custBankTranList);
				parameters.setCreditCount(String.valueOf(creditList.size()));
				parameters.setDebitCount(String.valueOf(debitList.size()));
				parameters.setTotalCredit(formatAmount(totalCreditAmt));
				parameters.setTotalDebit(formatAmount(totalDebitAmt));
				//parameters.setCurrency(currencyCode);
			}
			// data.setBankStmtFromDate(now.minusMonths(1L).format(txtDisplayFormatter));
			// data.setBankStmtToDate(now.format(txtDisplayFormatter));

			ThirdPartyService thirdServ = new ThirdPartyService();
			if (Long.valueOf("2").equals(data.getApp2())) {
				if(Strings.isNullOrEmpty(templateFile))
				templateFile = assetManagementStatementFileTemplate;
				parameters.setApp2(data.getApp2());
				BigDecimal totalDebitAmt = BigDecimal.ZERO;
				BigDecimal totalCreditAmt = BigDecimal.ZERO;
				List<AMFundAccountStatementRows> rows = new ArrayList<AMFundAccountStatementRows>();
				List<SubsidiaryCustomerDetail> scdList = dashboardDetails.getAmCustAIDList();
				scdList.stream().filter(a -> a.getFundCode() != null).collect(Collectors.toList());
				if (scdList.size() > 0) {
					List<SubsidiaryCustomerDetail> validScdList = dashboardDetails.getAmCustAIDList().stream()
							.filter(a -> !Strings.isNullOrEmpty(a.getFundCode())).collect(Collectors.toList());
					SubsidiaryCustomerDetail scd = validScdList.isEmpty() ? new SubsidiaryCustomerDetail()
							: validScdList.get(0);
					parameters.setAmCustomerAID(scd.getCustomerAid() == null ? "" : scd.getCustomerAid());
					parameters.setAmAddress(scd.getAddress1());
					thirdServ.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
					amLoginRes = thirdServ.assetMngmtLogin();

					// List<String> val =
					// Arrays.asList(data.getAmCustAID().split("#"));

					if (amLoginRes.getStatusId() != null && amLoginRes.getStatusId().equals(0L)) {
						amFundAcctStmntRes = thirdServ.getCustomerFundAccountStatement(amLoginRes.getOutValue(),
								scd.getCustomerAid(), scd.getFundCode());
						if (amFundAcctStmntRes.getStatusId() != null && amFundAcctStmntRes.getStatusId().equals(0L)) {
							rows = amFundAcctStmntRes.getDataTable().getAmfundAccountStatementRows();
						}
					}
					// System.out.println("data.getAmTransactionsDetail().size()=="
					// + data.getAmTransactionsDetail().size());
				}
				/*
				 * for (AMFundAccountStatementRows detail : rows) { try {
				 * System.out.println(detail.getCredit());
				 * System.out.println(new BigDecimal(detail.getCredit())); }
				 * catch (Exception ex) { ex.printStackTrace(); } }
				 */
				List<AMFundAccountStatementRows> creditList = rows.stream()
						.filter(a -> new BigDecimal(a.getCredit()).compareTo(BigDecimal.ZERO) > 0)
						.collect(Collectors.toList());
				List<AMFundAccountStatementRows> debitList = rows.stream()
						.filter(a -> new BigDecimal(a.getDebit()).compareTo(BigDecimal.ZERO) < 0)
						.collect(Collectors.toList());

				// creditList.forEach(a -> totalCreditAmt.add(new
				// BigDecimal(a.getCredit())));
				if (!creditList.isEmpty())
					totalCreditAmt = creditList.stream().map(a -> new BigDecimal(a.getCredit())).reduce(BigDecimal::add)
							.get();
				// debitList.forEach(a -> totalDebitAmt.add(new
				// BigDecimal(a.getDebit())));
				if (!debitList.isEmpty())
					totalDebitAmt = debitList.stream().map(a -> new BigDecimal(a.getDebit())).reduce(BigDecimal::add)
							.get();
				parameters.setAmCreditCount(String.valueOf(creditList.size()));
				parameters.setAmDebitCount(String.valueOf(debitList.size()));
				parameters.setAmTotalCredit(formatAmount(totalCreditAmt));
				parameters.setAmTotalDebit(formatAmount(totalDebitAmt));
				rows.forEach(a -> a.setCredit(processInterface.formatAmount(new BigDecimal(a.getCredit()))));
				rows.forEach(a -> a.setDebit(processInterface.formatAmount(new BigDecimal(a.getDebit()))));
				Comparator<AMFundAccountStatementRows> valueDateComparator = (o1, o2) -> o1.getValueDate()
						.compareTo(o2.getValueDate());
				rows.forEach(a -> a.setBalance("0.00".equals(a.getCredit())? "0.00":a.getCredit()));
				rows.forEach(a -> a.setCredit("0.00".equals(a.getCredit())? "":a.getCredit()));
				rows.forEach(a -> a.setDebit("0.00".equals(a.getDebit())? "":a.getDebit()));
				rows.sort(valueDateComparator.reversed());
				parameters.setAm_data_all(rows);

			}

			if (Long.valueOf("3").equals(dashboardDetails.getApp3())) {
				if(Strings.isNullOrEmpty(templateFile))
				templateFile = securitiesStatementFileTemplate;
				parameters.setApp3(data.getApp3());
				BigDecimal totalDebitAmt = BigDecimal.ZERO;
				BigDecimal totalCreditAmt = BigDecimal.ZERO;
				// data.setSecStmtFromDate(now.minusMonths(1L).format(txtDisplayFormatter));
				// data.setSecStmtToDate(now.format(txtDisplayFormatter));
				thirdServ.setBaseUrl(null);
				thirdServ.setBaseUrl(thirdpartyapiBaseUrlSec);
				if (dashboardDetails.getSecCustAIDList().size() > 0) {
					//List<SubsidiaryCustomerDetail> validScdList = dashboardDetails.getSecCustAIDList().stream()
					//		.filter(a -> Strings.isNullOrEmpty(a.getFundCode())).collect(Collectors.toList());
					//SubsidiaryCustomerDetail scdLocal = validScdList.isEmpty() ? new SubsidiaryCustomerDetail()
					//		: validScdList.get(0);
					for (SubsidiaryCustomerDetail scd : dashboardDetails.getSecCustAIDList()) {
						secRows = thirdServ
								.getSecurityTransactionsData(scd.getCustomerAid() == null ? "" : scd.getCustomerAid(),
										fromDate,
										endDate)
								.getDataTable().getSecurityTransactions();
						parameters.setSecCustomerAID(scd.getCustomerAid() == null ? "" : scd.getCustomerAid());
						parameters.setSecAddress(scd.getAddress1());
					}
					System.out.println("secRows.size()===" + secRows.size());

					parameters.setSecurity_data_all(secRows);
				}
				List<SecurityTransaction> creditList = secRows.stream()
						.filter(a -> new BigDecimal(a.getAmount()).compareTo(BigDecimal.ZERO) > 0)
						.collect(Collectors.toList());
				List<SecurityTransaction> debitList = secRows.stream()
						.filter(a -> new BigDecimal(a.getAmount()).compareTo(BigDecimal.ZERO) < 0)
						.collect(Collectors.toList());

				//creditList.forEach(a -> totalCreditAmt.add(new BigDecimal(a.getAmount())));
				//debitList.forEach(a -> totalCreditAmt.add(new BigDecimal(a.getAmount())));
				if (!creditList.isEmpty())
					totalCreditAmt = creditList.stream().map(a -> new BigDecimal(a.getAmount())).reduce(BigDecimal::add)
							.get();
				if (!debitList.isEmpty())
					totalDebitAmt = debitList.stream().map(a -> new BigDecimal(a.getAmount())).reduce(BigDecimal::add)
							.get();
				parameters.setSecCreditCount(String.valueOf(creditList.size()));
				parameters.setSecDebitCount(String.valueOf(debitList.size()));
				parameters.setSecTotalCredit(formatAmount(totalCreditAmt));
				parameters.setSecTotalDebit(formatAmount(totalDebitAmt));
			}

			if (Long.valueOf("4").equals(dashboardDetails.getApp4())) {
				parameters.setApp4(data.getApp4());

			}
			inputStream = new FileInputStream(templatePath.concat(consolidatedStatementFileTemplate));
			OpenReport rp = new OpenReport();
			rp.generateStatement(parameters, data.getFileType(), inputStream, fileName);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return fileName;
	}

}
