/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.io.File;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.assertj.core.util.Strings;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 * @author waliu.faleye
 *
 */
@Service
public class MailService {

	private JavaMailSender mailSender;
	private String username;
	private String toEmail;
	private String fromEmail;
	private String ccEmail;
	private String subject;
	private String filePath;
	private String[] attachments;
	private String content;
	private String emailBody;
	private boolean htmlEnabled;

	public void sendEmail() {

		// MimeMessagePreparator preparator = getMessagePreparator();
		MimeMessage message = getMailSender().createMimeMessage();

		// use the true flag to indicate you need a multipart message
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			// helper.setSubject("Coronation Merchant Bank Report");
			helper.setSubject(this.getSubject());
			helper.setFrom(new InternetAddress(getFromEmail()));
			if (getToEmail().split(",").length > 1) {
				helper.setTo(InternetAddress.parse(getToEmail()));
			} else {
				helper.setTo(InternetAddress.parse(getToEmail()));
			}
			System.out.println("sdffffffff getToEmail() == " + getToEmail());
			// helper.setTo(this.formatAddress(getToEmail()));
			if (this.getCcEmail() != null && !"".equals(this.getCcEmail())) {
				if (getCcEmail().split(",").length > 1) {
					helper.setCc(InternetAddress.parse(getCcEmail()));
				} else if (getCcEmail().split(",").length == 1) {

					helper.setCc(new InternetAddress(getCcEmail()));
				}
			}
			System.out.println("sdffffffff getCcEmail() == " + getCcEmail());
			// helper.setCc(this.formatAddress(getCcEmail()));
			String content = "Dear " + getUsername() + ", \n\nPlease find attached the report generated.\n\nRegards,";
			if (!Strings.isNullOrEmpty(this.getEmailBody())) {
				content = this.getEmailBody();
			}
			helper.setText(content);
			if (this.getHtmlEnabled())
				helper.setText(content, true);

			// File file1 = new
			// File("C:/system/NetBeansProjects/sample_report.xls");
			// FileSystemResource fr = new FileSystemResource(file1);
			// helper.addAttachment(file1.getName(), fr);
			// File file2 = new
			// File("C:/system/NetBeansProjects/cooperate-bank-rpt1.pdf");
			// FileSystemResource fr2 = new FileSystemResource(file2);
			// helper.addAttachment(file2.getName(), fr2);
			// add the file attachment
			System.out.println("Message Send...Hurrey" + getAttachments());
			if (this.getAttachments() != null) {
				for (String file : getAttachments()) {
					if (!Strings.isNullOrEmpty(file)) {
						File file3 = new File(this.getFilePath().trim() + file.trim());
						FileSystemResource fr1 = new FileSystemResource(file3);
						helper.addAttachment(file3.getName(), fr1);
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("sdffffffff");
			ex.printStackTrace();
		}

		try {
			getMailSender().send(message);

			// getMailSender().send(preparator);
			System.out.println("Message Send...Hurrey");
		} catch (MailException ex) {
			// throw new IOException();
			System.out.println("sdffffffff1");
			System.err.println(ex.getMessage());
		} catch (Throwable ex) {
			System.out.println("sdffffffff2");
			ex.printStackTrace();
		}
	}

	/*
	 * private InternetAddress[] formatAddress(String email) { String emailArr[]
	 * = email.split(";"); int lenght = emailArr.length; InternetAddress[]
	 * address = new InternetAddress[lenght];
	 * System.out.println("Email address lenght == " + lenght);
	 * 
	 * for (int i = 0; i < lenght; i++) { try { address[i] = new
	 * InternetAddress(emailArr[i]); } catch (Exception ex) {
	 * ex.printStackTrace(); } }
	 * System.out.println("Internet address lenght == " + address.length);
	 * 
	 * return address; }
	 */

	/**
	 * @return the mailSender
	 */
	public JavaMailSender getMailSender() {
		return mailSender;
	}

	/**
	 * @param mailSender
	 *            the mailSender to set
	 */
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the toEmail
	 */
	public String getToEmail() {
		return toEmail;
	}

	/**
	 * @param toEmail
	 *            the toEmail to set
	 */
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	/**
	 * @return the fromEmail
	 */
	public String getFromEmail() {
		return fromEmail;
	}

	/**
	 * @param fromEmail
	 *            the fromEmail to set
	 */
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the attachments
	 */
	public String[] getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the ccEmail
	 */
	public String getCcEmail() {
		return ccEmail;
	}

	/**
	 * @param ccEmail
	 *            the ccEmail to set
	 */
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the emailBody
	 */
	public String getEmailBody() {
		return emailBody;
	}

	/**
	 * @param emailBody
	 *            the emailBody to set
	 */
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	/**
	 * @return the htmlEnabled
	 */
	public boolean getHtmlEnabled() {
		return htmlEnabled;
	}

	/**
	 * @param htmlEnabled
	 *            the htmlEnabled to set
	 */
	public void setHtmlEnabled(boolean htmlEnabled) {
		this.htmlEnabled = htmlEnabled;
	}

}
