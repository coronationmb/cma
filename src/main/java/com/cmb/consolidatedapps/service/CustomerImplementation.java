/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ContactUsInfoRepository;
import com.cmb.consolidatedapps.interfaces.CustomerBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.CustomerInterface;
import com.cmb.consolidatedapps.interfaces.CustomerSubsidiaryBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.ImageRepository;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.ContactUsInfo;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;
import com.expertedge.entrustplugin.ws.EntrustMultiFactorAuthImplService;
import com.expertedge.entrustplugin.ws.UserAdminDTO;

/**
 * @author waliu.faleye
 *
 */
@Service("customerService")
public class CustomerImplementation implements CustomerInterface {

	private CustomerBeneficiaryRepository customerBeneficiaryRepo;
	private CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo;
	private ApplicationRepository applicationRepo;
	private UserRepository userRepo;
	private UserRoleRepository userRoleRepo;
	private StatusRepository statusRepo;
	private ContactUsInfoRepository contactUsInfoRepo;
	ImageRepository imageRepo;
	@Value("${staff.entrust.group}")
	String internalUserEntrustGroup;

	public CustomerImplementation(CustomerBeneficiaryRepository customerBeneficiaryRepo,
			CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo,
			ApplicationRepository applicationRepo, UserRepository userRepo, UserRoleRepository userRoleRepo,
			StatusRepository statusRepo, ContactUsInfoRepository contactUsInfoRepo, ImageRepository imageRepo) {
		this.customerBeneficiaryRepo = customerBeneficiaryRepo;
		this.customerSubsidiaryBeneficiaryRepo = customerSubsidiaryBeneficiaryRepo;
		this.applicationRepo = applicationRepo;
		this.userRepo = userRepo;
		this.userRoleRepo = userRoleRepo;
		this.statusRepo = statusRepo;
		this.contactUsInfoRepo = contactUsInfoRepo;
		this.imageRepo = imageRepo;
	}

	@Override
	public List<CustomerBeneficiary> findCustomerBeneficiariesByCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerBeneficiaryRepo.findByCustomerAndDeleteFlgNot(customer, "Y");
	}

	@Override
	public List<CustomerSubsidiaryBeneficiary> findCustomerSubBeneficiariesByCustomerAndApplication(Customer customer,
			Application application) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.findByCustomerAndApplicationAndDeleteFlgNot(customer, application,
				"Y");
	}

	@Override
	public void deleteCustomerBeneficiary(Long id) {
		// TODO Auto-generated method stub
		CustomerBeneficiary customerBeneficiary = customerBeneficiaryRepo.getOne(id);
		customerBeneficiary.setDeleteFlg("Y");
		customerBeneficiaryRepo.save(customerBeneficiary);
	}

	@Override
	public void deleteCustomerSubsidiaryBeneficiary(Long id) {
		// TODO Auto-generated method stub
		CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = customerSubsidiaryBeneficiaryRepo.getOne(id);
		customerSubsidiaryBeneficiary.setDeleteFlg("Y");
		customerSubsidiaryBeneficiaryRepo.save(customerSubsidiaryBeneficiary);
	}

	@Override
	public Application findApplicationById(Long id) {
		// TODO Auto-generated method stub
		return applicationRepo.getOne(id);
	}

	@Override
	public List<User> findByUserRoleNot(UserRole userRole) {
		// TODO Auto-generated method stub
		return userRepo.findByUserRoleNot(userRole);
	}

	@Override
	public UserRole findByRoleName(String roleName) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByRoleName(roleName);
	}

	@Override
	public List<UserRole> findByRoleNameNot(String roleName) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByRoleNameNot(roleName);
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userRepo.save(user);
	}

	@Override
	public UserRole findById(Long id) {
		// TODO Auto-generated method stub
		return userRoleRepo.getOne(id);
	}

	@Override
	public Status findByStatus(String status) {
		// TODO Auto-generated method stub
		return statusRepo.findByStatus(status);
	}

	@Override
	public List<Status> findAllStatus() {
		// TODO Auto-generated method stub
		return statusRepo.findAll();
	}

	@Override
	public Status findStatusById(Long id) {
		// TODO Auto-generated method stub
		return statusRepo.getOne(id);
	}

	@Override
	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		User deleteUser = userRepo.getOne(id);
		deleteUser.setDeleteFlg("Y");
		userRepo.save(deleteUser);
	}

	@Override
	public AdminResponseDTO createEntrustUser(User updatingUser) {
		EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
		String group = "";
		UserAdminDTO arg0 = new UserAdminDTO();
		arg0.setEnableOTP(true);
		arg0.setFullname(updatingUser.getUserId());
		if (updatingUser.getCustomer() == null) {
			group = internalUserEntrustGroup;
		} else {
			group = updatingUser.getCustomer().getCustomerType().getEntrustGroup();
		}
		arg0.setGroup(group);
		arg0.setUserName(updatingUser.getUserId());
		AdminResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performCreateEntrustUser(arg0);

		return res;

	}

	@Override
	public ContactUsInfo saveContactUsInfo(ContactUsInfo contactUsInfo) {
		// TODO Auto-generated method stub
		return contactUsInfoRepo.save(contactUsInfo);
	}

	@Override
	public List<User> findUsersByRoleName(UserRole userRole) {
		// TODO Auto-generated method stub
		return userRepo.findByUserRole(userRole);
	}

	@Override
	public Image saveImage(Image image) {
		// TODO Auto-generated method stub
		return imageRepo.save(image);
	}

}
