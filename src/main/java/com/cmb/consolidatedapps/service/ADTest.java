/**
 * 
 */
package com.cmb.consolidatedapps.service;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

/**
 * @author waliu.faleye
 *
 */
public class ADTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ADTest adTest = new ADTest();
		try {
			adTest.ldapQueryService();
		} catch (Exception exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
		}
	}
	
	public void ldapQueryService()throws Exception{
        try {
            System.out.println("Querying Active Directory Using Java");
            System.out.println("------------------------------------");
            String domain = "coronationmb.com";
            String url = "172.31.250.10:389";
            String username = "wfaleye";
            String password = "Fortran34$";
            String choice = "faleye";
            String searchTerm = "waliu";

            //Creating instance of ActiveDirectory
            ActiveDirectory activeDirectory = new ActiveDirectory(username, password, domain, url);

            //Searching
            NamingEnumeration<SearchResult> result = activeDirectory.searchUser(searchTerm, choice, null);

            while (result.hasMore()) {
                SearchResult rs = (SearchResult) result.next();
				Attributes attrs = rs.getAttributes();
                String temp = attrs.get("samaccountname").toString();
                System.out.println("Username     : " + temp.substring(temp.indexOf(":") + 1));
                String memberOf =  attrs.get("memberOf").toString();
                String stringToSearch = "CN=Configuration";
                boolean test = memberOf.toLowerCase().contains(stringToSearch.toLowerCase());
                if(test){ 
                   String mail = attrs.get("mail").toString();
                   System.out.println("Email ID  : " + mail.substring(mail.indexOf(":") + 1));
               }
            }
            activeDirectory.closeLdapConnection();
        }catch(Exception e){

        }
    }

}
