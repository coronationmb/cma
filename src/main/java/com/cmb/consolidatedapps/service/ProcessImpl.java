/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;
import com.cmb.consolidatedapps.interfaces.ContactUsInfoRepository;
import com.cmb.consolidatedapps.interfaces.CustomerApplicationRepository;
import com.cmb.consolidatedapps.interfaces.CustomerBankAccountMonthMapper;
import com.cmb.consolidatedapps.interfaces.CustomerBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.CustomerDetailMapper;
import com.cmb.consolidatedapps.interfaces.CustomerRepository;
import com.cmb.consolidatedapps.interfaces.CustomerSubsidiaryBeneficiaryRepository;
import com.cmb.consolidatedapps.interfaces.FinancialInstitutionRepository;
import com.cmb.consolidatedapps.interfaces.HtmlTemplateRepository;
import com.cmb.consolidatedapps.interfaces.ImageRepository;
import com.cmb.consolidatedapps.interfaces.InfowareResponseCodeRepository;
import com.cmb.consolidatedapps.interfaces.LoginDetailRepository;
import com.cmb.consolidatedapps.interfaces.MutualFundProductRepository;
import com.cmb.consolidatedapps.interfaces.ProcessInterfaces;
import com.cmb.consolidatedapps.interfaces.RelationshipOfficerRepository;
import com.cmb.consolidatedapps.interfaces.StatusRepository;
import com.cmb.consolidatedapps.interfaces.TopUpSourceRepository;
import com.cmb.consolidatedapps.interfaces.TopUpTypeRepository;
import com.cmb.consolidatedapps.interfaces.TopupTransactionDetailRepository;
import com.cmb.consolidatedapps.interfaces.TransactionDetailRepository;
import com.cmb.consolidatedapps.interfaces.TransactionTypeRepository;
import com.cmb.consolidatedapps.interfaces.UserRepository;
import com.cmb.consolidatedapps.interfaces.UserRoleRepository;
import com.cmb.consolidatedapps.model.AccountMonthDetail;
import com.cmb.consolidatedapps.model.Application;
import com.cmb.consolidatedapps.model.ApplicationCustomerDetailRp;
import com.cmb.consolidatedapps.model.ChartObject;
import com.cmb.consolidatedapps.model.ContactUsInfo;
import com.cmb.consolidatedapps.model.Customer;
import com.cmb.consolidatedapps.model.CustomerApplication;
import com.cmb.consolidatedapps.model.CustomerBankAccount;
import com.cmb.consolidatedapps.model.CustomerBankAccountBalance;
import com.cmb.consolidatedapps.model.CustomerBankAccountMonthData;
import com.cmb.consolidatedapps.model.CustomerBeneficiary;
import com.cmb.consolidatedapps.model.CustomerDashboardDetail;
import com.cmb.consolidatedapps.model.CustomerSubsidiaryBeneficiary;
import com.cmb.consolidatedapps.model.FTSingleCreditRequest;
import com.cmb.consolidatedapps.model.FinancialInstitution;
import com.cmb.consolidatedapps.model.HtmlTemplate;
import com.cmb.consolidatedapps.model.Image;
import com.cmb.consolidatedapps.model.InfowareResponseCode;
import com.cmb.consolidatedapps.model.LoanAccountDetail;
import com.cmb.consolidatedapps.model.LoginDetail;
import com.cmb.consolidatedapps.model.MailDetail;
import com.cmb.consolidatedapps.model.MutualFundProduct;
import com.cmb.consolidatedapps.model.OverdraftAccountDetail;
import com.cmb.consolidatedapps.model.RelationshipOfficer;
import com.cmb.consolidatedapps.model.Status;
import com.cmb.consolidatedapps.model.SubsidiaryCustomerDetail;
import com.cmb.consolidatedapps.model.TDAccountDetail;
import com.cmb.consolidatedapps.model.TopUpSource;
import com.cmb.consolidatedapps.model.TopUpType;
import com.cmb.consolidatedapps.model.TopupTransactionDetail;
import com.cmb.consolidatedapps.model.TransactionDetail;
import com.cmb.consolidatedapps.model.TransactionType;
import com.cmb.consolidatedapps.model.User;
import com.cmb.consolidatedapps.model.UserRole;
import com.cmb.thirdpartyapi.model.AMCurrentPortfolioValueRows;
import com.cmb.thirdpartyapi.model.AMInvestmentSummaryRows;
import com.cmb.thirdpartyapi.model.AssetManagementResponseData;
import com.cmb.thirdpartyapi.model.LosersGainersRows;
import com.cmb.thirdpartyapi.model.MFRows;
import com.cmb.thirdpartyapi.model.RequestData;
import com.cmb.thirdpartyapi.model.ResponseData;
import com.cmb.thirdpartyapi.model.Rows;
import com.cmb.thirdpartyapi.model.SecurityResponseData;
import com.cmb.thirdpartyapi.model.StockData;
import com.cmb.thirdpartyapi.model.SubFundsRows;
import com.expertedge.entrustplugin.ws.AdminResponseDTO;
import com.expertedge.entrustplugin.ws.EntrustMultiFactorAuthImplService;
import com.expertedge.entrustplugin.ws.UserAdminDTO;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author waliu.faleye
 *
 */
@Service
@Transactional
public class ProcessImpl implements ProcessInterfaces {
	private DataSource dataSource;

	@Value("${thirdpartyapi.baseurl.assetmgt}")
	String thirdpartyapiBaseUrlAssetMgt;

	@Value("${thirdpartyapi.baseurl.localapi}")
	String thirdpartyapiBaseUrlLocal;

	@Value("${thirdpartyapi.baseurl.securities}")
	String thirdpartyapiBaseUrlSec;

	@Value("${db.maximum.pool.size}")
	String maximumPoolSize;

	@Autowired
	@Qualifier("finacleTemplate")
	private JdbcTemplate finacleJdbcTemplate;

	@Autowired
	@Qualifier("assetMgtSqlTemplate")
	private JdbcTemplate assetMgtJdbcTemplate;

	@Autowired
	@Qualifier("securitiesSqlTemplate")
	private JdbcTemplate securitiesJdbcTemplate;

	ThirdPartyService tdptService = new ThirdPartyService();

	UserRepository userRepo;
	LoginDetailRepository loginDetailRepo;
	CustomerApplicationRepository customerAppsRepo;
	HtmlTemplateRepository htmlTemplateRepo;
	TransactionDetailRepository transactionDetailRepo;
	TransactionTypeRepository transactionTypeRepo;
	CustomerBeneficiaryRepository customerBeneficiaryRepo;
	CustomerRepository customerRepository;
	FinancialInstitutionRepository financialInstitutionRepo;
	MutualFundProductRepository mutualFundProductRepository;
	TopUpSourceRepository topUpSourceRepository;
	TopUpTypeRepository topUpTypeRepository;
	CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo;
	TopupTransactionDetailRepository topupTransactionDetailRepo;
	InfowareResponseCodeRepository infowareResponseCodeRepo;
	ApplicationRepository applicationRepo;
	ImageRepository imageRepo;
	UserRoleRepository userRoleRepo;
	StatusRepository statusRepo;
	ContactUsInfoRepository contactUsInfoRepo;
	RelationshipOfficerRepository roRepository;
	@Value("${staff.entrust.group}")
	String internalUserEntrustGroup;

	public ProcessImpl(UserRepository userRepo, LoginDetailRepository loginDetailRepo,
			CustomerApplicationRepository customerAppsRepo, HtmlTemplateRepository htmlTemplateRepo,
			TransactionDetailRepository transactionDetailRepo, TransactionTypeRepository transactionTypeRepo,
			CustomerBeneficiaryRepository customerBeneficiaryRepo, CustomerRepository customerRepository,
			FinancialInstitutionRepository financialInstitutionRepo,
			MutualFundProductRepository mutualFundProductRepository, TopUpSourceRepository topUpSourceRepository,
			TopUpTypeRepository topUpTypeRepository,
			CustomerSubsidiaryBeneficiaryRepository customerSubsidiaryBeneficiaryRepo,
			TopupTransactionDetailRepository topupTransactionDetailRepo,
			InfowareResponseCodeRepository infowareResponseCodeRepo, ApplicationRepository applicationRepo,
			ImageRepository imageRepo, UserRoleRepository userRoleRepo, StatusRepository statusRepo,
			ContactUsInfoRepository contactUsInfoRepo, RelationshipOfficerRepository roRepository) {
		this.userRepo = userRepo;
		this.loginDetailRepo = loginDetailRepo;
		this.customerAppsRepo = customerAppsRepo;
		this.htmlTemplateRepo = htmlTemplateRepo;
		this.transactionDetailRepo = transactionDetailRepo;
		this.transactionTypeRepo = transactionTypeRepo;
		this.customerBeneficiaryRepo = customerBeneficiaryRepo;
		this.customerRepository = customerRepository;
		this.financialInstitutionRepo = financialInstitutionRepo;
		this.mutualFundProductRepository = mutualFundProductRepository;
		this.topUpSourceRepository = topUpSourceRepository;
		this.topUpTypeRepository = topUpTypeRepository;
		this.customerSubsidiaryBeneficiaryRepo = customerSubsidiaryBeneficiaryRepo;
		this.topupTransactionDetailRepo = topupTransactionDetailRepo;
		this.infowareResponseCodeRepo = infowareResponseCodeRepo;
		this.applicationRepo = applicationRepo;
		this.imageRepo = imageRepo;
		this.userRoleRepo = userRoleRepo;
		this.statusRepo = statusRepo;
		this.contactUsInfoRepo = contactUsInfoRepo;
		this.roRepository = roRepository;
	}

	@Override
	public ApplicationCustomerDetailRp getCustomerDetail(Application app, String queryParam) {
		// TODO Auto-generated method stub
		ApplicationCustomerDetailRp rp = new ApplicationCustomerDetailRp();
		JdbcTemplate jdbcTemp = null;
		try {
			List<ApplicationCustomerDetailRp> list = new ArrayList<ApplicationCustomerDetailRp>();
			if (Strings.isNullOrEmpty(app.getCustomerDetailQuery()))
				return new ApplicationCustomerDetailRp();
			// jdbcTemp = new JdbcTemplate(getDataSource(app));
			jdbcTemp = getJdbcTemplate(app);
			String myQuery = app.getCustomerDetailQuery().replace("var1", queryParam);
			System.out.println("myQuery>>>" + myQuery);
			list = jdbcTemp.query(myQuery, new CustomerDetailMapper());
			if (list.size() > 0)
				rp = list.get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (jdbcTemp != null) {
				if (jdbcTemp.getDataSource() != null) {
					try {
						jdbcTemp.getDataSource().getConnection().close();
					} catch (SQLException exp) {
						// TODO Auto-generated catch block
						exp.printStackTrace();
					}
				}
			}
		}

		return rp;
	}

	private DataSource getDataSource(Application app) {
		HikariConfig dataSourceConfig = new HikariConfig();
		dataSourceConfig.setJdbcUrl(app.getDatabaseUrl());
		dataSourceConfig.setDriverClassName(app.getDatabaseClassName());
		dataSourceConfig.setUsername(app.getUsername());
		dataSourceConfig.setPassword(app.getPassword());
		dataSourceConfig.setConnectionTestQuery(app.getTestQuery());
		dataSourceConfig.setMaximumPoolSize(Integer.valueOf(maximumPoolSize));
		dataSourceConfig.setIdleTimeout(3600);
		dataSourceConfig.setMaxLifetime(3600); // dataSourceConfig.set

		return new HikariDataSource(dataSourceConfig);
	}

	private JdbcTemplate getJdbcTemplate(Application app) {
		if (app != null) {
			if (app.getName().equals("BANK")) {
				return finacleJdbcTemplate;
			} else if (app.getName().equals("ASSET MANAGEMENT")) {
				return assetMgtJdbcTemplate;
			} else if (app.getName().equals("SECURITIES")) {
				return securitiesJdbcTemplate;
			} else if (app.getName().equals("TRUSTEES")) {

			}
		}

		return null;
	}

	@Override
	public boolean sendMail(MailDetail mailDetail) {
		// TODO Auto-generated method stub
		boolean response = false;
		MailService mailService = new MailService();
		mailService.setAttachments(mailDetail.getAttachments());
		mailService.setEmailBody(mailDetail.getEmailBody());
		mailService.setFilePath(mailDetail.getFilePath());
		mailService.setFromEmail(mailDetail.getSender());
		mailService.setHtmlEnabled(mailDetail.isHtmlEnabled());
		mailService.setMailSender(mailDetail.getMailSender());
		mailService.setSubject(mailDetail.getSubject());
		mailService.setToEmail(mailDetail.getReciever());
		mailService.setUsername(mailDetail.getUsername());
		try {
			mailService.sendEmail();
			response = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

	@Override
	public String autoGenerateUserId(Long id) {
		// TODO Auto-generated method stub
		return String.format("%07d", id);
	}

	@Override
	public String autoGeneratePassword(Long id, String initPass) {
		// TODO Auto-generated method stub
		return initPass.concat(String.valueOf(id));
	}

	@Override
	public CustomerDashboardDetail dashboardDetails(User user) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		CustomerDashboardDetail customerDashboardDetail = new CustomerDashboardDetail();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		try {
			customerDashboardDetail.setUser(user);
			// String myQuery = "";
			String bvnrcnumber = !Strings.isNullOrEmpty(user.getCustomer().getBvn()) ? user.getCustomer().getBvn()
					: !Strings.isNullOrEmpty(user.getCustomer().getRcNumber()) ? user.getCustomer().getRcNumber()
							: user.getCorporateUserBvn();

			customerDashboardDetail.setBvn(bvnrcnumber);

			LoginDetail lastLoginDetail = loginDetailRepo.findTopByUserOrderByLoginTimeDesc(user);
			customerDashboardDetail.setLastLoginTime(lastLoginDetail == null ? LocalDateTime.now().format(format)
					: lastLoginDetail.getLoginTime().format(format));

			List<CustomerApplication> customerApps = customerAppsRepo.findByCustomer(user.getCustomer());
			List<AccountMonthDetail> monthDetailList = new ArrayList<AccountMonthDetail>();

			System.out.println("customerApps.size()==" + customerApps.size());

			for (CustomerApplication customerApplication : customerApps) {
				Application app = customerApplication.getApplication();
				if (app.getName().equals("BANK")) {
					customerDashboardDetail.setApp1(Long.valueOf("1"));
					bankDashboardDetail(sdf, customerDashboardDetail, bvnrcnumber, monthDetailList, customerApplication,
							app);
				} else if (app.getName().equals("ASSET MANAGEMENT")) {
					customerDashboardDetail.setApp2(Long.valueOf("2"));

					assetManagementDashboardDetail(bvnrcnumber, customerApplication, app, customerDashboardDetail);

				} else if (app.getName().equals("SECURITIES")) {
					customerDashboardDetail.setApp3(Long.valueOf("3"));

					securitiesDashboardDetail(bvnrcnumber, customerApplication, app, customerDashboardDetail);

				} else if (app.getName().equals("TRUSTEES")) {
					customerDashboardDetail.setApp4(Long.valueOf("4"));

					// assetManagementDashboardDetail(bvnrcnumber,
					// customerApplication, app);

				}

			}
			customerDashboardDetail.setCustomerNetWorth(
					"Net Worth	| ₦ ".concat(formatAmount(customerDashboardDetail.getCustomerWorth())));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return customerDashboardDetail;

	}

	/**
	 * @param bvnrcnumber
	 * @param customerApplication
	 * @param app
	 * @throws DataAccessException
	 */
	public void assetManagementDashboardDetail(String bvnrcnumber, CustomerApplication customerApplication,
			Application app, CustomerDashboardDetail customerDashboardDetail) throws DataAccessException {
		String sessionId = "";
		String myQuery;
		BigDecimal cfif = BigDecimal.ZERO;
		BigDecimal cbf = BigDecimal.ZERO;
		BigDecimal cmmf = BigDecimal.ZERO;
		BigDecimal cashBalanceTotal = BigDecimal.ZERO;
		BigDecimal amCashBalance = BigDecimal.ZERO;
		List<AMInvestmentSummaryRows> investmentSummary = new ArrayList<AMInvestmentSummaryRows>();
		List<AMInvestmentSummaryRows> investmentSummaryCashBalance = new ArrayList<AMInvestmentSummaryRows>();
		JdbcTemplate jdbcTemp = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmnt = null;
		List<SubsidiaryCustomerDetail> custAIDList = new ArrayList<SubsidiaryCustomerDetail>();
		List<SubsidiaryCustomerDetail> newList = new ArrayList<SubsidiaryCustomerDetail>();
		SubsidiaryCustomerDetail defScd = new SubsidiaryCustomerDetail();
		defScd.setDisplayCustomerAID("Select Customer ID");
		newList.add(defScd);
		try {
			tdptService.setBaseUrl(thirdpartyapiBaseUrlAssetMgt);
			AssetManagementResponseData amResponse = new AssetManagementResponseData();
			List<MFRows> products = new ArrayList<MFRows>();
			List<SubFundsRows> subscribedFunds = new ArrayList<SubFundsRows>();
			List<AMCurrentPortfolioValueRows> portfolios = new ArrayList<AMCurrentPortfolioValueRows>();
			// getting the accounts details
			// jdbcTemp = new
			// JdbcTemplate(getDataSource(customerApplication.getApplication()));
			// jdbcTemp = getJdbcTemplate(customerApplication.getApplication());
			/*
			 * DataSourceProperties dsp = new DataSourceProperties();
			 * dsp.setPassword(app.getPassword());
			 * dsp.setUsername(app.getUsername());
			 * dsp.setDriverClassName(app.getDatabaseClassName());
			 * dsp.setUrl(app.getDatabaseUrl());
			 * this.setDataSource(dsp.initializeDataSourceBuilder().build());
			 * con = getConnection(); myQuery =
			 * app.getCustomerDetailQuery().replace("var1", bvnrcnumber);
			 * System.out.println("myQuery>>>" + myQuery); stmnt =
			 * con.createStatement(); rs = this.getCustomerDetail(con, stmnt,
			 * myQuery); while (rs.next()) { System.out.
			 * println("it's connected and return more than one record"); }
			 */
			// custAIDList = jdbcTemp.query(myQuery, new
			// SubsidiaryCustomerDetailMapper());
			custAIDList = tdptService.getAMCustomerDetailFromBvn(bvnrcnumber);
			for (SubsidiaryCustomerDetail subsidiaryCustomerDetail : custAIDList) {
				if (subsidiaryCustomerDetail.getAccountOfficer() != null)
					customerDashboardDetail.setAssetManagementAccountManager(
							subsidiaryCustomerDetail.getAccountOfficerName().concat("   |   ")
									.concat(subsidiaryCustomerDetail.getAccountOfficerEmail() == null ? ""
											: subsidiaryCustomerDetail.getAccountOfficerEmail())
									.concat("   |   ").concat(subsidiaryCustomerDetail.getAccountOfficerPhone() == null ? ""
											: subsidiaryCustomerDetail.getAccountOfficerPhone()));
				customerDashboardDetail.setAmAddress(subsidiaryCustomerDetail.getAddress1() == null?"":subsidiaryCustomerDetail.getAddress1());
				amResponse = tdptService.assetMngmtLogin();
				if (amResponse != null && (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
					System.out.println("inside ==" + amResponse.getOutValue());
					sessionId = amResponse.getOutValue();
				}
				amResponse = tdptService.getMutualFundProducts(sessionId);
				if (amResponse != null && (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
					products = amResponse.getDataTable().getMfrows();
				}
				products.forEach(a -> a.setBidPrice(formatAmount(new BigDecimal(a.getBidPrice()))));
				products.forEach(a -> a.setOfferPrice(formatAmount(new BigDecimal(a.getOfferPrice()))));
				customerDashboardDetail.setMfProducts(products);
				amResponse = tdptService.getCustomerSubscribedFunds(sessionId,
						subsidiaryCustomerDetail.getCustomerAid());
				if (amResponse != null && (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L)))
					subscribedFunds = amResponse.getDataTable().getSubFundsRows();
				System.out.println("subscribedFunds.size()==" + subscribedFunds.size());
				System.out
						.println("subsidiaryCustomerDetail.getCustAID()==" + subsidiaryCustomerDetail.getCustomerAid());
				subscribedFunds.stream().filter(a -> a.getFundCode() != null).collect(Collectors.toList());
				for (SubFundsRows subFundsRows : subscribedFunds) {
					System.out.println("subFundsRows.getFundCode()==" + subFundsRows.getFundCode());
					if ("CBF".equals(subFundsRows.getFundCode())) {
						subsidiaryCustomerDetail
								.setFundCode(subFundsRows.getFundCode() == null ? "" : subFundsRows.getFundCode());
						// cbf = new
						// BigDecimal(subFundsRows.getTotalAssetValue()).setScale(2,
						// RoundingMode.CEILING);
						amResponse = tdptService.getFundInvestmentSummary(sessionId,
								subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
						customerDashboardDetail.setCbfCustAid(subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							investmentSummary = amResponse.getDataTable().getAminvestmentSummaryRows();
							investmentSummary.forEach(a -> a.setFundName("CBF"));
							investmentSummary.forEach(a -> a.setSubscription(formatAmount(
									new BigDecimal(a.getSubscription()).setScale(2, RoundingMode.CEILING))));
							investmentSummary.forEach(a -> a.setAccruedInt(
									formatAmount(new BigDecimal(a.getAccruedInt()).setScale(2, RoundingMode.CEILING))));
							cbf = new BigDecimal(investmentSummary.get(0).getMktValue()).setScale(2,
									RoundingMode.CEILING);
							investmentSummary.forEach(a -> a.setMktValue(
									formatAmount(new BigDecimal(a.getMktValue()).setScale(2, RoundingMode.CEILING))));
							customerDashboardDetail.setCbfInvestmentSummary(investmentSummary);
						}
						customerDashboardDetail.setCbfValueStr("Balanced Funds - ₦ ".concat(cbf.toString()));
						amResponse = tdptService.cashBalanceAssetMgt(sessionId,
								subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							// investmentSummaryCashBalance =
							// amResponse.getDataTable().getAminvestmentSummaryRows();
							AMInvestmentSummaryRows cashBalance = new AMInvestmentSummaryRows();
							cashBalance.setFundName("Cash Balance");
							cashBalance.setSubscription("NA");
							cashBalance.setAccruedInt("NA");
							//cashBalanceTotal = cashBalanceTotal
							//		.add(new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING));
							BigDecimal cbAmt = BigDecimal.ZERO;
							try{cbAmt = new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING);}catch(Exception ex){ex.printStackTrace();}

							cashBalance.setMktValue(formatAmount(cbAmt));
							//investmentSummary.add(cashBalance);
							investmentSummary.forEach(a -> a.setCashBalance(cashBalance.getMktValue()));
							amCashBalance = amCashBalance
									.add(cbAmt);
							customerDashboardDetail.setCbfInvestmentSummary(null);
							customerDashboardDetail.setCbfInvestmentSummary(investmentSummary);
						}
					}
					if ("CFIF".equals(subFundsRows.getFundCode())) {
						subsidiaryCustomerDetail
								.setFundCode(subFundsRows.getFundCode() == null ? "" : subFundsRows.getFundCode());
						// cfif = new
						// BigDecimal(subFundsRows.getTotalAssetValue()).setScale(2,
						// RoundingMode.CEILING);
						amResponse = tdptService.getFundInvestmentSummary(sessionId,
								subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
						customerDashboardDetail.setCfifCustAid(subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							investmentSummary = amResponse.getDataTable().getAminvestmentSummaryRows();
							investmentSummary.forEach(a -> a.setFundName("CFIF"));
							investmentSummary.forEach(a -> a.setSubscription(formatAmount(
									new BigDecimal(a.getSubscription()).setScale(2, RoundingMode.CEILING))));
							investmentSummary.forEach(a -> a.setAccruedInt(
									formatAmount(new BigDecimal(a.getAccruedInt()).setScale(2, RoundingMode.CEILING))));
							cfif = new BigDecimal(investmentSummary.get(0).getMktValue()).setScale(2,
									RoundingMode.CEILING);
							investmentSummary.forEach(a -> a.setMktValue(
									formatAmount(new BigDecimal(a.getMktValue()).setScale(2, RoundingMode.CEILING))));
							customerDashboardDetail.setCfifInvestmentSummary(investmentSummary);
						}
						customerDashboardDetail.setCfifValueStr("Fixed Income - ₦ ".concat(cfif.toString()));
						amResponse = tdptService.cashBalanceAssetMgt(sessionId,
								subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							// investmentSummaryCashBalance =
							// amResponse.getDataTable().getAminvestmentSummaryRows();
							AMInvestmentSummaryRows cashBalance = new AMInvestmentSummaryRows();
							cashBalance.setFundName("Cash Balance");
							cashBalance.setSubscription("NA");
							cashBalance.setAccruedInt("NA");
							//cashBalanceTotal = cashBalanceTotal
							//		.add(new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING));							
							BigDecimal cbAmt = BigDecimal.ZERO;
							try{cbAmt = new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING);}catch(Exception ex){ex.printStackTrace();}

							cashBalance.setMktValue(formatAmount(cbAmt));
							//investmentSummary.add(cashBalance);
							investmentSummary.forEach(a -> a.setCashBalance(cashBalance.getMktValue()));
							amCashBalance = amCashBalance
									.add(cbAmt);
							customerDashboardDetail.setCbfInvestmentSummary(null);
							customerDashboardDetail.setCbfInvestmentSummary(investmentSummary);
						}

					}
					if ("CMMFUND".equals(subFundsRows.getFundCode())) {
						subsidiaryCustomerDetail
								.setFundCode(subFundsRows.getFundCode() == null ? "" : subFundsRows.getFundCode());
						// cmmf = new
						// BigDecimal(subFundsRows.getTotalAssetValue()).setScale(2,
						// RoundingMode.CEILING);
						amResponse = tdptService.getFundInvestmentSummary(sessionId,
								subsidiaryCustomerDetail.getCustomerAid(), subFundsRows.getFundCode());
						customerDashboardDetail.setCmmfCustAid(subsidiaryCustomerDetail.getCustomerAid());
						System.out.println("subsidiaryCustomerDetail.getCustomerAid()==="
								+ subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							investmentSummary = amResponse.getDataTable().getAminvestmentSummaryRows();
							investmentSummary.forEach(a -> a.setFundName("CMMF"));
							investmentSummary.forEach(a -> a.setSubscription(formatAmount(
									new BigDecimal(a.getSubscription()).setScale(2, RoundingMode.CEILING))));
							investmentSummary.forEach(a -> a.setAccruedInt(
									formatAmount(new BigDecimal(a.getAccruedInt()).setScale(2, RoundingMode.CEILING))));
							cmmf = new BigDecimal(investmentSummary.get(0).getMktValue()).setScale(2,
									RoundingMode.CEILING);
							investmentSummary.forEach(a -> a.setMktValue(
									formatAmount(new BigDecimal(a.getMktValue()).setScale(2, RoundingMode.CEILING))));
							customerDashboardDetail.setCmmfInvestmentSummary(investmentSummary);

						}
						customerDashboardDetail.setCmmfValueStr("Money Market Funds - ₦ ".concat(cmmf.toString()));
						amResponse = tdptService.cashBalanceAssetMgt(sessionId,
								subsidiaryCustomerDetail.getCustomerAid());
						if (amResponse != null
								&& (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L))) {
							// investmentSummaryCashBalance =
							// amResponse.getDataTable().getAminvestmentSummaryRows();
							AMInvestmentSummaryRows cashBalance = new AMInvestmentSummaryRows();
							cashBalance.setFundName("Cash Balance");
							cashBalance.setSubscription("NA");
							cashBalance.setAccruedInt("NA");
							//cashBalanceTotal = cashBalanceTotal
							//		.add(new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING));
							BigDecimal cbAmt = BigDecimal.ZERO;
							try{cbAmt = new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING);}catch(Exception ex){ex.printStackTrace();}
							cashBalance.setMktValue(formatAmount(cbAmt));
							//investmentSummary.add(cashBalance);
							investmentSummary.forEach(a -> a.setCashBalance(cashBalance.getMktValue()));
							try{cbAmt = new BigDecimal(amResponse.getCashBalance()).setScale(2, RoundingMode.CEILING);}catch(Exception ex){ex.printStackTrace();}
							amCashBalance = amCashBalance
									.add(cbAmt);
							customerDashboardDetail.setCmmfInvestmentSummary(null);
							customerDashboardDetail.setCmmfInvestmentSummary(investmentSummary);
						}

					}
				}
				customerDashboardDetail.setCbfValue(cbf);
				customerDashboardDetail.setCfifValue(cfif);
				customerDashboardDetail.setCmmfValue(cmmf);
				customerDashboardDetail.setDisplaycbfValue(formatAmount(cbf.setScale(2, RoundingMode.CEILING)));
				customerDashboardDetail.setDisplaycmmfValue(formatAmount(cmmf.setScale(2, RoundingMode.CEILING)));;
				customerDashboardDetail.setDisplaycfifValue(formatAmount(cfif.setScale(2, RoundingMode.CEILING)));
				BigDecimal localWorth = BigDecimal.ZERO;
				localWorth = localWorth.add(customerDashboardDetail.getCustomerWorth());
				localWorth = localWorth.add(cmmf);
				localWorth = localWorth.add(cbf);
				localWorth = localWorth.add(cfif);
				localWorth = localWorth.add(cashBalanceTotal);
				customerDashboardDetail.setAmCashBalance(formatAmount(amCashBalance));
				System.out.println("before customerDashboardDetail.getCustomerWorth()=="
						+ customerDashboardDetail.getCustomerWorth());
				customerDashboardDetail.setCustomerWorth(localWorth);
				System.out.println("after customerDashboardDetail.getCustomerWorth()=="
						+ customerDashboardDetail.getCustomerWorth());
				amResponse = tdptService.getCustomerCurrentPortfolioValue(sessionId,
						subsidiaryCustomerDetail.getCustomerAid());
				if (amResponse != null && (amResponse.getStatusId() != null && amResponse.getStatusId().equals(0L)))
					portfolios = amResponse.getDataTable().getAmcurrentPortfolioValueRows().stream()
							.filter(a -> a.getDetails().equals("TOTAL")).collect(Collectors.toList());
				if (portfolios.size() > 0) {
					AMCurrentPortfolioValueRows portfolio = portfolios.get(0);
					// customerDashboardDetail.setPortfolioValueStr(
					// "Portfolio Holding | ₦ ".concat(formatAmount(new
					// BigDecimal(portfolio.getAmount()))));
					customerDashboardDetail.setPortfolioValueStr(
							"Portfolio Holding | ₦ ".concat(formatAmount(cmmf.add(cbf).add(cfif))));
				}
				subsidiaryCustomerDetail.setFundCode(
						subsidiaryCustomerDetail.getFundCode() == null ? "" : subsidiaryCustomerDetail.getFundCode());
				subsidiaryCustomerDetail.setDisplayCustomerAID(subsidiaryCustomerDetail.getCustomerAid().concat(" | ").concat(subsidiaryCustomerDetail.getFundCode()));
				newList.add(subsidiaryCustomerDetail);
			}
			customerDashboardDetail.setAmCustAIDList(newList);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(con, stmnt, rs);
			if (jdbcTemp != null) {
				if (jdbcTemp.getDataSource() != null) {
					try {
						jdbcTemp.getDataSource().getConnection().close();
					} catch (SQLException exp) {
						// TODO Auto-generated catch block
						exp.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * @param bvnrcnumber
	 * @param customerApplication
	 * @param app
	 * @throws DataAccessException
	 */
	public void securitiesDashboardDetail(String bvnrcnumber, CustomerApplication customerApplication, Application app,
			CustomerDashboardDetail customerDashboardDetail) throws DataAccessException {
		String sessionId = "";
		String myQuery;
		JdbcTemplate jdbcTemp = null;
		try {
			tdptService.setBaseUrl(thirdpartyapiBaseUrlSec);
			SecurityResponseData secResponse = new SecurityResponseData();
			// getting the accounts details
			// jdbcTemp = new
			// JdbcTemplate(getDataSource(customerApplication.getApplication()));
			jdbcTemp = getJdbcTemplate(customerApplication.getApplication());

			myQuery = app.getCustomerDetailQuery().replace("var1", bvnrcnumber);
			System.out.println("myQuery>>>" + myQuery);
			// List<SubsidiaryCustomerDetail> custAIDList =
			// jdbcTemp.query(myQuery, new SubsidiaryCustomerDetailMapper());
			List<SubsidiaryCustomerDetail> newCustAIDList = new ArrayList<SubsidiaryCustomerDetail>();
			List<SubsidiaryCustomerDetail> custAIDList = tdptService.getSecCustomerDetailFromBvn(bvnrcnumber);
			SubsidiaryCustomerDetail defScd = new SubsidiaryCustomerDetail();
			defScd.setDisplayCustomerAID("Select Customer ID");
			newCustAIDList.add(defScd);
			custAIDList.forEach(a -> a.setDisplayCustomerAID(a.getCustomerAid()));
			newCustAIDList.addAll(custAIDList);
			customerDashboardDetail.setSecCustAIDList(newCustAIDList);
			for (SubsidiaryCustomerDetail subsidiaryCustomerDetail : custAIDList) {
				System.out.println("subsidiaryCustomerDetail.getAccountOfficer()>>>"
						+ subsidiaryCustomerDetail.getAccountOfficer());
				if (subsidiaryCustomerDetail.getAccountOfficer() != null)
					customerDashboardDetail
							.setSecuritiesAccountManager(subsidiaryCustomerDetail.getAccountOfficerName().concat("   |   ")
									.concat(subsidiaryCustomerDetail.getAccountOfficerEmail() == null ? ""
											: subsidiaryCustomerDetail.getAccountOfficerEmail())
									.concat("   |   ").concat(subsidiaryCustomerDetail.getAccountOfficerPhone() == null ? ""
											: subsidiaryCustomerDetail.getAccountOfficerPhone()));
				customerDashboardDetail.setSecAddress(subsidiaryCustomerDetail.getAddress1() == null?"":subsidiaryCustomerDetail.getAddress1());
				customerDashboardDetail.setCsecCustAid(subsidiaryCustomerDetail.getCustomerAid());
				secResponse = tdptService.securityLogin();
				System.out.println("secResponse.getOutValue()>>>" + secResponse.getOutValue());
				System.out.println("secResponse>>>" + secResponse);
				if (secResponse != null
						&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
					System.out.println("secResponse.getStatusId()>>>" + secResponse.getStatusId());
					sessionId = secResponse.getOutValue();
				}
				secResponse = tdptService.getTotalPortfolioHoldings(sessionId,
						subsidiaryCustomerDetail.getCustomerAid(),
						LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")), "1");
				if (secResponse != null
						&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
					BigDecimal secPortfolio[] = new BigDecimal[2];
					int i = 0;
					for (Rows row : secResponse.getDataTable().getRows()) {
						System.out.println("inside secResponse.getDataTable()>>>" + secResponse.getDataTable());
						secPortfolio[i] = new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING);
						if ("Cash".equals(row.getName())) {
							customerDashboardDetail.setSecCashPercent(
									new BigDecimal(row.getPercent()).setScale(2, RoundingMode.CEILING));
							customerDashboardDetail
									.setSecCashValue(new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING));
							customerDashboardDetail.setDisplaySecCashValue(formatAmount(customerDashboardDetail.getSecCashValue()));
							System.out.println("row.getAmount()>>>" + row.getAmount());
							customerDashboardDetail.setSecCashValueStr("Cash - ₦ ".concat(
									new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING).toString()));
						} else if ("Equities".equals(row.getName())) {
							customerDashboardDetail.setSecEquitiesPercent(
									new BigDecimal(row.getPercent()).setScale(2, RoundingMode.CEILING));
							customerDashboardDetail.setSecEquitiesValue(
									new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING));
							customerDashboardDetail.setDisplaySecEquitiesValue(formatAmount(customerDashboardDetail.getSecEquitiesValue()));
							System.out.println("row.getAmount()>>>" + row.getAmount());
							customerDashboardDetail.setSecEquitiesValueStr("Equity - ₦ ".concat(
									new BigDecimal(row.getAmount()).setScale(2, RoundingMode.CEILING).toString()));
						}
						i++;
					}
					customerDashboardDetail.setSecPortfolio(secPortfolio);
				}
				secResponse = tdptService.getLosersGainers(sessionId, "2");
				if (secResponse != null
						&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
					System.out.println("inside secResponse.getDataTable().getLosersGainersRows()>>>"
							+ secResponse.getDataTable().getLosersGainersRows().size());
					List<LosersGainersRows> lgRows = secResponse.getDataTable().getLosersGainersRows();
					lgRows.forEach(a -> a.setPercentChange(formatAmount(new BigDecimal(a.getPercentChange()))));
					lgRows.forEach(a -> a.setlClose(formatAmount(new BigDecimal(a.getlClose()))));
					customerDashboardDetail.setGainers(lgRows);
				}
				secResponse = tdptService.getLosersGainers(sessionId, "3");
				if (secResponse != null
						&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
					List<LosersGainersRows> lgRows = secResponse.getDataTable().getLosersGainersRows();
					lgRows.forEach(a -> a.setPercentChange(formatAmount(new BigDecimal(a.getPercentChange()))));
					lgRows.forEach(a -> a.setlClose(formatAmount(new BigDecimal(a.getlClose()))));
					// System.out.println("inside
					// secResponse.getDataTable().getLosersGainersRows()>>>" +
					// secResponse.getDataTable().getLosersGainersRows().size());
					customerDashboardDetail.setLosers(lgRows);
				}
				secResponse = tdptService.getStockData(sessionId, subsidiaryCustomerDetail.getCustomerAid());
				if (secResponse != null
						&& (secResponse.getStatusId() != null && secResponse.getStatusId().equals(0L))) {
					List<StockData> stockData = secResponse.getDataTable().getStockDataRows();
					stockData.forEach(a -> a.setAvgUnitPrice(
							new BigDecimal(a.getAvgUnitPrice()).setScale(2, RoundingMode.CEILING).toString()));
					stockData.forEach(a -> a.setGain(new BigDecimal(a.getMktValue())
							.subtract(new BigDecimal(a.getCost())).setScale(2, RoundingMode.CEILING).toString()));
					stockData.forEach(
							a -> a.setCost(new BigDecimal(a.getCost()).setScale(2, RoundingMode.CEILING).toString()));
					stockData.forEach(a -> a.setGainPercent(new BigDecimal(a.getGain()).multiply(new BigDecimal("100"))
							.divide(new BigDecimal(a.getCost()), 2, RoundingMode.HALF_UP).setScale(2, RoundingMode.UP)
							.toString()));
					stockData.forEach(a -> a
							.setMktValue(new BigDecimal(a.getMktValue()).setScale(2, RoundingMode.CEILING).toString()));
					stockData.forEach(a -> a.setTotalUnits(
							formatAmount(new BigDecimal(a.getTotalUnits()))));
					stockData.forEach(a -> a
									.setMktValue(formatAmount(new BigDecimal(a.getMktValue()))));
					stockData.forEach(a -> a
							.setAvgUnitPrice(formatAmount(new BigDecimal(a.getAvgUnitPrice()))));
					stockData.forEach(a -> a
							.setCost(formatAmount(new BigDecimal(a.getCost()))));
					stockData.forEach(a -> a
							.setGain(formatAmount(new BigDecimal(a.getGain()))));
					stockData.forEach(a -> a
							.setGainPercent(formatAmount(new BigDecimal(a.getGainPercent()))));
					customerDashboardDetail.setStockData(stockData);

				}
			}
			BigDecimal secPortfolioValue = BigDecimal.ZERO;
			if (customerDashboardDetail.getSecCashValue() != null
					&& customerDashboardDetail.getSecEquitiesValue() != null)
				secPortfolioValue = customerDashboardDetail.getSecCashValue()
						.add(customerDashboardDetail.getSecEquitiesValue());
			// customerDashboardDetail.setSecPortfolioValueStr(secPortfolioValueStr);
			customerDashboardDetail
					.setSecPortfolioValueStr("Portfolio Holding | ₦ ".concat(formatAmount(secPortfolioValue)));
			BigDecimal localWorth = BigDecimal.ZERO;
			localWorth = localWorth.add(customerDashboardDetail.getCustomerWorth());
			if (customerDashboardDetail.getSecCashValue() != null)
				localWorth = localWorth.add(customerDashboardDetail.getSecCashValue());
			if (customerDashboardDetail.getSecEquitiesValue() != null)
				localWorth = localWorth.add(customerDashboardDetail.getSecEquitiesValue());
			customerDashboardDetail.setCustomerWorth(localWorth);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (jdbcTemp != null) {
				if (jdbcTemp.getDataSource() != null) {
					try {
						jdbcTemp.getDataSource().getConnection().close();
					} catch (SQLException exp) {
						// TODO Auto-generated catch block
						exp.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * @param sdf
	 * @param customerDashboardDetail
	 * @param bvnrcnumber
	 * @param monthDetailList
	 * @param customerApplication
	 * @param app
	 * @throws DataAccessException
	 */
	public void bankDashboardDetail(SimpleDateFormat sdf, CustomerDashboardDetail customerDashboardDetail,
			String bvnrcnumber, List<AccountMonthDetail> monthDetailList, CustomerApplication customerApplication,
			Application app) throws DataAccessException {
		String myQuery;
		List<CustomerBankAccount> currentAccountList = new ArrayList<CustomerBankAccount>();
		List<CustomerBankAccount> domAccountList = new ArrayList<CustomerBankAccount>();
		// getting the accounts details
		// JdbcTemplate jdbcTemp = new
		// JdbcTemplate(getDataSource(customerApplication.getApplication()));
		// JdbcTemplate jdbcTemp =
		// getJdbcTemplate(customerApplication.getApplication());
		try {
			myQuery = app.getCustomerAccountsQuery().replace("var1", bvnrcnumber);
			System.out.println("myQuery>>>" + myQuery);
			// List<CustomerBankAccount> accountList = jdbcTemp.query(myQuery,
			// new CustomerBankAccountMapper());
			List<CustomerBankAccount> accountList = tdptService.getBankCustomerAccountDetailsFromBvn(bvnrcnumber);

			System.out.println("accountList.size()==" + accountList.size());
			String currentAccounts = "";
			String domAccounts = "";
			BigDecimal bankWorth = BigDecimal.ZERO;
			User user = this.findUserById(customerDashboardDetail.getUser().getId());
			for (CustomerBankAccount customerBankAccount : accountList) {
				customerDashboardDetail.setBankAccountManager(customerBankAccount.getAccountOfficerName().concat("   |   ")
						.concat(customerBankAccount.getAccountOfficerEmail() == null ? ""
								: customerBankAccount.getAccountOfficerEmail())
						.concat("   |   ").concat(customerBankAccount.getAccountOfficerPhone() == null ? ""
								: customerBankAccount.getAccountOfficerPhone()));
				RelationshipOfficer rm = user.getRelationshipOfficer();
				if (rm == null)
					rm = new RelationshipOfficer();
				rm.setEmail(customerBankAccount.getAccountOfficerEmail());
				rm.setName(customerBankAccount.getAccountOfficerName());
				rm.setPhone(customerBankAccount.getAccountOfficerPhone());
				rm.setTeamCode(customerBankAccount.getAccountOfficerTeamCode());
				rm.setApplication(app);
				List<RelationshipOfficer> roList = roRepository.findByEmailAndNameAndPhone(
						customerBankAccount.getAccountOfficerEmail(), customerBankAccount.getAccountOfficerName(),
						customerBankAccount.getAccountOfficerPhone());
				if (roList.isEmpty()) {
					roRepository.save(rm);
				} else {
					rm = roList.get(0);
				}
				user.setRelationshipOfficer(rm);
				userRepo.save(user);
				bankWorth = bankWorth.add(customerBankAccount.getNgnBalanceAmount());
				AccountMonthDetail monthDetail = new AccountMonthDetail();
				BigDecimal totalAccountDr = BigDecimal.ZERO;
				BigDecimal totalAccountCr = BigDecimal.ZERO;
				// List<CustomerBankAccountMonthData>
				// accountMonthTransactionsList = getBankAccountMonthData(app,
				// customerBankAccount, jdbcTemp);
				List<CustomerBankAccountMonthData> accountMonthTransactionsList = tdptService.getBankAccountMonthData(
						customerBankAccount.getAcid(), getFirstDay(new Date()), getLastDay(new Date()));
				List<CustomerBankAccountMonthData> accountMonthDebitTransactionsList = accountMonthTransactionsList
						.stream().filter(data -> data.getPartTranType().equals("D")).collect(Collectors.toList());
				System.out.println(
						"accountMonthDebitTransactionsList.size()===" + accountMonthDebitTransactionsList.size());
				List<CustomerBankAccountMonthData> accountMonthCreditTransactionsList = accountMonthTransactionsList
						.stream().filter(data -> data.getPartTranType().equals("C")).collect(Collectors.toList());
				System.out.println(
						"accountMonthCreditTransactionsList.size()===" + accountMonthCreditTransactionsList.size());
				// accountMonthDebitTransactionsList.forEach(data ->
				// totalAccountDr.add(data.getTranAmount()));
				Function<CustomerBankAccountMonthData, BigDecimal> totalMapper = data -> data.getTranAmount();
				totalAccountDr = accountMonthDebitTransactionsList.stream().map(totalMapper).reduce(BigDecimal.ZERO,
						BigDecimal::add);
				// accountMonthCreditTransactionsList.forEach(data ->
				// totalAccountCr.add(data.getTranAmount()));
				totalAccountCr = accountMonthCreditTransactionsList.stream().map(totalMapper).reduce(BigDecimal.ZERO,
						BigDecimal::add);
				monthDetail.setTotalMonthCreditStr(formatAmount(totalAccountCr));
				monthDetail.setTotalMonthDebitStr(formatAmount(totalAccountDr));
				monthDetail.setAccountCurrency(customerBankAccount.getAccountCurrency());
				monthDetail.setAccountNumber(customerBankAccount.getAccountNumber());
				monthDetail.setAcid(customerBankAccount.getAcid());

				if (!"TDA".equals(customerBankAccount.getSchemeType()))
				monthDetailList.add(monthDetail);

				if ("CLA".equals(customerBankAccount.getSchemeType())
						|| "LAA".equals(customerBankAccount.getSchemeType())) {
					// Loan Accounts details here
					if (!Strings.isNullOrEmpty(app.getCustomerLoanAccountQuery())) {
						// List<LoanAccountDetail> loanAccountDetail =
						// this.getBankCustomerDetail(app,
						// app.getCustomerLoanAccountQuery().replace("var1",
						// customerBankAccount.getCifId())
						// .replace("var2", sdf.format(new Date())),
						// new LoanAccountDetailMapper(), jdbcTemp);
						List<LoanAccountDetail> loanAccountDetail = tdptService
								.getBankLoanAccountDetail(customerBankAccount.getCifId(), sdf.format(new Date()));
						customerDashboardDetail.setLoanAccountDetails(loanAccountDetail);
					}
				}

				if ("TDA".equals(customerBankAccount.getSchemeType())) {
					// Term Deposit Accounts details here
					if (!Strings.isNullOrEmpty(app.getCustomerTDAccountQuery())) {
						// List<TDAccountDetail> tdAccountDetail =
						// this.getBankCustomerDetail(
						// app, app.getCustomerTDAccountQuery().replace("var1",
						// customerBankAccount.getCifId())
						// .replace("var2", sdf.format(new Date())),
						// new TDAccountDetailMapper(), jdbcTemp);
						List<TDAccountDetail> tdAccountDetail = tdptService
								.getBankTDAccountDetail(customerBankAccount.getCifId(), sdf.format(new Date()));
						customerDashboardDetail.setTdAccountDetails(tdAccountDetail);
					}
				}

				if ("ODA".equals(customerBankAccount.getSchemeType())
						|| "CAA".equals(customerBankAccount.getSchemeType())) {
					//
					if (!Strings.isNullOrEmpty(app.getCustomerOverdraftAccountQuery())) {
						// List<OverdraftAccountDetail> odAccountDetail =
						// this.getBankCustomerDetail(app,
						// app.getCustomerOverdraftAccountQuery().replace("var1",
						// customerBankAccount.getCifId())
						// .replace("var2", sdf.format(new Date())),
						// new OverdraftAccountDetailMapper(), jdbcTemp);
						List<OverdraftAccountDetail> odAccountDetail = tdptService
								.getBankOverdraftAccountDetail(customerBankAccount.getCifId(), sdf.format(new Date()));
						customerDashboardDetail.setOverdraftAccountDetails(odAccountDetail);
					}
				}
				System.out
						.println("customerBankAccount.getAccountCurrency()==" + customerBankAccount.getAccountCurrency()
								+ " and schm ==" + customerBankAccount.getSchemeCode());

				if ("NGN".equals(customerBankAccount.getAccountCurrency())) {
					if ("ODA".equals(customerBankAccount.getSchemeType())
							|| "CAA".equals(customerBankAccount.getSchemeType())
							|| "SBA".equals(customerBankAccount.getSchemeType())) {
						if ("".equals(currentAccounts)) {
							currentAccounts = customerBankAccount.getAccountNumber().concat("#")
									.concat(customerBankAccount.getBalanceAmount().toString()).concat("#")
									.concat(customerBankAccount.getAccountName());
						} else {
							currentAccounts = currentAccounts.concat(",")
									.concat(customerBankAccount.getAccountNumber().concat("#")
											.concat(customerBankAccount.getBalanceAmount().toString()))
									.concat("#").concat(customerBankAccount.getAccountName());
						}
					}
				} else {
					if (!"TDA".equals(customerBankAccount.getSchemeType())) {
						if ("".equals(domAccounts)) {
							domAccounts = customerBankAccount.getAccountCurrency().concat("#")
									.concat(customerBankAccount.getAccountNumber().concat("#")
											.concat(customerBankAccount.getBalanceAmount().toString()));
						} else {
							domAccounts = domAccounts.concat(",")
									.concat(customerBankAccount.getAccountCurrency().concat("#"))
									.concat(customerBankAccount.getAccountNumber().concat("#")
											.concat(customerBankAccount.getBalanceAmount().toString()));
						}
					}
				}
			}
			// int i = 1;
			for (String currentAcct : Arrays.asList(currentAccounts.split(","))) {
				System.out.println("currentAcct>>>>" + currentAcct);
				String arr[] = currentAcct.split("#");
				if (arr.length > 2) {
					CustomerBankAccount accountDetail = new CustomerBankAccount();
					accountDetail.setAccountNumber(arr[0]);
					accountDetail.setBalanceAmountStr("₦".concat(formatAmount(arr[1] != null ? new BigDecimal(arr[1]) : BigDecimal.ZERO)));
					accountDetail.setAccountCurrency("NGN");
					accountDetail.setAccountName(arr[2]);
					currentAccountList.add(accountDetail);
				}
			}
			customerDashboardDetail.setCurrentAccountList(currentAccountList);
			for (String domAcct : Arrays.asList(domAccounts.split(","))) {
				String arr[] = domAcct.split("#");
				if ("USD".equals(arr[0])) {
					CustomerBankAccount accountDetail = new CustomerBankAccount();
					accountDetail.setAccountNumber(arr[1]);
					accountDetail.setBalanceAmountStr("$".concat(formatAmount(new BigDecimal(arr[2]))));
					domAccountList.add(accountDetail);
				}
				if ("GBP".equals(arr[0])) {
					CustomerBankAccount accountDetail = new CustomerBankAccount();
					accountDetail.setAccountNumber(arr[1]);
					accountDetail.setBalanceAmountStr("€".concat(formatAmount(new BigDecimal(arr[2]))));
					domAccountList.add(accountDetail);
				}
				if ("EUR".equals(arr[0])) {
					CustomerBankAccount accountDetail = new CustomerBankAccount();
					accountDetail.setAccountNumber(arr[1]);
					accountDetail.setBalanceAmountStr("£".concat(formatAmount(new BigDecimal(arr[2]))));
					domAccountList.add(accountDetail);
					// customerDashboardDetail.setDomEURAccountNumber(arr[1]);
					// customerDashboardDetail.setDomEURAccountBalance("£".concat(arr[2]));
				}
			}
			List<AccountMonthDetail> crncySortedData = new ArrayList<AccountMonthDetail>();
			crncySortedData.addAll(monthDetailList.stream().filter(a -> a.getAccountCurrency().equals("NGN")).collect(Collectors.toList()));
			crncySortedData.addAll(monthDetailList.stream().filter(a -> !a.getAccountCurrency().equals("NGN")).collect(Collectors.toList()));
			customerDashboardDetail.setDomAccountList(domAccountList);
			customerDashboardDetail.setAccountMonthDetails(crncySortedData);
			customerDashboardDetail.setCustomerBankWorth("Bank Worth	| ₦ ".concat(formatAmount(bankWorth)));
			customerDashboardDetail.setCustomerWorth(bankWorth);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			/*
			 * if (jdbcTemp != null) { if (jdbcTemp.getDataSource() != null) {
			 * try { jdbcTemp.getDataSource().getConnection().close(); } catch
			 * (SQLException exp) { // TODO Auto-generated catch block
			 * exp.printStackTrace(); } } }
			 */
		}
	}

	public String getFirstDay(Date d) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date dddd = calendar.getTime();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf1.format(dddd);
	}

	public String getLastDay(Date d) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date dddd = calendar.getTime();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf1.format(dddd);
	}

	@Override
	public List<CustomerBankAccountMonthData> getBankAccountMonthData(Application app,
			CustomerBankAccount customerBankAccount, JdbcTemplate jdbcTemp) {

		List<CustomerBankAccountMonthData> accountMonthTransactionsList = new ArrayList<CustomerBankAccountMonthData>();
		// JdbcTemplate jdbcTemp = null;
		try {
			// jdbcTemp = new JdbcTemplate(getDataSource(app));
			String myQuery = app.getCustomerAccountMonthlyTransactionQuery()
					.replace("var1", customerBankAccount.getAcid()).replace("var2", getFirstDay(new Date()))
					.replace("var3", getLastDay(new Date()));
			System.out.println("myQuery>>>" + myQuery);
			accountMonthTransactionsList = jdbcTemp.query(myQuery, new CustomerBankAccountMonthMapper());
		} catch (Exception ex) {
			ex.printStackTrace();
		} /*
			 * finally { if (jdbcTemp != null) { if (jdbcTemp.getDataSource() !=
			 * null) { try { jdbcTemp.getDataSource().getConnection().close(); }
			 * catch (SQLException exp) { // TODO Auto-generated catch block
			 * exp.printStackTrace(); } } } }
			 */
		return accountMonthTransactionsList;
	}

	@Override
	public String formatAmount(BigDecimal value) {
		// System.out.println("formatting value == "+value);
		String format1 = "###,###,##0.00";

		DecimalFormat fm1 = new DecimalFormat(format1);

		String formattedValue = fm1.format(Double.valueOf(value.toString()));

		return formattedValue;
	}

	@Override
	public <T, K> List<T> getBankCustomerDetail(Application app, String queryParam, RowMapper<K> k,
			JdbcTemplate jdbcTemp) {
		// TODO Auto-generated method stub
		List<T> list = new ArrayList<T>();
		// JdbcTemplate jdbcTemp = null;
		try {
			// jdbcTemp = new JdbcTemplate(getDataSource(app));

			// String myQuery = app.getCustomerDetailQuery().replace("var1",
			// queryParam);
			System.out.println("queryParam>>>" + queryParam);
			list = (List<T>) jdbcTemp.query(queryParam, k);
		} catch (Exception ex) {
			ex.printStackTrace();
		} /*
			 * finally { if (jdbcTemp != null) { if (jdbcTemp.getDataSource() !=
			 * null) { try { jdbcTemp.getDataSource().getConnection().close(); }
			 * catch (SQLException exp) { // TODO Auto-generated catch block
			 * exp.printStackTrace(); } } } }
			 */
		return list;
	}

	@Override
	public LoginDetail save(LoginDetail loginDetail) {
		// TODO Auto-generated method stub
		return loginDetailRepo.save(loginDetail);
	}

	@Override
	public HtmlTemplate findByTemplateName(String templateName) {
		// TODO Auto-generated method stub
		return htmlTemplateRepo.findByTemplateName(templateName);
	}

	@Override
	public TransactionDetail save(TransactionDetail transactionDetail) {
		// TODO Auto-generated method stub
		return transactionDetailRepo.save(transactionDetail);
	}

	@Override
	public TransactionType findByTransactionTypeById(Long transactionTypeId) {
		// TODO Auto-generated method stub
		return transactionTypeRepo.getOne(transactionTypeId);
	}

	@Override
	public CustomerBeneficiary save(CustomerBeneficiary customerBeneficiary) {
		return customerBeneficiaryRepo.save(customerBeneficiary);
	}

	@Override
	public Customer findCustomerById(Long customerId) {
		// TODO Auto-generated method stub
		Customer customer = new Customer();
		Optional<Customer> optCustomer = customerRepository.findById(customerId);
		if (optCustomer.isPresent())
			customer = optCustomer.get();
		return customer;
	}

	@Override
	public User findUserById(Long userId) {
		// TODO Auto-generated method stub
		return userRepo.getOne(userId);
	}

	@Override
	public FinancialInstitution findFinancialInstitutionById(String instituteCode) {
		// TODO Auto-generated method stub
		return financialInstitutionRepo.getOne(instituteCode);
	}

	@Override
	public List<FinancialInstitution> financialInstitutions() {
		// TODO Auto-generated method stub
		return financialInstitutionRepo.findAll();
	}

	@Override
	public List<CustomerBeneficiary> findBeneficiariesByCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerBeneficiaryRepo.findByCustomerAndDeleteFlgNot(customer, "Y");
	}

	@Override
	public CustomerBeneficiary findCustomerBeneficiaryById(Long customerBeneficiaryId) {
		// TODO Auto-generated method stub
		return customerBeneficiaryRepo.getOne(customerBeneficiaryId);
	}

	public String getSessionId(String bankCode) {
		String sessionId = "";

		sessionId = bankCode.concat(new SimpleDateFormat("yyMMddHHmmss").format(new Date())).concat("000")
				.concat(String.valueOf(generateRandom(9)));
		return sessionId;
	}

	public static Long generateRandom(int length) {
		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return Long.parseLong(new String(digits));
	}

	@Override
	public ResponseData interbankTransfer(FTSingleCreditRequest req) {
		// TODO Auto-generated method stub
		ResponseData resp = new ResponseData();
		MediaType mediaType = MediaType.parse("application/json");
		Gson gson = new Gson();
		OkHttpClient client = new OkHttpClient();
		com.squareup.okhttp.RequestBody body = com.squareup.okhttp.RequestBody.create(mediaType, gson.toJson(req));
		String url = thirdpartyapiBaseUrlLocal.concat("interBankTransfer");
		System.out.println("gson.toJson(req)==" + gson.toJson(req));
		System.out.println("url==" + url);
		Request request = new Request.Builder().url(url)// "https://customers.coronationsl.com/IWAPISvcsEBiz/api/json/Login/54321/COSECEBIZAPI/P@sw0rd2022")
				.post(body).addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {

			Response response = client.newCall(request).execute();
			String responseStr = response.body().string();
			System.out.println("responseStr==" + responseStr);
			resp = gson.fromJson(responseStr, ResponseData.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resp;

	}

	@Override
	public ResponseData localTransfer(RequestData req) {
		// TODO Auto-generated method stub
		ResponseData resp = new ResponseData();
		MediaType mediaType = MediaType.parse("application/json");
		Gson gson = new Gson();
		OkHttpClient client = new OkHttpClient();
		com.squareup.okhttp.RequestBody body = com.squareup.okhttp.RequestBody.create(mediaType, gson.toJson(req));
		String url = thirdpartyapiBaseUrlLocal.concat("localTransfer");
		System.out.println("gson.toJson(req)==" + gson.toJson(req));
		Request request = new Request.Builder().url(url)// "https://customers.coronationsl.com/IWAPISvcsEBiz/api/json/Login/54321/COSECEBIZAPI/P@sw0rd2022")
				.post(body).addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {

			Response response = client.newCall(request).execute();
			String responseStr = response.body().string();
			System.out.println("responseStr==" + responseStr);
			resp = gson.fromJson(responseStr, ResponseData.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resp;

	}

	@Override
	public TransactionDetail findTransactionDetailById(Long id) {
		// TODO Auto-generated method stub
		return transactionDetailRepo.getOne(id);
	}

	@Override
	public List<MutualFundProduct> findAllMutualFundProducts() {
		// TODO Auto-generated method stub
		return mutualFundProductRepository.findAll();
	}

	@Override
	public List<TopUpSource> findAllTopUpSources() {
		// TODO Auto-generated method stub
		return topUpSourceRepository.findAll();
	}

	@Override
	public List<TopUpType> findAllTopUpTypes() {
		// TODO Auto-generated method stub
		return topUpTypeRepository.findAll();
	}

	@Override
	public List<CustomerSubsidiaryBeneficiary> findCustomerSubsidiaryBeneficiariesByCustomerAndApplication(
			Customer customer, Application application) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.findByCustomerAndApplicationAndDeleteFlgNot(customer, application,
				"Y");
	}

	@Override
	public TopupTransactionDetail saveTopupTransactionDetail(TopupTransactionDetail topupTransactionDetail) {
		// TODO Auto-generated method stub
		return topupTransactionDetailRepo.save(topupTransactionDetail);
	}

	@Override
	public TopupTransactionDetail findTopupTransactionDetailById(Long topupTransactionDetailId) {
		// TODO Auto-generated method stub
		return topupTransactionDetailRepo.getOne(topupTransactionDetailId);
	}

	@Override
	public CustomerSubsidiaryBeneficiary saveCustomerSubsidiaryBeneficiary(
			CustomerSubsidiaryBeneficiary customerSubsudiaryBeneficiary) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.save(customerSubsudiaryBeneficiary);
	}

	@Override
	public MutualFundProduct findMutualFundProductById(Long mutualFundProductId) {
		// TODO Auto-generated method stub
		return mutualFundProductRepository.getOne(mutualFundProductId);
	}

	@Override
	public TopUpSource findTopUpSourceById(Long topUpSourceId) {
		// TODO Auto-generated method stub
		return topUpSourceRepository.getOne(topUpSourceId);
	}

	@Override
	public TopUpType findTopUpTypeById(Long topUpTypeId) {
		// TODO Auto-generated method stub
		return topUpTypeRepository.getOne(topUpTypeId);
	}

	@Override
	public ResponseData reverseLocalTransfer(String uniqueIdentifier) {
		// TODO Auto-generated method stub
		ResponseData resp = new ResponseData();
		MediaType mediaType = MediaType.parse("application/json");
		Gson gson = new Gson();
		OkHttpClient client = new OkHttpClient();
		System.out.println("uniqueIdentifier==" + uniqueIdentifier);
		com.squareup.okhttp.RequestBody body = com.squareup.okhttp.RequestBody.create(mediaType, "");
		String url = thirdpartyapiBaseUrlLocal.concat("reverseFundTransfer?uniqueIdentifier=").concat(uniqueIdentifier);
		System.out.println("url==" + url);
		Request request = new Request.Builder().url(url)// "https://customers.coronationsl.com/IWAPISvcsEBiz/api/json/Login/54321/COSECEBIZAPI/P@sw0rd2022")
				.post(body).addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "c3e98f53-4405-5ce4-2336-9cf00971481d").build();

		try {

			Response response = client.newCall(request).execute();
			String responseStr = response.body().string();
			System.out.println("responseStr==" + responseStr);
			resp = gson.fromJson(responseStr, ResponseData.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resp;
	}

	@Override
	public InfowareResponseCode findByStatusId(String statusId) {
		// TODO Auto-generated method stub
		return infowareResponseCodeRepo.getOne(statusId);
	}

	@Override
	public CustomerSubsidiaryBeneficiary findCustomerSubsidiaryBeneficiaryById(Long id) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.getOne(id);
	}

	@Override
	public List<CustomerBeneficiary> findByCustomerAndDeleteFlgAndBeneficiaryAccountNumber(Customer customer,
			String deleteFlg, String beneficiaryAccountNumber) {
		// TODO Auto-generated method stub
		return customerBeneficiaryRepo.findByCustomerAndDeleteFlgAndBeneficiaryAccountNumber(customer, deleteFlg,
				beneficiaryAccountNumber);
	}

	@Override
	public List<CustomerSubsidiaryBeneficiary> findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(
			Customer customer, Application application, String deleteFlg, String beneficiaryCustomerAid) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.findByCustomerAndApplicationAndDeleteFlgAndBeneficiaryCustomerAid(
				customer, application, deleteFlg, beneficiaryCustomerAid);
	}

	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = getDataSource().getConnection();
		} catch (SQLException cce) {
			cce.printStackTrace();
			// throw new ServiceLocatorException(cce);
		}
		return connection;
	}

	public ResultSet getCustomerDetail(Connection con, Statement stmnt, String myQuery) {

		ResultSet rs = null;
		try {

			System.out.println("after connection query == " + myQuery);

			rs = stmnt.executeQuery(myQuery);

			System.out.println("in here");

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(null, null, null);
		}
		return rs;
	}

	/**
	 * @param con
	 * @param stmnt
	 * @param rs
	 */
	public void closeConnection(Connection con, Statement stmnt, ResultSet rs) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (stmnt != null) {
			try {
				stmnt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<Application> findByNameNot(String name) {
		// TODO Auto-generated method stub
		return applicationRepo.findByNameNot(name);
	}

	@Override
	public List<ChartObject> getChartObject(String accountNumber) {

		List<ChartObject> myList = new ArrayList<ChartObject>();
		DateTimeFormatter dbDateFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

		LocalDate now = LocalDate.now();
		LocalDate initialDate = now.minusMonths(1L);

		Duration duration = Duration.between(now.atStartOfDay(), initialDate.atStartOfDay());
		long diff = Math.abs(duration.toDays());

		// for (CustomerBankAccount account : list) {

		for (int i = 0; i < diff; i++) {
			ChartObject obj = new ChartObject();
			String currDate = initialDate.plusDays(i).format(dbDateFormatter);
			ThirdPartyService thirdPS = new ThirdPartyService();
			CustomerBankAccountBalance balance = thirdPS.getBankCustomerAccountBalance(accountNumber, currDate);
			obj.setDay(currDate);
			obj.setNaira(balance.getClosingBalance());
			myList.add(obj);
		}

		// }

		return myList;

	}

	@Override
	public List<CustomerBeneficiary> findCustomerBeneficiariesByCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerBeneficiaryRepo.findByCustomerAndDeleteFlgNot(customer, "Y");
	}

	@Override
	public List<CustomerSubsidiaryBeneficiary> findCustomerSubBeneficiariesByCustomerAndApplication(Customer customer,
			Application application) {
		// TODO Auto-generated method stub
		return customerSubsidiaryBeneficiaryRepo.findByCustomerAndApplicationAndDeleteFlgNot(customer, application,
				"Y");
	}

	@Override
	public void deleteCustomerBeneficiary(Long id) {
		// TODO Auto-generated method stub
		CustomerBeneficiary customerBeneficiary = customerBeneficiaryRepo.getOne(id);
		customerBeneficiary.setDeleteFlg("Y");
		customerBeneficiaryRepo.save(customerBeneficiary);
	}

	@Override
	public void deleteCustomerSubsidiaryBeneficiary(Long id) {
		// TODO Auto-generated method stub
		CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary = customerSubsidiaryBeneficiaryRepo.getOne(id);
		customerSubsidiaryBeneficiary.setDeleteFlg("Y");
		customerSubsidiaryBeneficiaryRepo.save(customerSubsidiaryBeneficiary);
	}

	@Override
	public Application findApplicationById(Long id) {
		// TODO Auto-generated method stub
		return applicationRepo.getOne(id);
	}

	@Override
	public List<User> findByUserRoleNot(UserRole userRole) {
		// TODO Auto-generated method stub
		return userRepo.findByUserRoleNot(userRole);
	}

	@Override
	public UserRole findByRoleName(String roleName) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByRoleName(roleName);
	}

	@Override
	public List<UserRole> findByRoleNameNot(String roleName) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByRoleNameNot(roleName);
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userRepo.save(user);
	}

	@Override
	public UserRole findById(Long id) {
		// TODO Auto-generated method stub
		return userRoleRepo.getOne(id);
	}

	@Override
	public Status findByStatus(String status) {
		// TODO Auto-generated method stub
		return statusRepo.findByStatus(status);
	}

	@Override
	public List<Status> findAllStatus() {
		// TODO Auto-generated method stub
		return statusRepo.findAll();
	}

	@Override
	public Status findStatusById(Long id) {
		// TODO Auto-generated method stub
		return statusRepo.getOne(id);
	}

	@Override
	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		User deleteUser = userRepo.getOne(id);
		deleteUser.setDeleteFlg("Y");
		userRepo.save(deleteUser);
	}

	@Override
	public AdminResponseDTO createEntrustUser(User updatingUser) {
		EntrustMultiFactorAuthImplService srv = new EntrustMultiFactorAuthImplService();
		String group = "";
		UserAdminDTO arg0 = new UserAdminDTO();
		arg0.setEnableOTP(true);
		arg0.setFullname(updatingUser.getUserId());
		if (updatingUser.getCustomer() == null) {
			group = internalUserEntrustGroup;
		} else {
			group = updatingUser.getCustomer().getCustomerType().getEntrustGroup();
		}
		arg0.setGroup(group);
		arg0.setUserName(updatingUser.getUserId());
		AdminResponseDTO res = srv.getEntrustMultiFactorAuthImplPort().performCreateEntrustUser(arg0);

		return res;

	}

	@Override
	public ContactUsInfo saveContactUsInfo(ContactUsInfo contactUsInfo) {
		// TODO Auto-generated method stub
		return contactUsInfoRepo.save(contactUsInfo);
	}

	@Override
	public List<User> findUsersByRoleName(UserRole userRole) {
		// TODO Auto-generated method stub
		return userRepo.findByUserRole(userRole);
	}

	@Override
	public Image saveImage(Image image) {
		// TODO Auto-generated method stub
		return imageRepo.save(image);
	}
}
