/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * @author waliu.faleye
 *
 */
public class ActiveDirectory {

	//required private variables

    private Properties properties;
    private DirContext dirContext;
    private SearchControls searchCtls;
    private String[] returnAttributes = { "*"};
    private String domainBase;
    //private String baseFilter = "(&((&(objectCategory=Person)(objectClass=User)))";
    private String baseFilter = "(&((&(objectCategory=Person)(objectClass=User)(mail=*coronationmb.com)(memberOf=CN=Configuration,DC=Coronationmb,DC=com))))";


    public ActiveDirectory(String username, String password, String domainController,String url) {
        properties = new Properties();

        properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        properties.put(Context.PROVIDER_URL, "LDAP://" + url);
        properties.put(Context.SECURITY_PRINCIPAL, username + "@" + domainController);
        properties.put(Context.SECURITY_CREDENTIALS, password);

        //initializing active directory LDAP connection
        try {
            dirContext = new InitialDirContext(properties);
            System.out.println("hey");
        } catch (NamingException e) {
            //LOG.severe(e.getMessage());
            e.printStackTrace();
        }

        //default domain base for search
        domainBase = getDomainBase(domainController);

        //initializing search controls
        searchCtls = new SearchControls();
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchCtls.setReturningAttributes(returnAttributes);
    }



	public NamingEnumeration<SearchResult> searchUser(String searchValue, String searchBy, String searchBase) throws NamingException {
        String filter = getFilter(searchValue, searchBy);
        String base = (null == searchBase) ? domainBase : getDomainBase(searchBase);
       return this.dirContext.search(base, filter, this.searchCtls);
    }


    public void closeLdapConnection(){
        try {
            if(dirContext != null)
                dirContext.close();
        }
        catch (NamingException e) {
          //e.printStackTrace();
        }
    }


    private String getFilter(String searchValue, String searchBy) {
        String filter = this.baseFilter;
        if(searchBy.equals("email")) {
           filter += "(mail=" + searchValue + "))";
        } else if(searchBy.equals("username")) {
            filter += "(samaccountname=" + searchValue + "))";
        }else if(searchBy.equals("title")) {
            filter += "(title=" + searchValue + "))";
        }else if(searchBy.equals("department")) {
            filter += "(department=" + searchValue + "))";
        }else if(searchBy.equals("givenname")) {
            filter += "(givenname=" + searchValue + "))";
        }
        else if(searchBy.equals("samaccountname")) {
            filter += "(samaccountname=" + searchValue + "))";
        }

        return filter;
    }

    private static String getDomainBase(String base) {
        char[] namePair = base.toUpperCase().toCharArray();
        String dn = "DC=";
        for (int i = 0; i < namePair.length; i++) {
            if (namePair[i] == '.') {
                dn += ",DC=" + namePair[++i];
            } else {
                dn += namePair[i];
            }
        }
        return dn;
    }
}
