/**
 * 
 */
package com.cmb.consolidatedapps.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import com.cmb.consolidatedapps.model.CustomerBankAccountMonthData;
import com.cmb.consolidatedapps.model.FITransactionDetail;
import com.infosys.ci.fiusb.webservice.FIUsbWebServiceService;

/**
 * @author waliu.faleye
 *
 */
public class Test {

	public static void main(String[] arg) {
		try {
			System.out.println(new BigDecimal("2.3914126748E8"));
			DecimalFormat decimalFormat = new DecimalFormat("#,###.##");
			decimalFormat.setParseBigDecimal(true);
			BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse("₦399,950.96".substring(1));
			System.out.println(bigDecimal);			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");
			System.out.println(LocalDateTime.now().format(formatter));
			System.out.println(new BigDecimal("200000.0000"));
		} catch (Exception ex) {
		}
	}

	/*
	 * public static void main(String[] args) { FIUsbWebServiceService svc = new
	 * FIUsbWebServiceService(); try { List<FITransactionDetail>
	 * custBankTranList = new ArrayList<FITransactionDetail>();
	 * FITransactionDetail lastTranDetail = new FITransactionDetail(); int i =
	 * 0; do { System.out.println("lastTranDetail.getHasMoreData()==" +
	 * lastTranDetail.getHasMoreData()); List<FITransactionDetail> bankTranList
	 * = svc.getAccountStatement("1990007748-123451".concat(String.valueOf(i)),
	 * "2018-07-10T11:59:59.799", "1990003472", "2018-06-10T11:59:59.799",
	 * "2018-07-10T11:59:59.799", lastTranDetail);
	 * custBankTranList.addAll(bankTranList); lastTranDetail =
	 * bankTranList.get(bankTranList.size() - 1); i++; } while
	 * ("Y".equals(lastTranDetail.getHasMoreData()));
	 * 
	 * System.out.println("custBankTranList.size()===="+custBankTranList.size())
	 * ; // String gain = "17.74"; // String cost = "124.75"; //
	 * System.out.println(new BigDecimal(gain).multiply(new //
	 * BigDecimal("100")).divide(new BigDecimal(cost),2, //
	 * RoundingMode.HALF_UP));
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); } }
	 */
	public String getFirstDay(Date d) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date dddd = calendar.getTime();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf1.format(dddd);
	}

	public String getLastDay(Date d) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date dddd = calendar.getTime();
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		return sdf1.format(dddd);
	}
}
