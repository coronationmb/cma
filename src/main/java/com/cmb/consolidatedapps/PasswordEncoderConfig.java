/**
 * 
 */
package com.cmb.consolidatedapps;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cmb.consolidatedapps.interfaces.PasswordEncoder;

/**
 * @author waliu.faleye
 *
 */
@Configuration
public class PasswordEncoderConfig {

	@Bean
	public PasswordEncoder passwordEncoder() {

		return new PasswordEncoder() {

			@Override
			public String encode(CharSequence rawPassword) {
				return rawPassword.toString();
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return rawPassword.toString().equals(encodedPassword);
			}

		};

	}

}
