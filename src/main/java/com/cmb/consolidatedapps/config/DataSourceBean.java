/**
 * 
 */
package com.cmb.consolidatedapps.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author waliu.faleye
 *
 */

@Configuration
@Component
// @EnableJpaRepositories(basePackages = "com.cmb.repository.interfaces",
// entityManagerFactoryRef = "servicesEntityManager", transactionManagerRef =
// "servicesTransactionManager")
public class DataSourceBean {

	@ConfigurationProperties(prefix = "spring.oracledatasource")
	@Bean(name = "oracleDatasource")
	public DataSource getOracleDataSource() {
		return DataSourceBuilder.create().build();
	}

	@ConfigurationProperties(prefix = "db.infoware.asset")
	@Bean(name = "assetSqlDatasource")
	public DataSource getAssetSqlDataSource() {
		return DataSourceBuilder.create().build();
	}

	@ConfigurationProperties(prefix = "db.infoware.sec")
	@Bean(name = "secSqlDatasource")
	public DataSource getSecSqlDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Value("${spring.jpa.hibernate.ddl-auto}")
	String hibernateddlauto;

	@Value("${spring.jpa.database-platform}")
	String databaseplatform;

	@Bean(name = "finacleTemplate")
	public JdbcTemplate getFinacleJdbcTemplate() {
		return new JdbcTemplate(getOracleDataSource());
	}

	@Bean(name = "assetMgtSqlTemplate")
	public JdbcTemplate getJdbcTemplate() {
		return new JdbcTemplate(getAssetSqlDataSource());
	}

	@Bean(name = "securitiesSqlTemplate")
	public JdbcTemplate getSecuritiesJdbcTemplate() {
		return new JdbcTemplate(getSecSqlDataSource());
	}

}
