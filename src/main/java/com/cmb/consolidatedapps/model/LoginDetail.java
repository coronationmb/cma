/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name="login_detail")
public class LoginDetail {
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="login_detail_generator")
	@SequenceGenerator(name="login_detail_generator",allocationSize=1,sequenceName="login_detail_gen")
	@Id
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "user_id",nullable=false)
	private User user;
	
	@Column(name = "login_time",nullable=false)
	private LocalDateTime loginTime;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the logintime
	 */
	public LocalDateTime getLoginTime() {
		return loginTime;
	}

	/**
	 * @param logintime the logintime to set
	 */
	public void setLoginTime(LocalDateTime loginTime) {
		this.loginTime = loginTime;
	}

}
