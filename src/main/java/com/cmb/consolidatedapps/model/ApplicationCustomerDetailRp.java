/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class ApplicationCustomerDetailRp {

	private String customerName;

	private String phoneNumber;

	private String email;

	private String corporateUserBvn;

	private String cifId;
	
	private String firstName;
	
	
	private String middleName;
	
	
	private String lastName;
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the corporateUserBvn
	 */
	public String getCorporateUserBvn() {
		return corporateUserBvn;
	}

	/**
	 * @param corporateUserBvn
	 *            the corporateUserBvn to set
	 */
	public void setCorporateUserBvn(String corporateUserBvn) {
		this.corporateUserBvn = corporateUserBvn;
	}

	/**
	 * @return the cifId
	 */
	public String getCifId() {
		return cifId;
	}

	/**
	 * @param cifId the cifId to set
	 */
	public void setCifId(String cifId) {
		this.cifId = cifId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
