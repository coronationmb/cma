/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;

/**
 * @author waliu.faleye
 *
 */
public class CustomerBankAccountBalance {

	private String accountNumber;

	private String accountName;

	private BigDecimal openingBalance;

	private BigDecimal closingBalance;

	private BigDecimal clrBalanceAmount;

	private BigDecimal currentAvailableBalance;

	private String teamCode;

	private String accountOfficer;

	private BigDecimal lienAmount;
	
	private String currency;
	
	private String address;

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the closingBalance
	 */
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	/**
	 * @param closingBalance the closingBalance to set
	 */
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}

	/**
	 * @return the clrBalanceAmount
	 */
	public BigDecimal getClrBalanceAmount() {
		return clrBalanceAmount;
	}

	/**
	 * @param clrBalanceAmount the clrBalanceAmount to set
	 */
	public void setClrBalanceAmount(BigDecimal clrBalanceAmount) {
		this.clrBalanceAmount = clrBalanceAmount;
	}

	/**
	 * @return the currentAvailableBalance
	 */
	public BigDecimal getCurrentAvailableBalance() {
		return currentAvailableBalance;
	}

	/**
	 * @param currentAvailableBalance the currentAvailableBalance to set
	 */
	public void setCurrentAvailableBalance(BigDecimal currentAvailableBalance) {
		this.currentAvailableBalance = currentAvailableBalance;
	}

	/**
	 * @return the teamCode
	 */
	public String getTeamCode() {
		return teamCode;
	}

	/**
	 * @param teamCode the teamCode to set
	 */
	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}

	/**
	 * @return the accountOfficer
	 */
	public String getAccountOfficer() {
		return accountOfficer;
	}

	/**
	 * @param accountOfficer the accountOfficer to set
	 */
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}

	/**
	 * @return the lienAmount
	 */
	public BigDecimal getLienAmount() {
		return lienAmount;
	}

	/**
	 * @param lienAmount the lienAmount to set
	 */
	public void setLienAmount(BigDecimal lienAmount) {
		this.lienAmount = lienAmount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

}
