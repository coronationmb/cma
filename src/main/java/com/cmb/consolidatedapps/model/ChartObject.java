/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;

/**
 * @author waliu.faleye
 *
 */
public class ChartObject {
	
	private String Day;
	
	private BigDecimal Naira;

	/**
	 * @return the day
	 */
	public String getDay() {
		return Day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		Day = day;
	}

	/**
	 * @return the naira
	 */
	public BigDecimal getNaira() {
		return Naira;
	}

	/**
	 * @param naira the naira to set
	 */
	public void setNaira(BigDecimal naira) {
		Naira = naira;
	}

}
