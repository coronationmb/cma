/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.Date;

/**
 * @author waliu.faleye
 *
 */
public class FTSingleCreditRequest {

	
	private String sessionID;
	
	private String nameEnquiryRef;
	
	private String destinationInstitutionCode;
	
	private String channelCode;
	
	private String beneficiaryAccountName;
	
	private String beneficiaryAccountNumber;
	
	private String beneficiaryBankVerificationNumber;
	
	private String beneficiaryKYCLevel;
	
	private String originatorAccountName;
	
	private String originatorAccountNumber;
	
	private String originatorBankVerificationNumber;
	
	private String originatorKYCLevel;
	
	private String transactionLocation;
	
	private String narration;
	
	private String paymentReference;
	
	private String amount;
	
	private String transactionChannel;
	
	private String responseCode;
	
	private Date requestTime;
	
	private String responseDescription;

	@Override
	public String toString() {
		return "sessionID = " + sessionID + " nameEnquiryRef=" + nameEnquiryRef + " destinationInstitutionCode="
				+ destinationInstitutionCode + " channelCode=" + channelCode + " beneficiaryAccountName="
				+ beneficiaryAccountName + " beneficiaryAccountNumber=" + beneficiaryAccountNumber
				+ " beneficiaryBankVerificationNumber=" + beneficiaryBankVerificationNumber + " beneficiaryKYCLevel="
				+ beneficiaryKYCLevel + " originatorAccountName=" + originatorAccountName + " originatorAccountNumber="
				+ originatorAccountNumber + " originatorBankVerificationNumber=" + originatorBankVerificationNumber
				+ " originatorKYCLevel=" + originatorKYCLevel + " transactionLocation=" + transactionLocation
				+ " narration=" + narration + " paymentReference=" + paymentReference + " amount=" + amount;
	}

	/**
	 * @return the sessionID
	 */
	public String getSessionID() {
		return sessionID;
	}

	/**
	 * @param sessionID
	 *            the sessionID to set
	 */
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	/**
	 * @return the nameEnquiryRef
	 */
	public String getNameEnquiryRef() {
		return nameEnquiryRef;
	}

	/**
	 * @param nameEnquiryRef
	 *            the nameEnquiryRef to set
	 */
	public void setNameEnquiryRef(String nameEnquiryRef) {
		this.nameEnquiryRef = nameEnquiryRef;
	}

	/**
	 * @return the destinationInstitutionCode
	 */
	public String getDestinationInstitutionCode() {
		return destinationInstitutionCode;
	}

	/**
	 * @param destinationInstitutionCode
	 *            the destinationInstitutionCode to set
	 */
	public void setDestinationInstitutionCode(String destinationInstitutionCode) {
		this.destinationInstitutionCode = destinationInstitutionCode;
	}

	/**
	 * @return the channelCode
	 */
	public String getChannelCode() {
		return channelCode;
	}

	/**
	 * @param channelCode
	 *            the channelCode to set
	 */
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	/**
	 * @return the beneficiaryAccountName
	 */
	public String getBeneficiaryAccountName() {
		return beneficiaryAccountName;
	}

	/**
	 * @param beneficiaryAccountName
	 *            the beneficiaryAccountName to set
	 */
	public void setBeneficiaryAccountName(String beneficiaryAccountName) {
		this.beneficiaryAccountName = beneficiaryAccountName;
	}

	/**
	 * @return the beneficiaryAccountNumber
	 */
	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	/**
	 * @param beneficiaryAccountNumber
	 *            the beneficiaryAccountNumber to set
	 */
	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	/**
	 * @return the beneficiaryBankVerificationNumber
	 */
	public String getBeneficiaryBankVerificationNumber() {
		return beneficiaryBankVerificationNumber;
	}

	/**
	 * @param beneficiaryBankVerificationNumber
	 *            the beneficiaryBankVerificationNumber to set
	 */
	public void setBeneficiaryBankVerificationNumber(String beneficiaryBankVerificationNumber) {
		this.beneficiaryBankVerificationNumber = beneficiaryBankVerificationNumber;
	}

	/**
	 * @return the beneficiaryKYCLevel
	 */
	public String getBeneficiaryKYCLevel() {
		return beneficiaryKYCLevel;
	}

	/**
	 * @param beneficiaryKYCLevel
	 *            the beneficiaryKYCLevel to set
	 */
	public void setBeneficiaryKYCLevel(String beneficiaryKYCLevel) {
		this.beneficiaryKYCLevel = beneficiaryKYCLevel;
	}

	/**
	 * @return the originatorAccountName
	 */
	public String getOriginatorAccountName() {
		return originatorAccountName;
	}

	/**
	 * @param originatorAccountName
	 *            the originatorAccountName to set
	 */
	public void setOriginatorAccountName(String originatorAccountName) {
		this.originatorAccountName = originatorAccountName;
	}

	/**
	 * @return the originatorAccountNumber
	 */
	public String getOriginatorAccountNumber() {
		return originatorAccountNumber;
	}

	/**
	 * @param originatorAccountNumber
	 *            the originatorAccountNumber to set
	 */
	public void setOriginatorAccountNumber(String originatorAccountNumber) {
		this.originatorAccountNumber = originatorAccountNumber;
	}

	/**
	 * @return the originatorBankVerificationNumber
	 */
	public String getOriginatorBankVerificationNumber() {
		return originatorBankVerificationNumber;
	}

	/**
	 * @param originatorBankVerificationNumber
	 *            the originatorBankVerificationNumber to set
	 */
	public void setOriginatorBankVerificationNumber(String originatorBankVerificationNumber) {
		this.originatorBankVerificationNumber = originatorBankVerificationNumber;
	}

	/**
	 * @return the originatorKYCLevel
	 */
	public String getOriginatorKYCLevel() {
		return originatorKYCLevel;
	}

	/**
	 * @param originatorKYCLevel
	 *            the originatorKYCLevel to set
	 */
	public void setOriginatorKYCLevel(String originatorKYCLevel) {
		this.originatorKYCLevel = originatorKYCLevel;
	}

	/**
	 * @return the transactionLocation
	 */
	public String getTransactionLocation() {
		return transactionLocation;
	}

	/**
	 * @param transactionLocation
	 *            the transactionLocation to set
	 */
	public void setTransactionLocation(String transactionLocation) {
		this.transactionLocation = transactionLocation;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @param narration
	 *            the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}

	/**
	 * @return the paymentReference
	 */
	public String getPaymentReference() {
		return paymentReference;
	}

	/**
	 * @param paymentReference
	 *            the paymentReference to set
	 */
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the transactionChannel
	 */
	public String getTransactionChannel() {
		return transactionChannel;
	}

	/**
	 * @param transactionChannel the transactionChannel to set
	 */
	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the requestTime
	 */
	public Date getRequestTime() {
		return requestTime;
	}

	/**
	 * @param requestTime the requestTime to set
	 */
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	/**
	 * @return the responseDescription
	 */
	public String getResponseDescription() {
		return responseDescription;
	}

	/**
	 * @param responseDescription the responseDescription to set
	 */
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
}
