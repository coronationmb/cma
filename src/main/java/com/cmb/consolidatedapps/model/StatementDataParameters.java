/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.List;

/**
 * @author waliu.faleye
 *
 */
public class StatementDataParameters {

	private String customerName;
	private String address;
	private String currency;
	private String accountNo;
	private String openingBalance;
	private String closingBalance;
	private String availableBalance;
	private String totalDebit;
	private String totalCredit;
	private String debitCount;
	private String creditCount;
	private net.sf.jasperreports.engine.data.JRBeanCollectionDataSource DS1;
	private List security_data_all;
	private List am_data_all;
	private List bankaccount_data_all;
	private String amCustomerAID;
	private String secCustomerAID;
	private String amOpeningBalance;
	private String secOpeningBalance;
	private String amClosingBalance;
	private String secClosingBalance;
	private String amAvailableBalance;
	private String secAvailableBalance;
	private String amTotalDebit;
	private String secTotalDebit;
	private String secTotalCredit;
	private String amTotalCredit;
	private String amDebitCount;
	private String secDebitCount;
	private String amCreditCount;
	private String secCreditCount;
	private Long app1;
	private Long app2;
	private Long app3;
	private Long app4;
	private String amAddress;
	private String secAddress;
	private String fundName;
	private String accountOfficer;
	private String currentPrincipal;
	private String accruedInterest;
	private String valueOfTotalHoldings;
	private String cash;

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo
	 *            the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the openingBalance
	 */
	public String getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance
	 *            the openingBalance to set
	 */
	public void setOpeningBalance(String openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the closingBalance
	 */
	public String getClosingBalance() {
		return closingBalance;
	}

	/**
	 * @param closingBalance
	 *            the closingBalance to set
	 */
	public void setClosingBalance(String closingBalance) {
		this.closingBalance = closingBalance;
	}

	/**
	 * @return the availableBalance
	 */
	public String getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * @param availableBalance
	 *            the availableBalance to set
	 */
	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * @return the totalDebit
	 */
	public String getTotalDebit() {
		return totalDebit;
	}

	/**
	 * @param totalDebit
	 *            the totalDebit to set
	 */
	public void setTotalDebit(String totalDebit) {
		this.totalDebit = totalDebit;
	}

	/**
	 * @return the totalCredit
	 */
	public String getTotalCredit() {
		return totalCredit;
	}

	/**
	 * @param totalCredit
	 *            the totalCredit to set
	 */
	public void setTotalCredit(String totalCredit) {
		this.totalCredit = totalCredit;
	}

	/**
	 * @return the debitCount
	 */
	public String getDebitCount() {
		return debitCount;
	}

	/**
	 * @param debitCount
	 *            the debitCount to set
	 */
	public void setDebitCount(String debitCount) {
		this.debitCount = debitCount;
	}

	/**
	 * @return the creditCount
	 */
	public String getCreditCount() {
		return creditCount;
	}

	/**
	 * @param creditCount
	 *            the creditCount to set
	 */
	public void setCreditCount(String creditCount) {
		this.creditCount = creditCount;
	}

	/**
	 * @return the dS1
	 */
	public net.sf.jasperreports.engine.data.JRBeanCollectionDataSource getDS1() {
		return DS1;
	}

	/**
	 * @param dS1
	 *            the dS1 to set
	 */
	public void setDS1(net.sf.jasperreports.engine.data.JRBeanCollectionDataSource dS1) {
		DS1 = dS1;
	}

	/**
	 * @return the security_data_all
	 */
	public List getSecurity_data_all() {
		return security_data_all;
	}

	/**
	 * @param security_data_all
	 *            the security_data_all to set
	 */
	public void setSecurity_data_all(List security_data_all) {
		this.security_data_all = security_data_all;
	}

	/**
	 * @return the am_data_all
	 */
	public List getAm_data_all() {
		return am_data_all;
	}

	/**
	 * @param am_data_all
	 *            the am_data_all to set
	 */
	public void setAm_data_all(List am_data_all) {
		this.am_data_all = am_data_all;
	}

	/**
	 * @return the bankaccount_data_all
	 */
	public List getBankaccount_data_all() {
		return bankaccount_data_all;
	}

	/**
	 * @param bankaccount_data_all
	 *            the bankaccount_data_all to set
	 */
	public void setBankaccount_data_all(List bankaccount_data_all) {
		this.bankaccount_data_all = bankaccount_data_all;
	}

	/**
	 * @return the amCustomerAID
	 */
	public String getAmCustomerAID() {
		return amCustomerAID;
	}

	/**
	 * @param amCustomerAID
	 *            the amCustomerAID to set
	 */
	public void setAmCustomerAID(String amCustomerAID) {
		this.amCustomerAID = amCustomerAID;
	}

	/**
	 * @return the secCustomerAID
	 */
	public String getSecCustomerAID() {
		return secCustomerAID;
	}

	/**
	 * @param secCustomerAID
	 *            the secCustomerAID to set
	 */
	public void setSecCustomerAID(String secCustomerAID) {
		this.secCustomerAID = secCustomerAID;
	}

	/**
	 * @return the amOpeningBalance
	 */
	public String getAmOpeningBalance() {
		return amOpeningBalance;
	}

	/**
	 * @param amOpeningBalance
	 *            the amOpeningBalance to set
	 */
	public void setAmOpeningBalance(String amOpeningBalance) {
		this.amOpeningBalance = amOpeningBalance;
	}

	/**
	 * @return the secOpeningBalance
	 */
	public String getSecOpeningBalance() {
		return secOpeningBalance;
	}

	/**
	 * @param secOpeningBalance
	 *            the secOpeningBalance to set
	 */
	public void setSecOpeningBalance(String secOpeningBalance) {
		this.secOpeningBalance = secOpeningBalance;
	}

	/**
	 * @return the amClosingBalance
	 */
	public String getAmClosingBalance() {
		return amClosingBalance;
	}

	/**
	 * @param amClosingBalance
	 *            the amClosingBalance to set
	 */
	public void setAmClosingBalance(String amClosingBalance) {
		this.amClosingBalance = amClosingBalance;
	}

	/**
	 * @return the secClosingBalance
	 */
	public String getSecClosingBalance() {
		return secClosingBalance;
	}

	/**
	 * @param secClosingBalance
	 *            the secClosingBalance to set
	 */
	public void setSecClosingBalance(String secClosingBalance) {
		this.secClosingBalance = secClosingBalance;
	}

	/**
	 * @return the amAvailableBalance
	 */
	public String getAmAvailableBalance() {
		return amAvailableBalance;
	}

	/**
	 * @param amAvailableBalance
	 *            the amAvailableBalance to set
	 */
	public void setAmAvailableBalance(String amAvailableBalance) {
		this.amAvailableBalance = amAvailableBalance;
	}

	/**
	 * @return the secAvailableBalance
	 */
	public String getSecAvailableBalance() {
		return secAvailableBalance;
	}

	/**
	 * @param secAvailableBalance
	 *            the secAvailableBalance to set
	 */
	public void setSecAvailableBalance(String secAvailableBalance) {
		this.secAvailableBalance = secAvailableBalance;
	}

	/**
	 * @return the amTotalDebit
	 */
	public String getAmTotalDebit() {
		return amTotalDebit;
	}

	/**
	 * @param amTotalDebit
	 *            the amTotalDebit to set
	 */
	public void setAmTotalDebit(String amTotalDebit) {
		this.amTotalDebit = amTotalDebit;
	}

	/**
	 * @return the secTotalDebit
	 */
	public String getSecTotalDebit() {
		return secTotalDebit;
	}

	/**
	 * @param secTotalDebit
	 *            the secTotalDebit to set
	 */
	public void setSecTotalDebit(String secTotalDebit) {
		this.secTotalDebit = secTotalDebit;
	}

	/**
	 * @return the secTotalCredit
	 */
	public String getSecTotalCredit() {
		return secTotalCredit;
	}

	/**
	 * @param secTotalCredit
	 *            the secTotalCredit to set
	 */
	public void setSecTotalCredit(String secTotalCredit) {
		this.secTotalCredit = secTotalCredit;
	}

	/**
	 * @return the amTotalCredit
	 */
	public String getAmTotalCredit() {
		return amTotalCredit;
	}

	/**
	 * @param amTotalCredit
	 *            the amTotalCredit to set
	 */
	public void setAmTotalCredit(String amTotalCredit) {
		this.amTotalCredit = amTotalCredit;
	}

	/**
	 * @return the amDebitCount
	 */
	public String getAmDebitCount() {
		return amDebitCount;
	}

	/**
	 * @param amDebitCount
	 *            the amDebitCount to set
	 */
	public void setAmDebitCount(String amDebitCount) {
		this.amDebitCount = amDebitCount;
	}

	/**
	 * @return the secDebitCount
	 */
	public String getSecDebitCount() {
		return secDebitCount;
	}

	/**
	 * @param secDebitCount
	 *            the secDebitCount to set
	 */
	public void setSecDebitCount(String secDebitCount) {
		this.secDebitCount = secDebitCount;
	}

	/**
	 * @return the amCreditCount
	 */
	public String getAmCreditCount() {
		return amCreditCount;
	}

	/**
	 * @param amCreditCount
	 *            the amCreditCount to set
	 */
	public void setAmCreditCount(String amCreditCount) {
		this.amCreditCount = amCreditCount;
	}

	/**
	 * @return the secCreditCount
	 */
	public String getSecCreditCount() {
		return secCreditCount;
	}

	/**
	 * @param secCreditCount
	 *            the secCreditCount to set
	 */
	public void setSecCreditCount(String secCreditCount) {
		this.secCreditCount = secCreditCount;
	}

	/**
	 * @return the app1
	 */
	public Long getApp1() {
		return app1;
	}

	/**
	 * @param app1 the app1 to set
	 */
	public void setApp1(Long app1) {
		this.app1 = app1;
	}

	/**
	 * @return the app2
	 */
	public Long getApp2() {
		return app2;
	}

	/**
	 * @param app2 the app2 to set
	 */
	public void setApp2(Long app2) {
		this.app2 = app2;
	}

	/**
	 * @return the app3
	 */
	public Long getApp3() {
		return app3;
	}

	/**
	 * @param app3 the app3 to set
	 */
	public void setApp3(Long app3) {
		this.app3 = app3;
	}

	/**
	 * @return the app4
	 */
	public Long getApp4() {
		return app4;
	}

	/**
	 * @param app4 the app4 to set
	 */
	public void setApp4(Long app4) {
		this.app4 = app4;
	}

	/**
	 * @return the amAddress
	 */
	public String getAmAddress() {
		return amAddress;
	}

	/**
	 * @param amAddress the amAddress to set
	 */
	public void setAmAddress(String amAddress) {
		this.amAddress = amAddress;
	}

	/**
	 * @return the secAddress
	 */
	public String getSecAddress() {
		return secAddress;
	}

	/**
	 * @param secAddress the secAddress to set
	 */
	public void setSecAddress(String secAddress) {
		this.secAddress = secAddress;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the accountOfficer
	 */
	public String getAccountOfficer() {
		return accountOfficer;
	}

	/**
	 * @param accountOfficer the accountOfficer to set
	 */
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}

	/**
	 * @return the currentPrincipal
	 */
	public String getCurrentPrincipal() {
		return currentPrincipal;
	}

	/**
	 * @param currentPrincipal the currentPrincipal to set
	 */
	public void setCurrentPrincipal(String currentPrincipal) {
		this.currentPrincipal = currentPrincipal;
	}

	/**
	 * @return the accruedInterest
	 */
	public String getAccruedInterest() {
		return accruedInterest;
	}

	/**
	 * @param accruedInterest the accruedInterest to set
	 */
	public void setAccruedInterest(String accruedInterest) {
		this.accruedInterest = accruedInterest;
	}

	/**
	 * @return the valueOfTotalHoldings
	 */
	public String getValueOfTotalHoldings() {
		return valueOfTotalHoldings;
	}

	/**
	 * @param valueOfTotalHoldings the valueOfTotalHoldings to set
	 */
	public void setValueOfTotalHoldings(String valueOfTotalHoldings) {
		this.valueOfTotalHoldings = valueOfTotalHoldings;
	}

	/**
	 * @return the cash
	 */
	public String getCash() {
		return cash;
	}

	/**
	 * @param cash the cash to set
	 */
	public void setCash(String cash) {
		this.cash = cash;
	}

}
