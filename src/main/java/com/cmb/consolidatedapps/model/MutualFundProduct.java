/**
 * 
 */
package com.cmb.consolidatedapps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name="mutual_fund_product")
public class MutualFundProduct {
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	
	@Column(name="fund_name")
	private String fundName;
	
	@Column(unique = true,name="fund_code")
	private String fundCode;
	
	@Column(name="fund_type")
	private String fundType;
	
	@Column(name="dr_master_aid")
	private String drMasterAid;
	
	@Column(name="dr_sub_aid")
	private String drSubAid;
	
	@Column(name="cr_master_aid")
	private String crMasterAid;
	
	@Column(name="bank_credit_account")
	private String bankCreditAccount;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	/**
	 * @return the fundType
	 */
	public String getFundType() {
		return fundType;
	}

	/**
	 * @param fundType the fundType to set
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	/**
	 * @return the drMasterAid
	 */
	public String getDrMasterAid() {
		return drMasterAid;
	}

	/**
	 * @param drMasterAid the drMasterAid to set
	 */
	public void setDrMasterAid(String drMasterAid) {
		this.drMasterAid = drMasterAid;
	}

	/**
	 * @return the drSubAid
	 */
	public String getDrSubAid() {
		return drSubAid;
	}

	/**
	 * @param drSubAid the drSubAid to set
	 */
	public void setDrSubAid(String drSubAid) {
		this.drSubAid = drSubAid;
	}

	/**
	 * @return the crMasterAid
	 */
	public String getCrMasterAid() {
		return crMasterAid;
	}

	/**
	 * @param crMasterAid the crMasterAid to set
	 */
	public void setCrMasterAid(String crMasterAid) {
		this.crMasterAid = crMasterAid;
	}

	/**
	 * @return the bankCreditAccount
	 */
	public String getBankCreditAccount() {
		return bankCreditAccount;
	}

	/**
	 * @param bankCreditAccount the bankCreditAccount to set
	 */
	public void setBankCreditAccount(String bankCreditAccount) {
		this.bankCreditAccount = bankCreditAccount;
	}

}
