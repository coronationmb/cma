/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;

/**
 * @author waliu.faleye
 *
 */
public class CustomerBankAccount {

	private String acid;
	
	private String accountNumber;
	
	private String displayAccountNumber;
	
	private String accountName;
	
	private String schemeCode;
	
	private BigDecimal balanceAmount;
	
	private String balanceAmountStr;
	
	private String accountCurrency;
	
	private String schemeType;
	
	private String cifId;
	
	private String accountType;
	
	private BigDecimal ngnBalanceAmount;
	
	private String ngnBalanceAmountStr;
	
	private String accountOfficerCif;
	
	private String accountOfficerPhone;
	
	private String accountOfficerEmail;
	
	private String accountOfficerCode;
	
	private String accountOfficerTeamCode;
	
	private String accountOfficerName;

	/**
	 * @return the acid
	 */
	public String getAcid() {
		return acid;
	}

	/**
	 * @param acid the acid to set
	 */
	public void setAcid(String acid) {
		this.acid = acid;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the schemeCode
	 */
	public String getSchemeCode() {
		return schemeCode;
	}

	/**
	 * @param schemeCode the schemeCode to set
	 */
	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	/**
	 * @return the balanceAmount
	 */
	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	/**
	 * @param balanceAmount the balanceAmount to set
	 */
	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	/**
	 * @return the balanceAmountStr
	 */
	public String getBalanceAmountStr() {
		return balanceAmountStr;
	}

	/**
	 * @param balanceAmountStr the balanceAmountStr to set
	 */
	public void setBalanceAmountStr(String balanceAmountStr) {
		this.balanceAmountStr = balanceAmountStr;
	}

	/**
	 * @return the accountCurrency
	 */
	public String getAccountCurrency() {
		return accountCurrency;
	}

	/**
	 * @param accountCurrency the accountCurrency to set
	 */
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	/**
	 * @return the schemeType
	 */
	public String getSchemeType() {
		return schemeType;
	}

	/**
	 * @param schemeType the schemeType to set
	 */
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	/**
	 * @return the cifId
	 */
	public String getCifId() {
		return cifId;
	}

	/**
	 * @param cifId the cifId to set
	 */
	public void setCifId(String cifId) {
		this.cifId = cifId;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * @return the ngnBalanceAmount
	 */
	public BigDecimal getNgnBalanceAmount() {
		return ngnBalanceAmount;
	}

	/**
	 * @param ngnBalanceAmount the ngnBalanceAmount to set
	 */
	public void setNgnBalanceAmount(BigDecimal ngnBalanceAmount) {
		this.ngnBalanceAmount = ngnBalanceAmount;
	}

	/**
	 * @return the ngnBalanceAmountStr
	 */
	public String getNgnBalanceAmountStr() {
		return ngnBalanceAmountStr;
	}

	/**
	 * @param ngnBalanceAmountStr the ngnBalanceAmountStr to set
	 */
	public void setNgnBalanceAmountStr(String ngnBalanceAmountStr) {
		this.ngnBalanceAmountStr = ngnBalanceAmountStr;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the accountOfficerCif
	 */
	public String getAccountOfficerCif() {
		return accountOfficerCif;
	}

	/**
	 * @param accountOfficerCif the accountOfficerCif to set
	 */
	public void setAccountOfficerCif(String accountOfficerCif) {
		this.accountOfficerCif = accountOfficerCif;
	}

	/**
	 * @return the accountOfficerPhone
	 */
	public String getAccountOfficerPhone() {
		return accountOfficerPhone;
	}

	/**
	 * @param accountOfficerPhone the accountOfficerPhone to set
	 */
	public void setAccountOfficerPhone(String accountOfficerPhone) {
		this.accountOfficerPhone = accountOfficerPhone;
	}

	/**
	 * @return the accountOfficerEmail
	 */
	public String getAccountOfficerEmail() {
		return accountOfficerEmail;
	}

	/**
	 * @param accountOfficerEmail the accountOfficerEmail to set
	 */
	public void setAccountOfficerEmail(String accountOfficerEmail) {
		this.accountOfficerEmail = accountOfficerEmail;
	}

	/**
	 * @return the accountOfficerCode
	 */
	public String getAccountOfficerCode() {
		return accountOfficerCode;
	}

	/**
	 * @param accountOfficerCode the accountOfficerCode to set
	 */
	public void setAccountOfficerCode(String accountOfficerCode) {
		this.accountOfficerCode = accountOfficerCode;
	}

	/**
	 * @return the accountOfficerTeamCode
	 */
	public String getAccountOfficerTeamCode() {
		return accountOfficerTeamCode;
	}

	/**
	 * @param accountOfficerTeamCode the accountOfficerTeamCode to set
	 */
	public void setAccountOfficerTeamCode(String accountOfficerTeamCode) {
		this.accountOfficerTeamCode = accountOfficerTeamCode;
	}

	/**
	 * @return the accountOfficerName
	 */
	public String getAccountOfficerName() {
		return accountOfficerName;
	}

	/**
	 * @param accountOfficerName the accountOfficerName to set
	 */
	public void setAccountOfficerName(String accountOfficerName) {
		this.accountOfficerName = accountOfficerName;
	}

	/**
	 * @return the displayAccountNumber
	 */
	public String getDisplayAccountNumber() {
		return displayAccountNumber;
	}

	/**
	 * @param displayAccountNumber the displayAccountNumber to set
	 */
	public void setDisplayAccountNumber(String displayAccountNumber) {
		this.displayAccountNumber = displayAccountNumber;
	}
	
	
	
	
}
