/**
 * 
 */
package com.cmb.consolidatedapps.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name="customer_sub_beneficiary",schema="cmauser")
public class CustomerSubsidiaryBeneficiary {
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="cust_sub_bene_generator")
	@SequenceGenerator(name="cust_sub_bene_generator",allocationSize=1,sequenceName="cust_sub_bene_gen")
	@Id
	private Long id;

	@Column(nullable = false,name="beneficiary_customer_name")
	private String beneficiaryCustomerName;

	@Column(nullable = false,name="beneficiary_customer_aid")
	private String beneficiaryCustomerAid;

	@ManyToOne
    @JoinColumn(nullable=false,name="customer_id")
	private Customer customer;

	@OneToOne(cascade =  CascadeType.ALL , fetch= FetchType.EAGER)
	@JoinColumn(name = "institution_Code")
	private FinancialInstitution financialInstitution;

	@OneToOne
	@JoinColumn(name = "application_id",nullable=false)
	private Application application;

	@OneToOne
	@JoinColumn(name = "mutual_fund_product_id")
	private MutualFundProduct mutualFundProduct;

    @Column(name="delete_flg", length=1)
	private String deleteFlg = "N";

    @Transient
	private String displayBeneficiary;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the beneficiaryCustomerName
	 */
	public String getBeneficiaryCustomerName() {
		return beneficiaryCustomerName;
	}

	/**
	 * @param beneficiaryCustomerName the beneficiaryCustomerName to set
	 */
	public void setBeneficiaryCustomerName(String beneficiaryCustomerName) {
		this.beneficiaryCustomerName = beneficiaryCustomerName;
	}

	/**
	 * @return the beneficiaryCustomerAid
	 */
	public String getBeneficiaryCustomerAid() {
		return beneficiaryCustomerAid;
	}

	/**
	 * @param beneficiaryCustomerAid the beneficiaryCustomerAid to set
	 */
	public void setBeneficiaryCustomerAid(String beneficiaryCustomerAid) {
		this.beneficiaryCustomerAid = beneficiaryCustomerAid;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the financialInstitution
	 */
	public FinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	/**
	 * @param financialInstitution the financialInstitution to set
	 */
	public void setFinancialInstitution(FinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}

	/**
	 * @return the mutualFundProduct
	 */
	public MutualFundProduct getMutualFundProduct() {
		return mutualFundProduct;
	}

	/**
	 * @param mutualFundProduct the mutualFundProduct to set
	 */
	public void setMutualFundProduct(MutualFundProduct mutualFundProduct) {
		this.mutualFundProduct = mutualFundProduct;
	}

	/**
	 * @return the deleteFlg
	 */
	public String getDeleteFlg() {
		return deleteFlg;
	}

	/**
	 * @param deleteFlg the deleteFlg to set
	 */
	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	/**
	 * @return the displayBeneficiary
	 */
	public String getDisplayBeneficiary() {
		return displayBeneficiary;
	}

	/**
	 * @param displayBeneficiary the displayBeneficiary to set
	 */
	public void setDisplayBeneficiary(String displayBeneficiary) {
		this.displayBeneficiary = displayBeneficiary;
	}


}
