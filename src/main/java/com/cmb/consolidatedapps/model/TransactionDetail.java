/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name="transaction_detail")
public class TransactionDetail {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="tran_detail_generator")
	@SequenceGenerator(name="tran_detail_generator",allocationSize=1,sequenceName="tran_detail_gen")
	@Id
	private Long id;

	@OneToOne
	@JoinColumn(name="transaction_type_id",nullable=false)
	private TransactionType transactionType;

	@Transient
	private Long transactionTypeId;

	@Column(name="source_account_number",nullable=false)
	private String sourceAccountNumber;

	@Column(name="destination_account_number")
	private String destinationAccountNumber;

	@Column(name="destination_account_name")
	private String destinationAccountName;

	@Column(name="destination_account_bvn")
	private String destinationAccountBvn;

	@Transient
	private String addBeneficiary;
	
	@Transient
	private Long fundTypeId;
	
	@Transient
	private Long topUpTypeId;
	
	@Transient
	private Long topUpSrcId;

	@OneToOne
	@JoinColumn(name="mutual_fund_product_id")
	private MutualFundProduct mutualFundProduct;

	@OneToOne
	@JoinColumn(name="top_up_type_id")
	private TopUpType topUpType;

	@OneToOne
	@JoinColumn(name="top_up_source_id")
	private TopUpSource topUpSource;

	@Transient
	private String financialInstitutionCode;

	@OneToOne
	@JoinColumn(name = "institution_Code")
	private FinancialInstitution financialInstitution;

	@Column(nullable=false)
	private BigDecimal amount;

	@Transient
	private String formattedAmount;

	@Column(nullable=false)
	private String remark;

	@Column(name="response_code")
	private String responseCode;

	@Column(name="response_description")
	private String responseDescription;

	@Column(name="request_date",nullable=false)
	private LocalDateTime requestDate;

	@OneToOne
	@JoinColumn(name="user_id",nullable=false)
	private User user;

	@OneToOne
	@JoinColumn(name = "customer_beneficiary_id")
	private CustomerBeneficiary customerBeneficiary;

	@Transient
	private Long externalBeneficiaryId;

	@Transient
	private Long internalBeneficiaryId;

	@Transient
	private String ownAccountNumber;

	@Transient
	private Long userId;

	@Transient
	private List<CustomerBeneficiary> customerInternalBeneficiaries = new ArrayList<CustomerBeneficiary>();

	@Transient
	private List<CustomerBeneficiary> customerExternalBeneficiaries = new ArrayList<CustomerBeneficiary>();

	@Column(name="name_enquiry_session_id")
	private String nameEnquirySessionId;

	@Column(name="ft_single_credit_session_id")
	private String ftSingleCreditSessionId;

	@Column(name="unique_identifier")
	private String uniqueIdentifier;

	@Column(name="beneficiary_kyc")
	private String beneficiaryKyc;

    @Transient
	private String beneficiaryExist = "N";

    @Transient
	private String token;

    @Transient
	private String tokenRespMessage;

    @Transient
	private String errorMessage;

    @Transient
	private String existingBen;

    @Transient
	private String newBen;

    @Transient
	private String existBenCheck;

    @Transient
	private String newBenCheck;

    @Transient
	private String customerDailyTransactionAccountBalance;

    @Transient
	private String customerDailyTransactionAccountLimit;

    @Transient
	private String accountBalance;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the transactionType
	 */
	public TransactionType getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType
	 *            the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the sourceAccountNumber
	 */
	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	/**
	 * @param sourceAccountNumber
	 *            the sourceAccountNumber to set
	 */
	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	/**
	 * @return the destinationAccountNumber
	 */
	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	/**
	 * @param destinationAccountNumber
	 *            the destinationAccountNumber to set
	 */
	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	/**
	 * @return the mutualFundProduct
	 */
	public MutualFundProduct getMutualFundProduct() {
		return mutualFundProduct;
	}

	/**
	 * @param mutualFundProduct
	 *            the mutualFundProduct to set
	 */
	public void setMutualFundProduct(MutualFundProduct mutualFundProduct) {
		this.mutualFundProduct = mutualFundProduct;
	}

	/**
	 * @return the topUpType
	 */
	public TopUpType getTopUpType() {
		return topUpType;
	}

	/**
	 * @param topUpType
	 *            the topUpType to set
	 */
	public void setTopUpType(TopUpType topUpType) {
		this.topUpType = topUpType;
	}

	/**
	 * @return the financialInstitution
	 */
	public FinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	/**
	 * @param financialInstitution
	 *            the financialInstitution to set
	 */
	public void setFinancialInstitution(FinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseDescription
	 */
	public String getResponseDescription() {
		return responseDescription;
	}

	/**
	 * @param responseDescription
	 *            the responseDescription to set
	 */
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	/**
	 * @return the requestDate
	 */
	public LocalDateTime getRequestDate() {
		return requestDate;
	}

	/**
	 * @param requestDate
	 *            the requestDate to set
	 */
	public void setRequestDate(LocalDateTime requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @return the transactionTypeId
	 */
	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	/**
	 * @param transactionTypeId
	 *            the transactionTypeId to set
	 */
	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	/**
	 * @return the addBeneficiary
	 */
	public String getAddBeneficiary() {
		return addBeneficiary;
	}

	/**
	 * @param addBeneficiary
	 *            the addBeneficiary to set
	 */
	public void setAddBeneficiary(String addBeneficiary) {
		this.addBeneficiary = addBeneficiary;
	}

	/**
	 * @return the customerInternalBeneficiaries
	 */
	public List<CustomerBeneficiary> getCustomerInternalBeneficiaries() {
		return customerInternalBeneficiaries;
	}

	/**
	 * @param customerInternalBeneficiaries
	 *            the customerInternalBeneficiaries to set
	 */
	public void setCustomerInternalBeneficiaries(List<CustomerBeneficiary> customerInternalBeneficiaries) {
		this.customerInternalBeneficiaries = customerInternalBeneficiaries;
	}

	/**
	 * @return the customerExternalBeneficiaries
	 */
	public List<CustomerBeneficiary> getCustomerExternalBeneficiaries() {
		return customerExternalBeneficiaries;
	}

	/**
	 * @param customerExternalBeneficiaries
	 *            the customerExternalBeneficiaries to set
	 */
	public void setCustomerExternalBeneficiaries(List<CustomerBeneficiary> customerExternalBeneficiaries) {
		this.customerExternalBeneficiaries = customerExternalBeneficiaries;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the destinationAccountName
	 */
	public String getDestinationAccountName() {
		return destinationAccountName;
	}

	/**
	 * @param destinationAccountName
	 *            the destinationAccountName to set
	 */
	public void setDestinationAccountName(String destinationAccountName) {
		this.destinationAccountName = destinationAccountName;
	}

	/**
	 * @return the destinationAccountBvn
	 */
	public String getDestinationAccountBvn() {
		return destinationAccountBvn;
	}

	/**
	 * @param destinationAccountBvn
	 *            the destinationAccountBvn to set
	 */
	public void setDestinationAccountBvn(String destinationAccountBvn) {
		this.destinationAccountBvn = destinationAccountBvn;
	}

	/**
	 * @return the financialInstitutionCode
	 */
	public String getFinancialInstitutionCode() {
		return financialInstitutionCode;
	}

	/**
	 * @param financialInstitutionCode
	 *            the financialInstitutionCode to set
	 */
	public void setFinancialInstitutionCode(String financialInstitutionCode) {
		this.financialInstitutionCode = financialInstitutionCode;
	}

	/**
	 * @return the customerBeneficiary
	 */
	public CustomerBeneficiary getCustomerBeneficiary() {
		return customerBeneficiary;
	}

	/**
	 * @param customerBeneficiary
	 *            the customerBeneficiary to set
	 */
	public void setCustomerBeneficiary(CustomerBeneficiary customerBeneficiary) {
		this.customerBeneficiary = customerBeneficiary;
	}

	/**
	 * @return the ownAccountNumber
	 */
	public String getOwnAccountNumber() {
		return ownAccountNumber;
	}

	/**
	 * @param ownAccountNumber
	 *            the ownAccountNumber to set
	 */
	public void setOwnAccountNumber(String ownAccountNumber) {
		this.ownAccountNumber = ownAccountNumber;
	}

	/**
	 * @return the formattedAmount
	 */
	public String getFormattedAmount() {
		return formattedAmount;
	}

	/**
	 * @param formattedAmount
	 *            the formattedAmount to set
	 */
	public void setFormattedAmount(String formattedAmount) {
		this.formattedAmount = formattedAmount;
	}

	/**
	 * @return the nameEnquirySessionId
	 */
	public String getNameEnquirySessionId() {
		return nameEnquirySessionId;
	}

	/**
	 * @param nameEnquirySessionId
	 *            the nameEnquirySessionId to set
	 */
	public void setNameEnquirySessionId(String nameEnquirySessionId) {
		this.nameEnquirySessionId = nameEnquirySessionId;
	}

	/**
	 * @return the ftSingleCreditSessionId
	 */
	public String getFtSingleCreditSessionId() {
		return ftSingleCreditSessionId;
	}

	/**
	 * @param ftSingleCreditSessionId
	 *            the ftSingleCreditSessionId to set
	 */
	public void setFtSingleCreditSessionId(String ftSingleCreditSessionId) {
		this.ftSingleCreditSessionId = ftSingleCreditSessionId;
	}

	/**
	 * @return the externalBeneficiaryId
	 */
	public Long getExternalBeneficiaryId() {
		return externalBeneficiaryId;
	}

	/**
	 * @param externalBeneficiaryId
	 *            the externalBeneficiaryId to set
	 */
	public void setExternalBeneficiaryId(Long externalBeneficiaryId) {
		this.externalBeneficiaryId = externalBeneficiaryId;
	}

	/**
	 * @return the internalBeneficiaryId
	 */
	public Long getInternalBeneficiaryId() {
		return internalBeneficiaryId;
	}

	/**
	 * @param internalBeneficiaryId
	 *            the internalBeneficiaryId to set
	 */
	public void setInternalBeneficiaryId(Long internalBeneficiaryId) {
		this.internalBeneficiaryId = internalBeneficiaryId;
	}

	/**
	 * @return the uniqueIdentifier
	 */
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}

	/**
	 * @param uniqueIdentifier the uniqueIdentifier to set
	 */
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}

	/**
	 * @return the beneficiaryKyc
	 */
	public String getBeneficiaryKyc() {
		return beneficiaryKyc;
	}

	/**
	 * @param beneficiaryKyc the beneficiaryKyc to set
	 */
	public void setBeneficiaryKyc(String beneficiaryKyc) {
		this.beneficiaryKyc = beneficiaryKyc;
	}

	/**
	 * @return the fundTypeId
	 */
	public Long getFundTypeId() {
		return fundTypeId;
	}

	/**
	 * @param fundTypeId the fundTypeId to set
	 */
	public void setFundTypeId(Long fundTypeId) {
		this.fundTypeId = fundTypeId;
	}

	/**
	 * @return the topUpTypeId
	 */
	public Long getTopUpTypeId() {
		return topUpTypeId;
	}

	/**
	 * @param topUpTypeId the topUpTypeId to set
	 */
	public void setTopUpTypeId(Long topUpTypeId) {
		this.topUpTypeId = topUpTypeId;
	}

	/**
	 * @return the topUpSrcId
	 */
	public Long getTopUpSrcId() {
		return topUpSrcId;
	}

	/**
	 * @param topUpSrcId the topUpSrcId to set
	 */
	public void setTopUpSrcId(Long topUpSrcId) {
		this.topUpSrcId = topUpSrcId;
	}

	/**
	 * @return the topUpSource
	 */
	public TopUpSource getTopUpSource() {
		return topUpSource;
	}

	/**
	 * @param topUpSource the topUpSource to set
	 */
	public void setTopUpSource(TopUpSource topUpSource) {
		this.topUpSource = topUpSource;
	}

	/**
	 * @return the beneficiaryExist
	 */
	public String getBeneficiaryExist() {
		return beneficiaryExist;
	}

	/**
	 * @param beneficiaryExist the beneficiaryExist to set
	 */
	public void setBeneficiaryExist(String beneficiaryExist) {
		this.beneficiaryExist = beneficiaryExist;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenRespMessage
	 */
	public String getTokenRespMessage() {
		return tokenRespMessage;
	}

	/**
	 * @param tokenRespMessage the tokenRespMessage to set
	 */
	public void setTokenRespMessage(String tokenRespMessage) {
		this.tokenRespMessage = tokenRespMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the existingBen
	 */
	public String getExistingBen() {
		return existingBen;
	}

	/**
	 * @param existingBen the existingBen to set
	 */
	public void setExistingBen(String existingBen) {
		this.existingBen = existingBen;
	}

	/**
	 * @return the newBen
	 */
	public String getNewBen() {
		return newBen;
	}

	/**
	 * @param newBen the newBen to set
	 */
	public void setNewBen(String newBen) {
		this.newBen = newBen;
	}

	/**
	 * @return the existBenCheck
	 */
	public String getExistBenCheck() {
		return existBenCheck;
	}

	/**
	 * @param existBenCheck the existBenCheck to set
	 */
	public void setExistBenCheck(String existBenCheck) {
		this.existBenCheck = existBenCheck;
	}

	/**
	 * @return the newBenCheck
	 */
	public String getNewBenCheck() {
		return newBenCheck;
	}

	/**
	 * @param newBenCheck the newBenCheck to set
	 */
	public void setNewBenCheck(String newBenCheck) {
		this.newBenCheck = newBenCheck;
	}

	/**
	 * @return the customerDailyTransactionAccountBalance
	 */
	public String getCustomerDailyTransactionAccountBalance() {
		return customerDailyTransactionAccountBalance;
	}

	/**
	 * @param customerDailyTransactionAccountBalance the customerDailyTransactionAccountBalance to set
	 */
	public void setCustomerDailyTransactionAccountBalance(String customerDailyTransactionAccountBalance) {
		this.customerDailyTransactionAccountBalance = customerDailyTransactionAccountBalance;
	}

	/**
	 * @return the customerDailyTransactionAccountLimit
	 */
	public String getCustomerDailyTransactionAccountLimit() {
		return customerDailyTransactionAccountLimit;
	}

	/**
	 * @param customerDailyTransactionAccountLimit the customerDailyTransactionAccountLimit to set
	 */
	public void setCustomerDailyTransactionAccountLimit(String customerDailyTransactionAccountLimit) {
		this.customerDailyTransactionAccountLimit = customerDailyTransactionAccountLimit;
	}

	/**
	 * @return the accountBalance
	 */
	public String getAccountBalance() {
		return accountBalance;
	}

	/**
	 * @param accountBalance the accountBalance to set
	 */
	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}

}
