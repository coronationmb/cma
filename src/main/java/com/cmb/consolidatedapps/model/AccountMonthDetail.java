/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class AccountMonthDetail {

	private String accountNumber;

	private String accountCurrency;

	private String acid;

	private String totalMonthDebitStr;

	private String totalMonthCreditStr;

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the acid
	 */
	public String getAcid() {
		return acid;
	}

	/**
	 * @param acid the acid to set
	 */
	public void setAcid(String acid) {
		this.acid = acid;
	}

	/**
	 * @return the totalMonthDebitStr
	 */
	public String getTotalMonthDebitStr() {
		return totalMonthDebitStr;
	}

	/**
	 * @param totalMonthDebitStr the totalMonthDebitStr to set
	 */
	public void setTotalMonthDebitStr(String totalMonthDebitStr) {
		this.totalMonthDebitStr = totalMonthDebitStr;
	}

	/**
	 * @return the totalMonthCreditStr
	 */
	public String getTotalMonthCreditStr() {
		return totalMonthCreditStr;
	}

	/**
	 * @param totalMonthCreditStr the totalMonthCreditStr to set
	 */
	public void setTotalMonthCreditStr(String totalMonthCreditStr) {
		this.totalMonthCreditStr = totalMonthCreditStr;
	}

	/**
	 * @return the accountCurrency
	 */
	public String getAccountCurrency() {
		return accountCurrency;
	}

	/**
	 * @param accountCurrency the accountCurrency to set
	 */
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

}
