/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Audited
@Table(schema = "cmauser")
public class Customer {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_generator")
	@SequenceGenerator(name = "customer_generator", allocationSize = 1, sequenceName = "customer_gen")
	@Id
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(length = 11, nullable = true)
	private String bvn;

	@Column(nullable = true, name = "rc_number")
	private String rcNumber;

	@OneToOne
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "customer_type_id", nullable = false)
	private CustomerType customerType;

	@OneToOne
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "corporate_subgroup_id")
	private CorporateSubGroup corporateSubgroup;

	@Transient
	private String phone;

	@Transient
	private String email;

	@Column(nullable = false, name = "created_by")
	private String createdBy;

	@Column(nullable = false, name = "created_date")
	private Date createdDate;

	@Transient
	private Long customerTypeId;

	@Transient
	private Long corporateSubGroupId;

	@Transient
	private Long app1;

	@Transient
	private Long app2;

	@Transient
	private Long app3;

	@Transient
	private Long app4;

	@Transient
	private String corporateUserBvn;

	@Transient
	private String mode;

	//@Transient
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
	private Set<CustomerApplication> customerApplications;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "delete_flg", length = 1)
	private String deleteFlg = "N";

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "customer")
	private Set<User> users;

	// @Transient
	// @OneToMany//(mappedBy="customer")
	// @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	// @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	// private Set<CustomerBeneficiary> CustomerBeneficiaries;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the bvn
	 */
	public String getBvn() {
		return bvn;
	}

	/**
	 * @param bvn
	 *            the bvn to set
	 */
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	/**
	 * @return the rcNumber
	 */
	public String getRcNumber() {
		return rcNumber;
	}

	/**
	 * @param rcNumber
	 *            the rcNumber to set
	 */
	public void setRcNumber(String rcNumber) {
		this.rcNumber = rcNumber;
	}

	/**
	 * @return the customerType
	 */
	public CustomerType getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the customerTypeId
	 */
	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	/**
	 * @param customerTypeId
	 *            the customerTypeId to set
	 */
	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	/**
	 * @return the app1
	 */
	public Long getApp1() {
		return app1;
	}

	/**
	 * @param app1
	 *            the app1 to set
	 */
	public void setApp1(Long app1) {
		this.app1 = app1;
	}

	/**
	 * @return the app2
	 */
	public Long getApp2() {
		return app2;
	}

	/**
	 * @param app2
	 *            the app2 to set
	 */
	public void setApp2(Long app2) {
		this.app2 = app2;
	}

	/**
	 * @return the app3
	 */
	public Long getApp3() {
		return app3;
	}

	/**
	 * @param app3
	 *            the app3 to set
	 */
	public void setApp3(Long app3) {
		this.app3 = app3;
	}

	/**
	 * @return the app4
	 */
	public Long getApp4() {
		return app4;
	}

	/**
	 * @param app4
	 *            the app4 to set
	 */
	public void setApp4(Long app4) {
		this.app4 = app4;
	}

	/**
	 * @return the corporateSubgroup
	 */
	public CorporateSubGroup getCorporateSubgroup() {
		return corporateSubgroup;
	}

	/**
	 * @param corporateSubgroup
	 *            the corporateSubgroup to set
	 */
	public void setCorporateSubgroup(CorporateSubGroup corporateSubgroup) {
		this.corporateSubgroup = corporateSubgroup;
	}

	/**
	 * @return the corporateSubGroupId
	 */
	public Long getCorporateSubGroupId() {
		return corporateSubGroupId;
	}

	/**
	 * @param corporateSubGroupId
	 *            the corporateSubGroupId to set
	 */
	public void setCorporateSubGroupId(Long corporateSubGroupId) {
		this.corporateSubGroupId = corporateSubGroupId;
	}

	/**
	 * @return the corporateUserBvn
	 */
	public String getCorporateUserBvn() {
		return corporateUserBvn;
	}

	/**
	 * @param corporateUserBvn
	 *            the corporateUserBvn to set
	 */
	public void setCorporateUserBvn(String corporateUserBvn) {
		this.corporateUserBvn = corporateUserBvn;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the deleteFlg
	 */
	public String getDeleteFlg() {
		return deleteFlg;
	}

	/**
	 * @param deleteFlg
	 *            the deleteFlg to set
	 */
	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the customerApplications
	 */
	public Set<CustomerApplication> getCustomerApplications() {
		return customerApplications;
	}

	/**
	 * @param customerApplications the customerApplications to set
	 */
	public void setCustomerApplications(Set<CustomerApplication> customerApplications) {
		this.customerApplications = customerApplications;
	}

}
