/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.time.LocalDateTime;

/**
 * @author waliu.faleye
 *
 */
public class FITransactionDetail {
	
	private String hasMoreData;
	
	private LocalDateTime pstdDateDt;
	
	private String pstdDate;
	
	private String txnCat;
	
	private String txnId;
	
	private String txnSrlNo;
	
	private String valueDate;
	
	private String debit;
	
	private String credit;
	
	private FITxnSummary fiTxnSummary;
	
	private FITxnBalance fiTxnBalance;
	
	private String txnDesc;
	
	private String amountValue;

	/**
	 * @return the pstdDate
	 */
	public String getPstdDate() {
		return pstdDate;
	}

	/**
	 * @param pstdDate the pstdDate to set
	 */
	public void setPstdDate(String pstdDate) {
		this.pstdDate = pstdDate;
	}

	/**
	 * @return the txnCat
	 */
	public String getTxnCat() {
		return txnCat;
	}

	/**
	 * @param txnCat the txnCat to set
	 */
	public void setTxnCat(String txnCat) {
		this.txnCat = txnCat;
	}

	/**
	 * @return the txnId
	 */
	public String getTxnId() {
		return txnId;
	}

	/**
	 * @param txnId the txnId to set
	 */
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	/**
	 * @return the txnSrlNo
	 */
	public String getTxnSrlNo() {
		return txnSrlNo;
	}

	/**
	 * @param txnSrlNo the txnSrlNo to set
	 */
	public void setTxnSrlNo(String txnSrlNo) {
		this.txnSrlNo = txnSrlNo;
	}

	/**
	 * @return the valueDate
	 */
	public String getValueDate() {
		return valueDate;
	}

	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 * @return the fiTxnSummary
	 */
	public FITxnSummary getFiTxnSummary() {
		return fiTxnSummary;
	}

	/**
	 * @param fiTxnSummary the fiTxnSummary to set
	 */
	public void setFiTxnSummary(FITxnSummary fiTxnSummary) {
		this.fiTxnSummary = fiTxnSummary;
	}

	/**
	 * @return the fiTxnBalance
	 */
	public FITxnBalance getFiTxnBalance() {
		return fiTxnBalance;
	}

	/**
	 * @param fiTxnBalance the fiTxnBalance to set
	 */
	public void setFiTxnBalance(FITxnBalance fiTxnBalance) {
		this.fiTxnBalance = fiTxnBalance;
	}

	/**
	 * @return the debit
	 */
	public String getDebit() {
		return debit;
	}

	/**
	 * @param debit the debit to set
	 */
	public void setDebit(String debit) {
		this.debit = debit;
	}

	/**
	 * @return the credit
	 */
	public String getCredit() {
		return credit;
	}

	/**
	 * @param credit the credit to set
	 */
	public void setCredit(String credit) {
		this.credit = credit;
	}

	/**
	 * @return the pstdDateDt
	 */
	public LocalDateTime getPstdDateDt() {
		return pstdDateDt;
	}

	/**
	 * @param pstdDateDt the pstdDateDt to set
	 */
	public void setPstdDateDt(LocalDateTime pstdDateDt) {
		this.pstdDateDt = pstdDateDt;
	}

	/**
	 * @return the hasMoreData
	 */
	public String getHasMoreData() {
		return hasMoreData;
	}

	/**
	 * @param hasMoreData the hasMoreData to set
	 */
	public void setHasMoreData(String hasMoreData) {
		this.hasMoreData = hasMoreData;
	}

	/**
	 * @return the txnDesc
	 */
	public String getTxnDesc() {
		return txnDesc;
	}

	/**
	 * @param txnDesc the txnDesc to set
	 */
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	/**
	 * @return the amountValue
	 */
	public String getAmountValue() {
		return amountValue;
	}

	/**
	 * @param amountValue the amountValue to set
	 */
	public void setAmountValue(String amountValue) {
		this.amountValue = amountValue;
	}

}
