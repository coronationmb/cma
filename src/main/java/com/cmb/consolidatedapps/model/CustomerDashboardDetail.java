/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.cmb.thirdpartyapi.model.AMInvestmentSummaryRows;
import com.cmb.thirdpartyapi.model.LosersGainersRows;
import com.cmb.thirdpartyapi.model.MFRows;
import com.cmb.thirdpartyapi.model.StockData;

/**
 * @author waliu.faleye
 *
 */
public class CustomerDashboardDetail {

	private String lastLoginTime;

	private List<AccountMonthDetail> accountMonthDetails = new ArrayList<AccountMonthDetail>();

	private List<TDAccountDetail> tdAccountDetails = new ArrayList<TDAccountDetail>();

	private List<LoanAccountDetail> loanAccountDetails = new ArrayList<LoanAccountDetail>();

	private List<OverdraftAccountDetail> overdraftAccountDetails = new ArrayList<OverdraftAccountDetail>();

	private User user;

	private AccountManager accountManager;

	private Long app1;

	private Long app2;

	private Long app3;

	private Long app4;

	List<CustomerBankAccount> currentAccountList = new ArrayList<CustomerBankAccount>();

	List<CustomerBankAccount> domAccountList = new ArrayList<CustomerBankAccount>();

	/*
	 * private String currentAccountNumber1;
	 * 
	 * private String currentAccountBalance1;
	 * 
	 * private String currentAccountNumber2;
	 * 
	 * private String currentAccountBalance2;
	 * 
	 * private String currentAccountNumber3;
	 * 
	 * private String currentAccountBalance3;
	 * 
	 * private String domUSDAccountNumber;
	 * 
	 * private String domUSDAccountBalance;
	 * 
	 * private String domGBPAccountNumber;
	 * 
	 * private String domGBPAccountBalance;
	 * 
	 * private String domEURAccountNumber;
	 * 
	 * private String domEURAccountBalance;
	 */

	private BigDecimal cbfValue;

	private BigDecimal cfifValue;

	private BigDecimal cmmfValue;

	private String cbfValueStr;

	private String cfifValueStr;

	private String cmmfValueStr;

	private List<AMInvestmentSummaryRows> cbfInvestmentSummary = new ArrayList<AMInvestmentSummaryRows>();

	private List<AMInvestmentSummaryRows> cfifInvestmentSummary = new ArrayList<AMInvestmentSummaryRows>();

	private List<AMInvestmentSummaryRows> cmmfInvestmentSummary = new ArrayList<AMInvestmentSummaryRows>();

	private String customerBankWorth;

	private String portfolioValueStr;

	private String secPortfolioValueStr;

	List<MFRows> mfProducts = new ArrayList<MFRows>();

	private BigDecimal secCashValue;

	private BigDecimal secEquitiesValue;

	private BigDecimal secCashPercent;

	private BigDecimal secEquitiesPercent;

	private String secCashValueStr;

	private String secEquitiesValueStr;

	BigDecimal secPortfolio[] = new BigDecimal[2];

	// List<Rows> secPortfolio = new ArrayList<Rows>();

	List<LosersGainersRows> losers = new ArrayList<LosersGainersRows>();

	List<LosersGainersRows> gainers = new ArrayList<LosersGainersRows>();

	List<Equity> equities = new ArrayList<Equity>();

	private String bankAccountManager;

	private String assetManagementAccountManager;

	private String securitiesAccountManager;

	private String trusteesAccountManager;

	private String cmmfCustAid;

	private String cbfCustAid;

	private String cfifCustAid;

	private String csecCustAid;

	private List<SubsidiaryCustomerDetail> amCustAIDList;

	private List<SubsidiaryCustomerDetail> secCustAIDList;

	private String customerNetWorth;

	private BigDecimal customerWorth;

	private String bvn;

	private List<StockData> stockData;

	private String amCashBalance;

	private String displaycbfValue;

	private String displaycfifValue;

	private String displaycmmfValue;

	private String displaySecCashValue;

	private String displaySecEquitiesValue;

	private String amAddress;

	private String secAddress;

	/**
	 * @return the lastLoginTime
	 */
	public String getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLoginTime
	 *            the lastLoginTime to set
	 */
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	/**
	 * @return the accountMonthDetails
	 */
	public List<AccountMonthDetail> getAccountMonthDetails() {
		return accountMonthDetails;
	}

	/**
	 * @param accountMonthDetails
	 *            the accountMonthDetails to set
	 */
	public void setAccountMonthDetails(List<AccountMonthDetail> accountMonthDetails) {
		this.accountMonthDetails = accountMonthDetails;
	}

	/**
	 * @return the tdAccountDetails
	 */
	public List<TDAccountDetail> getTdAccountDetails() {
		return tdAccountDetails;
	}

	/**
	 * @param tdAccountDetails
	 *            the tdAccountDetails to set
	 */
	public void setTdAccountDetails(List<TDAccountDetail> tdAccountDetails) {
		this.tdAccountDetails = tdAccountDetails;
	}

	/**
	 * @return the loanAccountDetails
	 */
	public List<LoanAccountDetail> getLoanAccountDetails() {
		return loanAccountDetails;
	}

	/**
	 * @param loanAccountDetails
	 *            the loanAccountDetails to set
	 */
	public void setLoanAccountDetails(List<LoanAccountDetail> loanAccountDetails) {
		this.loanAccountDetails = loanAccountDetails;
	}

	/**
	 * @return the overdraftAccountDetails
	 */
	public List<OverdraftAccountDetail> getOverdraftAccountDetails() {
		return overdraftAccountDetails;
	}

	/**
	 * @param overdraftAccountDetails
	 *            the overdraftAccountDetails to set
	 */
	public void setOverdraftAccountDetails(List<OverdraftAccountDetail> overdraftAccountDetails) {
		this.overdraftAccountDetails = overdraftAccountDetails;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the accountManager
	 */
	public AccountManager getAccountManager() {
		return accountManager;
	}

	/**
	 * @param accountManager
	 *            the accountManager to set
	 */
	public void setAccountManager(AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	/**
	 * @return the app1
	 */
	public Long getApp1() {
		return app1;
	}

	/**
	 * @param app1
	 *            the app1 to set
	 */
	public void setApp1(Long app1) {
		this.app1 = app1;
	}

	/**
	 * @return the app2
	 */
	public Long getApp2() {
		return app2;
	}

	/**
	 * @param app2
	 *            the app2 to set
	 */
	public void setApp2(Long app2) {
		this.app2 = app2;
	}

	/**
	 * @return the app3
	 */
	public Long getApp3() {
		return app3;
	}

	/**
	 * @param app3
	 *            the app3 to set
	 */
	public void setApp3(Long app3) {
		this.app3 = app3;
	}

	/**
	 * @return the app4
	 */
	public Long getApp4() {
		return app4;
	}

	/**
	 * @param app4
	 *            the app4 to set
	 */
	public void setApp4(Long app4) {
		this.app4 = app4;
	}

	/**
	 * @return the customerBankWorth
	 */
	public String getCustomerBankWorth() {
		return customerBankWorth;
	}

	/**
	 * @param customerBankWorth
	 *            the customerBankWorth to set
	 */
	public void setCustomerBankWorth(String customerBankWorth) {
		this.customerBankWorth = customerBankWorth;
	}

	/**
	 * @return the currentAccountList
	 */
	public List<CustomerBankAccount> getCurrentAccountList() {
		return currentAccountList;
	}

	/**
	 * @param currentAccountList
	 *            the currentAccountList to set
	 */
	public void setCurrentAccountList(List<CustomerBankAccount> currentAccountList) {
		this.currentAccountList = currentAccountList;
	}

	/**
	 * @return the domAccountList
	 */
	public List<CustomerBankAccount> getDomAccountList() {
		return domAccountList;
	}

	/**
	 * @param domAccountList
	 *            the domAccountList to set
	 */
	public void setDomAccountList(List<CustomerBankAccount> domAccountList) {
		this.domAccountList = domAccountList;
	}

	/**
	 * @return the portfolioValueStr
	 */
	public String getPortfolioValueStr() {
		return portfolioValueStr;
	}

	/**
	 * @param portfolioValueStr
	 *            the portfolioValueStr to set
	 */
	public void setPortfolioValueStr(String portfolioValueStr) {
		this.portfolioValueStr = portfolioValueStr;
	}

	/**
	 * @return the mfProducts
	 */
	public List<MFRows> getMfProducts() {
		return mfProducts;
	}

	/**
	 * @param mfProducts
	 *            the mfProducts to set
	 */
	public void setMfProducts(List<MFRows> mfProducts) {
		this.mfProducts = mfProducts;
	}

	/**
	 * @return the losers
	 */
	public List<LosersGainersRows> getLosers() {
		return losers;
	}

	/**
	 * @param losers
	 *            the losers to set
	 */
	public void setLosers(List<LosersGainersRows> losers) {
		this.losers = losers;
	}

	/**
	 * @return the gainers
	 */
	public List<LosersGainersRows> getGainers() {
		return gainers;
	}

	/**
	 * @param gainers
	 *            the gainers to set
	 */
	public void setGainers(List<LosersGainersRows> gainers) {
		this.gainers = gainers;
	}

	/**
	 * @return the equities
	 */
	public List<Equity> getEquities() {
		return equities;
	}

	/**
	 * @param equities
	 *            the equities to set
	 */
	public void setEquities(List<Equity> equities) {
		this.equities = equities;
	}

	/**
	 * @return the bankAccountManager
	 */
	public String getBankAccountManager() {
		return bankAccountManager;
	}

	/**
	 * @param bankAccountManager
	 *            the bankAccountManager to set
	 */
	public void setBankAccountManager(String bankAccountManager) {
		this.bankAccountManager = bankAccountManager;
	}

	/**
	 * @return the assetManagementAccountManager
	 */
	public String getAssetManagementAccountManager() {
		return assetManagementAccountManager;
	}

	/**
	 * @param assetManagementAccountManager
	 *            the assetManagementAccountManager to set
	 */
	public void setAssetManagementAccountManager(String assetManagementAccountManager) {
		this.assetManagementAccountManager = assetManagementAccountManager;
	}

	/**
	 * @return the securitiesAccountManager
	 */
	public String getSecuritiesAccountManager() {
		return securitiesAccountManager;
	}

	/**
	 * @param securitiesAccountManager
	 *            the securitiesAccountManager to set
	 */
	public void setSecuritiesAccountManager(String securitiesAccountManager) {
		this.securitiesAccountManager = securitiesAccountManager;
	}

	/**
	 * @return the trusteesAccountManager
	 */
	public String getTrusteesAccountManager() {
		return trusteesAccountManager;
	}

	/**
	 * @param trusteesAccountManager
	 *            the trusteesAccountManager to set
	 */
	public void setTrusteesAccountManager(String trusteesAccountManager) {
		this.trusteesAccountManager = trusteesAccountManager;
	}

	/**
	 * @return the secCashValue
	 */
	public BigDecimal getSecCashValue() {
		return secCashValue;
	}

	/**
	 * @param secCashValue
	 *            the secCashValue to set
	 */
	public void setSecCashValue(BigDecimal secCashValue) {
		this.secCashValue = secCashValue;
	}

	/**
	 * @return the secEquitiesValue
	 */
	public BigDecimal getSecEquitiesValue() {
		return secEquitiesValue;
	}

	/**
	 * @param secEquitiesValue
	 *            the secEquitiesValue to set
	 */
	public void setSecEquitiesValue(BigDecimal secEquitiesValue) {
		this.secEquitiesValue = secEquitiesValue;
	}

	/**
	 * @return the secCashPercent
	 */
	public BigDecimal getSecCashPercent() {
		return secCashPercent;
	}

	/**
	 * @param secCashPercent
	 *            the secCashPercent to set
	 */
	public void setSecCashPercent(BigDecimal secCashPercent) {
		this.secCashPercent = secCashPercent;
	}

	/**
	 * @return the secEquitiesPercent
	 */
	public BigDecimal getSecEquitiesPercent() {
		return secEquitiesPercent;
	}

	/**
	 * @param secEquitiesPercent
	 *            the secEquitiesPercent to set
	 */
	public void setSecEquitiesPercent(BigDecimal secEquitiesPercent) {
		this.secEquitiesPercent = secEquitiesPercent;
	}

	/**
	 * @return the secPortfolio
	 */
	public BigDecimal[] getSecPortfolio() {
		return secPortfolio;
	}

	/**
	 * @param secPortfolio
	 *            the secPortfolio to set
	 */
	public void setSecPortfolio(BigDecimal[] secPortfolio) {
		this.secPortfolio = secPortfolio;
	}

	/**
	 * @return the cbfValue
	 */
	public BigDecimal getCbfValue() {
		return cbfValue;
	}

	/**
	 * @param cbfValue
	 *            the cbfValue to set
	 */
	public void setCbfValue(BigDecimal cbfValue) {
		this.cbfValue = cbfValue;
	}

	/**
	 * @return the cfifValue
	 */
	public BigDecimal getCfifValue() {
		return cfifValue;
	}

	/**
	 * @param cfifValue
	 *            the cfifValue to set
	 */
	public void setCfifValue(BigDecimal cfifValue) {
		this.cfifValue = cfifValue;
	}

	/**
	 * @return the cmmfValue
	 */
	public BigDecimal getCmmfValue() {
		return cmmfValue;
	}

	/**
	 * @param cmmfValue
	 *            the cmmfValue to set
	 */
	public void setCmmfValue(BigDecimal cmmfValue) {
		this.cmmfValue = cmmfValue;
	}

	/**
	 * @return the cbfInvestmentSummary
	 */
	public List<AMInvestmentSummaryRows> getCbfInvestmentSummary() {
		return cbfInvestmentSummary;
	}

	/**
	 * @param cbfInvestmentSummary
	 *            the cbfInvestmentSummary to set
	 */
	public void setCbfInvestmentSummary(List<AMInvestmentSummaryRows> cbfInvestmentSummary) {
		this.cbfInvestmentSummary = cbfInvestmentSummary;
	}

	/**
	 * @return the cfifInvestmentSummary
	 */
	public List<AMInvestmentSummaryRows> getCfifInvestmentSummary() {
		return cfifInvestmentSummary;
	}

	/**
	 * @param cfifInvestmentSummary
	 *            the cfifInvestmentSummary to set
	 */
	public void setCfifInvestmentSummary(List<AMInvestmentSummaryRows> cfifInvestmentSummary) {
		this.cfifInvestmentSummary = cfifInvestmentSummary;
	}

	/**
	 * @return the cmmfInvestmentSummary
	 */
	public List<AMInvestmentSummaryRows> getCmmfInvestmentSummary() {
		return cmmfInvestmentSummary;
	}

	/**
	 * @param cmmfInvestmentSummary
	 *            the cmmfInvestmentSummary to set
	 */
	public void setCmmfInvestmentSummary(List<AMInvestmentSummaryRows> cmmfInvestmentSummary) {
		this.cmmfInvestmentSummary = cmmfInvestmentSummary;
	}

	/**
	 * @return the cbfValueStr
	 */
	public String getCbfValueStr() {
		return cbfValueStr;
	}

	/**
	 * @param cbfValueStr
	 *            the cbfValueStr to set
	 */
	public void setCbfValueStr(String cbfValueStr) {
		this.cbfValueStr = cbfValueStr;
	}

	/**
	 * @return the cfifValueStr
	 */
	public String getCfifValueStr() {
		return cfifValueStr;
	}

	/**
	 * @param cfifValueStr
	 *            the cfifValueStr to set
	 */
	public void setCfifValueStr(String cfifValueStr) {
		this.cfifValueStr = cfifValueStr;
	}

	/**
	 * @return the cmmfValueStr
	 */
	public String getCmmfValueStr() {
		return cmmfValueStr;
	}

	/**
	 * @param cmmfValueStr
	 *            the cmmfValueStr to set
	 */
	public void setCmmfValueStr(String cmmfValueStr) {
		this.cmmfValueStr = cmmfValueStr;
	}

	/**
	 * @return the secCashValueStr
	 */
	public String getSecCashValueStr() {
		return secCashValueStr;
	}

	/**
	 * @param secCashValueStr
	 *            the secCashValueStr to set
	 */
	public void setSecCashValueStr(String secCashValueStr) {
		this.secCashValueStr = secCashValueStr;
	}

	/**
	 * @return the secEquitiesValueStr
	 */
	public String getSecEquitiesValueStr() {
		return secEquitiesValueStr;
	}

	/**
	 * @param secEquitiesValueStr
	 *            the secEquitiesValueStr to set
	 */
	public void setSecEquitiesValueStr(String secEquitiesValueStr) {
		this.secEquitiesValueStr = secEquitiesValueStr;
	}

	/**
	 * @return the cmmfCustAid
	 */
	public String getCmmfCustAid() {
		return cmmfCustAid;
	}

	/**
	 * @param cmmfCustAid
	 *            the cmmfCustAid to set
	 */
	public void setCmmfCustAid(String cmmfCustAid) {
		this.cmmfCustAid = cmmfCustAid;
	}

	/**
	 * @return the cbfCustAid
	 */
	public String getCbfCustAid() {
		return cbfCustAid;
	}

	/**
	 * @param cbfCustAid
	 *            the cbfCustAid to set
	 */
	public void setCbfCustAid(String cbfCustAid) {
		this.cbfCustAid = cbfCustAid;
	}

	/**
	 * @return the cfifCustAid
	 */
	public String getCfifCustAid() {
		return cfifCustAid;
	}

	/**
	 * @param cfifCustAid
	 *            the cfifCustAid to set
	 */
	public void setCfifCustAid(String cfifCustAid) {
		this.cfifCustAid = cfifCustAid;
	}

	/**
	 * @return the csecCustAid
	 */
	public String getCsecCustAid() {
		return csecCustAid;
	}

	/**
	 * @param csecCustAid
	 *            the csecCustAid to set
	 */
	public void setCsecCustAid(String csecCustAid) {
		this.csecCustAid = csecCustAid;
	}

	/**
	 * @return the amCustAIDList
	 */
	public List<SubsidiaryCustomerDetail> getAmCustAIDList() {
		return amCustAIDList;
	}

	/**
	 * @param amCustAIDList
	 *            the amCustAIDList to set
	 */
	public void setAmCustAIDList(List<SubsidiaryCustomerDetail> amCustAIDList) {
		this.amCustAIDList = amCustAIDList;
	}

	/**
	 * @return the secCustAIDList
	 */
	public List<SubsidiaryCustomerDetail> getSecCustAIDList() {
		return secCustAIDList;
	}

	/**
	 * @param secCustAIDList
	 *            the secCustAIDList to set
	 */
	public void setSecCustAIDList(List<SubsidiaryCustomerDetail> secCustAIDList) {
		this.secCustAIDList = secCustAIDList;
	}

	/**
	 * @return the customerNetWorth
	 */
	public String getCustomerNetWorth() {
		return customerNetWorth;
	}

	/**
	 * @param customerNetWorth
	 *            the customerNetWorth to set
	 */
	public void setCustomerNetWorth(String customerNetWorth) {
		this.customerNetWorth = customerNetWorth;
	}

	/**
	 * @return the customerWorth
	 */
	public BigDecimal getCustomerWorth() {
		return customerWorth;
	}

	/**
	 * @param customerWorth
	 *            the customerWorth to set
	 */
	public void setCustomerWorth(BigDecimal customerWorth) {
		this.customerWorth = customerWorth;
	}

	/**
	 * @return the secPortfolioValueStr
	 */
	public String getSecPortfolioValueStr() {
		return secPortfolioValueStr;
	}

	/**
	 * @param secPortfolioValueStr
	 *            the secPortfolioValueStr to set
	 */
	public void setSecPortfolioValueStr(String secPortfolioValueStr) {
		this.secPortfolioValueStr = secPortfolioValueStr;
	}

	/**
	 * @return the bvn
	 */
	public String getBvn() {
		return bvn;
	}

	/**
	 * @param bvn
	 *            the bvn to set
	 */
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	/**
	 * @return the stockData
	 */
	public List<StockData> getStockData() {
		return stockData;
	}

	/**
	 * @param stockData
	 *            the stockData to set
	 */
	public void setStockData(List<StockData> stockData) {
		this.stockData = stockData;
	}

	/**
	 * @return the amCashBalance
	 */
	public String getAmCashBalance() {
		return amCashBalance;
	}

	/**
	 * @param amCashBalance
	 *            the amCashBalance to set
	 */
	public void setAmCashBalance(String amCashBalance) {
		this.amCashBalance = amCashBalance;
	}

	/**
	 * @return the displaycbfValue
	 */
	public String getDisplaycbfValue() {
		return displaycbfValue;
	}

	/**
	 * @param displaycbfValue
	 *            the displaycbfValue to set
	 */
	public void setDisplaycbfValue(String displaycbfValue) {
		this.displaycbfValue = displaycbfValue;
	}

	/**
	 * @return the displaycfifValue
	 */
	public String getDisplaycfifValue() {
		return displaycfifValue;
	}

	/**
	 * @param displaycfifValue
	 *            the displaycfifValue to set
	 */
	public void setDisplaycfifValue(String displaycfifValue) {
		this.displaycfifValue = displaycfifValue;
	}

	/**
	 * @return the displaycmmfValue
	 */
	public String getDisplaycmmfValue() {
		return displaycmmfValue;
	}

	/**
	 * @param displaycmmfValue
	 *            the displaycmmfValue to set
	 */
	public void setDisplaycmmfValue(String displaycmmfValue) {
		this.displaycmmfValue = displaycmmfValue;
	}

	/**
	 * @return the displaySecCashValue
	 */
	public String getDisplaySecCashValue() {
		return displaySecCashValue;
	}

	/**
	 * @param displaySecCashValue
	 *            the displaySecCashValue to set
	 */
	public void setDisplaySecCashValue(String displaySecCashValue) {
		this.displaySecCashValue = displaySecCashValue;
	}

	/**
	 * @return the displaySecEquitiesValue
	 */
	public String getDisplaySecEquitiesValue() {
		return displaySecEquitiesValue;
	}

	/**
	 * @param displaySecEquitiesValue
	 *            the displaySecEquitiesValue to set
	 */
	public void setDisplaySecEquitiesValue(String displaySecEquitiesValue) {
		this.displaySecEquitiesValue = displaySecEquitiesValue;
	}

	/**
	 * @return the amAddress
	 */
	public String getAmAddress() {
		return amAddress;
	}

	/**
	 * @param amAddress the amAddress to set
	 */
	public void setAmAddress(String amAddress) {
		this.amAddress = amAddress;
	}

	/**
	 * @return the secAddress
	 */
	public String getSecAddress() {
		return secAddress;
	}

	/**
	 * @param secAddress the secAddress to set
	 */
	public void setSecAddress(String secAddress) {
		this.secAddress = secAddress;
	}

}
