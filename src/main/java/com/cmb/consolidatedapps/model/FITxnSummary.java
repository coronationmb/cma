package com.cmb.consolidatedapps.model;

public class FITxnSummary {
	
	private String instrumentId;
	
	private FITxnAmount fiTxnAmount;
	
	private String txnDate;
	
	private String txnDesc;
	
	private String txnType;

	/**
	 * @return the instrumentId
	 */
	public String getInstrumentId() {
		return instrumentId;
	}

	/**
	 * @param instrumentId the instrumentId to set
	 */
	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}

	/**
	 * @return the fiTxnAmount
	 */
	public FITxnAmount getFiTxnAmount() {
		return fiTxnAmount;
	}

	/**
	 * @param fiTxnAmount the fiTxnAmount to set
	 */
	public void setFiTxnAmount(FITxnAmount fiTxnAmount) {
		this.fiTxnAmount = fiTxnAmount;
	}

	/**
	 * @return the txnDate
	 */
	public String getTxnDate() {
		return txnDate;
	}

	/**
	 * @param txnDate the txnDate to set
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	/**
	 * @return the txnDesc
	 */
	public String getTxnDesc() {
		return txnDesc;
	}

	/**
	 * @param txnDesc the txnDesc to set
	 */
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}

	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

}
