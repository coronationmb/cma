/**
 * 
 */
package com.cmb.consolidatedapps.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name = "customer_beneficiary")
public class CustomerBeneficiary {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="cust_ben_generator")
	@SequenceGenerator(name="cust_ben_generator",allocationSize=1,sequenceName="cust_ben_gen")
	@Id
	private Long id;

	@Column(nullable = false,name="beneficiary_account_name")
	private String beneficiaryAccountName;

	@Column(nullable = false,name="beneficiary_account_number")
	private String beneficiaryAccountNumber;

	@Column(name="beneficiary_bvn")
	private String beneficiaryBvn;

	@ManyToOne
    @JoinColumn(nullable=false,name="customer_id")
	private Customer customer;

	@OneToOne(cascade =  CascadeType.ALL , fetch= FetchType.EAGER)
	@JoinColumn(name = "institution_Code",nullable=false)
	private FinancialInstitution financialInstitution;

	@OneToOne
    @JoinColumn(name="transaction_type_id",nullable=false)
	private TransactionType transactionType;

    @Column(name="name_enquiry_session_id")
	private String nameEnquirySessionId;

    @Column(name="beneficiary_kyc")
	private String beneficiaryKyc;

    @Column(name="delete_flg", length=1)
	private String deleteFlg = "N";

    @Transient
	private String displayBeneficiary;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the beneficiaryAccountName
	 */
	public String getBeneficiaryAccountName() {
		return beneficiaryAccountName;
	}

	/**
	 * @param beneficiaryAccountName the beneficiaryAccountName to set
	 */
	public void setBeneficiaryAccountName(String beneficiaryAccountName) {
		this.beneficiaryAccountName = beneficiaryAccountName;
	}

	/**
	 * @return the beneficiaryAccountNumber
	 */
	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	/**
	 * @param beneficiaryAccountNumber the beneficiaryAccountNumber to set
	 */
	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	/**
	 * @return the beneficiaryBvn
	 */
	public String getBeneficiaryBvn() {
		return beneficiaryBvn;
	}

	/**
	 * @param beneficiaryBvn the beneficiaryBvn to set
	 */
	public void setBeneficiaryBvn(String beneficiaryBvn) {
		this.beneficiaryBvn = beneficiaryBvn;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the financialInstitution
	 */
	public FinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	/**
	 * @param financialInstitution the financialInstitution to set
	 */
	public void setFinancialInstitution(FinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	/**
	 * @return the transactionType
	 */
	public TransactionType getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the nameEnquirySessionId
	 */
	public String getNameEnquirySessionId() {
		return nameEnquirySessionId;
	}

	/**
	 * @param nameEnquirySessionId the nameEnquirySessionId to set
	 */
	public void setNameEnquirySessionId(String nameEnquirySessionId) {
		this.nameEnquirySessionId = nameEnquirySessionId;
	}

	/**
	 * @return the beneficiaryKyc
	 */
	public String getBeneficiaryKyc() {
		return beneficiaryKyc;
	}

	/**
	 * @param beneficiaryKyc the beneficiaryKyc to set
	 */
	public void setBeneficiaryKyc(String beneficiaryKyc) {
		this.beneficiaryKyc = beneficiaryKyc;
	}

	/**
	 * @return the deleteFlg
	 */
	public String getDeleteFlg() {
		return deleteFlg;
	}

	/**
	 * @param deleteFlg the deleteFlg to set
	 */
	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	/**
	 * @return the displayBeneficiary
	 */
	public String getDisplayBeneficiary() {
		return displayBeneficiary;
	}

	/**
	 * @param displayBeneficiary the displayBeneficiary to set
	 */
	public void setDisplayBeneficiary(String displayBeneficiary) {
		this.displayBeneficiary = displayBeneficiary;
	}

}
