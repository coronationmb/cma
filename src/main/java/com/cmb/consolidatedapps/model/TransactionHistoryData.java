/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.List;

import com.cmb.thirdpartyapi.model.AMFundAccountStatementRows;
import com.cmb.thirdpartyapi.model.SecurityTransaction;

/**
 * @author waliu.faleye
 *
 */
public class TransactionHistoryData {

	private String bankStmtFromDate;

	private String bankStmtToDate;

	private String bankStmtAccount;

	private String openingBalance;

	private List<CustomerBankAccount> currentAccountList;

	private List<FITransactionDetail> bankTransactionsDetail;

	private String amStmtFromDate;

	private String amStmtToDate;

	private String amCustAID;

	private List<SubsidiaryCustomerDetail> amCustAIDList;

	private List<AMFundAccountStatementRows> amTransactionsDetail;

	private String secStmtFromDate;

	private String secStmtToDate;

	private String secCustAID;

	private List<SubsidiaryCustomerDetail> secCustAIDList;

	private List<SecurityTransaction> secTransactionsDetail;

	private String trusteesStmtFromDate;

	private String trusteesStmtToDate;

	private List<AMFundAccountStatementRows> trusteesTransactionsDetail;

	/**
	 * @return the bankStmtFromDate
	 */
	public String getBankStmtFromDate() {
		return bankStmtFromDate;
	}

	/**
	 * @param bankStmtFromDate
	 *            the bankStmtFromDate to set
	 */
	public void setBankStmtFromDate(String bankStmtFromDate) {
		this.bankStmtFromDate = bankStmtFromDate;
	}

	/**
	 * @return the bankStmtToDate
	 */
	public String getBankStmtToDate() {
		return bankStmtToDate;
	}

	/**
	 * @param bankStmtToDate
	 *            the bankStmtToDate to set
	 */
	public void setBankStmtToDate(String bankStmtToDate) {
		this.bankStmtToDate = bankStmtToDate;
	}

	/**
	 * @return the bankStmtAccount
	 */
	public String getBankStmtAccount() {
		return bankStmtAccount;
	}

	/**
	 * @param bankStmtAccount
	 *            the bankStmtAccount to set
	 */
	public void setBankStmtAccount(String bankStmtAccount) {
		this.bankStmtAccount = bankStmtAccount;
	}

	/**
	 * @return the currentAccountList
	 */
	public List<CustomerBankAccount> getCurrentAccountList() {
		return currentAccountList;
	}

	/**
	 * @param currentAccountList
	 *            the currentAccountList to set
	 */
	public void setCurrentAccountList(List<CustomerBankAccount> currentAccountList) {
		this.currentAccountList = currentAccountList;
	}

	/**
	 * @return the bankTransactionsDetail
	 */
	public List<FITransactionDetail> getBankTransactionsDetail() {
		return bankTransactionsDetail;
	}

	/**
	 * @param bankTransactionsDetail
	 *            the bankTransactionsDetail to set
	 */
	public void setBankTransactionsDetail(List<FITransactionDetail> bankTransactionsDetail) {
		this.bankTransactionsDetail = bankTransactionsDetail;
	}

	/**
	 * @return the amStmtFromDate
	 */
	public String getAmStmtFromDate() {
		return amStmtFromDate;
	}

	/**
	 * @param amStmtFromDate
	 *            the amStmtFromDate to set
	 */
	public void setAmStmtFromDate(String amStmtFromDate) {
		this.amStmtFromDate = amStmtFromDate;
	}

	/**
	 * @return the amStmtToDate
	 */
	public String getAmStmtToDate() {
		return amStmtToDate;
	}

	/**
	 * @param amStmtToDate
	 *            the amStmtToDate to set
	 */
	public void setAmStmtToDate(String amStmtToDate) {
		this.amStmtToDate = amStmtToDate;
	}

	/**
	 * @return the amCustAIDList
	 */
	public List<SubsidiaryCustomerDetail> getAmCustAIDList() {
		return amCustAIDList;
	}

	/**
	 * @param amCustAIDList
	 *            the amCustAIDList to set
	 */
	public void setAmCustAIDList(List<SubsidiaryCustomerDetail> amCustAIDList) {
		this.amCustAIDList = amCustAIDList;
	}

	/**
	 * @return the amTransactionsDetail
	 */
	public List<AMFundAccountStatementRows> getAmTransactionsDetail() {
		return amTransactionsDetail;
	}

	/**
	 * @param amTransactionsDetail
	 *            the amTransactionsDetail to set
	 */
	public void setAmTransactionsDetail(List<AMFundAccountStatementRows> amTransactionsDetail) {
		this.amTransactionsDetail = amTransactionsDetail;
	}

	/**
	 * @return the secStmtFromDate
	 */
	public String getSecStmtFromDate() {
		return secStmtFromDate;
	}

	/**
	 * @param secStmtFromDate
	 *            the secStmtFromDate to set
	 */
	public void setSecStmtFromDate(String secStmtFromDate) {
		this.secStmtFromDate = secStmtFromDate;
	}

	/**
	 * @return the secStmtToDate
	 */
	public String getSecStmtToDate() {
		return secStmtToDate;
	}

	/**
	 * @param secStmtToDate
	 *            the secStmtToDate to set
	 */
	public void setSecStmtToDate(String secStmtToDate) {
		this.secStmtToDate = secStmtToDate;
	}

	/**
	 * @return the secCustAIDList
	 */
	public List<SubsidiaryCustomerDetail> getSecCustAIDList() {
		return secCustAIDList;
	}

	/**
	 * @param secCustAIDList
	 *            the secCustAIDList to set
	 */
	public void setSecCustAIDList(List<SubsidiaryCustomerDetail> secCustAIDList) {
		this.secCustAIDList = secCustAIDList;
	}

	/**
	 * @return the secTransactionsDetail
	 */
	public List<SecurityTransaction> getSecTransactionsDetail() {
		return secTransactionsDetail;
	}

	/**
	 * @param secTransactionsDetail
	 *            the secTransactionsDetail to set
	 */
	public void setSecTransactionsDetail(List<SecurityTransaction> secTransactionsDetail) {
		this.secTransactionsDetail = secTransactionsDetail;
	}

	/**
	 * @return the trusteesStmtFromDate
	 */
	public String getTrusteesStmtFromDate() {
		return trusteesStmtFromDate;
	}

	/**
	 * @param trusteesStmtFromDate
	 *            the trusteesStmtFromDate to set
	 */
	public void setTrusteesStmtFromDate(String trusteesStmtFromDate) {
		this.trusteesStmtFromDate = trusteesStmtFromDate;
	}

	/**
	 * @return the trusteesStmtToDate
	 */
	public String getTrusteesStmtToDate() {
		return trusteesStmtToDate;
	}

	/**
	 * @param trusteesStmtToDate
	 *            the trusteesStmtToDate to set
	 */
	public void setTrusteesStmtToDate(String trusteesStmtToDate) {
		this.trusteesStmtToDate = trusteesStmtToDate;
	}

	/**
	 * @return the trusteesTransactionsDetail
	 */
	public List<AMFundAccountStatementRows> getTrusteesTransactionsDetail() {
		return trusteesTransactionsDetail;
	}

	/**
	 * @param trusteesTransactionsDetail
	 *            the trusteesTransactionsDetail to set
	 */
	public void setTrusteesTransactionsDetail(List<AMFundAccountStatementRows> trusteesTransactionsDetail) {
		this.trusteesTransactionsDetail = trusteesTransactionsDetail;
	}

	/**
	 * @return the amCustAID
	 */
	public String getAmCustAID() {
		return amCustAID;
	}

	/**
	 * @param amCustAID
	 *            the amCustAID to set
	 */
	public void setAmCustAID(String amCustAID) {
		this.amCustAID = amCustAID;
	}

	/**
	 * @return the secCustAID
	 */
	public String getSecCustAID() {
		return secCustAID;
	}

	/**
	 * @param secCustAID
	 *            the secCustAID to set
	 */
	public void setSecCustAID(String secCustAID) {
		this.secCustAID = secCustAID;
	}

	/**
	 * @return the openingBalance
	 */
	public String getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(String openingBalance) {
		this.openingBalance = openingBalance;
	}

}
