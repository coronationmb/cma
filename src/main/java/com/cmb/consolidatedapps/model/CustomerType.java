/**
 * 
 */
package com.cmb.consolidatedapps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name="customer_type")
public class CustomerType {
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	
	private String type;
	
	@Column(name = "entrust_group")
	private String entrustGroup;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the entrustGroup
	 */
	public String getEntrustGroup() {
		return entrustGroup;
	}

	/**
	 * @param entrustGroup the entrustGroup to set
	 */
	public void setEntrustGroup(String entrustGroup) {
		this.entrustGroup = entrustGroup;
	}

}
