/**
 * 
 */
package com.cmb.consolidatedapps.model;

import javax.persistence.Transient;

/**
 * @author waliu.faleye
 *
 */
public class StatementData {

	private String startDate;

	private String endDate;

	private String accountNumber;

	private Long app1;

	private Long app2;

	private Long app3;

	private Long app4;
	
	private String fileType;

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the app1
	 */
	public Long getApp1() {
		return app1;
	}

	/**
	 * @param app1 the app1 to set
	 */
	public void setApp1(Long app1) {
		this.app1 = app1;
	}

	/**
	 * @return the app2
	 */
	public Long getApp2() {
		return app2;
	}

	/**
	 * @param app2 the app2 to set
	 */
	public void setApp2(Long app2) {
		this.app2 = app2;
	}

	/**
	 * @return the app3
	 */
	public Long getApp3() {
		return app3;
	}

	/**
	 * @param app3 the app3 to set
	 */
	public void setApp3(Long app3) {
		this.app3 = app3;
	}

	/**
	 * @return the app4
	 */
	public Long getApp4() {
		return app4;
	}

	/**
	 * @param app4 the app4 to set
	 */
	public void setApp4(Long app4) {
		this.app4 = app4;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
