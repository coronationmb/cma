/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class ApplicationCustomerDetailRq {
	
	private Long applicationId;
	
	private String bvn;
	
	private String rcNumber;
	
	private String accountNumber;
	
	private String transactionChannel;

	/**
	 * @return the applicationId
	 */
	public Long getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the bvn
	 */
	public String getBvn() {
		return bvn;
	}

	/**
	 * @param bvn the bvn to set
	 */
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	/**
	 * @return the rcNumber
	 */
	public String getRcNumber() {
		return rcNumber;
	}

	/**
	 * @param rcNumber the rcNumber to set
	 */
	public void setRcNumber(String rcNumber) {
		this.rcNumber = rcNumber;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the transactionChannel
	 */
	public String getTransactionChannel() {
		return transactionChannel;
	}

	/**
	 * @param transactionChannel the transactionChannel to set
	 */
	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}

}
