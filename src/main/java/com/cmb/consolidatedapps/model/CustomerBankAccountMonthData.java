/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author waliu.faleye
 *
 */
public class CustomerBankAccountMonthData {
	
	private Date tranDate;

	private String tranDateStr;

	private String tranId;

	private BigDecimal tranAmount;

	private String tranAmountStr;

	private String tranParticular;

	private String referenceNumber;

	private String tranRemarks;

	private String partTranType;

	/**
	 * @return the tranDate
	 */
	public Date getTranDate() {
		return tranDate;
	}

	/**
	 * @param tranDate the tranDate to set
	 */
	public void setTranDate(Date tranDate) {
		this.tranDate = tranDate;
	}

	/**
	 * @return the tranDateStr
	 */
	public String getTranDateStr() {
		return tranDateStr;
	}

	/**
	 * @param tranDateStr the tranDateStr to set
	 */
	public void setTranDateStr(String tranDateStr) {
		this.tranDateStr = tranDateStr;
	}

	/**
	 * @return the tranId
	 */
	public String getTranId() {
		return tranId;
	}

	/**
	 * @param tranId the tranId to set
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}

	/**
	 * @return the tranAmount
	 */
	public BigDecimal getTranAmount() {
		return tranAmount;
	}

	/**
	 * @param tranAmount the tranAmount to set
	 */
	public void setTranAmount(BigDecimal tranAmount) {
		this.tranAmount = tranAmount;
	}

	/**
	 * @return the tranAmountStr
	 */
	public String getTranAmountStr() {
		return tranAmountStr;
	}

	/**
	 * @param tranAmountStr the tranAmountStr to set
	 */
	public void setTranAmountStr(String tranAmountStr) {
		this.tranAmountStr = tranAmountStr;
	}

	/**
	 * @return the tranParticular
	 */
	public String getTranParticular() {
		return tranParticular;
	}

	/**
	 * @param tranParticular the tranParticular to set
	 */
	public void setTranParticular(String tranParticular) {
		this.tranParticular = tranParticular;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the tranRemarks
	 */
	public String getTranRemarks() {
		return tranRemarks;
	}

	/**
	 * @param tranRemarks the tranRemarks to set
	 */
	public void setTranRemarks(String tranRemarks) {
		this.tranRemarks = tranRemarks;
	}

	/**
	 * @return the partTranType
	 */
	public String getPartTranType() {
		return partTranType;
	}

	/**
	 * @param partTranType the partTranType to set
	 */
	public void setPartTranType(String partTranType) {
		this.partTranType = partTranType;
	}

}
