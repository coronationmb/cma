/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class DailyLimitBalanceResponse {
	
	private String totalDailyLimit;
	
	private String dailyBalance;

	/**
	 * @return the totalDailyLimit
	 */
	public String getTotalDailyLimit() {
		return totalDailyLimit;
	}

	/**
	 * @param totalDailyLimit the totalDailyLimit to set
	 */
	public void setTotalDailyLimit(String totalDailyLimit) {
		this.totalDailyLimit = totalDailyLimit;
	}

	/**
	 * @return the dailyBalance
	 */
	public String getDailyBalance() {
		return dailyBalance;
	}

	/**
	 * @param dailyBalance the dailyBalance to set
	 */
	public void setDailyBalance(String dailyBalance) {
		this.dailyBalance = dailyBalance;
	}

}
