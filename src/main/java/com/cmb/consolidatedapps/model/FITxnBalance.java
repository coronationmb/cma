/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class FITxnBalance {

	private String amountValue;
	
	private String currencyCode;

	/**
	 * @return the amountValue
	 */
	public String getAmountValue() {
		return amountValue;
	}

	/**
	 * @param amountValue the amountValue to set
	 */
	public void setAmountValue(String amountValue) {
		this.amountValue = amountValue;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
