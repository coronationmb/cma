/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name = "topup_tran_detail", schema = "cmauser")
public class TopupTransactionDetail {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "topup_detail_generator")
	@SequenceGenerator(name = "topup_detail_generator", allocationSize = 1, sequenceName = "topup_tran_detail_gen")
	@Id
	private Long id;

	@OneToOne
	@JoinColumn(name = "application_id", nullable = false)
	private Application application;

	@Transient
	private Long applicationId;

	@Column(name = "account_number", nullable = false)
	private String accountNumber;

	@Column(name = "customer_aid")
	private String customerAid;

	@Column(name = "customer_name")
	private String customerName;

	@Transient
	private String addBeneficiary;

	@Transient
	private Long fundTypeId;

	@Transient
	private Long topUpTypeId;

	@Transient
	private Long topUpSrcId;

	@OneToOne
	@JoinColumn(name = "mutual_fund_product_id")
	private MutualFundProduct mutualFundProduct;

	@OneToOne
	@JoinColumn(name = "top_up_type_id", nullable = false)
	private TopUpType topUpType;

	@OneToOne
	@JoinColumn(name = "top_up_source_id", nullable = false)
	private TopUpSource topUpSource;

	@Transient
	private String financialInstitutionCode;

	@OneToOne
	@JoinColumn(name = "institution_Code")
	private FinancialInstitution financialInstitution;

	@Column(nullable = false)
	private BigDecimal amount;

	@Transient
	private String formattedAmount;

	@Column(nullable = false)
	private String remark;

	@Column(name = "response_code")
	private String responseCode;

	@Column(name = "response_description")
	private String responseDescription;

	@Column(name = "dr_response_code")
	private String drResponseCode;

	@Column(name = "dr_response_description")
	private String drResponseDescription;

	@Column(name = "request_date", nullable = false)
	private LocalDateTime requestDate;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToOne
	@JoinColumn(name = "customer_sub_bene_id")
	private CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary;

	@Transient
	private Long beneficiaryId;

	@Transient
	private String ownAccountNumber;

	@Transient
	private Long userId;

	@Transient
	private List<CustomerSubsidiaryBeneficiary> customerBeneficiaries = new ArrayList<CustomerSubsidiaryBeneficiary>();

	@Column(name = "reverse_transaction_flag", length = 1)
	private String reverseTransactionFlag;

	@Column(name = "unique_identifier")
	private String uniqueIdentifier;

	@Transient
	private String beneficiaryExist = "N";

	@Transient
	private String token;

	@Transient
	private String tokenRespMessage;

	@Transient
	private String amAddBeneficiary;

	@Transient
	private String amCustomerName;

	@Transient
	private String amCustomerAid;

	@Transient
	private String ownAmCustomerAid;

	@Transient
	private Long amBeneficiaryId;

	@Transient
	private String secAddBeneficiary;

	@Transient
	private String secCustomerName;

	@Transient
	private String secCustomerAid;

	@Transient
	private String ownSecCustomerAid;

	@Transient
	private Long secBeneficiaryId;

	@Transient
	private String trustAddBeneficiary;

	@Transient
	private String trustCustomerName;

	@Transient
	private String trustCustomerAid;

	@Transient
	private String ownTrustCustomerAid;

	@Transient
	private Long trustBeneficiaryId;

	@Transient
	private String mode;

    @Transient
	private String errorMessage;

    @Transient
	private String existingBen;

    @Transient
	private String newBen;

    @Transient
	private String existBenCheck;

    @Transient
	private String newBenCheck;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * @param application
	 *            the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}

	/**
	 * @return the applicationId
	 */
	public Long getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the addBeneficiary
	 */
	public String getAddBeneficiary() {
		return addBeneficiary;
	}

	/**
	 * @param addBeneficiary
	 *            the addBeneficiary to set
	 */
	public void setAddBeneficiary(String addBeneficiary) {
		this.addBeneficiary = addBeneficiary;
	}

	/**
	 * @return the fundTypeId
	 */
	public Long getFundTypeId() {
		return fundTypeId;
	}

	/**
	 * @param fundTypeId
	 *            the fundTypeId to set
	 */
	public void setFundTypeId(Long fundTypeId) {
		this.fundTypeId = fundTypeId;
	}

	/**
	 * @return the topUpTypeId
	 */
	public Long getTopUpTypeId() {
		return topUpTypeId;
	}

	/**
	 * @param topUpTypeId
	 *            the topUpTypeId to set
	 */
	public void setTopUpTypeId(Long topUpTypeId) {
		this.topUpTypeId = topUpTypeId;
	}

	/**
	 * @return the topUpSrcId
	 */
	public Long getTopUpSrcId() {
		return topUpSrcId;
	}

	/**
	 * @param topUpSrcId
	 *            the topUpSrcId to set
	 */
	public void setTopUpSrcId(Long topUpSrcId) {
		this.topUpSrcId = topUpSrcId;
	}

	/**
	 * @return the mutualFundProduct
	 */
	public MutualFundProduct getMutualFundProduct() {
		return mutualFundProduct;
	}

	/**
	 * @param mutualFundProduct
	 *            the mutualFundProduct to set
	 */
	public void setMutualFundProduct(MutualFundProduct mutualFundProduct) {
		this.mutualFundProduct = mutualFundProduct;
	}

	/**
	 * @return the topUpType
	 */
	public TopUpType getTopUpType() {
		return topUpType;
	}

	/**
	 * @param topUpType
	 *            the topUpType to set
	 */
	public void setTopUpType(TopUpType topUpType) {
		this.topUpType = topUpType;
	}

	/**
	 * @return the topUpSource
	 */
	public TopUpSource getTopUpSource() {
		return topUpSource;
	}

	/**
	 * @param topUpSource
	 *            the topUpSource to set
	 */
	public void setTopUpSource(TopUpSource topUpSource) {
		this.topUpSource = topUpSource;
	}

	/**
	 * @return the financialInstitutionCode
	 */
	public String getFinancialInstitutionCode() {
		return financialInstitutionCode;
	}

	/**
	 * @param financialInstitutionCode
	 *            the financialInstitutionCode to set
	 */
	public void setFinancialInstitutionCode(String financialInstitutionCode) {
		this.financialInstitutionCode = financialInstitutionCode;
	}

	/**
	 * @return the financialInstitution
	 */
	public FinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	/**
	 * @param financialInstitution
	 *            the financialInstitution to set
	 */
	public void setFinancialInstitution(FinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the formattedAmount
	 */
	public String getFormattedAmount() {
		return formattedAmount;
	}

	/**
	 * @param formattedAmount
	 *            the formattedAmount to set
	 */
	public void setFormattedAmount(String formattedAmount) {
		this.formattedAmount = formattedAmount;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseDescription
	 */
	public String getResponseDescription() {
		return responseDescription;
	}

	/**
	 * @param responseDescription
	 *            the responseDescription to set
	 */
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	/**
	 * @return the requestDate
	 */
	public LocalDateTime getRequestDate() {
		return requestDate;
	}

	/**
	 * @param requestDate
	 *            the requestDate to set
	 */
	public void setRequestDate(LocalDateTime requestDate) {
		this.requestDate = requestDate;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the customerSubsidiaryBeneficiary
	 */
	public CustomerSubsidiaryBeneficiary getCustomerSubsidiaryBeneficiary() {
		return customerSubsidiaryBeneficiary;
	}

	/**
	 * @param customerSubsidiaryBeneficiary
	 *            the customerSubsidiaryBeneficiary to set
	 */
	public void setCustomerSubsidiaryBeneficiary(CustomerSubsidiaryBeneficiary customerSubsidiaryBeneficiary) {
		this.customerSubsidiaryBeneficiary = customerSubsidiaryBeneficiary;
	}

	/**
	 * @return the beneficiaryId
	 */
	public Long getBeneficiaryId() {
		return beneficiaryId;
	}

	/**
	 * @param beneficiaryId
	 *            the beneficiaryId to set
	 */
	public void setBeneficiaryId(Long beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	/**
	 * @return the ownAccountNumber
	 */
	public String getOwnAccountNumber() {
		return ownAccountNumber;
	}

	/**
	 * @param ownAccountNumber
	 *            the ownAccountNumber to set
	 */
	public void setOwnAccountNumber(String ownAccountNumber) {
		this.ownAccountNumber = ownAccountNumber;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the customerBeneficiaries
	 */
	public List<CustomerSubsidiaryBeneficiary> getCustomerBeneficiaries() {
		return customerBeneficiaries;
	}

	/**
	 * @param customerBeneficiaries
	 *            the customerBeneficiaries to set
	 */
	public void setCustomerBeneficiaries(List<CustomerSubsidiaryBeneficiary> customerBeneficiaries) {
		this.customerBeneficiaries = customerBeneficiaries;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber
	 *            the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the customerAid
	 */
	public String getCustomerAid() {
		return customerAid;
	}

	/**
	 * @param customerAid
	 *            the customerAid to set
	 */
	public void setCustomerAid(String customerAid) {
		this.customerAid = customerAid;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the drResponseCode
	 */
	public String getDrResponseCode() {
		return drResponseCode;
	}

	/**
	 * @param drResponseCode
	 *            the drResponseCode to set
	 */
	public void setDrResponseCode(String drResponseCode) {
		this.drResponseCode = drResponseCode;
	}

	/**
	 * @return the drResponseDescription
	 */
	public String getDrResponseDescription() {
		return drResponseDescription;
	}

	/**
	 * @param drResponseDescription
	 *            the drResponseDescription to set
	 */
	public void setDrResponseDescription(String drResponseDescription) {
		this.drResponseDescription = drResponseDescription;
	}

	/**
	 * @return the reverseTransactionFlag
	 */
	public String getReverseTransactionFlag() {
		return reverseTransactionFlag;
	}

	/**
	 * @param reverseTransactionFlag
	 *            the reverseTransactionFlag to set
	 */
	public void setReverseTransactionFlag(String reverseTransactionFlag) {
		this.reverseTransactionFlag = reverseTransactionFlag;
	}

	/**
	 * @return the uniqueIdentifier
	 */
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}

	/**
	 * @param uniqueIdentifier
	 *            the uniqueIdentifier to set
	 */
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}

	/**
	 * @return the beneficiaryExist
	 */
	public String getBeneficiaryExist() {
		return beneficiaryExist;
	}

	/**
	 * @param beneficiaryExist
	 *            the beneficiaryExist to set
	 */
	public void setBeneficiaryExist(String beneficiaryExist) {
		this.beneficiaryExist = beneficiaryExist;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the tokenRespMessage
	 */
	public String getTokenRespMessage() {
		return tokenRespMessage;
	}

	/**
	 * @param tokenRespMessage
	 *            the tokenRespMessage to set
	 */
	public void setTokenRespMessage(String tokenRespMessage) {
		this.tokenRespMessage = tokenRespMessage;
	}

	/**
	 * @return the amAddBeneficiary
	 */
	public String getAmAddBeneficiary() {
		return amAddBeneficiary;
	}

	/**
	 * @param amAddBeneficiary the amAddBeneficiary to set
	 */
	public void setAmAddBeneficiary(String amAddBeneficiary) {
		this.amAddBeneficiary = amAddBeneficiary;
	}

	/**
	 * @return the amCustomerName
	 */
	public String getAmCustomerName() {
		return amCustomerName;
	}

	/**
	 * @param amCustomerName the amCustomerName to set
	 */
	public void setAmCustomerName(String amCustomerName) {
		this.amCustomerName = amCustomerName;
	}

	/**
	 * @return the amCustomerAid
	 */
	public String getAmCustomerAid() {
		return amCustomerAid;
	}

	/**
	 * @param amCustomerAid the amCustomerAid to set
	 */
	public void setAmCustomerAid(String amCustomerAid) {
		this.amCustomerAid = amCustomerAid;
	}

	/**
	 * @return the amBeneficiaryId
	 */
	public Long getAmBeneficiaryId() {
		return amBeneficiaryId;
	}

	/**
	 * @param amBeneficiaryId the amBeneficiaryId to set
	 */
	public void setAmBeneficiaryId(Long amBeneficiaryId) {
		this.amBeneficiaryId = amBeneficiaryId;
	}

	/**
	 * @return the secCustomerName
	 */
	public String getSecCustomerName() {
		return secCustomerName;
	}

	/**
	 * @param secCustomerName the secCustomerName to set
	 */
	public void setSecCustomerName(String secCustomerName) {
		this.secCustomerName = secCustomerName;
	}

	/**
	 * @return the secCustomerAid
	 */
	public String getSecCustomerAid() {
		return secCustomerAid;
	}

	/**
	 * @param secCustomerAid the secCustomerAid to set
	 */
	public void setSecCustomerAid(String secCustomerAid) {
		this.secCustomerAid = secCustomerAid;
	}

	/**
	 * @return the secBeneficiaryId
	 */
	public Long getSecBeneficiaryId() {
		return secBeneficiaryId;
	}

	/**
	 * @param secBeneficiaryId the secBeneficiaryId to set
	 */
	public void setSecBeneficiaryId(Long secBeneficiaryId) {
		this.secBeneficiaryId = secBeneficiaryId;
	}

	/**
	 * @return the trustAddBeneficiary
	 */
	public String getTrustAddBeneficiary() {
		return trustAddBeneficiary;
	}

	/**
	 * @param trustAddBeneficiary the trustAddBeneficiary to set
	 */
	public void setTrustAddBeneficiary(String trustAddBeneficiary) {
		this.trustAddBeneficiary = trustAddBeneficiary;
	}

	/**
	 * @return the trustCustomerName
	 */
	public String getTrustCustomerName() {
		return trustCustomerName;
	}

	/**
	 * @param trustCustomerName the trustCustomerName to set
	 */
	public void setTrustCustomerName(String trustCustomerName) {
		this.trustCustomerName = trustCustomerName;
	}

	/**
	 * @return the trustCustomerAid
	 */
	public String getTrustCustomerAid() {
		return trustCustomerAid;
	}

	/**
	 * @param trustCustomerAid the trustCustomerAid to set
	 */
	public void setTrustCustomerAid(String trustCustomerAid) {
		this.trustCustomerAid = trustCustomerAid;
	}

	/**
	 * @return the trustBeneficiaryId
	 */
	public Long getTrustBeneficiaryId() {
		return trustBeneficiaryId;
	}

	/**
	 * @param trustBeneficiaryId the trustBeneficiaryId to set
	 */
	public void setTrustBeneficiaryId(Long trustBeneficiaryId) {
		this.trustBeneficiaryId = trustBeneficiaryId;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the secAddBeneficiary
	 */
	public String getSecAddBeneficiary() {
		return secAddBeneficiary;
	}

	/**
	 * @param secAddBeneficiary the secAddBeneficiary to set
	 */
	public void setSecAddBeneficiary(String secAddBeneficiary) {
		this.secAddBeneficiary = secAddBeneficiary;
	}

	/**
	 * @return the ownAmCustomerAid
	 */
	public String getOwnAmCustomerAid() {
		return ownAmCustomerAid;
	}

	/**
	 * @param ownAmCustomerAid the ownAmCustomerAid to set
	 */
	public void setOwnAmCustomerAid(String ownAmCustomerAid) {
		this.ownAmCustomerAid = ownAmCustomerAid;
	}

	/**
	 * @return the ownSecCustomerAid
	 */
	public String getOwnSecCustomerAid() {
		return ownSecCustomerAid;
	}

	/**
	 * @param ownSecCustomerAid the ownSecCustomerAid to set
	 */
	public void setOwnSecCustomerAid(String ownSecCustomerAid) {
		this.ownSecCustomerAid = ownSecCustomerAid;
	}

	/**
	 * @return the ownTrustCustomerAid
	 */
	public String getOwnTrustCustomerAid() {
		return ownTrustCustomerAid;
	}

	/**
	 * @param ownTrustCustomerAid the ownTrustCustomerAid to set
	 */
	public void setOwnTrustCustomerAid(String ownTrustCustomerAid) {
		this.ownTrustCustomerAid = ownTrustCustomerAid;
	}

	/**
	 * @return the existingBen
	 */
	public String getExistingBen() {
		return existingBen;
	}

	/**
	 * @param existingBen the existingBen to set
	 */
	public void setExistingBen(String existingBen) {
		this.existingBen = existingBen;
	}

	/**
	 * @return the newBen
	 */
	public String getNewBen() {
		return newBen;
	}

	/**
	 * @param newBen the newBen to set
	 */
	public void setNewBen(String newBen) {
		this.newBen = newBen;
	}

	/**
	 * @return the existBenCheck
	 */
	public String getExistBenCheck() {
		return existBenCheck;
	}

	/**
	 * @param existBenCheck the existBenCheck to set
	 */
	public void setExistBenCheck(String existBenCheck) {
		this.existBenCheck = existBenCheck;
	}

	/**
	 * @return the newBenCheck
	 */
	public String getNewBenCheck() {
		return newBenCheck;
	}

	/**
	 * @param newBenCheck the newBenCheck to set
	 */
	public void setNewBenCheck(String newBenCheck) {
		this.newBenCheck = newBenCheck;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
