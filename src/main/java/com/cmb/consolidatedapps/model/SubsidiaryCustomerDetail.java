/**
 * 
 */
package com.cmb.consolidatedapps.model;

/**
 * @author waliu.faleye
 *
 */
public class SubsidiaryCustomerDetail {

	private String fundCode;
	private String accountOfficerEmail;
	private String accountOfficerPhone;
	private String accountOfficerName;
	private String customerAid;
	private String contact;
	private String lastName;
	private String firstName;
	private String middleName;
	private String name;
	private String title;
	private String nationality;
	private String clientType;
	private String branchCode;
	private String contactDate;
	private String accountOfficerID;
	private String accountOfficer;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String phone1;	
	private String phone2;
	private String phone3;
	private String fax;
	private String email1;
	private String email2;
	private String nextOfKin;
	private String relationship;
	private String website;
	private String birthDay;
	private String anniversary;
	private String notes;
	private String yahooMsgID;
	private String mSNMsgID2;
	private String skypeID;
	private String custom1;
	private String custom2;
	private String createdBy;
	private String createDate;
	private String id;
	private String profession;
	private String phoneNoofKin;
	private String emailAddofKin;
	private String nameofKin;
	private String RelationshipofKin;
	private String corpoarateIncNo;
	private String corporateDateFounded;
	private String clientMotherMaidenName;
	private String contactAddresNextofKin;
	private String category;
	private String designation;
	private String unit;
	private String lga;
	private String stateOfOrigin;
	private String maritalStat;
	private String nameofEmpKin;
	private String addressokEmpKin;
	private String phonenoEmp;
	private String relationshipofEmpKin;
	private String pensionNoEmp;
	private String pensionPFAEmp;
	private String basicPayEmp;
	private String grossPayEmp;
	private String firstPromo;
	private String secondPromo;
	private String thirdPromo;
	private String yearNYSC;
	private String clientCategory;
	private String riskinessLevel;
	private String IdNumber;
	//private String pep;
	private String typeOfIdIND;
	private String typeOfIdCorp;
	private String occupation;
	private String company;
	private String employmentType;
	private String bankName;
	private String bankAccountNumber;
	private String bvn;
	private String meansOfId;
	private String meansOfIdExpirationDate;
	private String nextOfKinName;
	private String nextOfKinRelationship;
	private String nextOfKinContactAddress;
	private String nextOfKinPhoneNo;
	private String nextOfKinEmailAddress;
	private String pep;
	private String customerBVN;
	private String displayCustomerAID;
	/**
	 * @return the accountName
	 */
	//public String getAccountName() {
		//return accountName;
	//}
	/**
	 * @param accountName the accountName to set
	 */
	//public void setAccountName(String accountName) {
		//this.accountName = accountName;
	//}
	/**
	 * @return the phone
	 */
	//public String getPhone() {
		//return phone;
	//}
	/**
	 * @param phone the phone to set
	 */
	//public void setPhone(String phone) {
		//this.phone = phone;
	//}
	/**
	 * @return the email
	 */
	//public String getEmail() {
		//return email;
	//}
	/**
	 * @param email the email to set
	 */
	//public void setEmail(String email) {
	//	this.email = email;
	//}
	/**
	 * @return the customerAid
	 */
	public String getCustomerAid() {
		return customerAid;
	}
	/**
	 * @param customerAid the customerAid to set
	 */
	public void setCustomerAid(String customerAid) {
		this.customerAid = customerAid;
	}
	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return the clientType
	 */
	public String getClientType() {
		return clientType;
	}
	/**
	 * @param clientType the clientType to set
	 */
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}
	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	/**
	 * @return the contactDate
	 */
	public String getContactDate() {
		return contactDate;
	}
	/**
	 * @param contactDate the contactDate to set
	 */
	public void setContactDate(String contactDate) {
		this.contactDate = contactDate;
	}
	/**
	 * @return the accountOfficerID
	 */
	public String getAccountOfficerID() {
		return accountOfficerID;
	}
	/**
	 * @param accountOfficerID the accountOfficerID to set
	 */
	public void setAccountOfficerID(String accountOfficerID) {
		this.accountOfficerID = accountOfficerID;
	}
	/**
	 * @return the accountOfficer
	 */
	public String getAccountOfficer() {
		return accountOfficer;
	}
	/**
	 * @param accountOfficer the accountOfficer to set
	 */
	public void setAccountOfficer(String accountOfficer) {
		this.accountOfficer = accountOfficer;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return phone1;
	}
	/**
	 * @param phone1 the phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return phone2;
	}
	/**
	 * @param phone2 the phone2 to set
	 */
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	/**
	 * @return the phone3
	 */
	public String getPhone3() {
		return phone3;
	}
	/**
	 * @param phone3 the phone3 to set
	 */
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the email1
	 */
	public String getEmail1() {
		return email1;
	}
	/**
	 * @param email1 the email1 to set
	 */
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	/**
	 * @return the email2
	 */
	public String getEmail2() {
		return email2;
	}
	/**
	 * @param email2 the email2 to set
	 */
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	/**
	 * @return the nextOfKin
	 */
	public String getNextOfKin() {
		return nextOfKin;
	}
	/**
	 * @param nextOfKin the nextOfKin to set
	 */
	public void setNextOfKin(String nextOfKin) {
		this.nextOfKin = nextOfKin;
	}
	/**
	 * @return the relationship
	 */
	public String getRelationship() {
		return relationship;
	}
	/**
	 * @param relationship the relationship to set
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the birthDay
	 */
	public String getBirthDay() {
		return birthDay;
	}
	/**
	 * @param birthDay the birthDay to set
	 */
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	/**
	 * @return the anniversary
	 */
	public String getAnniversary() {
		return anniversary;
	}
	/**
	 * @param anniversary the anniversary to set
	 */
	public void setAnniversary(String anniversary) {
		this.anniversary = anniversary;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the yahooMsgID
	 */
	public String getYahooMsgID() {
		return yahooMsgID;
	}
	/**
	 * @param yahooMsgID the yahooMsgID to set
	 */
	public void setYahooMsgID(String yahooMsgID) {
		this.yahooMsgID = yahooMsgID;
	}
	/**
	 * @return the mSNMsgID2
	 */
	public String getmSNMsgID2() {
		return mSNMsgID2;
	}
	/**
	 * @param mSNMsgID2 the mSNMsgID2 to set
	 */
	public void setmSNMsgID2(String mSNMsgID2) {
		this.mSNMsgID2 = mSNMsgID2;
	}
	/**
	 * @return the skypeID
	 */
	public String getSkypeID() {
		return skypeID;
	}
	/**
	 * @param skypeID the skypeID to set
	 */
	public void setSkypeID(String skypeID) {
		this.skypeID = skypeID;
	}
	/**
	 * @return the custom1
	 */
	public String getCustom1() {
		return custom1;
	}
	/**
	 * @param custom1 the custom1 to set
	 */
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	/**
	 * @return the custom2
	 */
	public String getCustom2() {
		return custom2;
	}
	/**
	 * @param custom2 the custom2 to set
	 */
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the profession
	 */
	public String getProfession() {
		return profession;
	}
	/**
	 * @param profession the profession to set
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}
	/**
	 * @return the phoneNoofKin
	 */
	public String getPhoneNoofKin() {
		return phoneNoofKin;
	}
	/**
	 * @param phoneNoofKin the phoneNoofKin to set
	 */
	public void setPhoneNoofKin(String phoneNoofKin) {
		this.phoneNoofKin = phoneNoofKin;
	}
	/**
	 * @return the emailAddofKin
	 */
	public String getEmailAddofKin() {
		return emailAddofKin;
	}
	/**
	 * @param emailAddofKin the emailAddofKin to set
	 */
	public void setEmailAddofKin(String emailAddofKin) {
		this.emailAddofKin = emailAddofKin;
	}
	/**
	 * @return the nameofKin
	 */
	public String getNameofKin() {
		return nameofKin;
	}
	/**
	 * @param nameofKin the nameofKin to set
	 */
	public void setNameofKin(String nameofKin) {
		this.nameofKin = nameofKin;
	}
	/**
	 * @return the relationshipofKin
	 */
	public String getRelationshipofKin() {
		return RelationshipofKin;
	}
	/**
	 * @param relationshipofKin the relationshipofKin to set
	 */
	public void setRelationshipofKin(String relationshipofKin) {
		RelationshipofKin = relationshipofKin;
	}
	/**
	 * @return the corpoarateIncNo
	 */
	public String getCorpoarateIncNo() {
		return corpoarateIncNo;
	}
	/**
	 * @param corpoarateIncNo the corpoarateIncNo to set
	 */
	public void setCorpoarateIncNo(String corpoarateIncNo) {
		this.corpoarateIncNo = corpoarateIncNo;
	}
	/**
	 * @return the corporateDateFounded
	 */
	public String getCorporateDateFounded() {
		return corporateDateFounded;
	}
	/**
	 * @param corporateDateFounded the corporateDateFounded to set
	 */
	public void setCorporateDateFounded(String corporateDateFounded) {
		this.corporateDateFounded = corporateDateFounded;
	}
	/**
	 * @return the clientMotherMaidenName
	 */
	public String getClientMotherMaidenName() {
		return clientMotherMaidenName;
	}
	/**
	 * @param clientMotherMaidenName the clientMotherMaidenName to set
	 */
	public void setClientMotherMaidenName(String clientMotherMaidenName) {
		this.clientMotherMaidenName = clientMotherMaidenName;
	}
	/**
	 * @return the contactAddresNextofKin
	 */
	public String getContactAddresNextofKin() {
		return contactAddresNextofKin;
	}
	/**
	 * @param contactAddresNextofKin the contactAddresNextofKin to set
	 */
	public void setContactAddresNextofKin(String contactAddresNextofKin) {
		this.contactAddresNextofKin = contactAddresNextofKin;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}
	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the lga
	 */
	public String getLga() {
		return lga;
	}
	/**
	 * @param lga the lga to set
	 */
	public void setLga(String lga) {
		this.lga = lga;
	}
	/**
	 * @return the stateOfOrigin
	 */
	public String getStateOfOrigin() {
		return stateOfOrigin;
	}
	/**
	 * @param stateOfOrigin the stateOfOrigin to set
	 */
	public void setStateOfOrigin(String stateOfOrigin) {
		this.stateOfOrigin = stateOfOrigin;
	}
	/**
	 * @return the maritalStat
	 */
	public String getMaritalStat() {
		return maritalStat;
	}
	/**
	 * @param maritalStat the maritalStat to set
	 */
	public void setMaritalStat(String maritalStat) {
		this.maritalStat = maritalStat;
	}
	/**
	 * @return the nameofEmpKin
	 */
	public String getNameofEmpKin() {
		return nameofEmpKin;
	}
	/**
	 * @param nameofEmpKin the nameofEmpKin to set
	 */
	public void setNameofEmpKin(String nameofEmpKin) {
		this.nameofEmpKin = nameofEmpKin;
	}
	/**
	 * @return the addressokEmpKin
	 */
	public String getAddressokEmpKin() {
		return addressokEmpKin;
	}
	/**
	 * @param addressokEmpKin the addressokEmpKin to set
	 */
	public void setAddressokEmpKin(String addressokEmpKin) {
		this.addressokEmpKin = addressokEmpKin;
	}
	/**
	 * @return the phonenoEmp
	 */
	public String getPhonenoEmp() {
		return phonenoEmp;
	}
	/**
	 * @param phonenoEmp the phonenoEmp to set
	 */
	public void setPhonenoEmp(String phonenoEmp) {
		this.phonenoEmp = phonenoEmp;
	}
	/**
	 * @return the relationshipofEmpKin
	 */
	public String getRelationshipofEmpKin() {
		return relationshipofEmpKin;
	}
	/**
	 * @param relationshipofEmpKin the relationshipofEmpKin to set
	 */
	public void setRelationshipofEmpKin(String relationshipofEmpKin) {
		this.relationshipofEmpKin = relationshipofEmpKin;
	}
	/**
	 * @return the pensionNoEmp
	 */
	public String getPensionNoEmp() {
		return pensionNoEmp;
	}
	/**
	 * @param pensionNoEmp the pensionNoEmp to set
	 */
	public void setPensionNoEmp(String pensionNoEmp) {
		this.pensionNoEmp = pensionNoEmp;
	}
	/**
	 * @return the pensionPFAEmp
	 */
	public String getPensionPFAEmp() {
		return pensionPFAEmp;
	}
	/**
	 * @param pensionPFAEmp the pensionPFAEmp to set
	 */
	public void setPensionPFAEmp(String pensionPFAEmp) {
		this.pensionPFAEmp = pensionPFAEmp;
	}
	/**
	 * @return the basicPayEmp
	 */
	public String getBasicPayEmp() {
		return basicPayEmp;
	}
	/**
	 * @param basicPayEmp the basicPayEmp to set
	 */
	public void setBasicPayEmp(String basicPayEmp) {
		this.basicPayEmp = basicPayEmp;
	}
	/**
	 * @return the grossPayEmp
	 */
	public String getGrossPayEmp() {
		return grossPayEmp;
	}
	/**
	 * @param grossPayEmp the grossPayEmp to set
	 */
	public void setGrossPayEmp(String grossPayEmp) {
		this.grossPayEmp = grossPayEmp;
	}
	/**
	 * @return the firstPromo
	 */
	public String getFirstPromo() {
		return firstPromo;
	}
	/**
	 * @param firstPromo the firstPromo to set
	 */
	public void setFirstPromo(String firstPromo) {
		this.firstPromo = firstPromo;
	}
	/**
	 * @return the secondPromo
	 */
	public String getSecondPromo() {
		return secondPromo;
	}
	/**
	 * @param secondPromo the secondPromo to set
	 */
	public void setSecondPromo(String secondPromo) {
		this.secondPromo = secondPromo;
	}
	/**
	 * @return the thirdPromo
	 */
	public String getThirdPromo() {
		return thirdPromo;
	}
	/**
	 * @param thirdPromo the thirdPromo to set
	 */
	public void setThirdPromo(String thirdPromo) {
		this.thirdPromo = thirdPromo;
	}
	/**
	 * @return the yearNYSC
	 */
	public String getYearNYSC() {
		return yearNYSC;
	}
	/**
	 * @param yearNYSC the yearNYSC to set
	 */
	public void setYearNYSC(String yearNYSC) {
		this.yearNYSC = yearNYSC;
	}
	/**
	 * @return the clientCategory
	 */
	public String getClientCategory() {
		return clientCategory;
	}
	/**
	 * @param clientCategory the clientCategory to set
	 */
	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}
	/**
	 * @return the riskinessLevel
	 */
	public String getRiskinessLevel() {
		return riskinessLevel;
	}
	/**
	 * @param riskinessLevel the riskinessLevel to set
	 */
	public void setRiskinessLevel(String riskinessLevel) {
		this.riskinessLevel = riskinessLevel;
	}
	/**
	 * @return the idNumber
	 */
	public String getIdNumber() {
		return IdNumber;
	}
	/**
	 * @param idNumber the idNumber to set
	 */
	public void setIdNumber(String idNumber) {
		IdNumber = idNumber;
	}
	/**
	 * @return the typeOfIdIND
	 */
	public String getTypeOfIdIND() {
		return typeOfIdIND;
	}
	/**
	 * @param typeOfIdIND the typeOfIdIND to set
	 */
	public void setTypeOfIdIND(String typeOfIdIND) {
		this.typeOfIdIND = typeOfIdIND;
	}
	/**
	 * @return the typeOfIdCorp
	 */
	public String getTypeOfIdCorp() {
		return typeOfIdCorp;
	}
	/**
	 * @param typeOfIdCorp the typeOfIdCorp to set
	 */
	public void setTypeOfIdCorp(String typeOfIdCorp) {
		this.typeOfIdCorp = typeOfIdCorp;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the employmentType
	 */
	public String getEmploymentType() {
		return employmentType;
	}
	/**
	 * @param employmentType the employmentType to set
	 */
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}
	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * @return the bankAccountNumber
	 */
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	/**
	 * @param bankAccountNumber the bankAccountNumber to set
	 */
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	/**
	 * @return the bvn
	 */
	public String getBvn() {
		return bvn;
	}
	/**
	 * @param bvn the bvn to set
	 */
	public void setBvn(String bvn) {
		this.bvn = bvn;
	}
	/**
	 * @return the meansOfId
	 */
	public String getMeansOfId() {
		return meansOfId;
	}
	/**
	 * @param meansOfId the meansOfId to set
	 */
	public void setMeansOfId(String meansOfId) {
		this.meansOfId = meansOfId;
	}
	/**
	 * @return the meansOfIdExpirationDate
	 */
	public String getMeansOfIdExpirationDate() {
		return meansOfIdExpirationDate;
	}
	/**
	 * @param meansOfIdExpirationDate the meansOfIdExpirationDate to set
	 */
	public void setMeansOfIdExpirationDate(String meansOfIdExpirationDate) {
		this.meansOfIdExpirationDate = meansOfIdExpirationDate;
	}
	/**
	 * @return the nextOfKinName
	 */
	public String getNextOfKinName() {
		return nextOfKinName;
	}
	/**
	 * @param nextOfKinName the nextOfKinName to set
	 */
	public void setNextOfKinName(String nextOfKinName) {
		this.nextOfKinName = nextOfKinName;
	}
	/**
	 * @return the nextOfKinRelationship
	 */
	public String getNextOfKinRelationship() {
		return nextOfKinRelationship;
	}
	/**
	 * @param nextOfKinRelationship the nextOfKinRelationship to set
	 */
	public void setNextOfKinRelationship(String nextOfKinRelationship) {
		this.nextOfKinRelationship = nextOfKinRelationship;
	}
	/**
	 * @return the nextOfKinContactAddress
	 */
	public String getNextOfKinContactAddress() {
		return nextOfKinContactAddress;
	}
	/**
	 * @param nextOfKinContactAddress the nextOfKinContactAddress to set
	 */
	public void setNextOfKinContactAddress(String nextOfKinContactAddress) {
		this.nextOfKinContactAddress = nextOfKinContactAddress;
	}
	/**
	 * @return the nextOfKinPhoneNo
	 */
	public String getNextOfKinPhoneNo() {
		return nextOfKinPhoneNo;
	}
	/**
	 * @param nextOfKinPhoneNo the nextOfKinPhoneNo to set
	 */
	public void setNextOfKinPhoneNo(String nextOfKinPhoneNo) {
		this.nextOfKinPhoneNo = nextOfKinPhoneNo;
	}
	/**
	 * @return the nextOfKinEmailAddress
	 */
	public String getNextOfKinEmailAddress() {
		return nextOfKinEmailAddress;
	}
	/**
	 * @param nextOfKinEmailAddress the nextOfKinEmailAddress to set
	 */
	public void setNextOfKinEmailAddress(String nextOfKinEmailAddress) {
		this.nextOfKinEmailAddress = nextOfKinEmailAddress;
	}
	/**
	 * @return the pep
	 */
	public String getPep() {
		return pep;
	}
	/**
	 * @param pep the pep to set
	 */
	public void setPep(String pep) {
		this.pep = pep;
	}
	/**
	 * @return the customerBVN
	 */
	public String getCustomerBVN() {
		return customerBVN;
	}
	/**
	 * @param customerBVN the customerBVN to set
	 */
	public void setCustomerBVN(String customerBVN) {
		this.customerBVN = customerBVN;
	}
	/**
	 * @return the accountOfficerEmail
	 */
	public String getAccountOfficerEmail() {
		return accountOfficerEmail;
	}
	/**
	 * @param accountOfficerEmail the accountOfficerEmail to set
	 */
	public void setAccountOfficerEmail(String accountOfficerEmail) {
		this.accountOfficerEmail = accountOfficerEmail;
	}
	/**
	 * @return the accountOfficerPhone
	 */
	public String getAccountOfficerPhone() {
		return accountOfficerPhone;
	}
	/**
	 * @param accountOfficerPhone the accountOfficerPhone to set
	 */
	public void setAccountOfficerPhone(String accountOfficerPhone) {
		this.accountOfficerPhone = accountOfficerPhone;
	}
	/**
	 * @return the accountOfficerName
	 */
	public String getAccountOfficerName() {
		return accountOfficerName;
	}
	/**
	 * @param accountOfficerName the accountOfficerName to set
	 */
	public void setAccountOfficerName(String accountOfficerName) {
		this.accountOfficerName = accountOfficerName;
	}
	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}
	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	/**
	 * @return the displayCustomerAID
	 */
	public String getDisplayCustomerAID() {
		return displayCustomerAID;
	}
	/**
	 * @param displayCustomerAID the displayCustomerAID to set
	 */
	public void setDisplayCustomerAID(String displayCustomerAID) {
		this.displayCustomerAID = displayCustomerAID;
	}
}
