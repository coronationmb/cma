/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * @author waliu.faleye
 *
 */
@Entity
@Table(name = "Users_Applications")
public class Application {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;

	private String name;

	@Column(name = "database_url")
	private String databaseUrl;

	@Column(name = "database_username")
	private String username;

	@Column(name = "database_password")
	private String password;

	@Column(name = "database_class_name")
	private String databaseClassName;

	@Lob
	@Column(length = 10000,name= "customer_detail_query")
	private String customerDetailQuery;

	@Lob
	@Column(length = 10000,name= "customer_accounts_query")
	private String customerAccountsQuery;

	@Lob
	@Column(length = 5000,name= "test_query")
	private String testQuery;

	@Lob
	@Column(length = 10000,name="account_Monthly_Tran_Query")
	private String customerAccountMonthlyTransactionQuery;

	@Lob
	@Column(length = 10000,name="td_Account_Query")
	private String customerTDAccountQuery;

	@Lob
	@Column(length = 10000,name="loan_Account_Query")
	private String customerLoanAccountQuery;

	@Lob
	@Column(length = 10000,name="overdraft_Account_Query")
	private String customerOverdraftAccountQuery;
	
	@OneToMany(mappedBy="application")
    private Set<TransactionType> transactionTypes;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the databaseUrl
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * @param databaseUrl
	 *            the databaseUrl to set
	 */
	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the databaseClassName
	 */
	public String getDatabaseClassName() {
		return databaseClassName;
	}

	/**
	 * @param databaseClassName
	 *            the databaseClassName to set
	 */
	public void setDatabaseClassName(String databaseClassName) {
		this.databaseClassName = databaseClassName;
	}

	/**
	 * @return the customerDetailQuery
	 */
	public String getCustomerDetailQuery() {
		return customerDetailQuery;
	}

	/**
	 * @param customerDetailQuery
	 *            the customerDetailQuery to set
	 */
	public void setCustomerDetailQuery(String customerDetailQuery) {
		this.customerDetailQuery = customerDetailQuery;
	}

	/**
	 * @return the testQuery
	 */
	public String getTestQuery() {
		return testQuery;
	}

	/**
	 * @param testQuery
	 *            the testQuery to set
	 */
	public void setTestQuery(String testQuery) {
		this.testQuery = testQuery;
	}

	/**
	 * @return the customerAccountsQuery
	 */
	public String getCustomerAccountsQuery() {
		return customerAccountsQuery;
	}

	/**
	 * @param customerAccountsQuery the customerAccountsQuery to set
	 */
	public void setCustomerAccountsQuery(String customerAccountsQuery) {
		this.customerAccountsQuery = customerAccountsQuery;
	}

	/**
	 * @return the customerAccountMonthlyTransactionQuery
	 */
	public String getCustomerAccountMonthlyTransactionQuery() {
		return customerAccountMonthlyTransactionQuery;
	}

	/**
	 * @param customerAccountMonthlyTransactionQuery the customerAccountMonthlyTransactionQuery to set
	 */
	public void setCustomerAccountMonthlyTransactionQuery(String customerAccountMonthlyTransactionQuery) {
		this.customerAccountMonthlyTransactionQuery = customerAccountMonthlyTransactionQuery;
	}

	/**
	 * @return the customerTDAccountQuery
	 */
	public String getCustomerTDAccountQuery() {
		return customerTDAccountQuery;
	}

	/**
	 * @param customerTDAccountQuery the customerTDAccountQuery to set
	 */
	public void setCustomerTDAccountQuery(String customerTDAccountQuery) {
		this.customerTDAccountQuery = customerTDAccountQuery;
	}

	/**
	 * @return the customerLoanAccountQuery
	 */
	public String getCustomerLoanAccountQuery() {
		return customerLoanAccountQuery;
	}

	/**
	 * @param customerLoanAccountQuery the customerLoanAccountQuery to set
	 */
	public void setCustomerLoanAccountQuery(String customerLoanAccountQuery) {
		this.customerLoanAccountQuery = customerLoanAccountQuery;
	}

	/**
	 * @return the customerOverdraftAccountQuery
	 */
	public String getCustomerOverdraftAccountQuery() {
		return customerOverdraftAccountQuery;
	}

	/**
	 * @param customerOverdraftAccountQuery the customerOverdraftAccountQuery to set
	 */
	public void setCustomerOverdraftAccountQuery(String customerOverdraftAccountQuery) {
		this.customerOverdraftAccountQuery = customerOverdraftAccountQuery;
	}

	/**
	 * @return the transactionTypes
	 */
	public Set<TransactionType> getTransactionTypes() {
		return transactionTypes;
	}

	/**
	 * @param transactionTypes the transactionTypes to set
	 */
	public void setTransactionTypes(Set<TransactionType> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

}
