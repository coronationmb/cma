/**
 * 
 */
package com.cmb.consolidatedapps.model;

import org.springframework.mail.javamail.JavaMailSender;

/**
 * @author waliu.faleye
 *
 */
public class MailDetail {

	private String sender;
	private String subject;
	private String reciever;
	private String ccEmail;
	private String username;
	private String filePath;
	private JavaMailSender mailSender;
	private String[] attachments;
	private String emailBody;
	private boolean htmlEnabled;

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param sender
	 *            the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the reciever
	 */
	public String getReciever() {
		return reciever;
	}

	/**
	 * @param reciever
	 *            the reciever to set
	 */
	public void setReciever(String reciever) {
		this.reciever = reciever;
	}

	/**
	 * @return the ccEmail
	 */
	public String getCcEmail() {
		return ccEmail;
	}

	/**
	 * @param ccEmail
	 *            the ccEmail to set
	 */
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the mailSender
	 */
	public JavaMailSender getMailSender() {
		return mailSender;
	}

	/**
	 * @param mailSender
	 *            the mailSender to set
	 */
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * @return the attachments
	 */
	public String[] getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the emailBody
	 */
	public String getEmailBody() {
		return emailBody;
	}

	/**
	 * @param emailBody the emailBody to set
	 */
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	/**
	 * @return the htmlEnabled
	 */
	public boolean isHtmlEnabled() {
		return htmlEnabled;
	}

	/**
	 * @param htmlEnabled the htmlEnabled to set
	 */
	public void setHtmlEnabled(boolean htmlEnabled) {
		this.htmlEnabled = htmlEnabled;
	}

}
