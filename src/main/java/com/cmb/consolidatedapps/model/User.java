/**
 * 
 */
package com.cmb.consolidatedapps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author waliu.faleye
 *
 */
@Entity
@Audited
@Table(name="users")
public class User {

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_generator")
	@SequenceGenerator(name="user_generator",allocationSize=1,sequenceName="user_gen")
	@Id
	private Long id;

	@Column(unique = true,name = "user_id")
	private String userId;	
	
	@Transient
	private String oldUserId;

	private String phone;

	private String email;

	@Column(nullable=false)
	private String password;

	@Column(name = "corporate_user_bvn")
	private String corporateUserBvn;

	@OneToOne
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(nullable=false)
	private Status status;

	@OneToOne
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@JoinColumn(name = "user_role_id",nullable=false)
	private UserRole userRole;

	//@OneToOne
	//@JoinColumn(name="customer_id")
	//private Customer customer;
	
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

	@Transient
	private String statusCode;

	@Transient
	private String rcnBvn;

	@Transient
	private String customerName;
	
	@Column(nullable = false,name="created_by")
	private String createdBy;
	
	@Column(nullable = false,name="created_date")
	private Date createdDate;

	@Transient
	private Long customerId;

	@Transient
	private Long userRoleId;

	@Transient
	private String oldPassword;
	
	private String username;

    @Column(name="delete_flg", length=1)
	private String deleteFlg = "N";

    @Transient
	private String token;

    @Transient
	private String tokenRespMessage;

    @Transient
	private String errorMessage;

	@Transient
	private Long statusId;

	@Transient
	private String parentPage;

	@Transient
	private String netWorth;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "relationship_officer_id")
    private RelationshipOfficer relationshipOfficer;
	
    @OneToOne
    @JoinColumn(name = "image_id")
    private Image image;

	//@Transient
	//private String picture;

	@Transient
	private String mode;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(User user) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the userRole
	 */
	public UserRole getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole
	 *            the userRole to set
	 */
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the rcnBvn
	 */
	public String getRcnBvn() {
		return rcnBvn;
	}

	/**
	 * @param rcnBvn
	 *            the rcnBvn to set
	 */
	public void setRcnBvn(String rcnBvn) {
		this.rcnBvn = rcnBvn;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the oldUserId
	 */
	public String getOldUserId() {
		return oldUserId;
	}

	/**
	 * @param oldUserId the oldUserId to set
	 */
	public void setOldUserId(String oldUserId) {
		this.oldUserId = oldUserId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the userRoleId
	 */
	public Long getUserRoleId() {
		return userRoleId;
	}

	/**
	 * @param userRoleId the userRoleId to set
	 */
	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the corporateUserBvn
	 */
	public String getCorporateUserBvn() {
		return corporateUserBvn;
	}

	/**
	 * @param corporateUserBvn the corporateUserBvn to set
	 */
	public void setCorporateUserBvn(String corporateUserBvn) {
		this.corporateUserBvn = corporateUserBvn;
	}

	/**
	 * @return the deleteFlg
	 */
	public String getDeleteFlg() {
		return deleteFlg;
	}

	/**
	 * @param deleteFlg the deleteFlg to set
	 */
	public void setDeleteFlg(String deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the tokenRespMessage
	 */
	public String getTokenRespMessage() {
		return tokenRespMessage;
	}

	/**
	 * @param tokenRespMessage the tokenRespMessage to set
	 */
	public void setTokenRespMessage(String tokenRespMessage) {
		this.tokenRespMessage = tokenRespMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the parentPage
	 */
	public String getParentPage() {
		return parentPage;
	}

	/**
	 * @param parentPage the parentPage to set
	 */
	public void setParentPage(String parentPage) {
		this.parentPage = parentPage;
	}

	/**
	 * @return the netWorth
	 */
	public String getNetWorth() {
		return netWorth;
	}

	/**
	 * @param netWorth the netWorth to set
	 */
	public void setNetWorth(String netWorth) {
		this.netWorth = netWorth;
	}

	/**
	 * @return the relationshipOfficer
	 */
	public RelationshipOfficer getRelationshipOfficer() {
		return relationshipOfficer;
	}

	/**
	 * @param relationshipOfficer the relationshipOfficer to set
	 */
	public void setRelationshipOfficer(RelationshipOfficer relationshipOfficer) {
		this.relationshipOfficer = relationshipOfficer;
	}

	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(Image image) {
		this.image = image;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

}
