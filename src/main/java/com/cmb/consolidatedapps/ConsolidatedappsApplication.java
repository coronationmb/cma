package com.cmb.consolidatedapps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cmb.consolidatedapps.interfaces.ApplicationRepository;


//@SpringBootApplication(scanBasePackages = { "com.cmb.consolidatedapps.controller", "com.cmb.consolidatedapps.interfaces",	"com.cmb.consolidatedapps.model", "com.cmb.consolidatedapps.service" })
@SpringBootApplication(scanBasePackages = { "com.cmb.consolidatedapps", "com.cmb.consolidatedapps.controller", "com.cmb.consolidatedapps.interfaces",	"com.cmb.consolidatedapps.model", "com.cmb.consolidatedapps.service" })
public class ConsolidatedappsApplication {

	static ApplicationRepository applicationRepo;

	public static void main(String[] args) {
		SpringApplication.run(ConsolidatedappsApplication.class, args);

	}
	
	
}
