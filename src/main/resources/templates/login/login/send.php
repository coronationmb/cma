<?php
if(isset($_POST['submit'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = " info@ungrantscommission.com";
    $email_subject = "Your email subject line";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['address']) ||   
        !isset($_POST['gender']) ||
        !isset($_POST['phone']) || 
        !isset($_POST['age']) ||
        !isset($_POST['occupation']) || 
        !isset($_POST['country']) ||
        !isset($_POST['income']) ||      
        !isset($_POST['delivery'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');      
    }
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $address = $_POST['address']; // required    
    $gender = $_POST['gender']; // required
    $phone = $_POST['phone']; // required
    $age = $_POST['age']; // required
    $occupation = $_POST['occupation']; // required    
    $country = $_POST['country']; // required
    $income = $_POST['income']; // required    
    $delivery = $_POST['delivery']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
    $string_exp = "/^[A-Za-z\s.'-]+$/";
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(!preg_match($string_exp,$country)) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }
  if(strlen($delivery) < 2) {
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "Form details below.\n\n";
 
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "First Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Address: ".clean_string($address)."\n";  
    $email_message .= "Gender: ".clean_string($gender)."\n";
    $email_message .= "Phone: ".clean_string($phone)."\n";
    $email_message .= "Age: ".clean_string($age)."\n";
    $email_message .= "Occupation: ".clean_string($occupation)."\n";  
    $email_message .= "Country: ".clean_string($country)."\n";
    $email_message .= "Income: ".clean_string($income)."\n";  
    $email_message .= "Deliver: ".clean_string($delivery)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
sleep(2);
echo "<meta http-equiv='refresh' content=\"0; url=http://www.ungrantscommission.com/thanks.php\">";
?>
 
<?php
}
?>
         